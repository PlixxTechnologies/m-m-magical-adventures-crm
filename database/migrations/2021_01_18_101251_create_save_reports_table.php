<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSaveReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('save_reports');

              Schema::create('save_reports', function (Blueprint $table) {
            $table->increments('savereportId');
            $table->integer('agentId')->nullable();
            $table->string('travelmonth')->nullable();;
            $table->string('reportcreation')->nullable();;
            $table->string('exportdate')->nullable();
            $table->integer('paymentstatus')->nullable();
            $table->float('ytd_commission')->nullable();
            $table->string('checkin_date')->nullable();
            $table->string('checkout_date')->nullable();
            $table->string('created_at')->nullable();
            $table->string('updated_at')->nullable();
            $table->timestamp('expire_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('save_reports');
    }
}
