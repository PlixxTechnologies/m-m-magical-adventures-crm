<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSaveReportDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('save_report_datas');

        Schema::create('save_report_datas', function (Blueprint $table) {
            $table->increments('Id');
            $table->integer('tripid')->nullable();
            $table->integer('paymentstatus')->nullable();
            $table->string('created_at')->nullable();
            $table->string('updated_at')->nullable();
            $table->timestamp('expire_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('save_report_datas');
    }
}
