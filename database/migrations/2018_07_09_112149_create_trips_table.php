<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTripsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trips', function (Blueprint $table) {
            $table->increments('id');
            $table->string('booking_date')->nullable();
            $table->string('trip_status')->nullable();
            $table->string('total_sale')->nullable();
            $table->string('ticket_type')->nullable();
            $table->string('destination')->nullable();
            $table->string('reservation_number')->nullable();
            $table->string('disneyworld_hotel_name')->nullable();
            $table->string('disneyresort_hotel_name')->nullable();
            $table->string('disney_dining_plan')->nullable();
            $table->string('memory_maker')->nullable();
            $table->string('magical_express')->nullable();
            $table->string('checkin_date')->nullable();
            $table->string('checkout_date')->nullable();
            $table->string('ship_name')->nullable();
            $table->string('castaway_member')->nullable();
            $table->string('bus_transportation')->nullable();
            $table->string('insurance')->nullable();
            $table->string('travel_with')->nullable();
            $table->string('special_request')->nullable();
            $table->text('notes')->nullable();
            $table->float('commission')->nullable();
            $table->float('expected_commission')->nullable();           
            $table->integer('customer_id')->nullable();
            $table->integer('user_id')->nullable();
            $table->string('promo_applied')->nullable();
            $table->string('advanced_dining_reservations')->nullable();
            $table->string('fast_pass_date')->nullable();
            $table->string('final_payment_due')->nullable();
            $table->string('magic_band_color')->nullable();
            $table->string('reminder')->nullable();
            $table->string('magical_express_completed')->nullable();
            $table->string('call_room_requests')->nullable();
            $table->string('itinerary_tip_sheets')->nullable();
            $table->string('good_neighbor_hotel')->nullable();
            $table->string('universal_orlando_resort_hotel')->nullable();
            $table->string('partners_hotel')->nullable();
            $table->string('promo_code')->nullable();
            $table->integer('is_active')->nullable();
            $table->integer('status')->default(0);
            $table->string('reason')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trips');
    }
}
