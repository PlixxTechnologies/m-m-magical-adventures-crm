<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('email')->nullable();
            $table->string('country_code')->nullable();
            $table->string('phone_no')->nullable();
            $table->string('address1')->nullable();
            $table->string('address2')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('zip')->nullable();
            $table->string('birth_date')->nullable();
            $table->string('passport')->nullable();
            $table->string('expire_date')->nullable();
            $table->string('referral')->nullable();
            $table->string('notes')->nullable();
            $table->string('disney_experience_username')->nullable();
            $table->string('disney_experience_password')->nullable();
            $table->integer('user_id')->nullable();
            $table->timestamps();
        });

         DB::statement("ALTER TABLE customers AUTO_INCREMENT = 100000000;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
