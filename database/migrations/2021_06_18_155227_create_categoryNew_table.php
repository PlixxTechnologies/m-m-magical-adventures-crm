<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoryNewTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
Schema::create('Category_destination', function (Blueprint $table) {

          $table->increments('id');
            $table->string('category_name')->nullable();
            $table->integer('destination_id')->nullable();
            $table->string('destination_name')->nullable();
              $table->timestamps();
      });
    

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
