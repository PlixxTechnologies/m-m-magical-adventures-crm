<?php

use Illuminate\Database\Seeder;

class DestinationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('destinations')->insert([
            'name' => 'Walt Disney World',
        ]);

        DB::table('destinations')->insert([
            'name' => 'Disneyland Resort',
        ]);

        DB::table('destinations')->insert([
            'name' => 'Disney Cruise Line',
        ]);

        DB::table('destinations')->insert([
            'name' => 'Universal Studios Florida',
        ]);

        DB::table('destinations')->insert([
            'name' => 'Disney Aulani',
        ]);

        DB::table('destinations')->insert([
            'name' => 'Adventures by Disney',
        ]);
        

        DB::table('destinations')->insert([
            'name' => 'Universal Studios Hollywood',
        ]);

        DB::table('destinations')->insert([
            'name' => 'Royal Caribbean Cruise Line',
        ]);

        DB::table('destinations')->insert([
            'name' => 'Celebrity X Cruises',
        ]);

        DB::table('destinations')->insert([
            'name' => 'Azamara Cruises',
        ]);

        DB::table('destinations')->insert([
            'name' => 'Norwegian Cruise Line',
        ]);

        DB::table('destinations')->insert([
            'name' => 'Carnival Cruise Line',
        ]);
    }
}
