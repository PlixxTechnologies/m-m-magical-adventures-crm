<?php

use Illuminate\Database\Seeder;

class DiningPlansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('dining_plans')->insert([
            'name' => 'Quick Service Dining',
            'adultprice' => '1.0',
            'childprice' => '1.0',
        ]);

        DB::table('dining_plans')->insert([
            'name' => 'Disney Dining Plan',
            'adultprice' => '1.0',
            'childprice' => '1.0',
        ]);

        DB::table('dining_plans')->insert([
            'name' => 'Deluxe Dining Plan',
            'adultprice' => '1.0',
            'childprice' => '1.0',
        ]);

        DB::table('dining_plans')->insert([
            'name' => 'Quick Service Dining',
            'adultprice' => '1.0',
            'childprice' => '1.0',
        ]);

        DB::table('dining_plans')->insert([
            'name' => 'Disney Dining Plan',
            'adultprice' => '1.0',
            'childprice' => '1.0',
        ]);

        DB::table('dining_plans')->insert([
            'name' => 'Deluxe Dining Plan',
            'adultprice' => '1.0',
            'childprice' => '1.0',
        ]);
    }
}
