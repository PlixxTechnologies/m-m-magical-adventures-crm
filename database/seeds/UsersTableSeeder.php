<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Alvin Gordin',
            'email' => 'alvingordin@gmail.com',
            'password' => bcrypt('secret'),
            'role' => 1,
            'commission' => '60',
            'sales_goal' => '250000',
        ]);
    }
}
