<?php $__env->startSection('content'); ?>
<!-- Datatables JS FILES -->

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.print.min.js"></script>

<!-- /Datatables JS FILES -->
   <!-- Page header -->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="<?php echo e(url('/home')); ?>">
                    <em class="fa fa-home"></em>
                </a></li>
                <li class="active">Edit Destination & Supplier</li>
            </ol>
        </div><!--/.row-->
    <div class="container-fluid"> 
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Edit Destination & Supplier</h1>
            </div>
        </div>  <!-- /page header -->
   </div>

<!--Add Hotel modal -->
<div id="addHotelModal" class="modal" style="display: none;">
    <div class="modal-dialog">
        <form  method="post" action="<?php echo e(url('hotel/save')); ?>"> 
            <?php echo csrf_field(); ?>

            <?php echo e(method_field('POST')); ?>

            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h6 class="modal-title">Add Hotel</h6>
                </div>

                <div class="modal-body">
                    <div class="form-group">
                        <label><span class="red">*</span>Hotel Name:</label>
                        <input type="text" name="hotel_name" class="form-control" required>
                    </div>
                    <?php if($destination->name == "Walt Disney World" || $destination->name == "Carnival Cruise Line" || $destination->name == "Royal Caribbean Cruise Line"): ?>
                    <div class="form-group">
                         <label><span class="red">*</span>Category:</label>
                            <select name="category_id" class="form-control" required>
                                <option value="">Choose One....</option>
                                <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($category->id); ?>"><?php echo e($category->category_name); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>        
                            </select>
                    </div>      
                    <?php endif; ?>
                </div>
                <input type="hidden" id="hotelDestinationId" name="destination_id" />

                <div class="modal-footer">
                    <button type="button" class="btn btn-link legitRipple" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary legitRipple">Add</button>
                </div>
            </div>
        </form>
    </div>
</div>

   <!--Delete Hotel modal -->
<div id="deleteHotelModal" class="modal" style="display: none;">
    <div class="modal-dialog">
        <form  method="post" action="<?php echo e(url('hotel/delete')); ?>"> 
            <?php echo csrf_field(); ?>

            <?php echo e(method_field('DELETE')); ?>

            <div class="modal-content">
                <div class="modal-header bg-danger">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h6 class="modal-title">Delete Hotel</h6>
                </div>

                <div class="modal-body">
                    <p>Are you sure you want to delete this Hotel?</p>
                </div>

                <input type="hidden" id="deleteHotelId" name="id" />

                <div class="modal-footer">
                    <button type="button" class="btn btn-link legitRipple" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger legitRipple">Confirm</button>
                </div>
            </div>
        </form>
    </div>
</div>

<!--Update Hotel modal -->
<div id="updateHotelModal" class="modal" style="display: none;">
    <div class="modal-dialog">
        <form  method="post" action="<?php echo e(url('hotel/edit')); ?>"> 
            <?php echo csrf_field(); ?>

            <?php echo e(method_field('POST')); ?>

            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h6 class="modal-title">Update Hotel</h6>
                </div>

                <div class="modal-body">
                    <div class="form-group">
                        <label><span class="red">*</span>Hotel Name:</label>
                        <input type="text" id="hotel_name" name="hotel_name" class="form-control" required>
                    </div>
                    <?php if($destination->name == "Walt Disney World" || $destination->name == "Carnival Cruise Line" || $destination->name == "Royal Caribbean Cruise Line"): ?>
                    <div class="form-group">
                         <label><span class="red">*</span>Category:</label>
                            <select name="category_id" class="form-control" required>
                                <option value="">Choose One....</option>
                                <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($category->id); ?>"><?php echo e($category->category_name); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>        
                            </select>
                    </div>      
                    <?php endif; ?>
                </div>
                <input type="hidden" id="updateHotelId" name="id" />

                <div class="modal-footer">
                    <button type="button" class="btn btn-link legitRipple" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary legitRipple">Update</button>
                </div>
            </div>
        </form>
    </div>
</div>


<!--Add Category modal -->
<div id="addCategoryModal" class="modal" style="display: none;">
    <div class="modal-dialog">
        <form  method="post" action="<?php echo e(url('category/save')); ?>"> 
            <?php echo csrf_field(); ?>

            <?php echo e(method_field('POST')); ?>

            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h6 class="modal-title">Add Category</h6>
                </div>

                <div class="modal-body">
                    <div class="form-group">
                        <label><span class="red">*</span>Category Name:</label>
                        <input type="text" name="category_name" class="form-control" required>
                    </div>
                </div>
                <input type="hidden" id="categoryDestinationId" name="destination_id" />

                <div class="modal-footer">
                    <button type="button" class="btn btn-link legitRipple" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary legitRipple">Add</button>
                </div>
            </div>
        </form>
    </div>
</div>

   <!--Delete Category modal -->
<div id="deleteCategoryModal" class="modal" style="display: none;">
    <div class="modal-dialog">
        <form  method="post" action="<?php echo e(url('category/delete')); ?>"> 
            <?php echo csrf_field(); ?>

            <?php echo e(method_field('DELETE')); ?>

            <div class="modal-content">
                <div class="modal-header bg-danger">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h6 class="modal-title">Delete Category</h6>
                </div>

                <div class="modal-body">
                    <p>Are you sure you want to delete this Category?</p>
                </div>

                <input type="hidden" id="deleteCategoryId" name="id" />

                <div class="modal-footer">
                    <button type="button" class="btn btn-link legitRipple" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger legitRipple">Confirm</button>
                </div>
            </div>
        </form>
    </div>
</div>

<!--Update Category modal -->
<div id="updateCategoryModal" class="modal" style="display: none;">
    <div class="modal-dialog">
        <form  method="post" action="<?php echo e(url('category/edit')); ?>"> 
            <?php echo csrf_field(); ?>

            <?php echo e(method_field('POST')); ?>

            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h6 class="modal-title">Update Category</h6>
                </div>

                <div class="modal-body">
                    <div class="form-group">
                        <label><span class="red">*</span>Category Name:</label>
                        <input type="text" id="category_name" name="category_name" class="form-control" required>
                    </div>
                </div>
                <input type="hidden" id="updateCategoryId" name="id" />

                <div class="modal-footer">
                    <button type="button" class="btn btn-link legitRipple" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary legitRipple">Update</button>
                </div>
            </div>
        </form>
    </div>
</div>



    <div class="container-fluid">
        <br>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">

                        <form action="<?php echo e(url('destination/update').'/'.$destination->id); ?>" method="post">
                            <?php echo csrf_field(); ?>


                            <?php if($destination->name == "Walt Disney World" || $destination->name == "Disney Cruise Line" || $destination->name == "Universal Studios Florida" || $destination->name == "Carnival Cruise Line" || $destination->name == "Royal Caribbean Cruise Line" || $destination->name == "Disneyland Resort"): ?>

                            <input type="hidden" name="name" value="<?php echo e($destination->id); ?>">

                            <div class="form-group">
                                <label>*Name:</label>
                                <input readonly="" type="text" name="name" value="<?php echo e($destination->name); ?>" class="form-control" required>
                            </div>
                            
                        
                           <?php else: ?>
                            <input type="hidden" name="name" value="<?php echo e($destination->id); ?>">

                          <!--   <div class="form-group">
                                <label>*Name:</label>
                                <input type="text" name="name" value="<?php echo e($destination->name); ?>" class="form-control" required>
                            </div> -->




                             <div class="form-group">
                                <label>*Category Name:</label>
                                <input type="text" name="name" class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label><span class="red">*</span>Select Supplier Category:</label>
                               <!--  <select name="destination_category" class="form-control" id="disney_select" onchange="switchDivs()" required>
                              <option value="Choose One...a">Choose One....</option>
                                <option value="xyz">XYZ</option>
                                <option value="Universal Studios Florida">Universal Studios Florida</option>   
                                <option value="Disney Cruise Line">Disney Cruise Line</option> 
                                <option value="Royal Caribbean Cruise Line">Royal Caribbean Cruise Line</option>  
                                <option value="Walt Disney World">Walt Disney World</option> 
                                </select> -->




                  <select name="destination_category" class="form-control" required>
                    <option value="All"  selected>Choose One....</option>
                    <?php $__currentLoopData = $categorynames; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $categoryname): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="<?php echo e($categoryname->destination_category); ?>"><?php echo e($categoryname->destination_category); ?></option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </select>  
                            </div> 
                            <button type="submit" class="btn btn-primary pull-right">Update Destination</button>

                           <?php endif; ?>
                        </form>

                    </div>
                </div>
            </div>
        </div><!--/.row-->
    </div>  <!--/.main-->

   <!--  <?php if($destination->name == "Walt Disney World" || $destination->name == "Carnival Cruise Line" || $destination->name == "Royal Caribbean Cruise Line"): ?>

   
                <div class="page-header page-header-default">
                    <div class="page-header-content">
                        <div class="page-title">
                            <h4><span class="text-semibold">All Hotels/Ships Categories</span></h4>
                        </div>
                    </div>
                </div>
 


                    <div class="panel panel-flat">
                        <div class="col-md-2 form-group">
                                <br><button type="button" onclick="addCategory(<?php echo e($destination->id); ?>)" class="btn btn-sm btn-primary">Add Category</button>
                            </div>
                        <table class="table datatable-button-init-basic">
                            <thead>
                                <tr>
                                <th>Name</th>
                                <?php if(Auth::user()->role == 1): ?>
                                    <th>Action</th>
                                    <?php endif; ?>
                            </tr>
                            </thead>
                            <tbody>
                                  <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <td>
                                        <?php echo e($category->category_name); ?>

                                    </td>
                            <?php if(Auth::user()->role == 1): ?>
                                    <td class="center">
                                        <button type="button" onclick="updateCategory(<?php echo e($category->id); ?>)" class="btn btn-sm btn-primary">Edit</button>
                                       <button type="button" onclick="deleteCategory(<?php echo e($category->id); ?>)" class="btn btn-danger btn-sm">Delete</button>
                                    </td>
                                <?php endif; ?>
                                  </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                               
                            </tbody>
                        </table>
                    </div>

                    <?php endif; ?> -->

<?php if($destination->name == "Walt Disney World" || $destination->name == "Disney Cruise Line" || $destination->name == "Universal Studios Florida" || $destination->name == "Carnival Cruise Line" || $destination->name == "Royal Caribbean Cruise Line" || $destination->name == "Disneyland Resort"): ?>

    <!-- Page header -->
    <div class="container-fluid">
                <div class="page-header page-header-default">
                    <div class="page-header-content">
                        <div class="page-title">
                            <?php if($destination->name == "Walt Disney World"): ?>
                            <h4><span class="text-semibold">All Walt Disney World Hotels</span></h4>
                            <?php elseif($destination->name == "Disneyland Resort"): ?>
                             <h4><span class="text-semibold">All Disneyland Resort Hotels</span></h4>
                              <?php elseif($destination->name == "Disney Cruise Line"): ?>
                             <h4><span class="text-semibold">All Disney Cruise Line Ships</span></h4>
                             <?php elseif($destination->name == "Universal Studios Florida"): ?>
                             <h4><span class="text-semibold">All Universal Studios Florida Hotels</span></h4>
                              <?php elseif($destination->name == "Carnival Cruise Line"): ?>
                             <h4><span class="text-semibold">All Carnival Cruise Line Ships</span></h4>
                             <?php elseif($destination->name == "Royal Caribbean Cruise Line"): ?>
                             <h4><span class="text-semibold">Royal Caribbean Cruise Line Ships</span></h4>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
   <!-- /page header -->

<!-- Basic initialization -->
<div class="container-fluid">
                    <div class="panel panel-flat">
                        <div class="col-md-2 form-group">
                                <br><button type="button" onclick="addHotel(<?php echo e($destination->id); ?>)" class="btn btn-sm btn-primary">Add</button>
                            </div>
                        <table class="table datatable-button-init-basic">
                            <thead>
                                <tr>
                                <th>Name</th>
                                <?php if(Auth::user()->role == 1): ?>
                                    <th>Action</th>
                                    <?php endif; ?>
                            </tr>
                            </thead>
                            <tbody>
                                  <?php $__currentLoopData = $hotels; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $hotel): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <td>
                                        <?php echo e($hotel->hotel_name); ?>

                                    </td>
                            <?php if(Auth::user()->role == 1): ?>
                                    <td class="center">
                                        <!-- <button type="button" onclick="updateHotel(<?php echo e($hotel->id); ?>)" class="btn btn-sm btn-primary">Edit</button> -->
                                        <a class="btn btn-info" href="<?php echo e(url('hotel/edit').'/'.$hotel->id); ?>">Edit</a>
                                       <button type="button" onclick="deleteHotel(<?php echo e($hotel->id); ?>)" class="btn btn-danger btn-sm">Delete</button>
                                    </td>
                                <?php endif; ?>
                                  </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                               
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
                    <!-- /basic initialization -->   
<?php endif; ?>

<script type="text/javascript">

    function addHotel(destination_id) {

        $('#hotelDestinationId').val(destination_id);
        $('#addHotelModal').modal('show');

    }

     function deleteHotel(id) {

        $('#deleteHotelId').val(id);
        $('#deleteHotelModal').modal('show');

    }

    function updateHotel(id) {

        $('#updateHotelId').val(id);

        $.get('/ajax-hotelUpdate?hotel_id='+id, function(data){

            console.log(data);

            $('#hotel_name').val(data.hotel_name);
           
        });

        $('#updateHotelModal').modal('show');

    }

    function addCategory(destination_id) {

        $('#categoryDestinationId').val(destination_id);
        $('#addCategoryModal').modal('show');

    }

     function deleteCategory(id) {

        $('#deleteCategoryId').val(id);
        $('#deleteCategoryModal').modal('show');

    }

    function updateCategory(id) {

        $('#updateCategoryId').val(id);

        $.get('/ajax-categoryUpdate?category_id='+id, function(data){

            console.log(data);

            $('#category_name').val(data.category_name);
           
        });

        $('#updateCategoryModal').modal('show');

    }
</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.dashboardx', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>