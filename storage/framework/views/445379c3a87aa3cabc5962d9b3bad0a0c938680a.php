<?php $__env->startSection('content'); ?>

<!-- Quick stats boxes -->

 <!-- Page header -->
                <div class="page-header page-header-default">
                    <div class="page-header-content">
                        <div class="page-title">
                            <h4><span class="text-semibold">Dashboard</span></h4>
                        </div>
                    </div>
                </div>
   <!-- /page header -->

<div class="container">
    
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-body border-top-primary">
                <div class="text-center">
                    <h1 class="no-margin"><b>Agent Sales Goal</b></h1>
                    <h3 class="content-group-sm text-muted"><code><b>$<?php echo e(Auth::user()->sales_goal); ?></b></code></h4>                             
                </div>

                <div class="progress">
                    <div class="progress-bar bg-blue-1000" style="width: <?php echo e($agentsalespercentage); ?>%">
                        <b><?php echo e($agentsalespercentage); ?>% Complete</b>
                    </div>
                </div>
            </div>
        </div>
    </div>


<div class="row">
            <div class="panel panel-body">
                 <h1><b><center>Upcoming TO DO List</center></b></h1>
                <hr>
                 <table class="table datatable-basic">
                    <thead>
                        <tr>
                            <th data-sortable="true">Customer Name</th>
                            <th data-sortable="true">Fast Pass Date</th>
                            <th data-sortable="true">Advanced Dining Reservations</th>
                            <th data-sortable="true">Itinerary Tip Sheets</th>
                            <th data-sortable="true">Final Payment Due</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if($todolistcount > 0): ?>
                             <?php $__currentLoopData = $todolist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $todo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  
                                <tr>
                                    <?php if($todo->customer == null): ?>
                                        <td>
                                            No Customer
                                        </td>
                                    <?php else: ?>
                                        <td>
                                            <?php echo e($todo->customer->first_name); ?> <?php echo e($todo->customer->last_name); ?>

                                        </td>
                                    <?php endif; ?>
                                    
                                    <?php if($todo->trip == null): ?>
                                    <td>
                                        
                                    </td>
                                    <td>
                                        
                                    </td>
                                    <td>
                                        
                                    </td>
                                    <td>
                                        
                                    </td>
                                    <?php else: ?>
                                    <td>
                                        <?php echo e($todo->trip->fast_pass_date); ?>

                                    </td>
                                    <td>
                                        <?php echo e($todo->trip->advanced_dining_reservations); ?>

                                    </td>
                                    <td>
                                        <?php echo e($todo->trip->itinerary_tip_sheets); ?>

                                    </td>
                                    <td>
                                        <?php echo e($todo->trip->final_payment_due); ?>

                                    </td>
                                    <td>
                                        <a href="<?php echo e(url('/toggleTodo/'.$todo->id)); ?>" class="btn btn-success">Mark as Complete</a>
                                    </td>
                                    <?php endif; ?>
                                </tr>
                             <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endif; ?>   
                    </tbody>
                </table>
            </div>
        </div>


    <div class="row">
            <div class="panel panel-body"> 
                <h1><b><center>Agent Stats</center></b></h1>
                <hr>
            <div class="row">
                <div class="col-lg-4">
                    <h3><strong>This Month's Stats</strong></h3>
                </div>

                 <div class="col-lg-4">
                    <div class="panel bg-teal-400">
                        <div class="panel-body">
                            <h4 class="no-margin"><center><?php echo e($customers_month); ?></center></h4>
                            <center>Customers</center>
                        </div>
                    </div>
                </div>                    

                <div class="col-lg-4">
                    <div class="panel bg-blue-400">
                        <div class="panel-body">
                            <h4 class="no-margin"><center><?php echo e($trips_month); ?></center></h4>
                            <center>Trips</center>
                        </div>
                    </div>
                </div>                    
            </div>

            <div class="row">
                <div class="col-lg-4">
                     <h3><strong>Overall Stats</strong></h3>
                </div>

                <div class="col-lg-4">
                    <div class="panel bg-teal-400">
                        <div class="panel-body">
                            <h4 class="no-margin"><center><?php echo e($total_customer); ?></center></h4>
                            <center>Customers</center>
                        </div>
                    </div>
                </div>             

                <div class="col-lg-4">
                    <div class="panel bg-blue-400">
                        <div class="panel-body">
                            <h4 class="no-margin"><center><?php echo e($total_trips); ?></center></h4>
                            <center>Trips</center>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-lg-6">
                    <h3><strong>This Month's Total Expected Commission</strong></h3>
                </div>

                <div class="col-lg-6">
                    <div class="panel bg-teal-700">
                        <div class="panel-body">
                         <br>
                            <h4 class="no-margin"><center>$<?php echo e($month_expected_commission); ?></center></h4>
                         <br>
                         </div>
                    </div>
                 </div> 
            </div>

            <div class="row">
                <div class="col-lg-6">
                    <h3><strong>Total Commission for <?php echo e(date("Y")); ?></strong></h3>
                 </div>
                                        
                <div class="col-lg-6">
                    <div class="panel bg-teal-700">
                        <div class="panel-body">
                        <br>
                            <h4 class="no-margin"><center>$<?php echo e($year_total_commission); ?></center></h4>
                        <br>
                        </div>
                    </div>
                </div>
            </div>

            </div>            
        </div>        

    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>