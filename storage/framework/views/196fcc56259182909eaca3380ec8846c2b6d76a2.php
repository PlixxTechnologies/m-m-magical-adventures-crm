<?php $__env->startSection('content'); ?>
<style>
.red{
    color: red;
}
</style>
<?php if(\Session::has('error')): ?>
    <div class="alert alert-danger">
        <ul>
            <li><?php echo \Session::get('error'); ?></li>
        </ul>
    </div>
<?php endif; ?>
<!-- Page header -->
                <div class="page-header page-header-default">
                    <div class="page-header-content">
                        <div class="page-title">
                            <h4><span class="text-semibold">Edit Agent</span></h4>
                        </div>
                    </div>
                </div>
   <!-- /page header -->
    <div class="container">
        <br>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">

                        <form action="<?php echo e(url('user/update/'.$user->id)); ?>" method="post">
                            <?php echo csrf_field(); ?>

                            
                            <div class="form-group">
                                <label><span class="red">*</span>Name:</label>
                                <input type="text" name="name" value="<?php echo e($user->name); ?>" class="form-control" required>
                                <?php if($errors->has('name')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('name')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                            <div class="form-group">
                                <label><span class="red">*</span>Email:</label>
                                <input type="email" name="email" value="<?php echo e($user->email); ?>" class="form-control" required>
                                <?php if($errors->has('email')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('email')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                            <div class="form-group">
                                <label><span class="red">*</span>Phone Number:</label>
                                <input type="text" name="phone" value="<?php echo e($user->phone); ?>" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label><span class="red">*</span>Address:</label>
                                <input type="text" name="address" value="<?php echo e($user->address); ?>" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label><span class="red">*</span>Contract Signed Date:</label>
                                <input type="date" name="contract_date" value="<?php echo e($user->contract_date); ?>" class="form-control"  required>
                            </div>
                            <div class="form-group">
                                <label><span class="red">*</span>Role:</label>
                                <select name="role" class="form-control" required>
                                    <option value= 0 <?php if($user->role == 0): ?> selected <?php endif; ?>>Agent</option>
                                    <option value= 1 <?php if($user->role == 1): ?> selected <?php endif; ?>>Admin</option>

                                </select>
                            </div>
                            <div class="form-group">
                                <label><span class="red">*</span>Commission:</label>
                                <select name="commission" class="form-control" required>
                                    <option value="">Choose One</option>
                                    <option value="60" <?php if($user->commission == "60"): ?> selected <?php endif; ?>>60%</option>
                                    <option value="65"  <?php if($user->commission == "65"): ?> selected <?php endif; ?>>65%</option>
                                    <option value="70"  <?php if($user->commission == "70"): ?> selected <?php endif; ?>>70%</option>
                                    <option value="75"  <?php if($user->commission == "75"): ?> selected <?php endif; ?>>75%</option>
                                    <option value="80"  <?php if($user->commission == "80"): ?> selected <?php endif; ?>>80%</option>
                                    <option value="100"  <?php if($user->commission == "100"): ?> selected <?php endif; ?>>100%</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label><span class="red">*</span>Sales Goal:</label>
                                <input type="text" name="sales_goal" value="<?php echo e($user->sales_goal); ?>" class="form-control" required>
                            </div>



                            <div class="form-group">
                                <label><span class="red">*</span>My Anniversary Date :</label>
                                <input type="date" name="fdacs_date" class="form-control" value="<?php echo e($user->fdacs_date); ?>" required>
                            </div>

                            <div class="form-group">
                                <label><span class="red">*</span>Status:</label>
                                <select name="status" class="form-control" required>
                                    <option disabled selected>--Select Status--</option>
                                    <option value="active" <?php if($user->status=="active"): ?>selected <?php endif; ?> >Active</option>
                                    <option value="inactive" <?php if($user->status=="inactive"): ?>selected <?php endif; ?>>Inactive</option>
                                    <option value="terminated" <?php if($user->status=="terminated"): ?>selected <?php endif; ?>>Terminated</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label><span class="red">*</span>Agent Mentor:</label>
                                <select name="user_id" class="form-control" required>
                                    <option disabled selected> --Select Mentor--</option>}
                                    
                                    <?php $__currentLoopData = $all_users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user_all): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    {
                                        <option value="<?php echo e($user_all->id); ?>" <?php if($user_all->id==$user->user_id): ?> selected <?php endif; ?> ><?php echo e($user_all->name); ?></option>
                                    }

                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                </select>
                            </div>

                            <div id="changePasswordFields">
                                
                            </div>




<?php $x=0; ?>

                            <?php $__currentLoopData = $destinations; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $destination): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  
                            <?php $x=0; ?>                                 
                                <div class="col-lg-6">
                                    <div class="form-group">
<!--                                         <label class="display-block text-semibold"><?php echo e($destination->name); ?></label>
 -->                                        
                                                 <?php $__currentLoopData = $agentdestinations; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $agentdestination): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                 
                                                 <?php if($destination->name== $agentdestination->destination_name): ?>
                                                <?php $x=1; ?>
                                                 <?php endif; ?>
                                                 
                                               <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


                                                <?php if($x==1): ?>                         
                                                    <div class="checkbox">
                                            <label>
                                                <input type="checkbox" checked class="styled" name="destinations[]" value="<?php echo e($destination->id); ?>">
                                                <?php echo e($destination->name); ?>

                                            </label>
                                                 </div>
                                                
                                        <?php else: ?>
                                                  <div class="checkbox">
                                            <label>
                                                <input type="checkbox" class="styled" name="destinations[]" value="<?php echo e($destination->id); ?>">
                                                <?php echo e($destination->name); ?>

                                            </label>
                                                 </div>
                                            

                                        <?php endif; ?>
                                    </div>
                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            


                            <button type="submit" class="btn btn-primary pull-right">Update Agent</button>
                            <button type="button" class="btn btn-danger pull-right mr-5" onclick="getPasswordFields()">Change Password</button>
                        </form>

                    </div>
                </div>
            </div>
        </div><!--/.row-->
    </div>	<!--/.main-->


<script type="text/javascript">
    function getPasswordFields()
    {
        $('#changePasswordFields').html("");
        var passwordFields=`<div class="form-group">
                                <label><span class="red">*</span>Password:</label>
                                <input type="password" name="password" value="" minlength='6' min='6' class="form-control" placeholder="Enter New Password" required>
                               
                            </div>
                            <div class="form-group">
                                <label><span class="red">*</span>Confirm Password:</label>
                                <input id="password-confirm" type="password" minlength='6' min='6'  class="form-control" placeholder="Confirm Password" name="password_confirmation" required>

                            </div>`;
        $('#changePasswordFields').append(passwordFields);
    }
</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>