<?php $__env->startSection('content'); ?>

<!-- Page header -->
                <div class="page-header page-header-default">
                    <div class="page-header-content">
                        <div class="page-title">
                            <h4><span class="text-semibold">All Agents</span></h4>
                        </div>
                    </div>
                </div>
   <!-- /page header -->
<!-- Basic initialization -->
                    <div class="panel panel-flat">
                        <table class="table datatable-button-init-basic">
                            <thead>
                               <tr>
                                <th  data-sortable="true" >ID</th>
                                <th  data-sortable="true">Name</th>
                                <th  data-sortable="true">Email</th>
                                <th  data-sortable="true">Phone</th>
                                <th  data-sortable="true">Adddress</th>
                                <th  data-sortable="true">Contract Signed Date</th>
                                <th  data-sortable="true">Commission Rate</th>
                                <th  data-sortable="true">Role</th>
                                <th  data-sortable="true">Sales Goal</th>
                                <th  data-sortable="true">Anniversary Date</th>
                                <!-- <th  data-sortable="true">FDACS Date</th> -->
                                <th  data-sortable="true">Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td>
                                        <?php echo e($user->id); ?>

                                    </td>
                                    <td>
                                        <?php echo e($user->name); ?>

                                    </td>
                                    <td>
                                        <?php echo e($user->email); ?>

                                    </td>
                                     <td>
                                        <?php echo e($user->phone); ?>

                                    </td>
                                     <td>
                                        <?php echo e($user->address); ?>

                                    </td>
                                     <td>
                                        <?php echo e(date("m", strtotime($user->contract_date))); ?>-<?php echo e(date("d", strtotime($user->contract_date))); ?>-<?php echo e(date("Y", strtotime($user->contract_date))); ?>

                                    </td>
                                    <?php if($user->commission == null): ?>                                    
                                    <td>
                                       Not Entered
                                    </td>
                                    <?php else: ?>
                                    <td>
                                         <?php echo e($user->commission); ?>%
                                    </td>
                                    <?php endif; ?>
                                    <?php if($user->role == 1): ?>
                                    <td>
                                        Admin
                                    </td>
                                <?php else: ?>
                                    <td>
                                        Agent
                                    </td>
                                <?php endif; ?>

                                <?php if($user->sales_goal == null): ?>
                                    <td>
                                        Not Entered
                                    </td>
                                <?php else: ?>
                                    <td>
                                        $<?php echo e($user->sales_goal); ?>

                                    </td>
                                <?php endif; ?>
                                <td>
                                    <?php echo e(\Carbon\Carbon::parse($user->fdacs_date)->format('m-d-Y')); ?>

                                    
                                </td>
                                <td class="text-capitalize">
                                    <?php echo e($user->status); ?>

                                </td>
                                    
                                    <td class="center">
                                        <a class="btn btn-info" href="<?php echo e(url('user/edit').'/'.$user->id); ?>">
                                            EDIT
                                        </a>
                                        <form style="display:inline" method="post" action="<?php echo e(url('user/delete')); ?>/<?php echo e($user->id); ?>" onsubmit="return confirm('Are you sure you want to delete this agent?');">
                                            <?php echo csrf_field(); ?>

                                            <?php echo e(method_field('DELETE')); ?>

                                            <button type="submit" class="btn btn-danger">DELETE</button>
                                        </form>
                                    </td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                       
                            </tbody>
                        </table>
                    </div>
                    <!-- /basic initialization -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>