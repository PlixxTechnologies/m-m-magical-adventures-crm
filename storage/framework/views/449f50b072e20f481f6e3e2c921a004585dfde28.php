<?php $__env->startSection('content'); ?>

<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

<?php
use App\TripsDTO;
use Illuminate\Support\Collection;
?>

<style type="text/css">
    .fc-event, .fc-event-dot{
        background-color: #448ea7 !important;
        color: #000000 !important;
    }
</style>

   <!-- Page header -->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="<?php echo e(url('/home')); ?>">
                    <em class="fa fa-home"></em>
                </a></li>
                <li class="active">Calendar</li>
            </ol>
        </div><!--/.row-->
    <div class="container-fluid"> 
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Calendar</h1>
            </div>
        </div>  <!-- /page header -->
   </div>

<div id="modal_theme_valuUP" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <form method="post">
            <div class="modal-content" style="width: max-content;">
                <div class="modal-header bg-primary text-center">
                    <h3 class="modal-title text-center">Trips Information</h3>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <div class="modal-body">
                    <input type="hidden" id="xyz" name="xyz" />
                    <div class="row">
                        <table class="table">
                          <thead>
                            <tr>
                              <th scope="col">Reservation Number</th>
                              <th scope="col">Customer Name</th>
                              <th scope="col">Customer ID</th>
                              <th scope="col">Fast Past Date</th>
                              <th scope="col">Advanced Dining Reservations</th>
                              <th scope="col">Final Payment Date</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <th scope="row">
                                <a  id="reser_number" href="" class="tr_Id"></a>
                                <p id="reser_numbers"></p>
                              </th>
                              <td>
                                <p id="cust_name"></p>
                              </td>
                              <td>
                                <p id="cust_id"></p>
                              </td>
                              <td>
                                <p id="fpd_id"></p>
                              </td>
                              <td>
                                <p id="adp_id"></p>
                              </td>
                              <td>
                                <p id="final_id"></p>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>


<?php if($user->role == 1): ?>

    <div class="row">
        <div class="col-md-6">
            <form>
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <!-- <label><span class="red">*</span>Agent Name:</label> -->
                            <select name="userId" class="form-control" required="" id="userId">
                                <option value="">Choose One</option>
                                <?php $__currentLoopData = $use; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $us): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 

                                <?php if($us->id==$agent_id): ?>
                                <option value="<?php echo e($us->id); ?>" selected="" name="usID"><?php echo e($us->name); ?></option>
                                <?php else: ?>
                                <option value="<?php echo e($us->id); ?>" name="usID"><?php echo e($us->name); ?></option>
                                <?php endif; ?>

                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <button type="submit"  class="btn btn-danger">Search</button>
                    </div>
                </div>
            </form>
        </div>

        <div class="col-lg-6">
             <form action="<?php echo e(url('calenders-list')); ?>" method="get">
                <div class="row">
                    <div class="col-md-8">
                          <div class="form-group">
                          <input type="search" class="form-control rounded" placeholder="Search" aria-label="Search" aria-describedby="search-addon"  required />
                          </div>
                    </div>
                    <div class="col-md-4">
                         <button type="button" class="btn btn-primary">search</button>
                    </div>
                </div>
            </form>
        </div>
        <br><br><br><br>
    </div>

<?php endif; ?>


<style type="text/css">
    label {
vertical-align: middle;
}
</style>
<div class="row">
       <div class="col-md-4">
            <form method="post">
                <div class="form-group">
                    <label style="vertical-align: middle;">Fast Past Date </label>&nbsp;&nbsp;
                    <label class="switch">
                      <input type="checkbox" onclick="myFunction()" id="CheckedFastPassDate" checked="">
                      <span class="slider round" style="background-color:#5A62C1"></span>
                    </label>
                </div>
            </form>
        </div>


                     <div class="col-md-4">
                            <div class="form-group">
                                <label style="vertical-align: middle;">Advanced Dining Reservations</label>&nbsp;&nbsp;
                                <label class="switch">
                                  <input type="checkbox" checked="checked">
                                  <span class="slider round" style="background-color:#6495ED"></span>
                                </label>
                            </div>
                    </div>
                     <div class="col-md-4">
                            <div class="form-group">
                                <label style="vertical-align: middle;">Final Payment Date</label>&nbsp;&nbsp;
                                <label class="switch">
                                  <input type="checkbox" checked="checked">
                                  <span class="slider round" style="background-color:#DE3163"></span>
                                </label>
                            </div>
                    </div>

    </div>
                                      <div class="row" id="myDIV" style="display: none;">
                                    <div class="col-lg-12"> This is my DIV element.</div>
                                    </div>


<br>
    <div class="panel panel-flat" style="padding-top: 30px; padding-bottom: 30px;">
        <div class="container-fluid">
        <div class="card">
                    <div class="card-header header-elements-inline">
                        <!-- <h5 class="card-title">Event colors</h5> -->
                        <div class="header-elements">
                            <div class="list-icons">
                                <a class="list-icons-item" data-action="collapse"></a>
                                <a class="list-icons-item" data-action="reload"></a>
                                <a class="list-icons-item" data-action="remove"></a>
                            </div>
                        </div>
                    </div>
                    
                    <div style="overflow-x:auto;" class="card-body-fluid">
                        <!-- <p class="mb-3">FullCalendar allows you to change the color of all events on the calendar using the <code>eventColor</code> option. Also you can change text, border and background colors for events in a specific Event Source with <code>event source</code> options (backgroundColor, color, textColor and borderColor) and for individual events with <code>color</code> option, which sets an event's background and border color. Example below demonstrates event colors based on a day of the week.</p> -->

                        <div class="fullcalendar-basic"></div>
                    </div>
                </div>
    </div>
</div>
<!-- <div class="content">

</div> -->

<script type="text/javascript">

var obj = $('#userId').val();

    //$.get('/ajax-GetTrips', function(data){
        //$.get('/ajax-GetTrips', function(){

    $.get('ajax-GetTrips?u_I_d='+obj, function(data){
         $('.fullcalendar-basic').fullCalendar({
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,basicWeek,basicDay'
                },
                defaultDate: new Date(),
                editable: true,
                events: data, 

                eventClick: function (event, jsEvent, view) {
                   if(event['customer_Id'] != null && event['customer_Id'] != "" && event['customer_name'] != null && event['customer_name'] != "" && event['reservation_number'] != null && event['reservation_number'] != "" || event['fastpassdate'] != null || event['fastpassdate'] != ""|| event['advancediningreservations'] != null || event['advancediningreservations'] != ""  || event['finalduedate'] != null || event['finalduedate'] != "")
                   {
                       $(".tr_Id").attr("href", href="<?php echo e(url('trip/view')); ?>/"+event['id']);
                       $("#reser_number").text(event['reservation_number']);
                       $("#cust_name").text(event['customer_name']);
                       $("#cust_id").text(event['customer_Id']);
                       $("#fpd_id").text(event['fastpassdate']);
                       $("#adp_id").text(event['advancediningreservations']);
                       $("#final_id").text(event['finalduedate']);                    
                       $("#modal_theme_valuUP").modal("show");
                   }
                },

                dayClick: function (date, jsEvent, view) {
                   

                },

                eventDrop: function (event) {
                    //alert(event.title + " and on starting date " + event.start.toISOString() + " and ending date " + event.end.toISOString());
                  
                },
                eventResize: function (event) {
                    //alert(info.title + " end is now " + info.end.toISOString());
                                    
                },

                eventAfterRender: function (event, element, view) {
                    var dataHoje = new Date();
                    if (event.start < dataHoje && event.end > dataHoje) {
                        //event.color = "#FFB347"; //Em andamento
                        element.css('background-color', '#FFB347');
                    } else if (event.start < dataHoje && event.end < dataHoje) {
                        //event.color = "#77DD77"; //Concluído OK
                        element.css('background-color', '#77DD77');
                    } else if (event.start > dataHoje && event.end > dataHoje) {
                        //event.color = "#AEC6CF"; //Não iniciado
                        element.css('background-color', '#AEC6CF');
                    }
                }

            });
        });

</script>

<script>
        function myFunction() {
          var x = $("#CheckedFastPassDate").is(":checked");
          console.log(x);
          if(x == false){

          }
        }
        </script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout.dashboardx', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>