<?php $__env->startSection('content'); ?>

<!-- Page header -->
                <div class="page-header page-header-default">
                    <div class="page-header-content">
                        <div class="page-title">
                            <h4><span class="text-semibold">Walt Disney World Commission Reports</span></h4>
                        </div>
                    </div>
                </div>
   <!-- /page header -->

<div class="container">
   
    <div class="row">
        <form action="<?php echo e(url('walt-disney-world-commission')); ?>" method="post">
          <?php echo csrf_field(); ?>


        <div class="col-md-4 col-lg-4" style="padding: 20px;">
            <div class="form-group">
                <label>*Start Date:</label>
                <input type="date" name="startdate" class="form-control" <?php if($startdate != null): ?> value="<?php echo e($startdate); ?>" <?php endif; ?> required>
            </div>
        </div>

        <div class="col-md-offset-2 col-md-4 col-lg-offset-2 col-lg-4" style="padding: 20px;">
            <div class="form-group">
                <label>*End Date:</label>
                <input type="date" name="enddate" class="form-control"  <?php if($enddate != null): ?> value="<?php echo e($enddate); ?>" <?php endif; ?> required>
            </div>
        </div> 
  </div>
    <button type="submit" class="btn btn-primary pull-right">Get Report</button>
  </form>
</div>

<br>

<?php if($trips != null): ?>
 <br><h3 class="box-title">Total Commission : <strong>$<?php echo e($commissionTotal); ?></strong></h3>
 <h3 class="box-title">Total Expected Commission : <strong>$<?php echo e($expectedCommissionTotal); ?></strong></h3>
  <h3 class="box-title">Total Sales : <strong>$<?php echo e($TotalSales); ?></strong></h3>

 <!-- Basic initialization -->
                    <div class="panel panel-flat">
                        <div class="panel-heading">
                            <h5 class="panel-title">Customers List</h5>
                        </div>

                        <table class="table datatable-button-init-basic">
                            <thead>
                                <tr>
                                <th data-sortable="true">Agent Name</th>
                                <th data-sortable="true">Trip ID</th>
                                <th data-sortable="true">Customer Name</th>
                                <th data-sortable="true">Reservation Number</th>                                
                                <th data-sortable="true">Booking Date</th>
                                <th data-sortable="true">Checkin Date</th>
                                <th data-sortable="true">Checkout Date</th>
                                <th data-sortable="true">Walt Disney World Hotel Name</th>
                                <th data-sortable="true">Commission</th>
                                <th data-sortable="true">Expected Commission</th>
                                <th data-sortable="true">Total Sale</th>                                 
                            </tr>
                            </thead>
                            <tbody>
                             <?php $__currentLoopData = $trips; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $trip): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    
                                    <td>
                                        <?php echo e($trip->user->name); ?>

                                    </td>
                                    <td>
                                        <?php echo e($trip->id); ?>

                                    </td>
                                    <td>
                                        <?php echo e($trip->customer->first_name); ?> <?php echo e($trip->customer->last_name); ?>

                                    </td>

                                     <td>
                                        <?php echo e($trip->reservation_number); ?>

                                    </td>
                                    
                                    <td>
                                        <?php echo e($trip->booking_date); ?>

                                    </td>
                                    
                                    <td>
                                        <?php echo e($trip->checkin_date); ?>

                                    </td>
                                    <td>
                                        <?php echo e($trip->checkout_date); ?>

                                    </td>
                                    <td>
                                        <?php echo e($trip->disneyworld_hotel_name); ?>

                                    </td>
                                    <td>
                                        <?php echo e($trip->commission); ?>

                                    </td>
                                    <td>
                                        <?php echo e($trip->expected_commission); ?>

                                    </td>
                                    <td>
                                        <?php echo e($trip->total_sale); ?>

                                    </td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                                            
                            </tbody>                            
                        </table>
                    </div>
                    <!-- /basic initialization -->
                    <?php endif; ?>

</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>