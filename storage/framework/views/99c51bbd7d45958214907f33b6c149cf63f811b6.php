<?php $__env->startSection('content'); ?>

<!-- Datatables JS FILES -->

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.print.min.js"></script>

<!-- /Datatables JS FILES -->
<!-- Page header -->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="<?php echo e(url('/home')); ?>">
                    <em class="fa fa-home"></em>
                </a></li>
                <li class="active">View Quotes</li>
            </ol>
        </div><!--/.row-->
    <div class="container-fluid"> 
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">View Quotes</h1>
            </div>
        </div>  <!-- /page header -->
   </div>


<div class="container-fluid">
<!-- Simple lists -->
<div class="row pull-right">
<input type="button" onclick="printDiv('printableArea')" value="Print" class="btn btn-primary btn-lg" />

    </div>

    <div id="printableArea">
        <div class="row" >
            <center>
                <h5 class="text-muted">Agent Name</h5>
                    <h3 class="media-heading text-semibold"><?php echo e($quotes->user->name); ?></h3>
            </center>  
            <br>
            <br>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <center>
                            <h3 class="panel-title">
                                <b>Lead Information</b>
                            </h3>
                        </center>
                    </div>

                    <div class="panel-body">
                        <div class="col-md-6">
                            <span class="text-muted">First Name</span>
                            <div class="media-heading text-semibold"><?php echo e($quotes->lead->first_name); ?></div>
                        </div> 

                        <div class="col-md-6">
                            <span class="text-muted">Last Name</span>
                            <div class="media-heading text-semibold"><?php echo e($quotes->lead->last_name); ?></div>
                        </div>

                        <div class="col-md-6">
                            <span class="text-muted">Email</span>
                            <div class="media-heading text-semibold"><?php echo e($quotes->lead->email); ?></div>
                        </div>  

                        <div class="col-md-6">
                            <span class="text-muted">Country Code</span>
                            <div class="media-heading text-semibold"><?php echo e($quotes->lead->country_code); ?></div>
                        </div>
                        <div class="col-md-6">
                            <span class="text-muted">Phone Number</span>
                            <div class="media-heading text-semibold">
                                <?php echo e($quotes->lead->phone_no); ?>

                            </div>
                        </div>

                        <div class="col-md-6">
                            <span class="text-muted">Address</span>
                            <div class="media-heading text-semibold"><?php echo e($quotes->lead->address1); ?></div>
                        </div>

                        <div class="col-md-6">
                            <span class="text-muted">City</span>
                            <div class="media-heading text-semibold"><?php echo e($quotes->lead->city); ?></div>
                        </div>  

                        <div class="col-md-6">
                            <span class="text-muted">State</span>
                            <div class="media-heading text-semibold"><?php echo e($quotes->lead->state); ?></div>
                        </div>

                        <div class="col-md-6">
                            <span class="text-muted">Zip</span>
                            <div class="media-heading text-semibold"><?php echo e($quotes->lead->zip); ?></div>
                        </div>

                        <?php if($quotes->Referal != null): ?>  
                        <div class="col-md-6">
                            <span class="text-muted">Referral</span>
                            <div class="media-heading text-semibold"><?php echo e($quotes->Referal); ?></div>
                        </div>
                        <?php else: ?>
                        <div class="col-md-6">
                            <span class="text-muted">Referral</span>
                            <div class="media-heading text-semibold"></div><br>
                        </div>
                        <?php endif; ?>                                           

                        <?php if($quotes->lead->disney_experience_username != null): ?> 
                            <div class="col-md-6">
                                <span class="text-muted">Disney Experience Username</span>
                                <div class="media-heading text-semibold"><?php echo e($quotes->lead->disney_experience_username); ?></div>
                            </div>
                        <?php else: ?>
                            <div class="col-md-6">
                                <span class="text-muted">Disney Experience Username</span>
                                <div class="media-heading text-semibold"></div><br>
                            </div>
                        <?php endif; ?>

                        <?php if($quotes->lead->disney_experience_password != null): ?>  
                            <div class="col-md-6">
                                <span class="text-muted">Disney Experience Password</span>
                                <div class="media-heading text-semibold"><?php echo e($quotes->lead->disney_experience_password); ?></div>
                            </div>
                        <?php else: ?>
                            <div class="col-md-6">
                                <span class="text-muted">Disney Experience Password</span>
                                <div class="media-heading text-semibold"></div><br>
                            </div>
                        <?php endif; ?>

                        <?php if($quotes->lead->notes != null): ?>  
                            <div class="col-md-6">
                                <span class="text-muted">Notes</span>
                                <div class="media-heading text-semibold"><?php echo e($quotes->lead->notes); ?></div>
                            </div> 
                        <?php else: ?>
                            <div class="col-md-6">
                                <span class="text-muted">Notes</span>
                                <div class="media-heading text-semibold"></div><br>
                            </div> 
                        <?php endif; ?>

                        <?php if($quotes->user->name != null): ?>  
                            <div class="col-md-6">
                                <span class="text-muted">Agent Name</span>
                                <div class="media-heading text-semibold"><?php echo e($quotes->user->name); ?></div>
                            </div> 
                        <?php else: ?>
                            <div class="col-md-6">
                                <span class="text-muted">Agent Name</span>
                                <div class="media-heading text-semibold">Agent has been deleted</div>
                            </div> 
                        <?php endif; ?>
                    </div>
                </div>
                <!-- /simple list -->
            </div>

            <div class="col-md-6">
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <center>
                            <h3 class="panel-title">
                                <b>Quotes Information</b>
                            </h3>
                        </center>
                    </div>
                    <div class="panel-body">
                        <ul class="media-list">
                           <!--  <li class="media"> -->
                                <div class="col-md-6">
                                    <span class="text-muted">Reservation Number</span>
                                    <div class="media-heading text-semibold">
                                        <a target="_blank" href="" onclick="reload();">
                                            <?php echo e($quotes->reservation_number); ?>

                                        </a>
                                    </div>
                                </div>  
                                <div class="col-md-6">
                                    <span class="text-muted">Booking Date</span>
                                    <div class="media-heading text-semibold"> <?php echo e(date("m", strtotime($quotes->booking_date))); ?>-<?php echo e(date("d", strtotime($quotes->booking_date))); ?>-<?php echo e(date("Y", strtotime($quotes->booking_date))); ?></div>
                                </div>

                                <div class="col-md-6">
                                    <span class="text-muted">Check In Date</span>
                                    <div class="media-heading text-semibold">
                                        <?php echo e(date("m", strtotime($quotes->checkin_date))); ?>-<?php echo e(date("d", strtotime($quotes->checkin_date))); ?>-<?php echo e(date("Y", strtotime($quotes->checkin_date))); ?>

                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <span class="text-muted">Check Out Date</span>
                                    <div class="media-heading text-semibold"> <?php echo e(date("m", strtotime($quotes->checkout_date))); ?>-<?php echo e(date("d", strtotime($quotes->checkout_date))); ?>-<?php echo e(date("Y", strtotime($quotes->checkout_date))); ?></div>
                                </div>

                                <div class="col-md-6">
                                    <span class="text-muted">Trip Status</span>
                                    <?php if($quotes->status ==0 ): ?>
                                        <div class="media-heading text-semibold"><?php echo e($quotes->trip_status); ?></div>
                                    <?php else: ?>
                                        <div class="media-heading text-semibold">Canceled</div>
                                    <?php endif; ?>
                                </div>  

                                <div class="col-md-6">
                                    <span class="text-muted">Total Sale</span>
                                    <div class="media-heading text-semibold">$<?php echo e($quotes->total_sale); ?></div>
                                </div>

                                <div class="col-md-6">
                                    <span class="text-muted">Total Commission</span>
                                    <div class="media-heading text-semibold">$<?php echo e($quotes->commission); ?></div>
                                </div>
                                <div class="col-md-6">
                                    <span class="text-muted">Expected Commission</span>
                                    <div class="media-heading text-semibold">$<?php echo e($quotes->expected_commission); ?></div>
                                </div>

                                <?php if($quotes->travel_with != null || $quotes->travel_with != ""): ?>
                                <?php if($flag == "true"): ?>
                                <div class="col-md-6">
                                    <span class="text-muted">Travel With</span>
                                    <div class="media-heading text-semibold"><?php echo e(substr($quotes->travel_with, 0,-1)); ?></div>
                                </div>
                                <?php else: ?> 
                                <div class="col-md-6">
                                    <span class="text-muted">Travel With</span>
                                    <div class="media-heading text-semibold"><?php echo e($quotes->travel_with); ?></div>
                                </div>
                                <?php endif; ?>
                                <?php endif; ?>

                                <?php if($quotes->special_request != null || $quotes->special_request != ""): ?>
                                 <div class="col-md-6">
                                    <span class="text-muted">Special Occasions or Requests</span>
                                    <div class="media-heading text-semibold"><?php echo e($quotes->special_request); ?></div>
                                </div>
                                <?php endif; ?>

                                <?php if($quotes->destination != null || $quotes->destination != ""): ?>
                                <div class="col-md-6">
                                    <span class="text-muted">Destinations</span>
                                    <div class="media-heading text-semibold"><?php echo e($quotes->destination); ?></div>
                                </div>
                                <?php endif; ?>

                                <?php if($quotes->disneyworld_hotel_name != null || $quotes->disneyworld_hotel_name != ""): ?>  
                                <div class="col-md-6">
                                    <span class="text-muted">Walt Disney World Hotel Name </span>
                                    <div class="media-heading text-semibold"><?php echo e($quotes->disneyworld_hotel_name); ?></div>
                                </div> 
                                <?php endif; ?>

                                <?php if($quotes->magical_express != null || $quotes->magical_express != ""): ?>
                                <div class="col-md-6">
                                    <span class="text-muted">Magical Express</span>
                                    <div class="media-heading text-semibold"><?php echo e($quotes->magical_express); ?></div>
                                </div>  
                                <?php endif; ?>

                                <?php if($quotes->memory_maker != null || $quotes->memory_maker != ""): ?>
                                <div class="col-md-6">
                                    <span class="text-muted">Memory Maker </span>
                                    <div class="media-heading text-semibold"><?php echo e($quotes->memory_maker); ?></div>
                                </div>  
                                <?php endif; ?>

                                <?php if($quotes->disney_dining_plan != null || $quotes->disney_dining_plan != ""): ?>
                                <div class="col-md-6">
                                    <span class="text-muted">Dining Plan</span>
                                    <div class="media-heading text-semibold"><?php echo e($quotes->disney_dining_plan); ?></div>
                                </div>  
                                <?php endif; ?>

                                <?php if($quotes->magic_band_color != null || $quotes->magic_band_color != ""): ?>
                                <div class="col-md-6">
                                    <span class="text-muted">Magic Band Color Selection </span>
                                    <div class="media-heading text-semibold"><?php echo e($quotes->magic_band_color); ?></div>
                                </div> 
                                <?php endif; ?>

                                <?php if($quotes->disney_experience_username != null || $quotes->disney_experience_username != ""): ?>
                                <div class="col-md-6">
                                    <span class="text-muted">My Disney Experience User Name</span>
                                    <div class="media-heading text-semibold"><?php echo e($quotes->disney_experience_username); ?></div>
                                </div>
                                <?php endif; ?>

                                <?php if($quotes->disney_experience_password != null || $quotes->disney_experience_password != ""): ?> 
                                <div class="col-md-6">
                                    <span class="text-muted">My Disney Experience Password </span>
                                    <div class="media-heading text-semibold"><?php echo e($quotes->disney_experience_password); ?></div>
                                </div>
                                <?php endif; ?>

                                <?php if($quotes->disneyresort_hotel_name != null || $quotes->disneyresort_hotel_name != ""): ?>
                                <div class="col-md-6">
                                    <span class="text-muted">Disneyland Hotel Name</span>
                                    <div class="media-heading text-semibold"><?php echo e($quotes->disneyresort_hotel_name); ?></div>
                                </div>
                                <?php endif; ?> 

                                <?php if($quotes->good_neighbor_hotel != null || $quotes->good_neighbor_hotel != ""): ?> 
                                <div class="col-md-6">
                                    <span class="text-muted">Good Neighbor Hotel </span>
                                    <div class="media-heading text-semibold"><?php echo e($quotes->good_neighbor_hotel); ?></diV>
                                </div>
                                <?php endif; ?>

                                <?php if($quotes->ship_name != null || $quotes->ship_name != ""): ?>
                                <div class="col-md-6">
                                    <span class="text-muted">Ship Name</span>
                                    <div class="media-heading text-semibold"><?php echo e($quotes->ship_name); ?></div>
                                </div> 
                                <?php endif; ?> 

                                <?php if($quotes->castaway_member != null || $quotes->castaway_member != ""): ?>
                                <div class="col-md-6">
                                    <span class="text-muted">Castaway Member </span>
                                    <div class="media-heading text-semibold"><?php echo e($quotes->castaway_member); ?></div>
                                </div>  
                                <?php endif; ?>

                                <?php if($quotes->bus_transportation != null || $quotes->bus_transportation != ""): ?>
                                <div class="col-md-6">
                                    <span class="text-muted">Bus Transportation</span>
                                    <div class="media-heading text-semibold"><?php echo e($quotes->bus_transportation); ?></div>
                                </div> 
                                <?php endif; ?>

                                <?php if($quotes->universal_orlando_resort_hotel != null || $quotes->universal_orlando_resort_hotel != ""): ?> 
                                <div class="col-md-6">
                                    <span class="text-muted">Universal Orlando Resort Hotels </span>
                                    <div class="media-heading text-semibold"><?php echo e($quotes->universal_orlando_resort_hotel); ?></div>
                                </div>  
                                <?php endif; ?>

                                <?php if($quotes->partners_hotel != null || $quotes->partners_hotel != ""): ?>
                                <div class="col-md-6">
                                    <span class="text-muted">Partners Hotel</span>
                                    <div class="media-heading text-semibold"><?php echo e($quotes->partners_hotel); ?></div>
                                </div>  
                                <?php endif; ?>

                                <?php if($quotes->ticket_type != null || $quotes->ticket_type != ""): ?>
                                <div class="col-md-6">
                                    <span class="text-muted">Ticket Only Sale </span>
                                    <div class="media-heading text-semibold"><?php echo e($quotes->ticket_type); ?></div>
                                </div>  
                                <?php endif; ?>

                                <?php if($quotes->insurance != null || $quotes->insurance != ""): ?>
                                <div class="col-md-6">
                                    <span class="text-muted">Insurance</span>
                                    <div class="media-heading text-semibold"><?php echo e($quotes->insurance); ?></div>
                                </div> 
                                <?php endif; ?>

                                <?php if($quotes->notes != null || $quotes->notes != ""): ?> 
                                <div class="col-md-6">
                                    <span class="text-muted">Notes </span>
                                    <div class="media-heading text-semibold"><?php echo e($quotes->notes); ?></div>
                                </div>   
                                <?php endif; ?>
                        </ul>
                    </div>
                </div>
                <!-- /simple list -->
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function printDiv(divName) {
         var printContents = document.getElementById(divName).innerHTML;
         var originalContents = document.body.innerHTML;
         document.body.innerHTML = printContents;
         window.print();
         document.body.innerHTML = originalContents;
    }

    function reload(){
        location.reload();
    }
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.dashboardx', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>