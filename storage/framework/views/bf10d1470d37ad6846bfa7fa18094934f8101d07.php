<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>M&M MAGICAL ADVENTURES</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="<?php echo e(url('assets/css/icons/icomoon/styles.css')); ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo e(url('assets/css/bootstrap.css')); ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo e(url('assets/css/core.css')); ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo e(url('assets/css/components.css')); ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo e(url('assets/css/colors.css')); ?>" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/loaders/pace.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/core/libraries/jquery.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/core/libraries/bootstrap.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/loaders/blockui.min.js')); ?>"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/tables/datatables/datatables.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/tables/datatables/extensions/buttons.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/forms/selects/select2.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js')); ?>"></script> 
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/tables/datatables/extensions/buttons.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/velocity/velocity.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/velocity/velocity.ui.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/ui/prism.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/forms/styling/uniform.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/notifications/pnotify.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/forms/selects/bootstrap_multiselect.js')); ?>"></script>

    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/forms/wizards/steps.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/forms/selects/select2.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/forms/styling/uniform.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/core/libraries/jasny_bootstrap.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/forms/validation/validate.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/extensions/cookie.js')); ?>"></script>

    <script type="text/javascript" src="<?php echo e(url('assets/js/core/app.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/pages/datatables_extension_buttons_init.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/pages/datatables_extension_buttons_html5.js')); ?>"></script>    
    <script type="text/javascript" src="<?php echo e(url('assets/js/pages/animations_velocity_examples.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/pages/form_multiselect.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/pages/wizard_steps.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/pages/user_pages_list.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/ui/ripple.min.js')); ?>"></script>
    <!-- /theme JS files -->

</head>

<body>    
<style>
    .imgwrapper {
   width: 100%;
}
</style>
    <!-- Main navbar -->
    <div class="navbar">
        <div class="imgwrapper">
        <?php if(Auth::user() != null): ?>
            <a href="<?php echo e(url('/home')); ?>" style="color: white;"><img src="<?php echo e(url('assets/images/New-M&M-Magical-Adventures-Logo-horz.jpg')); ?>" class="img-responsive"></a>
        <?php else: ?>                        
            <a href="#" style="color: white;"><img src="<?php echo e(url('assets/images/New-M&M-Magical-Adventures-Logo-horz.jpg')); ?>" class="img-responsive"></a>                 
        <?php endif; ?>


<!-- <img src="<?php echo e(url('assets/images/Logo.png')); ?>" class="img-responsive"> -->


</div>
    </div>
    <!-- /main navbar -->
    <!-- Page container -->
    <div class="page-container">
        <!-- Page content -->
        <div class="page-content">

            <!-- Main content -->
            <div class="content-wrapper">   
                <!-- Content area -->
                <div class="content">

<style>

#h1 {
   font-size: 37px;
}

td {
   font-weight: normal;
}


</style>
 <!-- Page header -->
                <div class="page-header page-header-default">
                    <div class="page-header-content">
                        <?php if(Auth::user() == null): ?>                      
                            <center><h1 id="h1"><strong>1-844-9MICKEY &nbsp; &nbsp; &nbsp; &nbsp; bookings@MandMMagicalAdventures.com</strong></h1></center><br>
                        <?php endif; ?>
                        <div class="page-title">                            
                            <center><h1 id="h1"><strong>M&M Magical Adventures Disney Dining Plan Calculator</strong></h1></center>
                        </div>
                        <form action="<?php echo e(url('dining-plan-calculator')); ?>" method="post" >
                            <?php echo csrf_field(); ?>

                        <div class="row">
                            <div class="col-md-2 form-group">
                            </div>

                            <div class="col-md-2 form-group">
                                <span>Year</span>
                                <select name="year" class="form-control">
                                    <?php $__currentLoopData = $years; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $calculatoryear): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($calculatoryear->year); ?>"><?php echo e($calculatoryear->year); ?></option>     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                               
                                </select>
                            </div>
                            <div class="col-md-2 form-group">
                                <span># of Adults</span>
                                <select name="noofadults" class="form-control">
                                    <?php if($noofadults == null): ?>                                    
                                     <?php for($x = 0; $x <= 12; $x++): ?> {
                                        <option value="<?php echo e($x); ?>"><?php echo e($x); ?></option> 
                                    <?php endfor; ?>
                                    <?php else: ?>
                                    <option value="<?php echo e($noofadults); ?>" selected><?php echo e($noofadults); ?></option> 
                                     <?php for($x = 0; $x <= 12; $x++): ?> {
                                        <option value="<?php echo e($x); ?>"><?php echo e($x); ?></option> 
                                    <?php endfor; ?>
                                    <?php endif; ?>                                   
                                </select>
                            </div>
                            <div class="col-md-2 form-group">
                                <span># of Children (Ages 3-9)</span>
                                <select name="noofchildren" class="form-control">
                                    <?php if($noofchildren == null): ?>
                                    <?php for($x = 0; $x <= 12; $x++): ?> {
                                        <option value="<?php echo e($x); ?>"><?php echo e($x); ?></option> 
                                    <?php endfor; ?> 
                                    <?php else: ?>
                                    <option value="<?php echo e($noofchildren); ?>" selected><?php echo e($noofchildren); ?></option>
                                    <?php for($x = 0; $x <= 12; $x++): ?> {
                                        <option value="<?php echo e($x); ?>"><?php echo e($x); ?></option> 
                                    <?php endfor; ?> 
                                    <?php endif; ?>
                                                                    
                                </select>
                            </div>
                            <div class="col-md-2 form-group">
                                <span># of Nights Staying</span>
                                <select name="noofnights" class="form-control">
                                    <?php if($noofnights == null): ?>
                                    <?php for($x = 1; $x <= 30; $x++): ?> {
                                        <option value="<?php echo e($x); ?>"><?php echo e($x); ?></option> 
                                    <?php endfor; ?> 
                                    <?php else: ?>
                                    <option value="<?php echo e($noofnights); ?>" selected><?php echo e($noofnights); ?></option>
                                    <?php for($x = 1; $x <= 30; $x++): ?> {
                                        <option value="<?php echo e($x); ?>"><?php echo e($x); ?></option> 
                                    <?php endfor; ?> 
                                    <?php endif; ?>                                                                      
                                </select>
                            </div>
                             <div class="col-md-1 form-group">
                               <br><button class="btn btn-primary btn-sm" type="submit">Get Result</button>
                            </div>
                         </div>
                     </form>
                    </div>
                 </div>  
  <!-- Bordered striped table -->
  <?php if(Auth::user() != null): ?>
  
    <input type="hidden" value="<?php echo e(Auth::user()->name); ?>" id="name">
    <input type="hidden" value="<?php echo e(Auth::user()->email); ?>" id="email">
    <input type="hidden" value="<?php echo e(Auth::user()->phone); ?>" id="phone">
  
<?php endif; ?>

                    <div class="col-md-offset-1">                       

                        <div class="table-responsive">
                           <?php if(Auth::user() == null): ?>
                            <table class="table datatable-button-html5-image1">                                
                                <thead>                                    
                                    <tr>
                                        <th><strong>Disney Quick Service Dining Plan</strong></th>
                                        <th>Per Day:</th>
                                        <th>Total:</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <?php if($noofadults == null): ?>     
                                        <td>Adult Dining Plan: 0 adult(s)</td>
                                        <?php else: ?>
                                        <td>Adult Dining Plan: <?php echo e($noofadults); ?> adult(s)</td>
                                        <?php endif; ?>

                                        <?php if($quickserviceadultperday == null): ?>
                                        <td>$0.00</td>
                                        <?php else: ?>
                                        <td>$<?php echo e($quickserviceadultperday); ?></td>
                                        <?php endif; ?>

                                       <?php if($quickserviceadulttotal == null): ?>
                                        <td>$0.00</td>
                                        <?php else: ?>
                                        <td>$<?php echo e($quickserviceadulttotal); ?></td>
                                        <?php endif; ?>                                    
                                    </tr>
                                    <tr>
                                        <?php if($noofchildren == null): ?>
                                        <td>Child Dining Plan: 0 children</td>
                                        <?php else: ?>
                                        <td>Child Dining Plan: <?php echo e($noofchildren); ?> children</td>
                                        <?php endif; ?>

                                        <?php if($quickservicechildperday == null): ?>
                                        <td>$0.00</td>
                                        <?php else: ?>
                                        <td>$<?php echo e($quickservicechildperday); ?></td>
                                        <?php endif; ?>

                                        <?php if($quickservicechildtotal == null): ?>
                                        <td>$0.00</td>
                                        <?php else: ?>
                                        <td>$<?php echo e($quickservicechildtotal); ?></td>
                                        <?php endif; ?>                                        
                                        
                                    </tr>
                                    <tr>
                                        <?php if($noofadults == null && $noofchildren == null): ?>
                                        <td>Total Rate: 0 adult(s) + 0 children</td>

                                        <?php elseif($noofadults != null && $noofchildren == null): ?>
                                        <td>Total Rate: <?php echo e($noofadults); ?> adult(s) + 0 children</td>

                                        <?php elseif($noofadults == null && $noofchildren != null): ?>
                                        <td>Total Rate: 0 adult(s) + <?php echo e($noofchildren); ?> children</td>

                                        <?php else: ?>
                                        <td>Total Rate: <?php echo e($noofadults); ?> adult(s) + <?php echo e($noofchildren); ?> children</td>
                                        <?php endif; ?>

                                        <?php if($quickservicetotalperday == null): ?>
                                        <td><strong>$0.00</strong></td>
                                        <?php else: ?>
                                        <td><strong>$<?php echo e($quickservicetotalperday); ?></strong></td>
                                        <?php endif; ?>

                                        <?php if($quickservicetotal == null): ?>
                                        <td><strong>$0.00</strong></td>
                                        <?php else: ?>
                                        <td><strong>$<?php echo e($quickservicetotal); ?></strong></td>
                                        <?php endif; ?> 
                                    </tr>                               
                                </tbody>
                            </table>
                            <?php else: ?>                            
                            <table class="table datatable-button-html5-image">
                                <thead>
                                    <tr>
                                        <th><strong>Disney Quick Service Dining Plan</strong></th>
                                        <th>Per Day:</th>
                                        <th>Total:</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <?php if($noofadults == null): ?>     
                                        <td>Adult Dining Plan: 0 adult(s)</td>
                                        <?php else: ?>
                                        <td>Adult Dining Plan: <?php echo e($noofadults); ?> adult(s)</td>
                                        <?php endif; ?>

                                        <?php if($quickserviceadultperday == null): ?>
                                        <td>$0.00</td>
                                        <?php else: ?>
                                        <td>$<?php echo e($quickserviceadultperday); ?></td>
                                        <?php endif; ?>

                                       <?php if($quickserviceadulttotal == null): ?>
                                        <td>$0.00</td>
                                        <?php else: ?>
                                        <td>$<?php echo e($quickserviceadulttotal); ?></td>
                                        <?php endif; ?>                                    
                                    </tr>
                                    <tr>
                                        <?php if($noofchildren == null): ?>
                                        <td>Child Dining Plan: 0 children</td>
                                        <?php else: ?>
                                        <td>Child Dining Plan: <?php echo e($noofchildren); ?> children</td>
                                        <?php endif; ?>

                                        <?php if($quickservicechildperday == null): ?>
                                        <td>$0.00</td>
                                        <?php else: ?>
                                        <td>$<?php echo e($quickservicechildperday); ?></td>
                                        <?php endif; ?>

                                        <?php if($quickservicechildtotal == null): ?>
                                        <td>$0.00</td>
                                        <?php else: ?>
                                        <td>$<?php echo e($quickservicechildtotal); ?></td>
                                        <?php endif; ?>                                        
                                        
                                    </tr>
                                    <tr>
                                        <?php if($noofadults == null && $noofchildren == null): ?>
                                        <td>Total Rate: 0 adult(s) + 0 children</td>

                                        <?php elseif($noofadults != null && $noofchildren == null): ?>
                                        <td>Total Rate: <?php echo e($noofadults); ?> adult(s) + 0 children</td>

                                        <?php elseif($noofadults == null && $noofchildren != null): ?>
                                        <td>Total Rate: 0 adult(s) + <?php echo e($noofchildren); ?> children</td>

                                        <?php else: ?>
                                        <td>Total Rate: <?php echo e($noofadults); ?> adult(s) + <?php echo e($noofchildren); ?> children</td>
                                        <?php endif; ?>

                                        <?php if($quickservicetotalperday == null): ?>
                                        <td><strong>$0.00</strong></td>
                                        <?php else: ?>
                                        <td><strong>$<?php echo e($quickservicetotalperday); ?></strong></td>
                                        <?php endif; ?>

                                        <?php if($quickservicetotal == null): ?>
                                        <td><strong>$0.00</strong></td>
                                        <?php else: ?>
                                        <td><strong>$<?php echo e($quickservicetotal); ?></strong></td>
                                        <?php endif; ?> 
                                    </tr>                               
                                </tbody>
                            </table>
                            <?php endif; ?>
                        </div>
                    </div>
                    <!-- /bordered striped table -->
                    <br><br>
                    <!-- Bordered striped table -->
                    <div class="col-md-offset-1">                       

                        <div class="table-responsive">
                            <?php if(Auth::user() == null): ?>
                            <table class="table datatable-button-html5-image1">
                                <thead>
                                    <tr>
                                        <th><strong>Disney Dining Plan</strong></th>
                                        <th>Per Day:</th>
                                        <th>Total:</th>
                                    </tr>
                                </thead>
                                <tbody>
                                     <tr>
                                        <?php if($noofadults == null): ?>     
                                        <td>Adult Dining Plan: 0 adult(s)</td>
                                        <?php else: ?>
                                        <td>Adult Dining Plan: <?php echo e($noofadults); ?> adult(s)</td>
                                        <?php endif; ?>

                                        <?php if($disneydiningadultperday == null): ?>
                                        <td>$0.00</td>
                                        <?php else: ?>
                                        <td>$<?php echo e($disneydiningadultperday); ?></td>
                                        <?php endif; ?>

                                       <?php if($disneydiningadulttotal == null): ?>
                                        <td>$0.00</td>
                                        <?php else: ?>
                                        <td>$<?php echo e($disneydiningadulttotal); ?></td>
                                        <?php endif; ?>                                    
                                    </tr>
                                    <tr>
                                        <?php if($noofchildren == null): ?>
                                        <td>Child Dining Plan: 0 children</td>
                                        <?php else: ?>
                                        <td>Child Dining Plan: <?php echo e($noofchildren); ?> children</td>
                                        <?php endif; ?>

                                        <?php if($disneydiningchildperday == null): ?>
                                        <td>$0.00</td>
                                        <?php else: ?>
                                        <td>$<?php echo e($disneydiningchildperday); ?></td>
                                        <?php endif; ?>

                                        <?php if($disneydiningchildtotal == null): ?>
                                        <td>$0.00</td>
                                        <?php else: ?>
                                        <td>$<?php echo e($disneydiningchildtotal); ?></td>
                                        <?php endif; ?>                                        
                                        
                                    </tr>
                                    <tr>
                                        <?php if($noofadults == null && $noofchildren == null): ?>
                                        <td>Total Rate: 0 adult(s) + 0 children</td>

                                        <?php elseif($noofadults != null && $noofchildren == null): ?>
                                        <td>Total Rate: <?php echo e($noofadults); ?> adult(s) + 0 children</td>

                                        <?php elseif($noofadults == null && $noofchildren != null): ?>
                                        <td>Total Rate: 0 adult(s) + <?php echo e($noofchildren); ?> children</td>

                                        <?php else: ?>
                                        <td>Total Rate: <?php echo e($noofadults); ?> adult(s) + <?php echo e($noofchildren); ?> children</td>
                                        <?php endif; ?>

                                        <?php if($disneydiningtotalperday == null): ?>
                                        <td><strong>$0.00</strong></td>
                                        <?php else: ?>
                                        <td><strong>$<?php echo e($disneydiningtotalperday); ?></strong></td>
                                        <?php endif; ?>

                                        <?php if($disneydiningtotal == null): ?>
                                        <td><strong>$0.00</strong></td>
                                        <?php else: ?>
                                        <td><strong>$<?php echo e($disneydiningtotal); ?></strong></td>
                                        <?php endif; ?> 
                                    </tr>                                   
                                </tbody>
                            </table>
                            <?php else: ?>
                            <table class="table datatable-button-html5-image">
                                <thead>
                                    <tr>
                                        <th><strong>Disney Dining Plan</strong></th>
                                        <th>Per Day:</th>
                                        <th>Total:</th>
                                    </tr>
                                </thead>
                                <tbody>
                                     <tr>
                                        <?php if($noofadults == null): ?>     
                                        <td>Adult Dining Plan: 0 adult(s)</td>
                                        <?php else: ?>
                                        <td>Adult Dining Plan: <?php echo e($noofadults); ?> adult(s)</td>
                                        <?php endif; ?>

                                        <?php if($disneydiningadultperday == null): ?>
                                        <td>$0.00</td>
                                        <?php else: ?>
                                        <td>$<?php echo e($disneydiningadultperday); ?></td>
                                        <?php endif; ?>

                                       <?php if($disneydiningadulttotal == null): ?>
                                        <td>$0.00</td>
                                        <?php else: ?>
                                        <td>$<?php echo e($disneydiningadulttotal); ?></td>
                                        <?php endif; ?>                                    
                                    </tr>
                                    <tr>
                                        <?php if($noofchildren == null): ?>
                                        <td>Child Dining Plan: 0 children</td>
                                        <?php else: ?>
                                        <td>Child Dining Plan: <?php echo e($noofchildren); ?> children</td>
                                        <?php endif; ?>

                                        <?php if($disneydiningchildperday == null): ?>
                                        <td>$0.00</td>
                                        <?php else: ?>
                                        <td>$<?php echo e($disneydiningchildperday); ?></td>
                                        <?php endif; ?>

                                        <?php if($disneydiningchildtotal == null): ?>
                                        <td>$0.00</td>
                                        <?php else: ?>
                                        <td>$<?php echo e($disneydiningchildtotal); ?></td>
                                        <?php endif; ?>                                        
                                        
                                    </tr>
                                    <tr>
                                        <?php if($noofadults == null && $noofchildren == null): ?>
                                        <td>Total Rate: 0 adult(s) + 0 children</td>

                                        <?php elseif($noofadults != null && $noofchildren == null): ?>
                                        <td>Total Rate: <?php echo e($noofadults); ?> adult(s) + 0 children</td>

                                        <?php elseif($noofadults == null && $noofchildren != null): ?>
                                        <td>Total Rate: 0 adult(s) + <?php echo e($noofchildren); ?> children</td>

                                        <?php else: ?>
                                        <td>Total Rate: <?php echo e($noofadults); ?> adult(s) + <?php echo e($noofchildren); ?> children</td>
                                        <?php endif; ?>

                                        <?php if($disneydiningtotalperday == null): ?>
                                        <td><strong>$0.00</strong></td>
                                        <?php else: ?>
                                        <td><strong>$<?php echo e($disneydiningtotalperday); ?></strong></td>
                                        <?php endif; ?>

                                        <?php if($disneydiningtotal == null): ?>
                                        <td><strong>$0.00</strong></td>
                                        <?php else: ?>
                                        <td><strong>$<?php echo e($disneydiningtotal); ?></strong></td>
                                        <?php endif; ?> 
                                    </tr>                                   
                                </tbody>
                            </table>
                            <?php endif; ?>
                        </div>
                    </div>
                    <!-- /bordered striped table -->
                    <br><br>
                    <!-- Bordered striped table -->
                    <div class="col-md-offset-1">                      

                        <div class="table-responsive">
                            <?php if(Auth::user() == null): ?>
                            <table class="table datatable-button-html5-image1">
                                <thead>
                                    <tr>
                                        <th><strong>Disney Deluxe Dining Plan</strong></th>
                                        <th>Per Day:</th>
                                        <th>Total:</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <?php if($noofadults == null): ?>     
                                        <td>Adult Dining Plan: 0 adult(s)</td>
                                        <?php else: ?>
                                        <td>Adult Dining Plan: <?php echo e($noofadults); ?> adult(s)</td>
                                        <?php endif; ?>

                                        <?php if($deluxediningadultperday == null): ?>
                                        <td>$0.00</td>
                                        <?php else: ?>
                                        <td>$<?php echo e($deluxediningadultperday); ?></td>
                                        <?php endif; ?>

                                       <?php if($deluxediningadulttotal == null): ?>
                                        <td>$0.00</td>
                                        <?php else: ?>
                                        <td>$<?php echo e($deluxediningadulttotal); ?></td>
                                        <?php endif; ?>                                    
                                    </tr>
                                    <tr>
                                        <?php if($noofchildren == null): ?>
                                        <td>Child Dining Plan: 0 children</td>
                                        <?php else: ?>
                                        <td>Child Dining Plan: <?php echo e($noofchildren); ?> children</td>
                                        <?php endif; ?>

                                        <?php if($deluxediningchildperday == null): ?>
                                        <td>$0.00</td>
                                        <?php else: ?>
                                        <td>$<?php echo e($deluxediningchildperday); ?></td>
                                        <?php endif; ?>

                                        <?php if($deluxediningchildtotal == null): ?>
                                        <td>$0.00</td>
                                        <?php else: ?>
                                        <td>$<?php echo e($deluxediningchildtotal); ?></td>
                                        <?php endif; ?>                                        
                                        
                                    </tr>
                                    <tr>
                                        <?php if($noofadults == null && $noofchildren == null): ?>
                                        <td>Total Rate: 0 adult(s) + 0 children</td>

                                        <?php elseif($noofadults != null && $noofchildren == null): ?>
                                        <td>Total Rate: <?php echo e($noofadults); ?> adult(s) + 0 children</td>

                                        <?php elseif($noofadults == null && $noofchildren != null): ?>
                                        <td>Total Rate: 0 adult(s) + <?php echo e($noofchildren); ?> children</td>

                                        <?php else: ?>
                                        <td>Total Rate: <?php echo e($noofadults); ?> adult(s) + <?php echo e($noofchildren); ?> children</td>
                                        <?php endif; ?>

                                        <?php if($deluxediningtotalperday == null): ?>
                                        <td><strong>$0.00</strong></td>
                                        <?php else: ?>
                                        <td><strong>$<?php echo e($deluxediningtotalperday); ?></strong></td>
                                        <?php endif; ?>

                                        <?php if($deluxediningtotal == null): ?>
                                        <td><strong>$0.00</strong></td>
                                        <?php else: ?>
                                        <td><strong>$<?php echo e($deluxediningtotal); ?></strong></td>
                                        <?php endif; ?> 
                                    </tr>                                  
                                </tbody>
                            </table>
                            <?php else: ?>
                            <table class="table datatable-button-html5-image">
                                <thead>
                                    <tr>
                                        <th><strong>Disney Deluxe Dining Plan</strong></th>
                                        <th>Per Day:</th>
                                        <th>Total:</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <?php if($noofadults == null): ?>     
                                        <td>Adult Dining Plan: 0 adult(s)</td>
                                        <?php else: ?>
                                        <td>Adult Dining Plan: <?php echo e($noofadults); ?> adult(s)</td>
                                        <?php endif; ?>

                                        <?php if($deluxediningadultperday == null): ?>
                                        <td>$0.00</td>
                                        <?php else: ?>
                                        <td>$<?php echo e($deluxediningadultperday); ?></td>
                                        <?php endif; ?>

                                       <?php if($deluxediningadulttotal == null): ?>
                                        <td>$0.00</td>
                                        <?php else: ?>
                                        <td>$<?php echo e($deluxediningadulttotal); ?></td>
                                        <?php endif; ?>                                    
                                    </tr>
                                    <tr>
                                        <?php if($noofchildren == null): ?>
                                        <td>Child Dining Plan: 0 children</td>
                                        <?php else: ?>
                                        <td>Child Dining Plan: <?php echo e($noofchildren); ?> children</td>
                                        <?php endif; ?>

                                        <?php if($deluxediningchildperday == null): ?>
                                        <td>$0.00</td>
                                        <?php else: ?>
                                        <td>$<?php echo e($deluxediningchildperday); ?></td>
                                        <?php endif; ?>

                                        <?php if($deluxediningchildtotal == null): ?>
                                        <td>$0.00</td>
                                        <?php else: ?>
                                        <td>$<?php echo e($deluxediningchildtotal); ?></td>
                                        <?php endif; ?>                                        
                                        
                                    </tr>
                                    <tr>
                                        <?php if($noofadults == null && $noofchildren == null): ?>
                                        <td>Total Rate: 0 adult(s) + 0 children</td>

                                        <?php elseif($noofadults != null && $noofchildren == null): ?>
                                        <td>Total Rate: <?php echo e($noofadults); ?> adult(s) + 0 children</td>

                                        <?php elseif($noofadults == null && $noofchildren != null): ?>
                                        <td>Total Rate: 0 adult(s) + <?php echo e($noofchildren); ?> children</td>

                                        <?php else: ?>
                                        <td>Total Rate: <?php echo e($noofadults); ?> adult(s) + <?php echo e($noofchildren); ?> children</td>
                                        <?php endif; ?>

                                        <?php if($deluxediningtotalperday == null): ?>
                                        <td><strong>$0.00</strong></td>
                                        <?php else: ?>
                                        <td><strong>$<?php echo e($deluxediningtotalperday); ?></strong></td>
                                        <?php endif; ?>

                                        <?php if($deluxediningtotal == null): ?>
                                        <td><strong>$0.00</strong></td>
                                        <?php else: ?>
                                        <td><strong>$<?php echo e($deluxediningtotal); ?></strong></td>
                                        <?php endif; ?> 
                                    </tr>                                  
                                </tbody>
                            </table>
                            <?php endif; ?>
                        </div>
                    </div>
                    <!-- /bordered striped table -->
                    <center><a class="btn btn-lg btn-primary" target="_blank" href="http://mandmmagicaladventures.com/free-quote.html" >Click for a Free Disney Vacation Quote</a></center>
 <!-- Footer --><br>
                    <div class="footer text-muted">
                        &nbsp &copy; 2021 Powered by <a href="https://www.nodlays.com/" target="_blank">Nodlays</a>
                    </div>
                    <!-- /footer -->

                </div>
                <!-- /content area -->

            </div>
            <!-- /main content -->

        </div>
        <!-- /page content -->

    </div>
    <!-- /page container -->
<script type="text/javascript">
    
    $(document).ready(function(){

        $('#DataTables_Table_0_filter').hide();
        $('#DataTables_Table_0_info').hide();
        $('#DataTables_Table_0_paginate').hide();
        $('#DataTables_Table_0_length').hide();
        

        $('#DataTables_Table_1_filter').hide();
        $('#DataTables_Table_1_info').hide();
        $('#DataTables_Table_1_paginate').hide();
        $('#DataTables_Table_1_length').hide();
        

        $('#DataTables_Table_2_filter').hide();
        $('#DataTables_Table_2_info').hide();
        $('#DataTables_Table_2_paginate').hide();
        $('#DataTables_Table_2_length').hide();

    });

    
</script>

</body>
</html>









             