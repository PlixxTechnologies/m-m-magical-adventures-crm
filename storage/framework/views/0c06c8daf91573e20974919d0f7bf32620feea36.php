<?php $__env->startSection('content'); ?>
<!-- Datatables JS FILES -->

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.print.min.js"></script>

<!-- /Datatables JS FILES -->
<style>
.red{
    color: red;
}

.select2-selection--multiple .select2-selection__choice {
    background-color: #eee;
    color: black;
}
</style>
<!-- Page header -->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="<?php echo e(url('/home')); ?>">
                    <em class="fa fa-home"></em>
                </a></li>
                <li class="active">Edit Trip</li>
            </ol>
        </div><!--/.row-->
    <div class="container-fluid"> 
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Edit Trip</h1>
            </div>
        </div>  <!-- /page header -->
   </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">

                        <form action="<?php echo e(url('trip/edit').'/'.$trip->id); ?>" method="post" >
                            <?php echo csrf_field(); ?>


							<input type="hidden" class="form-control" name="customer_id" value="<?php echo e($trip->customer_id); ?>">


                            <?php if($authuser->role == 1): ?>
                            <div class="form-group">
                                 <label><span class="red">*</span>Agent Name:</label>
                                    <select name="user_id" class="form-control" required>
                                        <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($user->id); ?>" <?php if($user->id == $trip->user_id): ?> selected <?php endif; ?>><?php echo e($user->name); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                                   
                                    </select>
                                </div>
                            <?php else: ?>
                                <div class="form-group">
                                <label>Agent Name:</label>
                                <input type="text"  class="form-control" value="<?php echo e($authuser->name); ?>" readonly>
                                <input type="hidden" class="form-control" name="user_id" value="<?php echo e($authuser->id); ?>">
                                </div>
                            <?php endif; ?>
                            
                            <div class="form-group">
                                <label>Customer Name:</label>
                                <input type="text" value="<?php echo e($name); ?>" class="form-control" readonly>
                            </div>
                           
                            <div class="form-group">
                                <label><span class="red">*</span>Reservation Number:</label>
                                <input type="text" name="reservation_number" value=" <?php echo e($trip->reservation_number); ?>" class="form-control" required>
                            </div>

                           <div class="form-group">
                                <label><span class="red">*</span>Booking Date:</label>
                                <input id="bookingDate" onfocus="DateType()" type="datetime" name="booking_date" value=" <?php echo e($trip->booking_date); ?>" class="form-control" class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label><span class="red">*</span>Check In Date:</label>
                                <input type="date" name="checkin_date" class="form-control" value="<?php echo e($trip->checkin_date); ?>" required>
                            </div>
                            <div class="form-group">
                                <label>Check Out Date:</label>
                                <input type="date" name="checkout_date" class="form-control" value="<?php echo e($trip->checkout_date); ?>">
                                <span class="red"><b>Note: </b></span>If a check-out date is not selected then system will automatically save the <b>check-in date</b> as check-out date.
                            </div>
                            <div class="form-group">
                                <label><span class="red">*</span>Trip Status:</label>
                                <select name="trip_status" class="form-control" required>
                                    <option value="">Choose One....</option>
                                    <option value="Inquiry" <?php if($trip->trip_status == "Inquiry"): ?> selected <?php endif; ?>>Inquiry</option>
                                    <option value="Quote Sent" <?php if($trip->trip_status == "Quote Sent"): ?> selected <?php endif; ?>>Quote Sent</option>
                                    <option value="Booked Deposit" <?php if($trip->trip_status == "Booked Deposit"): ?> selected <?php endif; ?>>Booked Deposit</option>
                                    <option value="On Hold" <?php if($trip->trip_status == "On Hold"): ?> selected <?php endif; ?>>On Hold</option>
                                    <option value="Paid in Full" <?php if($trip->trip_status == "Paid in Full"): ?> selected <?php endif; ?>>Paid in Full</option>
                                    <option value="Complete – Commission Paid" <?php if($trip->trip_status == "Complete – Commission Paid"): ?> selected <?php endif; ?>>Complete – Commission Paid</option>
                                    <option value="Canceled" <?php if($trip->trip_status == "Canceled"): ?> selected <?php endif; ?>>Canceled</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label><span class="red">*</span>Total Sale:</label>
                                <input type="number" step="any" name="total_sale" value="<?php echo e($trip->total_sale); ?>" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label>Total Commission:</label>
                                 <input type="number" step="any" name="commission" id="commission" onfocusout="mycommission()" value="<?php echo e($trip->commission); ?>" class="form-control">
                                <input type="hidden" id="user_commission" value="<?php echo e($authuser->commission); ?>">
                            </div>
                            <div class="form-group">
                                <label>Expected Commission:</label>
                                <input type="number" step="any" name="expected_commission" id="expected_commission" value="<?php echo e($trip->expected_commission); ?>" class="form-control">
                            </div>
                            <?php if($flag == "false" && $trip->travel_with != null && count($guests) == 0): ?>
                            <div class="form-group">
                                <label>Travel With:</label>
                                <input type="text" name="travel_with" value="<?php echo e($trip->travel_with); ?>" class="form-control">
                            </div>
                            <?php elseif($flag == "false" && $trip->travel_with != null && count($guests) > 0): ?>
                            <div class="form-group form-group-material">
                                        <label class="control-label has-margin animate is-visible">Travel With:</label>
                                        <!-- <select name="travel_with[]" multiple="" class="select select2-hidden-accessible" tabindex="-1" aria-hidden="true"> -->

                                     <select class="form-control selectpicker" name="travel_with[]" multiple="" data-live-search="true" tabindex="-1" required="">
                                            <?php $__currentLoopData = $guests; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $guest): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option 
                                                value="<?php echo e($guest->guest_first_name.' '.$guest->guest_last_name); ?>" selected="selected">
                                                    <?php echo e($guest->guest_first_name.' '.$guest->guest_last_name); ?>

                                                </option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
                                        </select>
                                    </div>
                            <?php else: ?>
                            <?php $x=0; $count = 0?>
                             <div class="form-group form-group-material">
                                        <label class="control-label has-margin animate is-visible">Travel With:</label>
                                      <!--   <select name="travel_with[]" multiple="" class="select select2-hidden-accessible" tabindex="-1" aria-hidden="true"> -->
                                      <select class="form-control selectpicker" multiple="" name="travel_with[]" data-live-search="true" tabindex="-1" required="">
                                            <?php $__currentLoopData = $guests; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $guest): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php $x=0; ?>
                                               <?php $__currentLoopData = $travelWith; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $travel): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                               <?php if($travel == $guest->guest_first_name.' '.$guest->guest_last_name): ?>
                                              
                                               <?php $x=1; ?>
                                                <?php endif; ?>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 

                                                <?php if($x==1): ?> 
                                                <option 
                                                value="<?php echo e($guest->guest_first_name.' '.$guest->guest_last_name); ?>" selected="selected">
                                                   <?php echo e($guest->guest_first_name.' '.$guest->guest_last_name); ?>

                                                </option>
                                               <?php else: ?>
                                               <option 
                                                value="<?php echo e($guest->guest_first_name.' '.$guest->guest_last_name); ?>">
                                                   <?php echo e($guest->guest_first_name.' '.$guest->guest_last_name); ?>

                                                </option>
                                                <?php endif; ?>
                                               
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 

                                        </select>
                             </div>
                             <?php endif; ?>
                            <div class="form-group">
                                <label>Special Occasions or Requests:</label>
                                <input type="text" name="special_request" value="<?php echo e($trip->special_request); ?>" class="form-control">
                            </div> 
                            <div class="form-group">
                                <label><span class="red">*</span>Destinations:</label>
                                <select name="destination" class="form-control" id="disney_select" onchange="switchDivs()" >
                                    <option value="">Choose One....</option>
                                   <!--  <option value="Walt Disney World" <?php if($trip->destination == "Walt Disney World"): ?> selected <?php endif; ?>>Walt Disney World</option>
                                    <option value="Disneyland Resort" <?php if($trip->destination == "Disneyland Resort"): ?> selected <?php endif; ?>>Disneyland Resort </option>
                                    <option value="Disney Cruise Line" <?php if($trip->destination == "Disney Cruise Line"): ?> selected <?php endif; ?>>Disney Cruise Line</option>
                                    <option value="Universal Studios Florida" <?php if($trip->destination == "Universal Studios Florida"): ?> selected <?php endif; ?>>Universal Studios Florida</option> -->
                                    <?php $__currentLoopData = $destinations; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $destination): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
     <option value="<?php echo e($destination->destination_name); ?>" <?php if($trip->destination ==  $destination->destination_name ): ?> selected <?php endif; ?> ><?php echo e($destination->destination_name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>    



                                <script type="text/javascript">
                       function yesnoCheck(that) {
                                if (that.value == "other") {
                                    document.getElementById("ifYes").style.display = "block";
                                } else {
                                    document.getElementById("ifYes").style.display = "none";
                                }
                            }
                 </script>

                 <div class="form-group ">
                 <label><span class="red">*</span>Referral:</label>
                <select onchange="yesnoCheck(this);" name="Referal" value="Referal" class="form-control" required>
                                    <?php if($trip->Referal != null): ?>{
                                        <option value="<?php echo e($trip->Referal); ?>" selected=""><?php echo e($trip->Referal); ?></option>
                                        <option disabled>--Select--</option>
                                        <option value="Facebook">Facebook</option>
                                    <option value="Instagram">Instagram</option>
                                    <option value="Google">Google</option>
                                    <option value="Friend">Friend</option>
                                    <option value="Family">Family</option>
                                    <option value="Social Media Ad">Social Media Ad</option>
                                    <option value="Disney/Universal FAM">Disney/Universal FAM</option>
                                    <option value="other">Other</option>
                                    }
                                    <?php else: ?>{
                                        <option disabled selected>--Select--</option>
                                        <option value="Facebook">Facebook</option>
                                    <option value="Instagram">Instagram</option>
                                    <option value="Google">Google</option>
                                    <option value="Friend">Friend</option>
                                    <option value="Family">Family</option>
                                    <option value="Social Media Ad">Social Media Ad</option>
                                    <option value="Disney/Universal FAM">Disney/Universal FAM</option>
                                    <option value="other">Other</option>
                                    }
                                    <?php endif; ?>
                                </select><br>
                                <div id="ifYes" style="display: none;">
                                   <input placeholder="Type Referral" type="text" id="other" name="other" class="form-control"/>
                                </div>
                            </div>
    



                            <div id="first">
                            <div class="form-group" id = "worldHotels">
                                <label>Walt Disney World Hotel Name:</label>
                                <select name="disneyworld_hotel_name" class="form-control">
                                    <option value="">Choose One....</option>
                                    <?php $__currentLoopData = $valueResorts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                          <option value="<?php echo e($value->hotel_name); ?>" <?php if($trip->disneyworld_hotel_name ==  $value->hotel_name ): ?> selected <?php endif; ?>><?php echo e($value->hotel_name); ?></option>
                                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                          <option disabled><b>Moderate Resorts</b></option>
                                          <?php $__currentLoopData = $moderateResorts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                          <option value="<?php echo e($value->hotel_name); ?>"<?php if($trip->disneyworld_hotel_name ==  $value->hotel_name ): ?> selected <?php endif; ?>><?php echo e($value->hotel_name); ?></option>
                                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                          <option disabled><b>Deluxe Resorts </b></option>
                                          <?php $__currentLoopData = $deluxeResorts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                          <option value="<?php echo e($value->hotel_name); ?>"<?php if($trip->disneyworld_hotel_name ==  $value->hotel_name ): ?> selected <?php endif; ?>><?php echo e($value->hotel_name); ?></option>
                                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                          <option disabled><b>Deluxe Villas </b></option>
                                          <?php $__currentLoopData = $deluxeVillas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                          <option value="<?php echo e($value->hotel_name); ?>"<?php if($trip->disneyworld_hotel_name ==  $value->hotel_name ): ?> selected <?php endif; ?>><?php echo e($value->hotel_name); ?></option>
                                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>                                
                            <div class="form-group">
                                    <label>Dining Plan:</label>
                                    <select name="disney_dining_plan" class="form-control">
                                        <option <?php if($trip->disney_dining_plan == "None"): ?> selected <?php endif; ?> value="None">None</option>
                                        <option <?php if($trip->disney_dining_plan == "Quick Service Dining"): ?> selected <?php endif; ?> value="Quick Service Dining">Quick Service Dining</option>
                                        <option <?php if($trip->disney_dining_plan == "Disney Dining Plan"): ?> selected <?php endif; ?> value="Disney Dining Plan">Disney Dining Plan</option>
                                        <option <?php if($trip->disney_dining_plan == "Deluxe Dining Plan"): ?> selected <?php endif; ?> value="Deluxe Dining Plan">Deluxe Dining Plan</option>
                                    </select>
                            </div>
                            <div class="form-group">
                                <label>Memory Maker:</label>
                                <select name="memory_maker" class="form-control">
                                    <option value="">Choose One....</option>
                                    <option value="Yes" <?php if($trip->memory_maker == "Yes" || $trip->memory_maker == "YES" || $trip->memory_maker == "yes"): ?> selected <?php endif; ?>>Yes</option>
                                    <option value="No" <?php if($trip->memory_maker == "No" || $trip->memory_maker == "no" || $trip->memory_maker == "NO"): ?> selected <?php endif; ?>>No</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Magical Express:</label>
                                <select name="magical_express" class="form-control">
                                    <option value="">Choose One....</option>
                                    <option value="Yes" <?php if($trip->magical_express == "Yes" || $trip->magical_express == "yes" || $trip->magical_express == "YES"): ?> selected <?php endif; ?>>Yes</option>
                                    <option value="No" <?php if($trip->magical_express == "No" || $trip->magical_express == "no" || $trip->magical_express == "NO"): ?> selected <?php endif; ?>>No</option>
                                </select>
                            </div>
                            
                            <div class="form-group">
                                <label>Magic Band Color Selection:</label>
                                <input type="text" name="magic_band_color" value="<?php echo e($trip->magic_band_color); ?>" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>My Disney Experience User Name:</label>
                                <input type="text" name="disney_experience_username" value="<?php echo e($trip->customer->disney_experience_username); ?>" class="form-control" readonly>
                            </div>
                            <div class="form-group">
                                <label>My Disney Experience Password:</label>
                                <input type="text" name="disney_experience_password" value="<?php echo e($trip->customer->disney_experience_password); ?>" class="form-control" readonly>
                            </div>    
                        </div>

                            <div id="second">                            
                            <div class="form-group" id ="resortHotels">
                                    <label>Disneyland Hotel Name:</label>
                                    <select name="disneyresort_hotel_name" class="form-control">
                                        <option value="">Choose One....</option>
                                         <?php $__currentLoopData = $disneylandResortHotels; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($value->hotel_name); ?>"<?php if($trip->disneyresort_hotel_name == $value->hotel_name): ?> selected <?php endif; ?>><?php echo e($value->hotel_name); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                        
                                    </select>
                                </div>
                                <div id="goodneighbour">
                                    <div class="form-group">
                                        <label>Good Neighbor Hotel</label>
                                        <input type="text" name="good_neighbor_hotel" value="<?php echo e($trip->good_neighbor_hotel); ?>" class="form-control">
                                    </div>
                                </div>                                
                            </div>

                         <div id="third">
                             <div class="form-group">
                                 <label>Ship Name:</label>
                                 <select name="ship_name2" class="form-control">
                                     <option value="">Choose One....</option>

                                     <?php $__currentLoopData = $disneyCruiseLineShips; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                     <option value="<?php echo e($value->hotel_name); ?>" <?php if($trip->ship_name == $value->hotel_name): ?> selected <?php endif; ?>><?php echo e($value->hotel_name); ?></option>
                                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                   
                                 </select>
                             </div>
                            <div class="form-group">
                                <label>Castaway Member:</label>
                                <input type="text" name="castaway_member" value=" <?php echo e($trip->castaway_member); ?>" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Bus Transportation:</label>
                               <div class="checkbox">
                                  <label><input type="checkbox" name="bus_transportation" <?php if($trip->bus_transportation == "Yes" || $trip->bus_transportation == "YES" || $trip->bus_transportation == "yes"): ?> checked <?php endif; ?> value="Yes">Yes</label>
                                </div>
                                <div class="checkbox">
                                  <label><input type="checkbox" name="bus_transportation"  <?php if($trip->bus_transportation == "No" || $trip->bus_transportation == "NO" || $trip->bus_transportation == "no"): ?> checked <?php endif; ?> value="No">No</label>
                                </div>
                            </div>
                            </div>

                                <div id="fourth">
                                <!-- <div id="universalorlandoresorthotels"> -->
                                    <div class="form-group">
                                        <label>Universal Orlando Resort Hotels:</label>
                                        <select name="universal_orlando_resort_hotel" class="form-control" id="universal_orlando_resort_hotel" onchange="switchOrlandoDiv()">
                                            <option value="">Choose One....</option>
                                            
                                           <?php $__currentLoopData = $universalStudiosHotels; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($value->hotel_name); ?>"  <?php if($trip->universal_orlando_resort_hotel == $value->hotel_name): ?> selected <?php endif; ?>><?php echo e($value->hotel_name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                    </div>
                                <!-- </div> -->

                                <div id="PartnersHotel">
                                    <div class="form-group">
                                        <label>Partners Hotel</label>
                                        <input type="text" name="partners_hotel" value="<?php echo e($trip->partners_hotel); ?>" class="form-control">
                                    </div>
                                </div>
                            </div>

                            <div id="ticketdiv">
                            <div class="form-group">
                                <label>Ticket Only Sale:</label>
                                <select name="ticket_type" class="form-control">
                                    <option value="">Choose One....</option>
                                    <option value="Partner Hotels" <?php if($trip->ticket_type == "Park Ticket Only"): ?> selected <?php endif; ?>>Park Ticket Only</option>
                                    <option value="Special Event Only" <?php if($trip->ticket_type == "Special Event Only"): ?> selected <?php endif; ?>>Special Event Only</option>
                                </select>
                            </div>
                        </div>
<!--start from here 2 remains -->
                         <div id="fifth">
                             <div class="form-group">
                                 <label>Ship Name:</label>
                                 <select name="ship_name1" class="form-control">
                                     <option value="">Choose One....</option>
                                     <optgroup label="XL Class">
                                    <?php $__currentLoopData = $xl; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($value->hotel_name); ?>" <?php if($trip->ship_name == $value->hotel_name): ?> selected <?php endif; ?> ><?php echo e($value->hotel_name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </optgroup>
                                    <optgroup label="Vista Class">
                                    <?php $__currentLoopData = $vista; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($value->hotel_name); ?>" <?php if($trip->ship_name == $value->hotel_name): ?> selected <?php endif; ?>><?php echo e($value->hotel_name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </optgroup>
                                    <optgroup label="Sunshine Class">
                                    <?php $__currentLoopData = $sunshine; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($value->hotel_name); ?>" <?php if($trip->ship_name == $value->hotel_name): ?> selected <?php endif; ?>><?php echo e($value->hotel_name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </optgroup>
                                    <optgroup label="Dream Class">
                                      <?php $__currentLoopData = $dream; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($value->hotel_name); ?>" <?php if($trip->ship_name == $value->hotel_name): ?> selected <?php endif; ?>><?php echo e($value->hotel_name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </optgroup>
                                    <optgroup label="Splendor Class">
                                      <?php $__currentLoopData = $splendor; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($value->hotel_name); ?>" <?php if($trip->ship_name == $value->hotel_name): ?> selected <?php endif; ?>><?php echo e($value->hotel_name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </optgroup>
                                     <optgroup label="Conquest Class">
                                     <?php $__currentLoopData = $conquest; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($value->hotel_name); ?>" <?php if($trip->ship_name == $value->hotel_name): ?> selected <?php endif; ?>><?php echo e($value->hotel_name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </optgroup>
                                     <optgroup label="Spirit Class">
                                    <?php $__currentLoopData = $spirit; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($value->hotel_name); ?>" <?php if($trip->ship_name == $value->hotel_name): ?> selected <?php endif; ?>><?php echo e($value->hotel_name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </optgroup>
                                    <optgroup label="Fantasy Class">
                                    <?php $__currentLoopData = $fantasy; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($value->hotel_name); ?>" <?php if($trip->ship_name == $value->hotel_name): ?> selected <?php endif; ?>><?php echo e($value->hotel_name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </optgroup>
                                 </select>
                             </div>
                        
                        </div>

                         <div id="sixth">
                             <div class="form-group">
                                 <label>Ship Name:</label>
                                 <select name="ship_name" class="form-control">
                                     <option value="">Choose One....</option>
                                     <optgroup label="Oasis Class">
                                     <?php $__currentLoopData = $oasis; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($value->hotel_name); ?>" <?php if($trip->ship_name == $value->hotel_name): ?> selected <?php endif; ?>><?php echo e($value->hotel_name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </optgroup>
                                    <optgroup label="Quantum Class">
                                     <?php $__currentLoopData = $quantum; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($value->hotel_name); ?>" <?php if($trip->ship_name == $value->hotel_name): ?> selected <?php endif; ?>><?php echo e($value->hotel_name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </optgroup>
                                   <optgroup label="Freedom Class">
                                   <?php $__currentLoopData = $freedom; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($value->hotel_name); ?>" <?php if($trip->ship_name == $value->hotel_name): ?> selected <?php endif; ?>><?php echo e($value->hotel_name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </optgroup>
                                   <optgroup label="Voyager Class">
                                    <?php $__currentLoopData = $voyager; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($value->hotel_name); ?>" <?php if($trip->ship_name == $value->hotel_name): ?> selected <?php endif; ?>><?php echo e($value->hotel_name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </optgroup>
                                     <optgroup label="Radiance Class">
                                      <?php $__currentLoopData = $radiance; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($value->hotel_name); ?>" <?php if($trip->ship_name == $value->hotel_name): ?> selected <?php endif; ?>><?php echo e($value->hotel_name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </optgroup>
                                    <optgroup label="Vision Class">
                                     <?php $__currentLoopData = $vision; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($value->hotel_name); ?>" <?php if($trip->ship_name == $value->hotel_name): ?> selected <?php endif; ?>><?php echo e($value->hotel_name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </optgroup>
                                    <optgroup label="Other">
                                    <?php $__currentLoopData = $other; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($value->hotel_name); ?>" <?php if($trip->ship_name == $value->hotel_name): ?> selected <?php endif; ?>><?php echo e($value->hotel_name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </optgroup>
                                 </select>
                             </div>
                        
                        </div>

                            <div class="form-group">
                                    <label>Insurance:</label>
                                    <select name="insurance" class="form-control">
                                        <option value="">Choose One....</option>
                                        <option <?php if($trip->insurance == "YES" || $trip->insurance == "Yes" || $trip->insurance == "yes"): ?> selected <?php endif; ?> value="Yes">Yes</option>
                                        <option <?php if($trip->insurance == "No" || $trip->insurance == "no" || $trip->insurance == "NO"): ?> selected <?php endif; ?> value="No">No</option>
                                    </select>
                            </div>
                            <div class="form-group">
                                <label>Notes:</label>
                                <textarea name="notes" class="form-control" rows="4"><?php echo e($trip->notes); ?></textarea>
                            </div>
                            <button type="submit" class="btn btn-primary pull-right">Update Trip</button>
                        </form>
                    </div>
                </div>
            </div>
        </div><!--/.row-->
    </div>	<!--/.main-->
</div>

<script type="text/javascript">

if($("#bookingDate").val() == " "){
    $("#bookingDate").val('1970-01-01');
}

function DateType(){
     $("#bookingDate").attr('type', 'date');
}

    function mycommission() {    
    var x = $('#commission').val();        
    var usercommission = $('#user_commission').val();

        if(x != "")
            $('#expected_commission').val(Math.round(parseFloat(x) * ((parseFloat(usercommission)/100) * 100)) / 100);
}

/*
$( document ).ready(function() {
    var selectedItem=document.getElementById('disney_select');

    if(selectedItem.value=="Walt Disney World")
    {
        document.getElementById('first').style.display='block';
        document.getElementById('worldHotels').style.display='block';
        document.getElementById('resortHotels').style.display='none';
        document.getElementById('second').style.display='none';
        document.getElementById('third').style.display='none';
        document.getElementById('fourth').style.display='none';
        document.getElementById('ticketdiv').style.display='block';
    }
    else if(selectedItem.value=="Disneyland Resort")
    {
        document.getElementById('second').style.display='block';

        document.getElementById('worldHotels').style.display='none';
        document.getElementById('resortHotels').style.display='block';
        document.getElementById('first').style.display='none';
        document.getElementById('third').style.display='none';
        document.getElementById('fourth').style.display='none';
        document.getElementById('ticketdiv').style.display='block';

    }
    else if(selectedItem.value=='Disney Cruise Line')
    {
        document.getElementById('first').style.display='none';
        document.getElementById('worldHotels').style.display='none';
        document.getElementById('resortHotels').style.display='none';
        document.getElementById('third').style.display='block';
        document.getElementById('second').style.display='none';
        document.getElementById('fourth').style.display='none';
         document.getElementById('ticketdiv').style.display='none';

    }
    else if(selectedItem.value=='Universal Studios Florida')
    {
        document.getElementById('first').style.display='none';
        document.getElementById('worldHotels').style.display='none';
        document.getElementById('resortHotels').style.display='none';
        document.getElementById('third').style.display='none';
        document.getElementById('second').style.display='none';
        document.getElementById('fourth').style.display='block';
        document.getElementById('ticketdiv').style.display='block';

    }
    else
    {
        document.getElementById('first').style.display='none';
        document.getElementById('worldHotels').style.display='none';
        document.getElementById('resortHotels').style.display='none';
        document.getElementById('second').style.display='none';
        document.getElementById('third').style.display='none';
        document.getElementById('fourth').style.display='none';
        document.getElementById('ticketdiv').style.display='none';
    }
});


function switchDivs()
{
    var selectedItem=document.getElementById('disney_select');

    if(selectedItem.value=="Walt Disney World")
    {
        document.getElementById('first').style.display='block';
        document.getElementById('worldHotels').style.display='block';
        document.getElementById('resortHotels').style.display='none';
        document.getElementById('second').style.display='none';
        document.getElementById('third').style.display='none';
        document.getElementById('fourth').style.display='none';
        document.getElementById('ticketdiv').style.display='block';
    }
    else if(selectedItem.value=="Disneyland Resort")
    {
        document.getElementById('second').style.display='block';

        document.getElementById('worldHotels').style.display='none';
        document.getElementById('resortHotels').style.display='block';
        document.getElementById('first').style.display='none';
        document.getElementById('third').style.display='none';
        document.getElementById('fourth').style.display='none';
        document.getElementById('ticketdiv').style.display='block';

    }
    else if(selectedItem.value=='Disney Cruise Line')
    {
        document.getElementById('first').style.display='none';
        document.getElementById('worldHotels').style.display='none';
        document.getElementById('resortHotels').style.display='none';
        document.getElementById('third').style.display='block';
        document.getElementById('second').style.display='none';
        document.getElementById('fourth').style.display='none';
         document.getElementById('ticketdiv').style.display='none';

    }
    else if(selectedItem.value=='Universal Studios Florida')
    {
        document.getElementById('first').style.display='none';
        document.getElementById('worldHotels').style.display='none';
        document.getElementById('resortHotels').style.display='none';
        document.getElementById('third').style.display='none';
        document.getElementById('second').style.display='none';
        document.getElementById('fourth').style.display='block';
        document.getElementById('ticketdiv').style.display='block';

    }
    else
    {
        document.getElementById('first').style.display='none';
        document.getElementById('worldHotels').style.display='none';
        document.getElementById('resortHotels').style.display='none';
        document.getElementById('second').style.display='none';
        document.getElementById('third').style.display='none';
        document.getElementById('fourth').style.display='none';
        document.getElementById('ticketdiv').style.display='none';
    }
}*/


$( document ).ready(function() {
   var selectedItem=document.getElementById('disney_select');

    if(selectedItem.value=="Walt Disney World")
    {
        document.getElementById('first').style.display='block';
        document.getElementById('worldHotels').style.display='block';
        document.getElementById('resortHotels').style.display='none';
        document.getElementById('second').style.display='none';
        document.getElementById('third').style.display='none';
        document.getElementById('fourth').style.display='none';
        document.getElementById('ticketdiv').style.display='block';
        document.getElementById('sixth').style.display='none';
        document.getElementById('fifth').style.display='none';
    }
    else if(selectedItem.value=="Disneyland Resort")
    {
        document.getElementById('second').style.display='block';
        document.getElementById('worldHotels').style.display='none';
        document.getElementById('resortHotels').style.display='block';
        document.getElementById('first').style.display='none';
        document.getElementById('third').style.display='none';
        document.getElementById('fourth').style.display='none';
        document.getElementById('ticketdiv').style.display='block';
        document.getElementById('fifth').style.display='none';
        document.getElementById('sixth').style.display='none';

    }
    else if(selectedItem.value=='Disney Cruise Line')
    {
        document.getElementById('first').style.display='none';
        document.getElementById('worldHotels').style.display='none';
        document.getElementById('resortHotels').style.display='none';
        document.getElementById('third').style.display='block';
        document.getElementById('second').style.display='none';
        document.getElementById('fourth').style.display='none';
        document.getElementById('ticketdiv').style.display='none';
        document.getElementById('fifth').style.display='none';
        document.getElementById('sixth').style.display='none';

    }
    else if(selectedItem.value=='Universal Studios Florida')
    {
        document.getElementById('first').style.display='none';
        document.getElementById('worldHotels').style.display='none';
        document.getElementById('resortHotels').style.display='none';
        document.getElementById('third').style.display='none';
        document.getElementById('second').style.display='none';
        document.getElementById('fourth').style.display='block';
        document.getElementById('ticketdiv').style.display='block';
        document.getElementById('fifth').style.display='none';
        document.getElementById('sixth').style.display='none';

    }
    else if(selectedItem.value=='Carnival Cruise Line')
    {
        document.getElementById('fifth').style.display='block';
        document.getElementById('first').style.display='none';
        document.getElementById('worldHotels').style.display='none';
        document.getElementById('resortHotels').style.display='none';
        document.getElementById('second').style.display='none';
        document.getElementById('third').style.display='none';
        document.getElementById('fourth').style.display='none';
        document.getElementById('ticketdiv').style.display='none';
        document.getElementById('sixth').style.display='none';
    }
    else if(selectedItem.value=='Royal Caribbean Cruise Line')
    {
        document.getElementById('sixth').style.display='block';
        document.getElementById('fifth').style.display='none';
        document.getElementById('first').style.display='none';
        document.getElementById('worldHotels').style.display='none';
        document.getElementById('resortHotels').style.display='none';
        document.getElementById('second').style.display='none';
        document.getElementById('third').style.display='none';
        document.getElementById('fourth').style.display='none';
        document.getElementById('ticketdiv').style.display='none';
    }
    else
    {
        document.getElementById('first').style.display='none';
        document.getElementById('worldHotels').style.display='none';
        document.getElementById('resortHotels').style.display='none';
        document.getElementById('second').style.display='none';
        document.getElementById('third').style.display='none';
        document.getElementById('fourth').style.display='none';
        document.getElementById('ticketdiv').style.display='none';
        document.getElementById('fifth').style.display='none';
        document.getElementById('sixth').style.display='none';
    }
});


function switchDivs()
{
    var selectedItem=document.getElementById('disney_select');

    if(selectedItem.value=="Walt Disney World")
    {
        document.getElementById('first').style.display='block';
        document.getElementById('worldHotels').style.display='block';
        document.getElementById('resortHotels').style.display='none';
        document.getElementById('second').style.display='none';
        document.getElementById('third').style.display='none';
        document.getElementById('fourth').style.display='none';
        document.getElementById('ticketdiv').style.display='block';
        document.getElementById('sixth').style.display='none';
        document.getElementById('fifth').style.display='none';
    }
    else if(selectedItem.value=="Disneyland Resort")
    {
        document.getElementById('second').style.display='block';
        document.getElementById('worldHotels').style.display='none';
        document.getElementById('resortHotels').style.display='block';
        document.getElementById('first').style.display='none';
        document.getElementById('third').style.display='none';
        document.getElementById('fourth').style.display='none';
        document.getElementById('ticketdiv').style.display='block';
        document.getElementById('fifth').style.display='none';
        document.getElementById('sixth').style.display='none';

    }
    else if(selectedItem.value=='Disney Cruise Line')
    {
        document.getElementById('first').style.display='none';
        document.getElementById('worldHotels').style.display='none';
        document.getElementById('resortHotels').style.display='none';
        document.getElementById('third').style.display='block';
        document.getElementById('second').style.display='none';
        document.getElementById('fourth').style.display='none';
        document.getElementById('ticketdiv').style.display='none';
        document.getElementById('fifth').style.display='none';
        document.getElementById('sixth').style.display='none';

    }
    else if(selectedItem.value=='Universal Studios Florida')
    {
        document.getElementById('first').style.display='none';
        document.getElementById('worldHotels').style.display='none';
        document.getElementById('resortHotels').style.display='none';
        document.getElementById('third').style.display='none';
        document.getElementById('second').style.display='none';
        document.getElementById('fourth').style.display='block';
        document.getElementById('ticketdiv').style.display='block';
        document.getElementById('fifth').style.display='none';
        document.getElementById('sixth').style.display='none';

    }
    else if(selectedItem.value=='Carnival Cruise Line')
    {
       
        document.getElementById('fifth').style.display='block';
        document.getElementById('first').style.display='none';
        document.getElementById('worldHotels').style.display='none';
        document.getElementById('resortHotels').style.display='none';
        document.getElementById('second').style.display='none';
        document.getElementById('third').style.display='none';
        document.getElementById('fourth').style.display='none';
        document.getElementById('ticketdiv').style.display='none';
        document.getElementById('sixth').style.display='none';
    }
    else if(selectedItem.value=='Royal Caribbean Cruise Line')
    {
        document.getElementById('sixth').style.display='block';
        document.getElementById('fifth').style.display='none';
        document.getElementById('first').style.display='none';
        document.getElementById('worldHotels').style.display='none';
        document.getElementById('resortHotels').style.display='none';
        document.getElementById('second').style.display='none';
        document.getElementById('third').style.display='none';
        document.getElementById('fourth').style.display='none';
        document.getElementById('ticketdiv').style.display='none';
    }
    else
    {
        document.getElementById('first').style.display='none';
        document.getElementById('worldHotels').style.display='none';
        document.getElementById('resortHotels').style.display='none';
        document.getElementById('second').style.display='none';
        document.getElementById('third').style.display='none';
        document.getElementById('fourth').style.display='none';
        document.getElementById('ticketdiv').style.display='none';
        document.getElementById('fifth').style.display='none';
        document.getElementById('sixth').style.display='none';
    }
}

</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.dashboardx', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>