<?php $__env->startSection('content'); ?>

 <!-- Page header -->
                <div class="page-header page-header-default">
                    <div class="page-header-content">
                        <div class="page-title">
                            <h4><span class="text-semibold">Users</span></h4>
                        </div>
                    </div>
                </div>
   <!-- /page header -->


<br>
<div class="container"> 

 
  <div class="row"> 
    <div class="form-group col-md-2 ">
    </div>

   <?php if($user->role == 1): ?>

   <form action="<?php echo e(url('commission-reports')); ?>" method="get" >
                            <?php echo csrf_field(); ?>

        <div class="form-group col-md-4 ">
          <label><b>Admins</b></label>
          <select class="select-search" name="id" required="">
            <option value="">Choose One</option>
            <?php $__currentLoopData = $admins; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $admin): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <option value="<?php echo e($admin->id); ?>"><?php echo e($admin->name); ?></option>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>  
          </select> 
       </div>
<br>
       <div class="form-group col-md-4">
        <button class="btn btn-primary" type="Submit">Submit</button>
      </div>

      <div class="form-group col-md-2 ">
    </div>
    </form>
    <?php endif; ?>
</div>
<br><br><br><br><br>
  <div class="row">
    <div class="form-group col-md-2 ">
    </div>

                           <form action="<?php echo e(url('commission-reports')); ?>" method="get" >
                            <?php echo csrf_field(); ?>

        <div class="form-group col-md-4 ">
          <label><b>Agents</b></label>
          <select class="select-search" name="id" required="">
            <option value="">Choose One</option>
           <?php $__currentLoopData = $agents; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $agent): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <option value="<?php echo e($agent->id); ?>"><?php echo e($agent->name); ?></option>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>  
          </select> 
       </div>
<br>
       <div class="form-group col-md-4">
        <button class="btn btn-primary" type="Submit">Submit</button>
      </div>

      <div class="form-group col-md-2 ">
    </div>
    </form>

                           

      <div class="form-group col-md-2 ">
    </div>
  </div>

  
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>