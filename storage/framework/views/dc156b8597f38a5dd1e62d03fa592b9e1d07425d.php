<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>M&M MAGICAL ADVENTURES</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="<?php echo e(url('assets/css/icons/icomoon/styles.css')); ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo e(url('assets/css/bootstrap.css')); ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo e(url('assets/css/core.css')); ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo e(url('assets/css/components.css')); ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo e(url('assets/css/colors.css')); ?>" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/loaders/pace.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/core/libraries/jquery.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/core/libraries/bootstrap.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/loaders/blockui.min.js')); ?>"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/tables/datatables/datatables.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/tables/datatables/extensions/buttons.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/forms/selects/select2.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js')); ?>"></script> 
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/tables/datatables/extensions/buttons.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/velocity/velocity.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/velocity/velocity.ui.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/ui/prism.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/forms/styling/uniform.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/notifications/pnotify.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/forms/selects/bootstrap_multiselect.js')); ?>"></script>

    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/forms/wizards/steps.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/core/libraries/jquery_ui/interactions.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/forms/selects/select2.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/forms/styling/uniform.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/core/libraries/jquery_ui/core.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/forms/selects/selectboxit.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/forms/inputs/typeahead/typeahead.bundle.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/forms/tags/tagsinput.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/forms/tags/tokenfield.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/forms/inputs/touchspin.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/forms/inputs/maxlength.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/forms/inputs/formatter.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/core/libraries/jasny_bootstrap.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/forms/validation/validate.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/extensions/cookie.js')); ?>"></script>

    <script type="text/javascript" src="<?php echo e(url('assets/js/core/app.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/pages/datatables_extension_buttons_init.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/pages/datatables_extension_buttons_html5.js')); ?>"></script>    
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/tables/datatables/extensions/responsive.min.js')); ?>"></script> 
    <script type="text/javascript" src="<?php echo e(url('assets/js/pages/form_select2.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/pages/form_multiselect.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/pages/wizard_steps.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/pages/user_pages_list.js')); ?>"></script>
     <script type="text/javascript" src="<?php echo e(url('assets/js/pages/datatables_advanced.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/ui/ripple.min.js')); ?>"></script>
   <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/ui/moment/moment.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/ui/fullcalendar/fullcalendar.min.js')); ?>"></script>

    <!-- <script type="text/javascript" src="<?php echo e(url('assets/js/app.js')); ?>"></script> -->
    <!-- <script type="text/javascript" src="<?php echo e(url('js/demo_pages/fullcalendar_styling.js')); ?>"></script> -->
    <!-- <script type="text/javascript" src="<?php echo e(url('assets/js/pages/extra_fullcalendar.js')); ?>"></script> -->
    <!-- /theme JS files -->
<style>
    
    .fontt{
        width: 130px;
        font-weight: 900;
    }

</style>

</head>

<body>

    <!-- Main navbar -->
    <div class="navbar navbar-inverse">
        <div class="navbar-header">
            <a href="<?php echo e(url('/home')); ?>" style="color: white;"><img src="<?php echo e(url('assets/images/Logo.png')); ?>" width="100" height="80"></a>

            <ul class="nav navbar-nav visible-xs-block">
                <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
                <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
            </ul>
        </div>

        <div class="navbar-collapse collapse" id="navbar-mobile">
            <ul class="nav navbar-nav">
                <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
            </ul>

            <div class="navbar-right">
                <ul class="nav navbar-nav">
                    <li class="dropdown dropdown-user">
                        <a class="dropdown-toggle" data-toggle="dropdown">
                            <span><?php echo e(Auth::user()->name); ?></span> 
                            <i class="caret"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right" style="width: 186px !important;">
                            <li><a href="<?php echo e(url('user/editProfile')); ?>"><i class="icon-pencil"></i> Edit Profile</a></li>
                            <li><a href="<?php echo e(url('my-commission')); ?>"><i class="icon-coins"></i></i>Commission Reports</a></li>
                            <li><a href="<?php echo e(url('logout')); ?>"><i class="icon-switch2"></i> Logout</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- /main navbar -->
    <!-- Page container -->
    <div class="page-container">
        <!-- Page content -->
        <div class="page-content">
            <!-- Main sidebar -->
            <div class="sidebar sidebar-main">
                <div class="sidebar-content">

                    <!-- User menu -->
                    <div class="sidebar-user">
                        <div class="category-content">
                            <div class="media">
                                <div class="media-body">
                                   <center><span class="media-heading text-semibold"><?php echo e(Auth::user()->name); ?></span></center>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /user menu -->
                     <!-- Main navigation -->
                    <div class="sidebar-category sidebar-category-visible">
                        <div class="category-content no-padding">
                            <ul class="navigation navigation-main navigation-accordion">
                                <!-- Main -->
                                <li><a href="<?php echo e(url('/home')); ?>"><i class="icon-home4"></i> <b>Dashboard</b></a></li>
                                <?php if(Auth::user()->role == 1): ?>
                                <li>
                                    <a href="#"><i class="icon-user"></i> <span>Agents</span></a>
                                    <ul>
                                        <li><a href="<?php echo e(url('user/list')); ?>">List Agents</a></li>
                                      <li><a href="<?php echo e(url('user/add')); ?>">Add Agent</a></li>
                                    </ul>
                                </li>                             
                                <?php endif; ?>
                                <li>
                                    <a href="#"><i class="icon-user"></i> <span>Customers</span></a>
                                    <ul>
                                        <li><a href="<?php echo e(url('customer/list')); ?>">List Customers</a></li>
                                        <li><a href="<?php echo e(url('customer/create')); ?>">Add Customer</a></li>
                                        <li><a href="<?php echo e(url('import-excel')); ?>">Customer Import</a></li>
                                        <li><a href="<?php echo e(url('sms-list')); ?>">List Customer Sms</a></li>
                                        <li><a href="<?php echo e(url('reminder-sms-list')); ?>">List Payment Reminder Sms</a></li>
                                        
                                        <li><a href="<?php echo e(url('sms')); ?>">Customer Sms</a></li>
                                    </ul>
                                </li> 
                                <li>
                                    <a href="<?php echo e(url('todo/list')); ?>"><i class="icon-stack"></i> <span>TO-DO's</span></a>
                                </li> 
                                <li>
                                    <a href="<?php echo e(url('trip/list')); ?>"><i class="icon-stack"></i> <span>Trips</span></a>
                                </li> 

                                <?php if(Auth::user()->role == 1): ?>
                                <li>
                                    <a href="#"><i class="icon-home"></i> <span>Destinations</span></a>
                                    <ul>
                                        <li><a href="<?php echo e(url('destination/list')); ?>">List Destinations</a></li>
                                      <li><a href="<?php echo e(url('destination/add')); ?>">Add Destination</a></li>
                                    </ul>
                                </li> 
                                <li><a href="<?php echo e(url('diningplan/list')); ?>"><i class="icon-stack"></i> <span>Disney Dining Plans</span></a></li>

                                
                                <?php endif; ?>
                                <li><a href="<?php echo e(url('/dining-plan-calculator')); ?>" target="_blank"><i class="icon-calculator"></i> <span>Disney Dining Plan Calculator</span></a></li>
                                <li><a href="<?php echo e(url('important-date-calculator')); ?>"><i class="icon-calculator"></i> <span>Important Dates Calculator</span></a></li>

                                <?php if(Auth::user()->role == 0): ?>
                                <li>
                                    <a href="#"><i class="icon-stack2"></i> <span>Reports</span></a>
                                    <ul>
                                        <li><a href="<?php echo e(url('trips-checkin')); ?>">Date Traveled Commission Report</a></li>                    </ul>
                                </li>
                                <li>
                                    <a href="<?php echo e(url('calenders-list')); ?>"><i class="icon-calculator"></i> <span>Calendar</span></a>
                                </li>
                                <?php endif; ?>

                                <?php if(Auth::user()->role == 1): ?>
                                <li>
                                    <a href="#"><i class="icon-stack2"></i> <span>Reports</span></a>
                                    <ul>
                                        <li><a href="<?php echo e(url('trips-checkin')); ?>">Date Traveled Commission Report</a></li>
                                        <li><a href="<?php echo e(url('commission-agents')); ?>">All Agent Reports</a></li>
                                        <li><a href="<?php echo e(url('walt-disney-world-reports')); ?>">Walt Disney World Reports</a></li>
                                        <li><a href="<?php echo e(url('disneyland-resort-reports')); ?>">Disneyland Resort Reports</a></li>
                                        <li><a href="<?php echo e(url('disney-cruise-line-reports')); ?>">Disney Cruise Line Reports</a></li>
                                        <li><a href="<?php echo e(url('commission-destinations')); ?>">Other Destination Reports</a></li>
                                        <li><a href="<?php echo e(url('getagency-total-revenue')); ?>">Agency Total Revenue Report</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="<?php echo e(url('auditlogs/list')); ?>"><i class="icon-list"></i> <span>Audit Logs</span></a>
                                </li>
                                <li>
                                    <a href="<?php echo e(url('calenders-list')); ?>"><i class="icon-calculator"></i> <span>Calendar</span></a>
                                </li> 
                                <li>
                                    <a href="<?php echo e(url('commisions-list')); ?>"><i class="icon-coins"></i> <span>Commissions</span></a>
                                </li> 
                                <?php endif; ?>

                                
                            </ul>
                        </div>
                    </div>
                    <!-- /main navigation -->
                </div>
            </div>
            <!-- /main sidebar -->

            <!-- Main content -->
            <div class="content-wrapper">   
                <!-- Content area -->
                <div class="content">

                                <?php echo $__env->yieldContent('content'); ?>

                    <!-- Footer -->
                    <div class="footer text-muted">
                        &nbsp &copy; 2019 Powered by <a href="https://www.nodlays.com/" target="_blank">Nodlays</a>
                    </div>
                    <!-- /footer -->

                </div>
                <!-- /content area -->

            </div>
            <!-- /main content -->

        </div>
        <!-- /page content -->

    </div>
    <!-- /page container -->

</body>
</html>
