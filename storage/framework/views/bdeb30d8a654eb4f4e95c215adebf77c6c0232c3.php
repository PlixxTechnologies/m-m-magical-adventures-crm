<?php $__env->startSection('content'); ?>
<!-- Datatables JS FILES -->

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.print.min.js"></script>

<!-- /Datatables JS FILES -->
<style>
.red{
    color: red;
}
.select2-selection--multiple .select2-selection__choice {
    background-color: #eee;
    color: black;
}
</style>
<!-- Page header -->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="<?php echo e(url('/home')); ?>">
                    <em class="fa fa-home"></em>
                </a></li>
                <li class="active">Add Quote</li>
            </ol>
        </div><!--/.row-->
    <div class="container-fluid"> 
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Add Quote</h1>
            </div>
        </div>  <!-- /page header -->
   </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">

                        <form action="<?php echo e(url('quote/save')); ?>" method="post" >
                            <?php echo csrf_field(); ?>


                            <input type="hidden" class="form-control" name="user_id" value="<?php echo e($user->id); ?>">
							<input type="hidden" class="form-control" name="customer_id" value="<?php echo e($customer->id); ?>">

                            <div class="form-group">
                                <label>Agent Name:</label>
                                <input type="text"  class="form-control" value="<?php echo e($user->name); ?>" readonly>
                            </div>
                            <div class="form-group">
                                <label>Customer Name:</label>
                                <input type="text"  class="form-control" value="<?php echo e($name); ?>" readonly>
                            </div>
                            <div class="form-group">
                                <label><span class="red">*</span>Reservation Number:</label>
                                <input type="text" name="reservation_number"  class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label><span class="red">*</span>Quote Date:</label>
                                <input type="date" name="booking_date" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label><span class="red">*</span>Check In Date:</label>
                                <input type="date" name="checkin_date" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label>Check Out Date:</label>
                                <input type="date" name="checkout_date" class="form-control">
                                <span class="red"><b>Note: </b></span>If a check-out date is not selected then system will automatically save the <b>check-in date</b> as check-out date.
                            </div>
                            <div class="form-group">
                                <label><span class="red">*</span>Trip Status:</label>
                           <!--      <select name="trip_status" class="form-control" required> -->
                                    <select class="form-control selectpicker" name="trip_status" data-live-search="true" required="">   
                                    <option value="">Choose One....</option>
                                    <option value="Inquiry">Inquiry</option>
                                    <option value="Quote Sent">Quote Sent</option>
                              <!--       <option value="Booked Deposit">Booked Deposit</option> -->
                                    <option value="On Hold">On Hold</option>
                                   <!--  <option value="Paid in Full">Paid in Full</option>
                                    <option value="Complete – Commission Paid">Complete – Commission Paid</option>
                                    <option value="Canceled">Canceled</option> -->
                                </select>
                            </div>
                            <div class="form-group">
                                <label><span class="red">*</span>Total Sale:</label>
                                <input step="any" name="total_sale" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label><span class="red">*</span>Total Commission:</label>
                                <input step="any" name="commission" id="commission" onfocusout="mycommission()" class="form-control"  required >                                
                            </div>
                            <div class="form-group">
                                <label><span class="red">*</span>Expected Commission:</label>
                                <input type="text" step="any" name="expected_commission" id="expected_commission" class="form-control" >
                                <input type="hidden" id="user_commission" value="<?php echo e(Auth::user()->commission); ?>" required>
                            </div>
                            <!-- <div class="form-group">
                                    <label>Travel With:</label>
                                    <input type="text" value="<?php echo e($guests); ?>" name="travel_with"  class="form-control">
                            </div> -->
 <script type="text/javascript">
// Jquery Dependency



// $("input[data-type='currency']").on({
//     keyup: function() {
//       formatCurrency($(this));
//     },
//     blur: function() { 
//       formatCurrency($(this), "blur");
//     }
// });


// function formatNumber(n) {
//   // format number 1000000 to 1,234,567
//   return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
// }


// function formatCurrency(input, blur) {
//   // appends $ to value, validates decimal side
//   // and puts cursor back in right position.
  
//   // get input value
//   var input_val = input.val();
  
//   // don't validate empty input
//   if (input_val === "") { return; }
  
//   // original length
//   var original_len = input_val.length;

//   // initial caret position 
//   var caret_pos = input.prop("selectionStart");
    
//   // check for decimal
//   if (input_val.indexOf(".") >= 0) {

//     // get position of first decimal
//     // this prevents multiple decimals from
//     // being entered
//     var decimal_pos = input_val.indexOf(".");

//     // split number by decimal point
//     var left_side = input_val.substring(0, decimal_pos);
//     var right_side = input_val.substring(decimal_pos);

//     // add commas to left side of number
//     left_side = formatNumber(left_side);

//     // validate right side
//     right_side = formatNumber(right_side);
    
//     // On blur make sure 2 numbers after decimal
//     if (blur === "blur") {
//       right_side += "00";
//     }
    
//     // Limit decimal to only 2 digits
//     right_side = right_side.substring(0, 2);

//     // join number by .
//     input_val = "$" + left_side + "." + right_side;

//   } else {
//     // no decimal entered
//     // add commas to number
//     // remove all non-digits
//     input_val = formatNumber(input_val);
//     input_val = "$" + input_val;
    
//     // final formatting
//     if (blur === "blur") {
//       input_val += ".00";
//     }
//   }
  
//   // send updated string to input
//   input.val(input_val);

//   // put caret back in the right position
//   var updated_len = input_val.length;
//   caret_pos = updated_len - original_len + caret_pos;
//   input[0].setSelectionRange(caret_pos, caret_pos);
// }
// console.log(c);
</script>

                              

                            <div class="form-group form-group-material">
                                        <label class="control-label has-margin animate is-visible">Travel With:</label>
                                       <!--  <select name="travel_with[]" class="select select2-hidden-accessible" tabindex="-1" aria-hidden="true"> -->
                                    <select class="form-control selectpicker" multiple=""  name="travel_with[]" data-live-search="true" tabindex="-1" aria-hidden="true"> 

                                        <!-- <option selected>Nothing Found</option> -->
                                            <?php $__currentLoopData = $guests; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $guest): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option>                                                
                                                    <?php echo e($guest->guest_first_name.' '.$guest->guest_last_name); ?>

                                                </option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
                                        </select>
                            </div>

                            <div class="form-group">
                                    <label>Special Occasions or Requests:</label>
                                    <input type="text" name="special_request" class="form-control">
                            </div>
                            <div class="form-group">
                                <label><span class="red">*</span>Destinations:</label>
                                <select name="destination" class="form-control" id="disney_select" onchange="switchDivs()" required>
                              <option value="Choose One...a">Choose One....</option>
                                <?php $__currentLoopData = $agentdestinations; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $destination): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($destination->destination_name); ?>"><?php echo e($destination->destination_name); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
                                <option value="Universal Studios Florida">Universal Studios Florida</option>   
                                <option value="Disney Cruise Line">Disney Cruise Line</option> 
                                <option value="Royal Caribbean Cruise Line">Royal Caribbean Cruise Line</option>  
                                <option value="Walt Disney World">Walt Disney World</option> 
                                </select>
                            </div>                            

                            <div id="ticketdiv">
                            <div class="form-group">
                                <label>Ticket Only Sale:</label>
                                <select name="ticket_type" class="form-control">
                                    <option value="">Choose One....</option>
                                    <option value="Park Ticket Only">Park Ticket Only</option>
                                    <option value="Special Event Only">Special Event Only</option>
                                </select>
                            </div>
                        </div>
                        

                            <div id="first">
                                <div class="form-group" id = "worldHotels">
                                    <label>Walt Disney World Hotel Name:</label>
                                    <select name="disneyworld_hotel_name" class="form-control">
                                        <option value="">Choose One....</option>
                                         <option disabled><b>Value Resorts</b></option>
                                          <?php $__currentLoopData = $valueResorts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                          <option value="<?php echo e($value->hotel_name); ?>"><?php echo e($value->hotel_name); ?></option>
                                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                          <option disabled><b>Moderate Resorts</b></option>
                                          <?php $__currentLoopData = $moderateResorts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                          <option value="<?php echo e($value->hotel_name); ?>"><?php echo e($value->hotel_name); ?></option>
                                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                          <option disabled><b>Deluxe Resorts </b></option>
                                          <?php $__currentLoopData = $deluxeResorts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                          <option value="<?php echo e($value->hotel_name); ?>"><?php echo e($value->hotel_name); ?></option>
                                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                          <option disabled><b>Deluxe Villas </b></option>
                                          <?php $__currentLoopData = $deluxeVillas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                          <option value="<?php echo e($value->hotel_name); ?>"><?php echo e($value->hotel_name); ?></option>
                                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                       
                                    </select>
                                 
                                </div> 

                                <div class="form-group">
                                    <label>Dining Plan:</label>
                                    <select name="disney_dining_plan" class="form-control">
                                        <option value="None">None</option>
                                        <option value="Quick Service Dining">Quick Service Dining</option>
                                        <option value="Disney Dining Plan">Disney Dining Plan</option>
                                        <option value="Deluxe Dining Plan">Deluxe Dining Plan</option>
                                    </select>
                                </div>                             
                                <div class="form-group">
                                    <label>Memory Maker:</label>
                                    <select name="memory_maker" class="form-control">
                                        <option value="">Choose One....</option>
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Magical Express:</label>
                                    <select name="magical_express" class="form-control">
                                        <option value="">Choose One....</option>
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>
                                    </select>
                                </div> 
                                
                                <div class="form-group">
                                    <label>Magic Band Color Selection:</label>
                                    <input type="text" name="magic_band_color" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>My Disney Experience User Name:</label>
                                    <input type="text" name="disney_experience_username" value="<?php echo e($customer->disney_experience_username); ?>" class="form-control" readonly>
                                </div>
                                <div class="form-group">
                                    <label>My Disney Experience Password:</label>
                                    <input type="text" name="disney_experience_password" value="<?php echo e($customer->disney_experience_password); ?>" class="form-control" readonly>
                                </div>
                            </div>

                            <div id="second">
                                <div class="form-group" id ="resortHotels">
                                    <label>Disneyland Hotel Name:</label>
                                    <select name="disneyresort_hotel_name" class="form-control" id="goodneighbourselect" onchange="switchGoodDiv()">
                                        <option value="">Choose One....</option>
                                        <?php $__currentLoopData = $disneylandResortHotels; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($value->hotel_name); ?>"><?php echo e($value->hotel_name); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                </div>

                                <div id="goodneighbour">
                                    <div class="form-group">
                                        <label>Good Neighbor Hotel</label>
                                        <input type="text" name="good_neighbor_hotel" class="form-control">
                                    </div>
                                </div>                               
                            </div>

                         <div id="third">
                             <div class="form-group">
                                 <label>Ship Name:</label>
                                 <select name="ship_name2" class="form-control">
                                     <option value="">Choose One....</option>
                                     <?php $__currentLoopData = $disneyCruiseLineShips; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                     <option value="<?php echo e($value->hotel_name); ?>"><?php echo e($value->hotel_name); ?></option>
                                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                 </select>
                             </div>
                             <div class="form-group">
                                <label>Castaway Member:</label>
                                <input type="text" name="castaway_member"  class="form-control">
                             </div>
                             <div class="form-group">
                                <label>Bus Transportation:</label>
                               <div class="checkbox">
                                  <label><input type="checkbox" name="bus_transportation" value="Yes">Yes</label>
                                </div>
                                <div class="checkbox">
                                  <label><input type="checkbox" name="bus_transportation" value="No">No</label>
                                </div>
                             </div>
                        </div>

                        <div id="fourth">
                            <!-- <div id="universalorlandoresorthotels"> -->
                                    <div class="form-group">
                                        <label>Universal Orlando Resort Hotels:</label>
                                        <select name="universal_orlando_resort_hotel" class="form-control" id="universal_orlando_resort_hotel" onchange="switchOrlandoDiv()">
                                            <option value="">Choose One....</option>
                                            <?php $__currentLoopData = $universalStudiosHotels; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($value->hotel_name); ?>"><?php echo e($value->hotel_name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                    </div>
                                <!-- </div>  -->

                                <div id="PartnersHotel">
                                    <div class="form-group">
                                        <label>Partners Hotel</label>
                                        <input type="text" name="partners_hotel" class="form-control">
                                    </div>
                                </div>
                        </div>

                        <div id="fifth">
                             <div class="form-group">
                                 <label>Ship Name:</label>
                                 <select name="ship_name1" class="form-control">
                                     <option value="">Choose One....</option>
                                     <optgroup label="XL Class">
                                     <?php $__currentLoopData = $xl; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($value->hotel_name); ?>"><?php echo e($value->hotel_name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </optgroup>
                                    <optgroup label="Vista Class">
                                   <?php $__currentLoopData = $vista; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($value->hotel_name); ?>"><?php echo e($value->hotel_name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </optgroup>
                                    <optgroup label="Sunshine Class">
                                    <?php $__currentLoopData = $sunshine; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($value->hotel_name); ?>"><?php echo e($value->hotel_name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </optgroup>
                                    <optgroup label="Dream Class">
                                    <?php $__currentLoopData = $dream; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($value->hotel_name); ?>"><?php echo e($value->hotel_name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </optgroup>
                                    <optgroup label="Splendor Class">
                                   <?php $__currentLoopData = $splendor; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($value->hotel_name); ?>"><?php echo e($value->hotel_name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </optgroup>
                                     <optgroup label="Conquest Class">
                                     <?php $__currentLoopData = $conquest; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($value->hotel_name); ?>"><?php echo e($value->hotel_name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </optgroup>
                                     <optgroup label="Spirit Class">
                                     <?php $__currentLoopData = $spirit; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($value->hotel_name); ?>"><?php echo e($value->hotel_name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </optgroup>
                                    <optgroup label="Fantasy Class">
                                    <?php $__currentLoopData = $fantasy; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($value->hotel_name); ?>"><?php echo e($value->hotel_name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </optgroup>
                                 </select>
                             </div>
                        
                        </div>
    

                         <div id="sixth">
                             <div class="form-group">
                                 <label>Ship Name:</label>
                                 <select name="ship_name" class="form-control">
                                    <option value="">Choose One....</option>
                                        <option disabled><b>Quantum Class</b></option>
                                          
                                          <option value="Odyssey of the Seas">Odyssey of the Seas</option>
                                          <option value="Spectrum of the Seas">Spectrum of the Seas</option>
                                          <option value="Ovation of the Seas">Ovation of the Seas</option>
                                          <option value="Anthem of the Seas">Anthem of the Seas</option>
                                          <option value="Quantum of the Seas">Quantum of the Seas</option>

                                        <option disabled><b>Oasis Class</b></option>
                                          
                                          <option value="Symphony of the Seas">Symphony of the Seas</option>
                                          <option value="Harmony of the Seas">Harmony of the Seas</option>
                                          <option value="Wonder of the Seas">Wonder of the Seas</option>
                                          <option value="Allure of the Seass">Allure of the Seas</option>
                                          <option value="Oasis of the Seas">Oasis of the Seas</option>

                                        <option disabled><b>Freedom Class</b></option>
                                          
                                          <option value="Independence of the Seas">Independence of the Seas</option>
                                          <option value="Liberty of the Seas">Liberty of the Seas</option>
                                          <option value="Freedom of the Seas">Freedom of the Seas</option>

                                        <option disabled><b>Voyager Class</b></option>
                                          
                                          <option value="Mariner of the Seas">Mariner of the Seas</option>
                                          <option value="Navigator of the Seas">Navigator of the Seas</option>
                                          <option value="Adventure of the Seas">Adventure of the Seas</option> 
                                          <option value="Explorer of the Seas">Explorer of the Seas</option>
                                          <option value="Voyager of the Seas">Voyager of the Seas</option>  

                                        <option disabled><b>Radiance Class</b></option>
                                          
                                          <option value="Jewel of the Seas">Jewel of the Seas</option>
                                          <option value="Serenade of the Seas">Serenade of the Seas</option>
                                          <option value="Brilliance of the Seas">Brilliance of the Seas</option> 
                                          <option value="Radiance of the Seas">Radiance of the Seas</option>
                                          <option value="Vision Class">Vision Class</option>

                                        <option disabled><b>Vision of the Seas</b></option>
                                          
                                          <option value="Enchantment of the Seas">Enchantment of the Seas</option>
                                          <option value="Rhapsody of the Seas">Rhapsody of the Seas</option>
                                          <option value="Grandeur of the Seas">Grandeur of the Seas</option>                                                                                   
                                   </select>
                             </div>

                             <div class="form-group">
                                 <label>Stateroom Category</label>
                                 <select name="stateroom_category_caribbean" class="form-control">
                                     <option value="">Choose One....</option>
                                     <option value="Inside Stateroomsteroom">Inside Staterooms</option>
                                     <option value="Promenade View">Promenade View</option>
                                     <option value="Virtual Balcony">Virtual Balcony </option>
                                     <option value="Oceanview">Oceanview</option>
                                     <option value="Panoramic Oceanview">Panoramic Oceanview</option>
                                     <option value="Balcony">Balcony</option>
                                     <option value="Central Park Balcony">Central Park Balcony</option>
                                     <option value="Boardwalk Balcony">Boardwalk Balcony</option>
                                     <option value="Royal Loft Suite">Royal Loft Suite</option>
                                     <option value="Star Loft Suite">Star Loft Suite</option>
                                     <option value="Crown Loft Suite with Balcony">Crown Loft Suite with Balcony</option>
                                     <option value="Grand Suite ">Grand Suite </option>
                                     <option value="Owner’s Suite">Owner’s Suite</option>
                                 </select>
                             </div>

                             <div class="form-group">
                                 <label>Crown & Anchor Level</label>
                                 <select name="crown_anchor_level" class="form-control">
                                     <option value="">Choose One....</option>
                                     <option value="Gold ">Gold </option>
                                     <option value="Platinum">Platinum</option>
                                     <option value="Emerald">Emerald </option>
                                     <option value="Diamond">Diamond</option>
                                     <option value="Diamond Plus">Diamond Plus</option>
                                     <option value="Pinnacle Club">Pinnacle Club</option>
                                 </select>
                             </div>
                        
                        </div>
                        <div id="seventh">
                             <div class="form-group">
                                 <label>Ship Name:</label>
                                 <select name="ship_name" class="form-control">
                                     <option value="">Choose One....</option>
                                     <optgroup label="Oasis Class">
                                    <?php $__currentLoopData = $oasis; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($value->hotel_name); ?>"><?php echo e($value->hotel_name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </optgroup>
                                    <optgroup label="Quantum Class">
                                     <?php $__currentLoopData = $quantum; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($value->hotel_name); ?>"><?php echo e($value->hotel_name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </optgroup>
                                   <optgroup label="Freedom Class">
                                   <?php $__currentLoopData = $freedom; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($value->hotel_name); ?>"><?php echo e($value->hotel_name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </optgroup>
                                   <optgroup label="Voyager Class">
                                    <?php $__currentLoopData = $voyager; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($value->hotel_name); ?>"><?php echo e($value->hotel_name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </optgroup>
                                     <optgroup label="Radiance Class">
                                    <?php $__currentLoopData = $radiance; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($value->hotel_name); ?>"><?php echo e($value->hotel_name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </optgroup>
                                    <optgroup label="Vision Class">
                                     <?php $__currentLoopData = $vision; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($value->hotel_name); ?>"><?php echo e($value->hotel_name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </optgroup>
                                    <optgroup label="Other">
                                    <?php $__currentLoopData = $other; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($value->hotel_name); ?>"><?php echo e($value->hotel_name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </optgroup>
                                 </select>
                             </div>
                        
                        </div>

                        <div id="eight">
                                <div class="form-group" id = "universalHotelsflorida">
                                    <label>Universal Studios Florida Hotel Names:</label>
                                    <select name="universal_studios_florida_hotel_names" class="form-control">
                                        <option value="">Choose One....</option>
                                         <option disabled><b>Value Resorts</b></option>
                                          
                                          <option value="Universal's Endless Summer Resort - Surfside Inn and Suites">Universal's Endless Summer Resort - Surfside Inn and Suites</option>
                                          <option value="Universal's Endless Summer Resort – Dockside Inn and Suites">Universal's Endless Summer Resort – Dockside Inn and Suites</option>
                                          
                                          <option disabled><b>Prime Value</b></option>
                                        
                                          <option value="Universal's Cabana Bay Beach Resort">Universal's Cabana Bay Beach Resort</option>
                                          <option value="Universal's Aventura Hotel">Universal's Aventura Hotel</option>
                                       
                                          <option disabled><b>Preferred </b></option>
                                         
                                          <option value="Loews Sapphire Falls Resort at Universal Orlando">Loews Sapphire Falls Resort at Universal Orlando</option>

                                         
                                          <option disabled><b>Premier </b></option>
                                         
                                          <option value="Loews Royal Pacific Resort">Loews Royal Pacific Resort</option>
                                          <option value="Hard Rock Hotel at Universal Orlando Resort">Hard Rock Hotel at Universal Orlando Resort</option>
                                          <option value="Loews Portofino Bay Hotel at Universal Orlando">Loews Portofino Bay Hotel at Universal Orlando</option>
                                        
                                       
                                    </select>
                                 
                                </div> 

                                <div class="form-group">
                                    <label>Universal Dining Plan:</label>
                                    <select name="universal_dining_plan" class="form-control">
                                        <option value="">Choose One....</option>
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>
                                    </select>
                                </div>   
                                <div class="form-group">
                                    <label>My Universal Photos:</label>
                                    <select name="my_universal_photos" class="form-control">
                                        <option value="">Choose One....</option>
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>
                                    </select>
                                </div> 
                                <div class="form-group">
                                    <label>Universal Super Shuttle:</label>
                                    <select name="universal_super_shuttle" class="form-control">
                                        <option value="">Choose One....</option>
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>
                                    </select>
                                </div> 
                                <div class="form-group">
                                    <label>Universal Express Pass:</label>
                                    <select name="universal_express_pass" class="form-control">
                                        <option value="">Choose One....</option>
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>
                                    </select>
                                </div> 

                                <div class="form-group">
                                <label>Ticket Only Sale:</label>
                                <select name="ticket_type" class="form-control">
                                    <option value="">Choose One....</option>
                                    <option value="Park Ticket Only">Park Ticket Only</option>
                                    <option value="Special Event Only">Special Event Only</option>
                                </select>
                            </div>
                        </div>

                        <div id="nine">
                            <div class="form-group">
                                 <label>Ship Name:</label>
                                 <select name="ship_name2" class="form-control">
                                     <option value="">Choose One....</option>
                                     <?php $__currentLoopData = $disneyCruiseLineShips; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                     <option value="<?php echo e($value->hotel_name); ?>"><?php echo e($value->hotel_name); ?></option>
                                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                     <option value="Disney Wish">Disney Wish</option>
                                 </select>
                             </div>
                              <div class="form-group">
                                 <label>Stateroom Category</label>
                                 <select name="stateroom_category" class="form-control">
                                     <option value="">Choose One....</option>
                                     <option value="Standard Inside Stateroom">Standard Inside Stateroom</option>
                                     <option value="Deluxe Inside Stateroom">Deluxe Inside Stateroom</option>
                                     <option value="Deluxe Oceanview Stateroom">Deluxe Oceanview Stateroom</option>
                                     <option value="Deluxe Family Oceanview">Deluxe Family Oceanview</option>
                                     <option value="Deluxe Oceanview with Verandah">Deluxe Oceanview with Verandah </option>
                                     <option value="Deluxe Family Oceanview with Verandah">Deluxe Family Oceanview with Verandah</option>
                                     <option value="Concierge Family Oceanview with Verandah">Concierge Family Oceanview with Verandah</option>
                                     <option value="Concierge 1-Bedroom Suite with Verandah">Concierge 1-Bedroom Suite with Verandah</option>
                                     <option value="Concierge 2-Bedroom Suite with Verandah">Concierge 2-Bedroom Suite with Verandah</option>
                                     <option value="Concierge Royal Suite with Verandah">Concierge Royal Suite with Verandah</option>
                                 </select>
                             </div>
                             <div class="form-group">
                                <label>Castaway Member:</label>
                                <input type="text" name="castaway_member"  class="form-control">
                             </div>
                           <!--   <div class="form-group">
                                <label>Bus Transportation:</label>
                               <div class="checkbox">
                                  <label><input type="checkbox" name="bus_transportation" value="Yes">Yes</label>
                                </div>
                                <div class="checkbox">
                                  <label><input type="checkbox" name="bus_transportation" value="No">No</label>
                                </div>
                             </div> -->
                               <div class="form-group">
                                 <label>Castaway Club Level:</label>
                                 <select name="castaway_club_level" class="form-control">
                                     <option value="">none</option>                                 
                                     <option value="Silver Castaway">Silver Castaway</option>
                                     <option value="Gold Castaway">Gold Castaway</option>
                                     <option value="Platinum Castaway">Platinum Castaway</option>
                                 </select>
                             </div>
                        </div>


                                <script type="text/javascript">
                       function yesnoCheck(that) {
                                if (that.value == "other") {
                                    document.getElementById("ifYes").style.display = "block";
                                } else {
                                    document.getElementById("ifYes").style.display = "none";
                                }
                            }
                 </script>

                 <div class="form-group ">
                 <label><span class="red">*</span>Referral:</label>
                <select onchange="yesnoCheck(this);" name="Referal" class="form-control" required>
                                    <option disabled selected>--Select--</option>
                                    <option value="Facebook">Facebook</option>
                                    <option value="Instagram">Instagram</option>
                                    <option value="Google">Google</option>
                                    <option value="Friend">Friend</option>
                                    <option value="Family">Family</option>
                                    <option value="Social Media Ad">Social Media Ad</option>
                                    <option value="Disney/Universal FAM">Disney/Universal FAM</option>
                                    <option value="other">Other</option>
                                </select><br>
                                <div id="ifYes" style="display: none;">
                                   <input placeholder="Type Referral" type="text" id="other" name="other" class="form-control"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Notes:</label>
                                <textarea name="notes" class="form-control" rows="4"></textarea>
                            </div>
                                                       
                            <button type="submit" class="btn btn-primary pull-right">Save</button>
                        </form>
                    </div>
                </div>
            </div>
        </div><!--/.row-->
    </div>  <!--/.main-->
</div>


<script type="text/javascript">

hidedivs();

    function mycommission() {    
    var x = $('#commission').val();        
    var usercommission = $('#user_commission').val();

        if(x != "")
            $('#expected_commission').val(Math.round(parseFloat(x) * ((parseFloat(usercommission)/100) * 100)) / 100);

}

function hidedivs()
{
     
        var divEl=document.getElementById('first');
          divEl.style.display = 'none';

          var divE2=document.getElementById('second');
        divE2.style.display = 'none';

        var divE3=document.getElementById('third');
        divE3.style.display = 'none';

        var divE4=document.getElementById('fourth');
        divE4.style.display = 'none';  
        
        var divE5=document.getElementById('ticketdiv');
        divE5.style.display = 'none';  

        var divE6=document.getElementById('fifth');
        divE6.style.display = 'none';  

        var divE7=document.getElementById('sixth');
        divE7.style.display = 'none'; 
         var divE7=document.getElementById('eight');
        divE7.style.display = 'none'; 
          var divE7=document.getElementById('seventh');
        divE7.style.display = 'none'; 
         var divE7=document.getElementById('nine');
        divE7.style.display = 'none'; 
}

function switchDivs()
{
    var selectedItem=document.getElementById('disney_select');

    if(selectedItem.value=="Walt Disney World")
    {
        document.getElementById('first').style.display='block';
        document.getElementById('worldHotels').style.display='block';
        document.getElementById('resortHotels').style.display='none';
        document.getElementById('second').style.display='none';
        document.getElementById('third').style.display='none';
        document.getElementById('fourth').style.display='none';
        document.getElementById('ticketdiv').style.display='block';
        document.getElementById('sixth').style.display='none';
        document.getElementById('fifth').style.display='none';
         document.getElementById('seventh').style.display='none';
        document.getElementById('eight').style.display='none';
        document.getElementById('nine').style.display='none';
    }
     else if(selectedItem.value=="Choose One...a")
    {
        document.getElementById('second').style.display='none';
        document.getElementById('worldHotels').style.display='none';
        document.getElementById('resortHotels').style.display='none';
        document.getElementById('first').style.display='none';
        document.getElementById('third').style.display='none';
        document.getElementById('fourth').style.display='none';
        document.getElementById('ticketdiv').style.display='none';
        document.getElementById('fifth').style.display='none';
        document.getElementById('sixth').style.display='none';
        document.getElementById('seventh').style.display='none';
        document.getElementById('eight').style.display='none';
        document.getElementById('nine').style.display='none';

    }
    else if(selectedItem.value=="Disneyland Resort")
    {
        document.getElementById('second').style.display='block';
        document.getElementById('worldHotels').style.display='none';
        document.getElementById('resortHotels').style.display='block';
        document.getElementById('first').style.display='none';
        document.getElementById('third').style.display='none';
        document.getElementById('fourth').style.display='none';
        document.getElementById('ticketdiv').style.display='block';
        document.getElementById('fifth').style.display='none';
        document.getElementById('sixth').style.display='none';
         document.getElementById('eight').style.display='none';
        document.getElementById('nine').style.display='none';
         document.getElementById('seventh').style.display='none';

    }
     else if(selectedItem.value=="Disney Aulani")
    {
        document.getElementById('second').style.display='none';
        document.getElementById('worldHotels').style.display='none';
        document.getElementById('resortHotels').style.display='none';
        document.getElementById('first').style.display='none';
        document.getElementById('third').style.display='none';
        document.getElementById('fourth').style.display='none';
        document.getElementById('ticketdiv').style.display='none';
        document.getElementById('fifth').style.display='none';
        document.getElementById('sixth').style.display='none';
        document.getElementById('seventh').style.display='none';
        document.getElementById('eight').style.display='none';
        document.getElementById('nine').style.display='none';

    }
    else if(selectedItem.value=='Disney Cruise Line')
    {
        document.getElementById('first').style.display='none';
        document.getElementById('worldHotels').style.display='none';
        document.getElementById('resortHotels').style.display='none';
        document.getElementById('third').style.display='none';
        document.getElementById('second').style.display='none';
        document.getElementById('fourth').style.display='none';
        document.getElementById('ticketdiv').style.display='none';
        document.getElementById('fifth').style.display='none';
        document.getElementById('sixth').style.display='none';
        document.getElementById('eight').style.display='none';
        document.getElementById('nine').style.display='block';

    }
    // else if(selectedItem.value=='Universal Studios Florida')
    // {
    //     document.getElementById('first').style.display='none';
    //     document.getElementById('worldHotels').style.display='none';
    //     document.getElementById('resortHotels').style.display='none';
    //     document.getElementById('third').style.display='none';
    //     document.getElementById('second').style.display='none';
    //     document.getElementById('fourth').style.display='block';
    //     document.getElementById('ticketdiv').style.display='block';
    //     document.getElementById('fifth').style.display='none';
    //     document.getElementById('sixth').style.display='none';

    // }
    else if(selectedItem.value=='Carnival Cruise Line')
    {
       
        document.getElementById('fifth').style.display='block';
        document.getElementById('first').style.display='none';
        document.getElementById('worldHotels').style.display='none';
        document.getElementById('resortHotels').style.display='none';
        document.getElementById('second').style.display='none';
        document.getElementById('third').style.display='none';
        document.getElementById('fourth').style.display='none';
        document.getElementById('ticketdiv').style.display='none';
        document.getElementById('sixth').style.display='none';
        document.getElementById('seventh').style.display='none';
        document.getElementById('eight').style.display='none';
        document.getElementById('nine').style.display='none';
    }
    else if(selectedItem.value=='Royal Caribbean Cruise Line')
    {
        document.getElementById('sixth').style.display='block';
        document.getElementById('fifth').style.display='none';
        document.getElementById('first').style.display='none';
        document.getElementById('worldHotels').style.display='none';
        document.getElementById('resortHotels').style.display='none';
        document.getElementById('second').style.display='none';
        document.getElementById('third').style.display='none';
        document.getElementById('fourth').style.display='none';
        document.getElementById('ticketdiv').style.display='none';
        document.getElementById('seventh').style.display='none';
        document.getElementById('eight').style.display='none';
        document.getElementById('nine').style.display='none';
    }
      else if(selectedItem.value=='Universal Studios Florida')
    {
        document.getElementById('eight').style.display='block';
        document.getElementById('fifth').style.display='none';
        document.getElementById('first').style.display='none';
        document.getElementById('worldHotels').style.display='none';
        document.getElementById('resortHotels').style.display='none';
        document.getElementById('second').style.display='none';
        document.getElementById('third').style.display='none';
        document.getElementById('fourth').style.display='none';
        document.getElementById('ticketdiv').style.display='none';
        document.getElementById('sixth').style.display='none';
        document.getElementById('nine').style.display='none';
    }
    else
    {
        document.getElementById('first').style.display='none';
        document.getElementById('worldHotels').style.display='none';
        document.getElementById('resortHotels').style.display='none';
        document.getElementById('second').style.display='none';
        document.getElementById('third').style.display='none';
        document.getElementById('fourth').style.display='none';
        document.getElementById('ticketdiv').style.display='none';
        document.getElementById('fifth').style.display='none';
        document.getElementById('sixth').style.display='none';
         document.getElementById('eight').style.display='none';
        document.getElementById('nine').style.display='none';
    }
}


</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.dashboardx', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>