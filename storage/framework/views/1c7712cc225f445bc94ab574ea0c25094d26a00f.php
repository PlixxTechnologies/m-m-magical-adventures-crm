<?php $__env->startSection('content'); ?>

<?php
use App\TripsDTO;
use Illuminate\Support\Collection;
?>

<style type="text/css">
    .fc-event, .fc-event-dot{
        background-color: #448ea7 !important;
        color: #000000 !important;
    }
</style>

<!-- Page header -->
                <div class="page-header page-header-default">
                    <div class="page-header-content">
                        <div class="page-title">
                            <h4><span class="text-semibold">Calendar</span></h4>
                        </div>
                    </div>
                </div>
   <!-- /page header -->

<div id="modal_theme_valuUP" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <form method="post">
            <div class="modal-content">
                <div class="modal-header bg-primary text-center">
                    <h3 class="modal-title text-center">Trips Information</h3>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <div class="modal-body">
                    <input type="hidden" id="xyz" name="xyz" />
                    <div class="row">
                        <table class="table">
                          <thead>
                            <tr>
                              <th scope="col">Reservation Number</th>
                              <th scope="col">Customer Name</th>
                              <th scope="col">Customer ID</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <th scope="row">
                                <a  id="reser_number" href="" class="tr_Id"></a>
                                <p id="reser_numbers"></p>
                              </th>
                              <td>
                                <p id="cust_name"></p>
                              </td>
                              <td>
                                <p id="cust_id"></p>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>


<?php if($user->role == 1): ?>

    <div class="row">
        <div class="col-md-4">
            <form>
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <!-- <label><span class="red">*</span>Agent Name:</label> -->
                            <select name="userId" class="form-control" required="" id="userId">
                                <option value="">Choose One</option>
                                <?php $__currentLoopData = $use; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $us): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 

                                <?php if($us->id==$agent_id): ?>
                                <option value="<?php echo e($us->id); ?>" selected="" name="usID"><?php echo e($us->name); ?></option>
                                <?php else: ?>
                                <option value="<?php echo e($us->id); ?>" name="usID"><?php echo e($us->name); ?></option>
                                <?php endif; ?>

                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                        </div>
                    </div>
            
                    <div class="col-md-4">
                        <button type="submit"  class="btn btn-danger btn-sm">Search</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

<?php endif; ?>



    <div class="panel panel-flat" style="padding-top: 30px; padding-bottom: 30px;">
        <div class="container">
        <div class="card">
                    <div class="card-header header-elements-inline">
                        <!-- <h5 class="card-title">Event colors</h5> -->
                        <div class="header-elements">
                            <div class="list-icons">
                                <a class="list-icons-item" data-action="collapse"></a>
                                <a class="list-icons-item" data-action="reload"></a>
                                <a class="list-icons-item" data-action="remove"></a>
                            </div>
                        </div>
                    </div>
                    
                    <div class="card-body">
                        <!-- <p class="mb-3">FullCalendar allows you to change the color of all events on the calendar using the <code>eventColor</code> option. Also you can change text, border and background colors for events in a specific Event Source with <code>event source</code> options (backgroundColor, color, textColor and borderColor) and for individual events with <code>color</code> option, which sets an event's background and border color. Example below demonstrates event colors based on a day of the week.</p> -->

                        <div class="fullcalendar-basic"></div>
                    </div>
                </div>
    </div>
</div>
<div class="content">

</div>

<script type="text/javascript">

var obj = $('#userId').val();

    //$.get('/ajax-GetTrips', function(data){
        //$.get('/ajax-GetTrips', function(){

    $.get('ajax-GetTrips?u_I_d='+obj, function(data){
         $('.fullcalendar-basic').fullCalendar({
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,basicWeek,basicDay'
                },
                defaultDate: new Date(),
                editable: true,
                events: data, 

                eventClick: function (event, jsEvent, view) {
                   if(event['customer_Id'] != null && event['customer_Id'] != "" && event['customer_name'] != null && event['customer_name'] != "" && event['reservation_number'] != null && event['reservation_number'] != "")
                   {
                       $(".tr_Id").attr("href", href="<?php echo e(url('trip/view')); ?>/"+event['id']);
                       $("#reser_number").text(event['reservation_number']);
                       $("#cust_name").text(event['customer_name']);
                       $("#cust_id").text(event['customer_Id']);
                       $("#modal_theme_valuUP").modal("show");
                   }
                },

                dayClick: function (date, jsEvent, view) {
                   

                },

                eventDrop: function (event) {
                    //alert(event.title + " and on starting date " + event.start.toISOString() + " and ending date " + event.end.toISOString());
                  
                },
                eventResize: function (event) {
                    //alert(info.title + " end is now " + info.end.toISOString());
                                    
                }

            });
        });

</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout.dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>