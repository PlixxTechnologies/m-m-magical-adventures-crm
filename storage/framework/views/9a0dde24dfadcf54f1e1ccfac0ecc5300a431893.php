<?php $__env->startSection('content'); ?>

    <style>
    .red{
        color: red;
    }
    </style>
    <!-- Page header -->
    <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="<?php echo e(url('/home')); ?>">
                    <em class="fa fa-home"></em>
                </a></li>
                <li class="active">Edit Lead</li>
            </ol>
        </div>

        <div class="container-fluid"> 
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Edit Lead</h1>
                </div>
            </div>
       </div>

        <!--Delete Guest modal -->
        <div id="deleteGuestModal" class="modal">
            <div class="modal-dialog">
                <form  method="post" action="<?php echo e(url('leadguest/delete')); ?>"> 
                    <?php echo csrf_field(); ?>

                    <?php echo e(method_field('DELETE')); ?>

                    <div class="modal-content">
                        <div class="modal-header bg-danger">
                            <button type="button" class="close" data-dismiss="modal">×</button>
                            <h3 class="modal-title" style="color:#fff; font-weight: 600;">Delete Guest</h3>
                        </div>

                        <div class="modal-body">
                            <p style="font-size: 15px;">Are you sure you want to delete this guest?</p>
                        </div>

                        <input type="hidden" id="deleteGuestId" name="id" />

                        <div class="modal-footer">
                            <button type="button" class="btn btn-link legitRipple" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-danger legitRipple">Confirm</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <!--Update Guest modal -->
        <div id="updateGuestModal" class="modal" style="display:none">
            <div class="modal-dialog">
                <form  method="post" action="<?php echo e(url('leadguest/edit')); ?>"> 
                    <?php echo csrf_field(); ?>

                    <?php echo e(method_field('POST')); ?>

                    <div class="modal-content">
                        <div class="modal-header bg-primary">
                            <button type="button" class="close" data-dismiss="modal">×</button>
                            <h6 class="modal-title">Update Guest</h6>
                        </div>

                        <div class="modal-body">
                            <div class="form-group">
                                <label><span class="red">*</span>First Name:</label>
                                <input type="text" id="guest_first_name" name="guest_first_name" class="form-control" required>
                            </div>
                             <div class="form-group">
                                <label><span class="red">*</span>Last Name:</label>
                                <input type="text" id="guest_last_name" name="guest_last_name" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label>Birthdate:</label>
                                <input onfocus="GuestBirthDateType()" type="datetime" name="guest_birth_date"
                                 id="guest_birth_date" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Valid Passport:</label>
                                <select id="guest_passport" name="guest_passport" class="form-control">
                                   <!--  <option value="">Choose One....</option>
                                    <option value="Yes">Yes</option>
                                    <option value="No">No</option> -->
                                </select>
                            </div> 
                            <div class="form-group">
                                <label>Expiration Date:</label>
                                <input onfocus="GuestExpireDateType()" type="datetime" name="guest_expire_date"
                                 id="guest_expire_date" class="form-control">
                            </div>
                        </div>

                        <input type="hidden" id="updateGuestId" name="id" />

                        <div class="modal-footer">
                            <button type="button" class="btn btn-link legitRipple" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary legitRipple">Update</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-body">

                            <form action="<?php echo e(url('lead/edit').'/'.$lead->id); ?>" method="post" >
                                <?php echo csrf_field(); ?>


                                <input type="hidden" value="<?php echo e($lead->disney); ?>" id="disney">

                                <?php if($authuser->role == 1): ?>
                                    <div class="form-group">
                                        <label><span class="red">*</span>Agent Name:</label>
                                        <select name="userId" class="form-control" required>
                                        <?php $__currentLoopData = $allusers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php if($user->id == $lead->user_id): ?>
                                                <option value="<?php echo e($user->id); ?>" selected><?php echo e($user->name); ?></option>
                                            <?php endif; ?>    
                                            <option value="<?php echo e($user->id); ?>"><?php echo e($user->name); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                    </div>
                                <?php else: ?>
                                    <div class="form-group">
                                        <label>Agent Name:</label>
                                        <input type="text"  class="form-control" value="<?php echo e($user->name); ?>" readonly>
                                    </div>
                                <?php endif; ?>         

                                <div class="form-group">
                                    <label><span class="red">*</span>First Name:</label>
                                    <input type="text" name="first_name" value=" <?php echo e($lead->first_name); ?>" class="form-control" required>
                                </div>

                                <div class="form-group">
                                    <label><span class="red">*</span>Last Name:</label>
                                    <input type="text" name="last_name" value=" <?php echo e($lead->last_name); ?>" class="form-control" required>
                                </div>

                                <div class="form-group">
                                    <label><span class="red">*</span>Email:</label>
                                    <input type="email" name="email" value=" <?php echo e($lead->email); ?>" class="form-control" required>
                                </div>

                                <div class="form-group">
                                    <label>Country Code:</label>
                                    <input type="text" value="+1"  class="form-control" readonly="">
                                </div>

                                <div class="form-group">
                                    <label><span class="red">*</span>Phone Number:</label>
                                    <input type="text" name="phone_no" value=" <?php echo e($lead->phone_no); ?>" class="form-control" id="bar" onkeydown="javascript:backspacerDOWN(this,event);" onkeyup="javascript:backspacerUP(this,event);" required>
                                </div>

                                <div class="form-group">
                                    <label><span class="red">*</span>Address 1:</label>
                                    <input type="text" name="address1" value=" <?php echo e($lead->address1); ?>" class="form-control" required>
                                </div>

                                <div class="form-group">
                                    <label>Address 2:</label>
                                    <input type="text" name="address2" value=" <?php echo e($lead->address2); ?>" class="form-control">
                                </div>

                                <div class="form-group">
                                    <label><span class="red">*</span>City:</label>
                                    <input type="text" name="city" value=" <?php echo e($lead->city); ?>" class="form-control" required>
                                </div>

                                <div class="form-group">
                                    <label><span class="red">*</span>State:</label>
                                    <input type="text" maxlength="2" id="sessionNo" onkeypress="return isNumberKey(event)" name="state" class="form-control" value=" <?php echo e($lead->state); ?>" required>
                                </div>

                                <div class="form-group">
                                    <label><span class="red">*</span>Zip:</label>
                                    <input type="text" name="zip" value=" <?php echo e($lead->zip); ?>" class="form-control" required>
                                </div>

                                <div class="form-group">
                                    <label>Birthdate:</label>
                                    <input id="birth_date" onfocus="BirthDateType()" type="datetime" name="birth_date" value=" <?php echo e($lead->birth_date); ?>" class="form-control">
                                </div>
                             
                                 <!-- Customer Guest Table -->
                                 <input type="hidden" name="count" id="count" value=0 />
                                 <?php if(count($allUserGuests) > 0): ?>

                                  <hr />
                                  <?php $__currentLoopData = $allUserGuests; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $guest): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <div class="row">
                                        <div class="col-lg-10 text-left">
                                            <h5><strong>Guest No <?php echo e(++$count); ?></strong></h5>

                                        </div>
                                        <?php if($count == 2): ?>
                                        <div class="col-lg-2 ">
                                            <h5><strong>Modify Guest</strong></h5>
                                        </div>
                                        <?php endif; ?>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-2">
                                            <h5><strong>First Name</strong></h5>
                                            <h6><?php echo e($guest->guest_first_name); ?></h6>
                                        </div>
                                        <div class="col-lg-2">
                                             <h5><strong>Last Name</strong></h5>
                                            <h6><?php echo e($guest->guest_last_name); ?></h6>
                                        </div>
                                         <div class="col-lg-2">
                                             <h5><strong>Birth Date</strong></h5>
                                            <h6><?php echo e($guest->guest_birth_date); ?></h6>
                                        </div>
                                        <div class="col-lg-2">
                                             <h5><strong>Valid Passport</strong></h5>
                                            <h6><?php echo e($guest->guest_passport); ?></h6>
                                        </div>
                                        <div class="col-lg-2">
                                             <h5><strong>Expiration Date</strong></h5>
                                            <h6><?php echo e($guest->guest_expire_date); ?></h6>
                                        </div>
                                        <div class="col-lg-2">
                                            <br/>
                                            <button type="button" onclick="updateLeadGuest(<?php echo e($guest->id); ?>)" class="btn btn-sm btn-primary">Edit</button>
                                            <button type="button" onclick="deleteLeadGuest(<?php echo e($guest->id); ?>)" class="btn btn-sm btn-danger">DELETE</button>
                                        </div>
                                    </div>
                                   <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                <?php endif; ?>

                                <input type="hidden" name="countHeading" id="countHeading" value="<?php echo e($count); ?>" />

                                  <div id="guestSection"></div>

                                <?php if(count($allUserGuests) == 0): ?>
                                <div class="form-group">
                                    <button onclick="addGuest()" type="button" class="btn btn-success">Add Guest</button>
                                </div>  
                                <?php else: ?>
                                <div style="margin-right: 6em;" class="form-group">
                                    <button onclick="addGuest()" type="button" class="btn btn-success">Add Guest</button>
                                </div> 

                                <?php endif; ?>
                                <br />
                                <button type="submit" class="btn btn-primary pull-right">
                                    Update Lead
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">

        function BirthDateType(){
            $("#birth_date").attr('type', 'date');
        }

        function ExpireDateType(){
            $("#expire_date").attr('type', 'date');
        }

        function GuestBirthDateType(){
            $("#guest_birth_date").attr('type', 'date');
        }

        function GuestExpireDateType(){
            $("#guest_expire_date").attr('type', 'date');
        }

        function deleteLeadGuest(id) {

            $('#deleteGuestId').val(id);
            $('#deleteGuestModal').modal('show');

        }

        function updateLeadGuest(id) {

            $('#updateGuestId').val(id);

            $.get('/ajax-leadguestUpdate?guest_id='+id, function(data){

                console.log(data);

                $('#guest_first_name').val(data.guest_first_name);
                $('#guest_last_name').val(data.guest_last_name);
                $('#guest_birth_date').val(data.guest_birth_date);
                $('#guest_expire_date').val(data.guest_expire_date);

                if(data.guest_passport == null){
                    $("#guest_passport").append('<option selected value="">Choose One....</option>');
                    $("#guest_passport").append('<option value="Yes">Yes</option>');
                    $("#guest_passport").append('<option value="No">No</option>');
                }
                else if(data.guest_passport == "Yes"){
                    $("#guest_passport").append('<option value="">Choose One....</option>');
                    $("#guest_passport").append('<option selected value="Yes">Yes</option>');
                    $("#guest_passport").append('<option value="No">No</option>');
                }
                else if(data.guest_passport == "No"){
                    $("#guest_passport").append('<option value="">Choose One....</option>');
                    $("#guest_passport").append('<option value="Yes">Yes</option>');
                    $("#guest_passport").append('<option selected value="No">No</option>');
                }
               
            });

            $('#updateGuestModal').modal('show');
            // $('#updateGuestModal').show();

        }

       var guestCount = 1;
       var headingCount =  document.getElementById('countHeading').value;
       headingCount = parseInt(headingCount) + 1;

       function addGuest(){

            var objTo = document.getElementById('guestSection');

            var guestDiv = document.createElement("div");
            guestDiv.setAttribute("class", "form-group removeclass" + guestCount);

            var removeGuestDiv = 'removeclass' + guestCount;
           
            var guestHeading = '<h3><strong>Guest No '+ headingCount +':</strong></h3>';

            var firstname = '<div class="form-group"><label><span class="red">*</span>First Name:</label><input type="text" name="guest_first_name'+ guestCount +'" id="guest_first_name'+ guestCount +'"  class="form-control" required></div>';

            var lastname = '<div class="form-group"><label><span class="red">*</span>Last Name:</label><input type="text" name="guest_last_name'+ guestCount +'" id="guest_last_name'+ guestCount +'"  class="form-control" required></div>';

            var birthdate = '<div class="form-group"><label>Birthdate:</label><input type="date" name="guest_birth_date'+ guestCount +'" id="guest_birth_date'+ guestCount +'"  class="form-control"></div>';

            var passport = '<div class="form-group"> <label>Valid Passport:</label><select name="guest_passport'+ guestCount +'" id="guest_passport'+ guestCount +'" class="form-control"><option value="">Choose One....</option><option value="Yes">Yes</option><option value="No">No</option></select></div>';


            var expirationdate = '<div class="form-group"><label>Expiration Date:</label><input type="date" name="guest_expire_date'+ guestCount +'" id="guest_expire_date'+ guestCount +'" class="form-control"></div>';

            var hiddenGuestId = '<input type="hidden" name="guest_no' + headingCount + '" id="guest_no' + headingCount + '" value='+ headingCount +' />';

            var deleteGuest = '<div class="form-group"><button onclick="removeGuest(' + guestCount + ');" type="button" class="btn btn-danger pull-right">Remove Guest</button></div>';

             guestDiv.innerHTML = guestHeading + firstname + lastname + birthdate + passport + expirationdate 
             + hiddenGuestId + deleteGuest;

            objTo.appendChild(guestDiv);

            document.getElementById('count').value = guestCount;

            guestCount++;
            headingCount++;

       }

       function removeGuest(gid){

             $('.removeclass' + gid).remove();
             guestCount--;
             headingCount--;
             document.getElementById('count').value = guestCount;
       }
    </script>

    <script type="text/javascript">
        var zChar = new Array(' ', '(', ')', '-', '.');
        var maxphonelength = 13;
        var phonevalue1;
        var phonevalue2;
        var cursorposition;

        function ParseForNumber1(object) {
            phonevalue1 = ParseChar(object.value, zChar);
        }

        function ParseForNumber2(object) {
            phonevalue2 = ParseChar(object.value, zChar);
        }

        function backspacerUP(object, e) {
            if (e) {
                e = e
            } else {
                e = window.event
            }
            if (e.which) {
                var keycode = e.which
            } else {
                var keycode = e.keyCode
            }

            ParseForNumber1(object)

            if (keycode >= 48) {
                ValidatePhone(object)
            }
        }

        function backspacerDOWN(object, e) {
            if (e) {
                e = e
            } else {
                e = window.event
            }
            if (e.which) {
                var keycode = e.which
            } else {
                var keycode = e.keyCode
            }
            ParseForNumber2(object)
        }

        function GetCursorPosition() {

            var t1 = phonevalue1;
            var t2 = phonevalue2;
            var bool = false
            for (i = 0; i < t1.length; i++) {
                if (t1.substring(i, 1) != t2.substring(i, 1)) {
                    if (!bool) {
                        cursorposition = i
                        bool = true
                    }
                }
            }
        }

        function ValidatePhone(object) {

            var p = phonevalue1

            p = p.replace(/[^\d]*/gi, "")

            if (p.length < 3) {
                object.value = p
            } else if (p.length == 3) {
                pp = p;
                d4 = p.indexOf('(')
                d5 = p.indexOf(')')
                if (d4 == -1) {
                    pp = "(" + pp;
                }
                if (d5 == -1) {
                    pp = pp + ")";
                }
                object.value = pp;
            } else if (p.length > 3 && p.length < 7) {
                p = "(" + p;
                l30 = p.length;
                p30 = p.substring(0, 4);
                p30 = p30 + ")"

                p31 = p.substring(4, l30);
                pp = p30 + p31;

                object.value = pp;

            } else if (p.length >= 7) {
                p = "(" + p;
                l30 = p.length;
                p30 = p.substring(0, 4);
                p30 = p30 + ")"

                p31 = p.substring(4, l30);
                pp = p30 + p31;

                l40 = pp.length;
                p40 = pp.substring(0, 8);
                p40 = p40 + "-"

                p41 = pp.substring(8, l40);
                ppp = p40 + p41;

                object.value = ppp.substring(0, maxphonelength);
            }

            GetCursorPosition()

            if (cursorposition >= 0) {
                if (cursorposition == 0) {
                    cursorposition = 2
                } else if (cursorposition <= 2) {
                    cursorposition = cursorposition + 1
                } else if (cursorposition <= 5) {
                    cursorposition = cursorposition + 2
                } else if (cursorposition == 6) {
                    cursorposition = cursorposition + 2
                } else if (cursorposition == 7) {
                    cursorposition = cursorposition + 4
                    e1 = object.value.indexOf(')')
                    e2 = object.value.indexOf('-')
                    if (e1 > -1 && e2 > -1) {
                        if (e2 - e1 == 4) {
                            cursorposition = cursorposition - 1
                        }
                    }
                } else if (cursorposition < 11) {
                    cursorposition = cursorposition + 3
                } else if (cursorposition == 11) {
                    cursorposition = cursorposition + 1
                } else if (cursorposition >= 12) {
                    cursorposition = cursorposition
                }

                var txtRange = object.createTextRange();
                txtRange.moveStart("character", cursorposition);
                txtRange.moveEnd("character", cursorposition - object.value.length);
                txtRange.select();
            }

        }

        function ParseChar(sStr, sChar) {
            if (sChar.length == null) {
                zChar = new Array(sChar);
            } else zChar = sChar;

            for (i = 0; i < zChar.length; i++) {
                sNewStr = "";

                var iStart = 0;
                var iEnd = sStr.indexOf(sChar[i]);

                while (iEnd != -1) {
                    sNewStr += sStr.substring(iStart, iEnd);
                    iStart = iEnd + 1;
                    iEnd = sStr.indexOf(sChar[i], iStart);
                }
                sNewStr += sStr.substring(sStr.lastIndexOf(sChar[i]) + 1, sStr.length);

                sStr = sNewStr;
            }

            return sNewStr;
        }
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout.dashboardx', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>