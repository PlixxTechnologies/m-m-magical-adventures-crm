<?php $__env->startSection('content'); ?>

 <!-- Page header -->
                <div class="page-header page-header-default">
                    <div class="page-header-content">
                        <div class="page-title">
                            <h4><span class="text-semibold">List Text Messages</span></h4>
                        </div>
                    </div>
                </div>
   <!-- /page header -->

<!-- Basic initialization -->
                    <div class="panel panel-flat">

                        <table class="table datatable-button-init-basic">
                            <thead>
                                <tr>
                                <th data-sortable="true">Agent Name</th>
                                <th data-sortable="true">Customer Name</th>
                                <th data-sortable="true">Country Code</th>
                                <th data-sortable="true">Phone Number</th>
                                <th data-sortable="true">Message</th>
                            </tr>
                            </thead>
                            <tbody>
                                  <?php $__currentLoopData = $textmessages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $textmessage): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                      
                                <tr>
                                    <?php if($textmessage->user == null): ?>
                                    <td>
                                         No Agent
                                    </td>
                                    <?php else: ?>
                                    <td>
                                         <?php echo e($textmessage->user->name); ?>

                                    </td>
                                    <?php endif; ?>

                                    <?php if($textmessage->customer == null): ?>
                                        <td>
                                            No Customer
                                        </td>
                                    <?php else: ?>
                                        <td>
                                            <?php echo e($textmessage->customer->first_name); ?> <?php echo e($textmessage->customer->last_name); ?>

                                        </td>
                                    <?php endif; ?>
                                    <td>
                                        <?php echo e($textmessage->country_code); ?>

                                    </td>
                                   
                                    <td>
                                        <?php echo e($textmessage->number); ?>

                                    </td>
                                    <td>
                                        <?php echo e($textmessage->message); ?>

                                    </td>
                                  </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                               
                            </tbody>
                        </table>
                    </div>
                    <!-- /basic initialization -->   
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>