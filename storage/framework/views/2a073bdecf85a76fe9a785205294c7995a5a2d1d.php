<?php $__env->startSection('content'); ?>
<!-- Datatables JS FILES -->

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.print.min.js"></script>

<!-- /Datatables JS FILES -->  
<style>
    .red{
color:red;
    }
</style>

<!-- Page header -->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="<?php echo e(url('/home')); ?>">
                    <em class="fa fa-home"></em>
                </a></li>
                <li class="active">Commission Report</li>
            </ol>
        </div><!--/.row-->
    <div class="container-fluid"> 
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Commission Report</h1>
            </div>
        </div>  <!-- /page header -->
   </div>

<div class="container-fluid">

   <?php if(Auth::user()->role == 1): ?> 
<div class="row">
    <form action="<?php echo e(url('commisions-list')); ?>" method="post">
        <?php echo csrf_field(); ?>

        <input type="hidden" name="id" value="<?php echo e(Auth::user()->id); ?>">
      
        <div class="col-md-3 col-lg-3" style="padding: 20px;">
            <div class="form-group">
                <label><span class="red">*</span>Agent:</label>
                <select name="role" id="agentId" class="form-control" required>
                    <option value="All" <?php if($selectedagentid == "All"): ?> selected <?php endif; ?>>All Agents</option>
                    <?php $__currentLoopData = $agents; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $agent): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="<?php echo e($agent->id); ?>" <?php if($selectedagentid == $agent->id): ?> selected <?php endif; ?>><?php echo e($agent->name); ?></option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </select>  
            </div>
        </div>

        <div class="col-md-3 col-lg-3" style="padding: 20px;">
            <div class="form-group">
                <label><span class="red">*</span>Check In Date:</label>
                <input type="date" name="startdate" id="startdate" class="form-control" <?php if($startdate != null): ?> value="<?php echo e($startdate); ?>" <?php endif; ?> required>
            </div>
        </div>

        <div class="col-md-3 col-lg-3" style="padding: 20px;">
            <div class="form-group">
                <label><span class="red">*</span>Check Out Date:</label>
                <input type="date" name="enddate" id="enddate" class="form-control"  <?php if($enddate != null): ?> value="<?php echo e($enddate); ?>" <?php endif; ?> required>
            </div>
        </div>
        <div class="col-md-offset-9" style="padding-top: 45px;">
            <button type="submit" class="btn btn-primary" >Get Report</button>
        </div>
    </form>
</div>

<?php else: ?>

<div class="row">
    <form action="<?php echo e(url('commisions-list')); ?>" method="post">
        <?php echo csrf_field(); ?>

        <input type="hidden" name="id" id="agentId" value="<?php echo e(Auth::user()->id); ?>">
        <div class="col-md-4 col-lg-4" style="padding: 20px;">
            <div class="form-group">
                <label><span class="red">*</span>Check In Date:</label>
                <input type="date" name="startdate" id="startdate" class="form-control" <?php if($startdate != null): ?> value="<?php echo e($startdate); ?>" <?php endif; ?> required>
            </div>
        </div>

        <div class=" col-md-4 col-lg-4" style="padding: 20px;">
            <div class="form-group">
                <label><span class="red">*</span>Check Out Date:</label>
                <input type="date" name="enddate" id="enddate" class="form-control"  <?php if($enddate != null): ?> value="<?php echo e($enddate); ?>" <?php endif; ?> required>
            </div>
        </div>
        <div class="col-md-4 col-lg-4" style="padding-top: 47px;"> 
            <button type="submit" class="btn btn-primary">Get Report</button>
        </div>
    </form>
</div>
<?php endif; ?>
<br>

<?php if($report != null || $report != ""): ?>
    <?php if(Auth::user()->role == 1): ?>
        <!-- Basic initialization -->
        <div style="padding-top:20px; padding-bottom:20px; padding-right:20px; padding-left:20px;overflow-x: auto;"  class="panel panel-flat">
            <div class="panel-heading">
                <div class="col-md-12">
                    <!-- <h5 class="panel-title">Trips List</h5> -->
                </div>
            </div>

            <table id="example" class="display nowrap" style="width:100%">
                <thead>
                    <tr>
                        <th data-sortable="true">Report ID</th>
                        <th data-sortable="true">Agent Name</th>
                        <th data-sortable="true">Report Creation Date</th>
                        <th data-sortable="true">Export Date</th>
                        <th data-sortable="true">Payment Status</th>
                        <th data-sortable="true">Expected Commission</th>
                        <th data-sortable="true">YTD Commission</th>
                        <th data-sortable="true">Action</th>
                    </tr>
                </thead>
                <tbody>
                      <?php $__currentLoopData = $report; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $reports): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <tr>
                        <td>
                            <?php echo e($reports->id); ?>

                        </td>
                        <td>
                            <?php echo e($reports->agentName); ?>

                        </td>
                        <td>
                             <?php echo e(date('m-d-Y', strtotime($reports->creation_date))); ?>

                        </td>
                        <td>
                            <?php echo e(date('m-d-Y', strtotime($reports->export_date))); ?>

                        </td>
                        <td>
                        <?php if($reports->isActive != 0): ?>
                        <form id="reportForm" action="<?php echo e(url('AdminstatusChange')); ?>" method="post" >
                            <?php echo csrf_field(); ?>

                            <?php
                                $abc = "status".$reports->id;
                             ?>
                            <select onchange="Report('<?php echo e($reports->id); ?>')" id="<?php echo e($abc); ?>">
                                <?php if($reports->paymentstatus == 1): ?>
                                    <option value="1" selected="selected">Processed</option>
                                    <option value="2">Processing</option>
                                    <option value="3">Incomplete</option>
                                    <option value="4">Canceled</option>
                                    <option value="5">Paid</option>
                                    <option value="6">Awaiting Supplier</option>
                                
                                <?php elseif($reports->paymentstatus == 2): ?>
                                    <option value="1" >Processed</option>
                                    <option value="2" selected="selected">Processing</option>
                                    <option value="3">Incomplete</option>
                                    <option value="4">Canceled</option>
                                    <option value="5">Paid</option>
                                    <option value="6">Awaiting Supplier</option>
                                
                                <?php elseif($reports->paymentstatus == 3): ?>
                                    <option value="1" >Processed</option>
                                    <option value="2" >Processing</option>
                                    <option value="3" selected="selected">Incomplete</option>
                                    <option value="4">Canceled</option>
                                    <option value="5">Paid</option>
                                    <option value="6">Awaiting Supplier</option>
                                <?php elseif($reports->paymentstatus == 4): ?>
                                    <option value="1" >Processed</option>
                                    <option value="2">Processing</option>
                                    <option value="3">Incomplete</option>
                                    <option value="4" selected="selected">Canceled</option>
                                    <option value="5">Paid</option>
                                    <option value="6">Awaiting Supplier</option>
                                <?php elseif($reports->paymentstatus == 5): ?>
                                    <option value="1" >Processed</option>
                                    <option value="2">Processing</option>
                                    <option value="3">Incomplete</option>
                                    <option value="4">Canceled</option>
                                    <option value="5" selected="selected">Paid</option>
                                    <option value="6">Awaiting Supplier</option>
                                <?php elseif($reports->paymentstatus == 6): ?>
                                    <option value="1" >Processed</option>
                                    <option value="2">Processing</option>
                                    <option value="3">Incomplete</option>
                                    <option value="4">Canceled</option>
                                    <option value="5">Paid</option>
                                    <option value="6" selected="selected">Awaiting Supplier</option>  
                                <?php else: ?>
                                    <option value="" selected="selected">Select Status</option>
                                    <option value="1" >Processed</option>
                                    <option value="2">Processing</option>
                                    <option value="3">Incomplete</option>
                                    <option value="4">Canceled</option>
                                    <option value="5">Paid</option>
                                    <option value="6">Awaiting Supplier</option>  
                                <?php endif; ?>
                            </select>
                            <input type="hidden" name="status" id="status1">
                            <input type="hidden" name="role" id="agentIdss">
                            <input type="hidden" name="hid" id="hid">
                            <input type="hidden" name="startdate" id="startdatess">
                            <input type="hidden" name="enddate" id="enddatess">
                        </form>
                        <?php else: ?>
                        <button class="btn btn-danger">Deleted</button>
                        <!-- <?php endif; ?> -->
                        </td>
                        <td>
                           <strong>$</strong><?php echo e(number_format($reports->expectedcommssion, 2)); ?> 
                        </td>
                        <td>
                            <strong>$</strong><?php echo e(number_format($reports->commissionTotal, 2)); ?>

                           <!-- ytdcommission -->
                        </td>
                        <td class="center">
                            <?php if($reports->isActive != 0): ?>
                            <a class="btn btn-info" href="<?php echo e(url('commisions/edit').'/'.$reports->id); ?>" target="_blank">EDIT</a>
                            <?php endif; ?>
                            <a target="_blank" href="<?php echo e(url('commisions/saved-reports').'/'.$reports->id); ?>" class="btn btn-success" id="savereportId" value="<?php echo e($reports->id); ?>">VIEW</a>
                            <?php if($reports->isActive != 0): ?>
                            <form style="display:inline" method="post" action="<?php echo e(url('commisions/delete')); ?>/<?php echo e($reports->id); ?>" onsubmit="return confirm('Are you sure you want to delete this Report?');">
                                <?php echo csrf_field(); ?>

                                <?php echo e(method_field('DELETE')); ?>

                                <button type="submit" class="btn btn-danger">DELETE</button>
                            </form>
                            <?php endif; ?>
                            <a target="_blank" class="btn" style="background: #f1f1f1; color: #000000" href="<?php echo e(url('commisions/history').'/'.$reports->id); ?>">History</a>
                        </td>
                      </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                               
                </tbody>                            
            </table>
        </div>
        <!-- /basic initialization -->
    <?php endif; ?>
    <?php if(Auth::user()->role != 1): ?>
        <!-- Basic initialization -->
        <div style="padding-top:20px; padding-bottom:20px; padding-right:20px; padding-left:20px;overflow-x: auto;"   class="panel panel-flat">
            <div class="panel-heading">
                <div class="col-md-12">
                    <!-- <h5 class="panel-title">Trips List</h5> -->
                </div>
            </div>

            <table id="example" class="display nowrap text-center" style="width:100%">
                <thead>
                    <tr>
                        <th data-sortable="true">Report ID</th>
                        <th data-sortable="true">Agent Name</th>
                        <th data-sortable="true">Travel Month</th>
                        <th data-sortable="true">Report Creation Date</th>
                        <th data-sortable="true">Export Date</th>
                        <th data-sortable="true">Payment Status</th>
                        <th data-sortable="true">Expected Commission</th>
                        <th data-sortable="true">Commissions Paid</th>
                        <th data-sortable="true">Cancel Request</th>
                        <th data-sortable="true">Action</th>
                    </tr>
                </thead>
                <tbody>
                      <?php $__currentLoopData = $report; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $reports): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <?php if($reports->isActive != 0): ?>
                      <tr>
                        <td>
                            <?php echo e($reports->id); ?>

                        </td>              
                        <td>
                             <?php echo e(Auth::user()->name); ?>

                        </td>
                        <td>
                            <?php echo e($reports->travelmonth); ?>

                        </td>
                        <td>
                            <?php echo e(date('m-d-Y', strtotime($reports->creation_date))); ?>

                        </td>
                        <td>
                            <?php echo e(date('m-d-Y', strtotime($reports->export_date))); ?>

                        </td>
                        <?php if($reports->paymentstatus == 1): ?>
                        <td>
                            <span class="bg-success p-5">Processed</span>
                        </td>
                        <?php endif; ?>
                        <?php if($reports->paymentstatus == 2): ?>
                        <td>
                            <span class="bg-primary p-5">Processing</span>
                        </td>
                        <?php endif; ?>
                        <?php if($reports->paymentstatus == 3): ?>
                        <td>
                            <span class="bg-danger p-5">Incomplete</span>
                        </td>
                        <?php endif; ?>
                        <?php if($reports->paymentstatus == 4): ?>
                        <td>
                            <span class="p-5" style="background:#f1f1f1;">Canceled</span>
                        </td>
                        <?php endif; ?>
                        <?php if($reports->paymentstatus == 5): ?>
                        <td>
                            <span class="bg-success p-5">Processed</span>
                        </td>
                        <?php endif; ?>
                        <?php if($reports->paymentstatus == 6): ?>
                        <td>
                            <span class="bg-warning p-5">Awaiting_Supplier</span>
                        </td>
                        <?php endif; ?>
                        <td>
                           <strong>$</strong><?php echo e($reports->expectedcommssion); ?> 
                        </td>
                        <td>
                            <!-- <strong>$</strong><?php echo e($reports->ytdcommission); ?> -->
                        </td>
                        <td>
                            <form id="cancelrequestForm" method="post" action="<?php echo e(url('cancel-request').'/'.$reports->id); ?>">
                                <?php echo csrf_field(); ?>

                                <input type="hidden" name="role" id="agentIds">
                                <input type="hidden" name="startdate" id="startdates">
                                <input type="hidden" name="enddate" id="enddates">
                                <button class="btn btn-secondary btn-sm" onclick="CancelRequest('<?php echo e($reports->id); ?>')">Cancel Request</button>
                            </form>
                        </td>
                        <td>
                            <form style="display:inline" method="post" action="<?php echo e(url('commisions/delete')); ?>/<?php echo e($reports->id); ?>" onsubmit="return confirm('Are you sure you want to delete this Report?');">
                                <?php echo csrf_field(); ?>

                                <?php echo e(method_field('DELETE')); ?>

                                <button type="submit" class="btn btn-danger">DELETE</button>
                            </form>
                        </td>
                      </tr>
                      <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                               
                </tbody>                            
            </table>
        </div>
        <!-- /basic initialization -->
    <?php endif; ?>
<?php endif; ?>


</div>
</div>

<script type="text/javascript">

    function Report(reportid) {
        var status = $('#status'+reportid).val();
        var objAgent = $('#agentId').val();
        var objstartdate = $('#startdate').val();
        var objenddate = $('#enddate').val();
        if(status != "" && status != undefined && status != null && reportid != "" && reportid != undefined && reportid != null){
            $("#hid").val(reportid);
            $('#agentIdss').val(objAgent);
            $('#startdatess').val(objstartdate);
            $('#enddatess').val(objenddate);
            $('#status1').val(status);
            $("#reportForm").submit();
        }

    }
    function CancelRequest(id){
        var objAgent = $('#agentId').val();
        var objstartdate = $('#startdate').val();
        var objenddate = $('#enddate').val();



        if(objAgent != "" && objAgent != undefined && objAgent != null &&
         objstartdate != "" && objstartdate != undefined && objstartdate != null &&
         objenddate != "" && objenddate != undefined && objenddate != null){
            $('#agentIds').val(objAgent);
            $('#startdates').val(objstartdate);
            $('#enddates').val(objenddate);
            $("#cancelrequestForm").submit();
        }
    }

    function viewReportData(id) {
        var rId = document.getElementById('savereportId').value;
        console.log(id);

        $.ajax({
            url: 'reports-view',
            type: "get",
            data: {
                ReportId:id
            },
            success: function(response){
                   if(response['Id'] != null || response['Id'] != "")
                   {
                    var tr;
                    for (var i = 0; i < response.length; i++) {
                        tr = $('<tr/>');  
                        tr.append("<td>" + response[i].Id + "</td>");  
                        tr.append("<td>" + response[i].save_reports['Id'] + "</td>");  
                        // tr.append("<td>" + response[i].City + "</td>");  
                        // tr.append("<td>" + response[i].Address + "</td>");
                        $('.custom-table').append(tr);              
                    }
                    $("#modal_theme_reports").modal("show");
                   }
                }
        });
        
    }
</script>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('admin.layout.dashboardx', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>