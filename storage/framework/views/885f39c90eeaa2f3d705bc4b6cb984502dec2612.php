<?php $__env->startSection('content'); ?>

<!-- Page header -->
                <div class="page-header page-header-default">
                    <div class="page-header-content">
                        <div class="page-title">
                            <h4><span class="text-semibold">List Destinations</span></h4>
                        </div>
                    </div>
                </div>
   <!-- /page header -->

<!-- Basic initialization -->
                    <div class="panel panel-flat">

                        <table class="table datatable-button-init-basic">
                            <thead>
                                <tr>
                                <tr>
                               <!--  <th data-sortable="true" >ID</th> -->
                                <th data-sortable="true">Name</th>
                                <?php if($user->role == 1): ?>
                                    <th>Action</th>
                                    <?php endif; ?>
                            </tr>
                            </tr>
                            </thead>
                            <tbody>
                                 <?php $__currentLoopData = $destinations; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $destination): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <!-- <td id="order-id">
                                        <?php echo e($destination->id); ?>

                                    </td> -->
                                    <td>
                                         <?php echo e($destination->name); ?>

                                    </td>
                            <?php if($user->role == 1): ?>
                                    <td class="center">
                                        <a class="btn btn-info" href="<?php echo e(url('destination/edit').'/'.$destination->id); ?>">EDIT</a>
                                        <form style="display:inline" method="post" action="<?php echo e(url('destination/delete')); ?>/<?php echo e($destination->id); ?>" onsubmit="return confirm('Are you sure you want to delete this destination?');">
                                            <?php echo csrf_field(); ?>

                                            <?php echo e(method_field('DELETE')); ?>

                                            <button type="submit" class="btn btn-danger">DELETE</button>
                                        </form>
                                    </td>
                                <?php endif; ?>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                             
                            </tbody>
                        </table>
                    </div>
                    <!-- /basic initialization -->
   
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout.dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>