<?php $__env->startSection('content'); ?>

<style>
    .red{
color:red;
    }
</style>
<!-- Page header -->
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><span class="text-semibold">View Reports Data</span></h4>
            </div>
        </div>
    </div>
<!-- /page header -->

<style type="text/css">
    .modal-content{
        width: 150%;
    }
</style>

<div id="modal_theme_reports" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <form method="post">
            <div class="modal-content">
                <div class="modal-header bg-primary text-center">
                    <h3 class="modal-title text-center">Trips Information</h3>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <div class="modal-body">
                    <input type="hidden" id="xyz" name="xyz" />
                    <div class="row">
                        <table class="table custom-table">
                          <thead>
                            <tr>
                              <th scope="col">ReportID</th>
                              <th scope="col">Agent Name</th>
                              <th scope="col">Travel Month</th>
                              <th scope="col">Report Creation Date</th>
                              <th scope="col">Export Date</th>
                              <th scope="col">Payment Status</th>
                              <th scope="col">Expected Commission</th>
                              <th scope="col">YTD Commission</th>
                              <th scope="col">Action</th>
                            </tr>
                          </thead>
                          <tbody>
                          </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<div class="container">

<?php if($Savereports != null || $Savereports != ""): ?>
    <?php if(Auth::user()->role == 1): ?>
        <!-- Basic initialization -->
        <div class="panel panel-flat">
            <div class="panel-heading">
                <div class="col-md-12">
                    <!-- <h5 class="panel-title">View Reports Data</h5> -->
                </div>
            </div>

            <table class="table datatable-button-init-basic">
                <thead>
                    <tr>
                        <th data-sortable="true">Agent Name</th>
                        <th data-sortable="true">Customer Name</th>
                        <th data-sortable="true">Reservation Number</th>
                        <th data-sortable="true">Destination</th>
                        <th data-sortable="true">Check In Date</th>
                        <th data-sortable="true">Check Out Date</th>
                        <th data-sortable="true">Commission</th>
                        <th data-sortable="true">Expected Commission</th>
                        <th data-sortable="true">Set Status</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $__currentLoopData = $Savereports; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $reports): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            
                            <td>
                                <?php echo e($reports->agentName); ?>

                            </td>
                            <td>
                                <?php echo e($reports->CustomerName); ?>

                            </td>

                            <td>
                                <?php echo e($reports->reservation); ?>

                            </td>
                            <td>
                                <?php echo e($reports->destination); ?>

                            </td>

                            <td>
                                <?php echo e(date('m-d-Y', strtotime($reports->CheckinDate))); ?>

                            </td>
                            <td>
                                <?php echo e(date('m-d-Y', strtotime($reports->CheckoutDate))); ?>

                            </td>
                            <td>
                                <?php echo e($reports->ytdcommission); ?>

                            </td>
                            <td>
                                <?php echo e($reports->expectedcommsion); ?>

                            </td>
                            <?php if($getpay == 0): ?>
                                <?php if($reports->paymentstatus == 1): ?>
                                <td>
                                    <span class="bg-success p-5">Processed</span>
                                </td>
                                <?php endif; ?>
                                <?php if($reports->paymentstatus == 2): ?>
                                <td>
                                    <span class="bg-primary p-5">Processing</span>
                                </td>
                                <?php endif; ?>
                                <?php if($reports->paymentstatus == 3): ?>
                                <td>
                                    <span class="bg-danger p-5">Incomplete</span>
                                </td>
                                <?php endif; ?>
                                <?php if($reports->paymentstatus == 4): ?>
                                <td>
                                    <span class="p-5" style="background:#f1f1f1;">Canceled</span>
                                </td>
                                <?php endif; ?>
                                <?php if($reports->paymentstatus == 5): ?>
                                <td>
                                    <span class="bg-success p-5">Paid</span>
                                </td>
                                <?php endif; ?>
                                <?php if($reports->paymentstatus == 6): ?>
                                <td>
                                    <span class="bg-warning p-5">Awaiting_Supplier</span>
                                </td>
                                <?php endif; ?>
                            <?php endif; ?>    
                            <?php if($getpay != 0): ?>
                                <?php if($reports->paymentstatus == 1): ?>
                                <td>
                                    <span class="bg-success p-5">Processed</span>
                                </td>
                                <?php endif; ?>
                                <?php if($reports->paymentstatus == 2): ?>
                                <td>
                                    <span class="bg-primary p-5">Processing</span>
                                </td>
                                <?php endif; ?>
                                <?php if($reports->paymentstatus == 3): ?>
                                <td>
                                    <span class="bg-danger p-5">Incomplete</span>
                                </td>
                                <?php endif; ?>
                                <?php if($reports->paymentstatus == 4): ?>
                                <td>
                                    <span class="p-5" style="background:#f1f1f1;">Canceled</span>
                                </td>
                                <?php endif; ?>
                                <?php if($reports->paymentstatus == 5): ?>
                                <td>
                                    <span class="bg-success p-5">Paid</span>
                                </td>
                                <?php endif; ?>
                                <?php if($reports->paymentstatus == 6): ?>
                                <td>
                                    <span class="bg-warning p-6">Awaiting_Supplier</span>
                                </td>
                                <?php endif; ?>
                            <?php endif; ?>                            
                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                               
                </tbody>                            
            </table>
        </div>
        <!-- /basic initialization -->
    <?php endif; ?>
    <?php if(Auth::user()->role != 1): ?>
        <!-- Basic initialization -->
        <div class="panel panel-flat">
            <div class="panel-heading">
                <div class="col-md-12">
                    <h5 class="panel-title">Trips List</h5>
                </div>
            </div>

            <table class="table datatable-button-init-custom">
                <thead>
                    <tr>
                    <th data-sortable="true">Agent Name</th>
                    <th data-sortable="true">Customer Name</th>
                    <th data-sortable="true">Reservation Number</th>
                    <th data-sortable="true">Destination</th>
                    <th data-sortable="true">Check In Date</th>
                    <th data-sortable="true">Check Out Date</th>
                    <th data-sortable="true">Commission</th>
                    <th data-sortable="true">Expected Commission</th>
                    <th data-sortable="true">Set Status</th>
                </tr>
                </thead>
                <tbody>
                      <?php $__currentLoopData = $Savereports; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $trip): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <td>
                            Null
                        </td>
                        <td>
                            Null
                        </td>
                        <td>
                            <?php echo e($trip->reservation_number); ?>

                        </td>   
                        <td>
                            <?php echo e($trip->destination); ?>

                        </td>                                   
                        <td>
                            <?php echo e($trip->checkin_date); ?>

                        </td>
                       <td>
                            <?php echo e($trip->checkout_date); ?>

                       </td>
                        <td>
                            <?php echo e($trip->commission); ?>

                        </td>
                         <td>
                            <?php echo e($trip->total_sale); ?>

                        </td>
                        <td>
                            <?php echo e($trip->expected_commission); ?>

                        </td>
                       <!-- <td>
                           <?php echo e(Carbon\Carbon::parse($trip->created_at)->format('d M Y')); ?>

                        </td> -->
                      </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                               
                </tbody>                            
            </table>
        </div>
        <!-- /basic initialization -->
    <?php endif; ?>
<?php endif; ?>

</div>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('admin.layout.dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>