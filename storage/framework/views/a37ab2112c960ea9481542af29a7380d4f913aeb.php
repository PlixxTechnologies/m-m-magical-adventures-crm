<?php $__env->startSection('content'); ?>

<!-- Datatables JS FILES -->

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.print.min.js"></script>

<!-- /Datatables JS FILES -->

<!-- Page header -->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="<?php echo e(url('/home')); ?>">
                    <em class="fa fa-home"></em>
                </a></li>
                <li class="active">List Agent</li>
            </ol>
        </div><!--/.row-->
    <div class="container-fluid"> 
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">List Agent</h1>
            </div>
        </div>  <!-- /page header -->
   </div>

<!-- Basic initialization -->
   <div style="padding-top:20px; padding-bottom:20px; padding-right:20px; padding-left:20px;" class="panel panel-flat">
                <div style="overflow-x:auto;">        
                  <!--   <table class="table datatable-button-init-basic"> -->
                    <table id="example" class="display nowrap" style="width:100%">
                            <thead>
                               <tr>
                                <th>Action</th>
                                <th  data-sortable="true" >ID</th>
                                <th  data-sortable="true">Name</th>
                                <th  data-sortable="true">Email</th>
                                <th  data-sortable="true">Phone</th>
                                <th  data-sortable="true">Adddress</th>
                                <th  data-sortable="true">Contract Signed Date</th>
                                <th  data-sortable="true">Commission Rate</th>
                                <th  data-sortable="true">Role</th>
                                <th  data-sortable="true">Sales Goal</th>
                                <th  data-sortable="true">Anniversary Date</th>
                                <!-- <th  data-sortable="true">FDACS Date</th> -->
                                <th  data-sortable="true">Status</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                     <td class="center">
                                   <a class="btn btn-info" href="<?php echo e(url('user/edit').'/'.$user->id); ?>">
                                            EDIT
                                        </a>
                                       <form style="display:inline" method="post" action="<?php echo e(url('user/delete')); ?>/<?php echo e($user->id); ?>" onsubmit="return confirm('Are you sure you want to delete this agent?');">
                                            <?php echo csrf_field(); ?>

                                            <?php echo e(method_field('DELETE')); ?>

                                            <button type="submit" class="btn btn-danger">DELETE</button>
                                        </form>
                                    </td>
                                    <td>
                                        <?php echo e($user->id); ?>

                                    </td>
                                    <td>
                                        <?php echo e($user->name); ?>

                                    </td>
                                    <td>
                                        <?php echo e($user->email); ?>

                                    </td>
                                     <td>
                                        <?php echo e($user->phone); ?>

                                    </td>
                                     <td>
                                        <?php echo e($user->address); ?>

                                    </td>
                                     <td>
                                        <?php echo e(date("m", strtotime($user->contract_date))); ?>-<?php echo e(date("d", strtotime($user->contract_date))); ?>-<?php echo e(date("Y", strtotime($user->contract_date))); ?>

                                    </td>
                                    <?php if($user->commission == null): ?>                                    
                                    <td>
                                       Not Entered
                                    </td>
                                    <?php else: ?>
                                    <td>
                                         <?php echo e($user->commission); ?>%
                                    </td>
                                    <?php endif; ?>
                                    <?php if($user->role == 1): ?>
                                    <td>
                                        Admin
                                    </td>
                                <?php else: ?>
                                    <td>
                                        Agent
                                    </td>
                                <?php endif; ?>

                                <?php if($user->sales_goal == null): ?>
                                    <td>
                                        Not Entered
                                    </td>
                                <?php else: ?>
                                    <td>
                                        <?php echo e(number_format($user->sales_goal, 2)); ?>

                                    </td>
                                <?php endif; ?>
                                <td>
                                    <?php echo e(\Carbon\Carbon::parse($user->fdacs_date)->format('m-d-Y')); ?>

                                    
                                </td>
                                <td class="text-capitalize">
                                    <?php echo e($user->status); ?>

                                </td>                                                         
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                       
                            </tbody>
                        </table>
                      </div>
                    </div>
                </div>
                    <!-- /basic initialization -->

  <!--   <script scr="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script scr="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
    <script scr="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap4.min.js"></script>
    <script scr="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
    <script scr="https://cdn.datatables.net/buttons/1.7.0/js/buttons.bootstrap4.min.js"></script>
    <script scr="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script scr="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script scr="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
    <script scr="https://cdn.datatables.net/buttons/1.7.0/js/buttons.print.min.js"></script>
    <script scr="https://cdn.datatables.net/buttons/1.7.0/js/buttons.colVis.min.js"></script>

                    <script>
                            $(document).ready(function() {
                                var table = $('#example').DataTable( {
                                    lengthChange: false,
                                    buttons: [ 'copy', 'excel', 'pdf', 'colvis' ]
                                } );
                             
                                table.buttons().container()
                                    .appendTo( '#example_wrapper .col-md-6:eq(0)' );
                            } );
                    </script> -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.dashboardx', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>