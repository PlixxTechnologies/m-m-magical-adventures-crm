<?php $__env->startSection('content'); ?>
<!-- Datatables JS FILES -->

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.print.min.js"></script>

<!-- /Datatables JS FILES -->
<style>
.red{
    color: red;
}
</style>
<?php if(\Session::has('error')): ?>
    <div class="alert alert-danger">
        <ul>
            <li><?php echo \Session::get('error'); ?></li>
        </ul>
    </div>
<?php endif; ?>
<!-- Page header -->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="<?php echo e(url('/home')); ?>">
                    <em class="fa fa-home"></em>
                </a></li>
                <li class="active">Edit Agent</li>
            </ol>
        </div><!--/.row-->
    <div class="container-fluid"> 
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Edit Agent</h1>
            </div>
        </div>  <!-- /page header -->
   </div>
    <div class="container-fluid">
        <br>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">

                        <form action="<?php echo e(url('user/update/'.$user->id)); ?>" method="post">
                            <?php echo csrf_field(); ?>

                            
                            <div class="form-group">
                                <label><span class="red">*</span>Name:</label>
                                <input type="text" name="name" value="<?php echo e($user->name); ?>" class="form-control" required>
                                <?php if($errors->has('name')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('name')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                            <div class="form-group">
                                <label><span class="red">*</span>Email:</label>
                                <input type="email" name="email" value="<?php echo e($user->email); ?>" class="form-control" required>
                                <?php if($errors->has('email')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('email')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                            <div class="form-group">
                                <label><span class="red">*</span>Phone Number:</label>
                                <input type="text" name="phone" value="<?php echo e($user->phone); ?>" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label><span class="red">*</span>Address:</label>
                                <input type="text" name="address" value="<?php echo e($user->address); ?>" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label><span class="red">*</span>Contract Signed Date:</label>
                                <input type="date" name="contract_date" value="<?php echo e($user->contract_date); ?>" class="form-control"  required>
                            </div>
                            <div class="form-group">
                                <label><span class="red">*</span>Role:</label>
                                <select name="role" class="form-control" required>
                                    <option value= 0 <?php if($user->role == 0): ?> selected <?php endif; ?>>Agent</option>
                                    <option value= 1 <?php if($user->role == 1): ?> selected <?php endif; ?>>Admin</option>

                                </select>
                            </div>
                            <div class="form-group">
                                <label><span class="red">*</span>Commission:</label>
                                <select name="commission" class="form-control" required>
                                    <option value="">Choose One</option>
                                    <option value="60" <?php if($user->commission == "60"): ?> selected <?php endif; ?>>60%</option>
                                    <option value="65"  <?php if($user->commission == "65"): ?> selected <?php endif; ?>>65%</option>
                                    <option value="70"  <?php if($user->commission == "70"): ?> selected <?php endif; ?>>70%</option>
                                    <option value="75"  <?php if($user->commission == "75"): ?> selected <?php endif; ?>>75%</option>
                                    <option value="80"  <?php if($user->commission == "80"): ?> selected <?php endif; ?>>80%</option>
                                    <option value="100"  <?php if($user->commission == "100"): ?> selected <?php endif; ?>>100%</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label><span class="red">*</span>Sales Goal:</label>
                                <input type="text" name="sales_goal" value="<?php echo e($user->sales_goal); ?>" class="form-control" required>
                            </div>


<script type="text/javascript">
// Jquery Dependency

$("input[data-type='currency']").on({
    keyup: function() {
      formatCurrency($(this));
    },
    blur: function() { 
      formatCurrency($(this), "blur");
    }
});


function formatNumber(n) {
  // format number 1000000 to 1,234,567
  return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
}


function formatCurrency(input, blur) {
  // appends $ to value, validates decimal side
  // and puts cursor back in right position.
  
  // get input value
  var input_val = input.val();
  
  // don't validate empty input
  if (input_val === "") { return; }
  
  // original length
  var original_len = input_val.length;

  // initial caret position 
  var caret_pos = input.prop("selectionStart");
    
  // check for decimal
  if (input_val.indexOf(".") >= 0) {

    // get position of first decimal
    // this prevents multiple decimals from
    // being entered
    var decimal_pos = input_val.indexOf(".");

    // split number by decimal point
    var left_side = input_val.substring(0, decimal_pos);
    var right_side = input_val.substring(decimal_pos);

    // add commas to left side of number
    left_side = formatNumber(left_side);

    // validate right side
    right_side = formatNumber(right_side);
    
    // On blur make sure 2 numbers after decimal
    if (blur === "blur") {
      right_side += "00";
    }
    
    // Limit decimal to only 2 digits
    right_side = right_side.substring(0, 2);

    // join number by .
    input_val = "$" + left_side + "." + right_side;

  } else {
    // no decimal entered
    // add commas to number
    // remove all non-digits
    input_val = formatNumber(input_val);
    input_val = "$" + input_val;
    
    // final formatting
    if (blur === "blur") {
      input_val += ".00";
    }
  }
  
  // send updated string to input
  input.val(input_val);

  // put caret back in the right position
  var updated_len = input_val.length;
  caret_pos = updated_len - original_len + caret_pos;
  input[0].setSelectionRange(caret_pos, caret_pos);
}

</script>


                            <div class="form-group">
                                <label><span class="red">*</span>My Anniversary Date :</label>
                                <input type="date" name="fdacs_date" class="form-control" value="<?php echo e($user->fdacs_date); ?>" required>
                            </div>

                            <div class="form-group">
                                <label><span class="red">*</span>Status:</label>
                                <select name="status" class="form-control" required>
                                    <option disabled selected>--Select Status--</option>
                                    <option value="active" <?php if($user->status=="active"): ?>selected <?php endif; ?> >Active</option>
                                    <option value="inactive" <?php if($user->status=="inactive"): ?>selected <?php endif; ?>>Inactive</option>
                                    <option value="terminated" <?php if($user->status=="terminated"): ?>selected <?php endif; ?>>Terminated</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label><span class="red">*</span>Agent Mentor:</label>
                                <select name="user_id" class="form-control" required>
                                    <option disabled selected> --Select Mentor--</option>}
                                    
                                    <?php $__currentLoopData = $all_users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user_all): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    {
                                        <option value="<?php echo e($user_all->id); ?>" <?php if($user_all->id==$user->user_id): ?> selected <?php endif; ?> ><?php echo e($user_all->name); ?></option>
                                    }

                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                </select>
                            </div>






<?php $x=0; ?>

                            <?php $__currentLoopData = $destinations; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $destination): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  
                            <?php $x=0; ?>                                 
                                <div class="col-lg-6">
                                    <div class="form-group">
<!--                                         <label class="display-block text-semibold"><?php echo e($destination->name); ?></label>
 -->                                        
                                                 <?php $__currentLoopData = $agentdestinations; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $agentdestination): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                 
                                                 <?php if($destination->name== $agentdestination->destination_name): ?>
                                                <?php $x=1; ?>
                                                 <?php endif; ?>
                                                 
                                               <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


                                                <?php if($x==1): ?>                         
                                                    <div class="checkbox">
                                            <label>
                                                <input type="checkbox" checked class="styled" name="destinations[]" value="<?php echo e($destination->id); ?>">
                                                <?php echo e($destination->name); ?>

                                            </label>
                                                 </div>
                                                
                                        <?php else: ?>
                                                  <div class="checkbox">
                                            <label>
                                                <input type="checkbox" class="styled" name="destinations[]" value="<?php echo e($destination->id); ?>">
                                                <?php echo e($destination->name); ?>

                                            </label>
                                                 </div>
                                            

                                        <?php endif; ?>
                                    </div>
                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

         <div class="row" id="changePasswordFields">
                                
                  </div>
                            <button type="submit" class="btn btn-primary pull-right">Update Agent</button>
                            <button style="margin-right:10px;" type="button" class="btn btn-danger pull-right mr-5" onclick="getPasswordFields()">Change Password</button>
                      </form>
                    </div>



                </div>

            </div>


        </div><!--/.row-->
    </div>	<!--/.main-->
</div>


<script type="text/javascript">
    function getPasswordFields()
    {
        $('#changePasswordFields').html("");
        var passwordFields=`<div class="form-group container-fluid col-md-12">
                                <label><span class="red">*</span>Password:</label>
                                <input type="password" name="password" value="" minlength='6' min='6' class="form-control" placeholder="Enter New Password" required>
                               
                            </div>
                            <div class="form-group container-fluid col-md-12">
                                <label><span class="red">*</span>Confirm Password:</label>
                                <input id="password-confirm" type="password" minlength='6' min='6'  class="form-control" placeholder="Confirm Password" name="password_confirmation" required>

                            </div>`;
        $('#changePasswordFields').append(passwordFields);
    }
</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.dashboardx', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>