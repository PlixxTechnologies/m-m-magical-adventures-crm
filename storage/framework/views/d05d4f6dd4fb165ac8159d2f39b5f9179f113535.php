<?php $__env->startSection('content'); ?>
<style>
.red{
    color: red;
}
.select2-selection--multiple .select2-selection__choice {
    background-color: #eee;
    color: black;
}
</style>
 <!-- Page header -->
                <div class="page-header page-header-default">
                    <div class="page-header-content">
                        <div class="page-title">
                            <h4><span class="text-semibold">Add Trip</span></h4>
                        </div>
                    </div>
                </div>
   <!-- /page header -->

    <div class="container">
        <br>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">

                        <form action="<?php echo e(url('trip/save')); ?>" method="post" >
                            <?php echo csrf_field(); ?>


                            <input type="hidden" class="form-control" name="user_id" value="<?php echo e($user->id); ?>">
							<input type="hidden" class="form-control" name="customer_id" value="<?php echo e($customer->id); ?>">

                            <div class="form-group">
                                <label>Agent Name:</label>
                                <input type="text"  class="form-control" value="<?php echo e($user->name); ?>" readonly>
                            </div>
                            <div class="form-group">
                                <label>Customer Name:</label>
                                <input type="text"  class="form-control" value="<?php echo e($name); ?>" readonly>
                            </div>
                            <div class="form-group">
                                <label><span class="red">*</span>Reservation Number:</label>
                                <input type="text" name="reservation_number"  class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label><span class="red">*</span>Booking Date:</label>
                                <input type="date" name="booking_date" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label><span class="red">*</span>Check In Date:</label>
                                <input type="date" name="checkin_date" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label>Check Out Date:</label>
                                <input type="date" name="checkout_date" class="form-control">
                                <span class="red"><b>Note: </b></span>If a check-out date is not selected then system will automatically save the <b>check-in date</b> as check-out date.
                            </div>
                            <div class="form-group">
                                <label><span class="red">*</span>Trip Status:</label>
                                <select name="trip_status" class="form-control" required>
                                    <option value="">Choose One....</option>
                                    <option value="Inquiry">Inquiry</option>
                                    <option value="Quote Sent">Quote Sent</option>
                                    <option value="Booked Deposit">Booked Deposit</option>
                                    <option value="On Hold">On Hold</option>
                                    <option value="Paid in Full">Paid in Full</option>
                                    <option value="Complete – Commission Paid">Complete – Commission Paid</option>
                                    <option value="Canceled">Canceled</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label><span class="red">*</span>Total Sale:</label>
                                <input type="number" step="any" name="total_sale" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label><span class="red">*</span>Total Commission:</label>
                                <input type="number" step="any" name="commission" id="commission" onfocusout="mycommission()" class="form-control" required>                                
                            </div>
                            <div class="form-group">
                                <label><span class="red">*</span>Expected Commission:</label>
                                <input type="number" step="any" name="expected_commission" id="expected_commission" class="form-control">
                                <input type="hidden" id="user_commission" value="<?php echo e(Auth::user()->commission); ?>" required>
                            </div>
                            <!-- <div class="form-group">
                                    <label>Travel With:</label>
                                    <input type="text" value="<?php echo e($guests); ?>" name="travel_with"  class="form-control">
                            </div> -->

                            <div class="form-group form-group-material">
                                        <label class="control-label has-margin animate is-visible">Travel With:</label>
                                        <select name="travel_with[]" multiple="" class="select select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                            <?php $__currentLoopData = $guests; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $guest): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option 
                                                value="<?php echo e($guest->guest_first_name.' '.$guest->guest_last_name); ?>" selected="selected">
                                                    <?php echo e($guest->guest_first_name.' '.$guest->guest_last_name); ?>

                                                </option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
                                        </select>
                                    </div>

                            <div class="form-group">
                                    <label>Special Occasions or Requests:</label>
                                    <input type="text" name="special_request" class="form-control">
                            </div>
                            <div class="form-group">
                                <label><span class="red">*</span>Destinations:</label>
                                <select name="destination" class="form-control" id="disney_select" onchange="switchDivs()" required>
                                    <option value="">Choose One....</option>
                                <?php $__currentLoopData = $destinations; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $destination): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                                <option value="<?php echo e($destination->destination_name); ?>"><?php echo e($destination->name); ?></option>
                                    

                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>        
                                </select>
                            </div>                            

                            <div id="ticketdiv">
                            <div class="form-group">
                                <label>Ticket Only Sale:</label>
                                <select name="ticket_type" class="form-control">
                                    <option value="">Choose One....</option>
                                    <option value="Park Ticket Only">Park Ticket Only</option>
                                    <option value="Special Event Only">Special Event Only</option>
                                </select>
                            </div>
                        </div>
                        

                            <div id="first">
                                <div class="form-group" id = "worldHotels">
                                    <label>Walt Disney World Hotel Name:</label>
                                    <select name="disneyworld_hotel_name" class="form-control">
                                        <option value="">Choose One....</option>
                                         <option disabled><b>Value Resorts</b></option>
                                          <?php $__currentLoopData = $valueResorts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                          <option value="<?php echo e($value->hotel_name); ?>"><?php echo e($value->hotel_name); ?></option>
                                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                          <option disabled><b>Moderate Resorts</b></option>
                                          <?php $__currentLoopData = $moderateResorts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                          <option value="<?php echo e($value->hotel_name); ?>"><?php echo e($value->hotel_name); ?></option>
                                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                          <option disabled><b>Deluxe Resorts </b></option>
                                          <?php $__currentLoopData = $deluxeResorts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                          <option value="<?php echo e($value->hotel_name); ?>"><?php echo e($value->hotel_name); ?></option>
                                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                          <option disabled><b>Deluxe Villas </b></option>
                                          <?php $__currentLoopData = $deluxeVillas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                          <option value="<?php echo e($value->hotel_name); ?>"><?php echo e($value->hotel_name); ?></option>
                                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                       
                                    </select>
                                 
                                </div> 

                                <div class="form-group">
                                    <label>Dining Plan:</label>
                                    <select name="disney_dining_plan" class="form-control">
                                        <option value="None">None</option>
                                        <option value="Quick Service Dining">Quick Service Dining</option>
                                        <option value="Disney Dining Plan">Disney Dining Plan</option>
                                        <option value="Deluxe Dining Plan">Deluxe Dining Plan</option>
                                    </select>
                                </div>                             
                                <div class="form-group">
                                    <label>Memory Maker:</label>
                                    <select name="memory_maker" class="form-control">
                                        <option value="">Choose One....</option>
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Magical Express:</label>
                                    <select name="magical_express" class="form-control">
                                        <option value="">Choose One....</option>
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>
                                    </select>
                                </div> 
                                
                                <div class="form-group">
                                    <label>Magic Band Color Selection:</label>
                                    <input type="text" name="magic_band_color" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>My Disney Experience User Name:</label>
                                    <input type="text" name="disney_experience_username" value="<?php echo e($customer->disney_experience_username); ?>" class="form-control" readonly>
                                </div>
                                <div class="form-group">
                                    <label>My Disney Experience Password:</label>
                                    <input type="text" name="disney_experience_password" value="<?php echo e($customer->disney_experience_password); ?>" class="form-control" readonly>
                                </div>
                            </div>

                            <div id="second">
                                <div class="form-group" id ="resortHotels">
                                    <label>Disneyland Hotel Name:</label>
                                    <select name="disneyresort_hotel_name" class="form-control" id="goodneighbourselect" onchange="switchGoodDiv()">
                                        <option value="">Choose One....</option>
                                        <?php $__currentLoopData = $disneylandResortHotels; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($value->hotel_name); ?>"><?php echo e($value->hotel_name); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                </div>

                                <div id="goodneighbour">
                                    <div class="form-group">
                                        <label>Good Neighbor Hotel</label>
                                        <input type="text" name="good_neighbor_hotel" class="form-control">
                                    </div>
                                </div>                               
                            </div>

                         <div id="third">
                             <div class="form-group">
                                 <label>Ship Name:</label>
                                 <select name="ship_name2" class="form-control">
                                     <option value="">Choose One....</option>
                                     <?php $__currentLoopData = $disneyCruiseLineShips; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                     <option value="<?php echo e($value->hotel_name); ?>"><?php echo e($value->hotel_name); ?></option>
                                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                 </select>
                             </div>
                             <div class="form-group">
                                <label>Castaway Member:</label>
                                <input type="text" name="castaway_member"  class="form-control">
                             </div>
                             <div class="form-group">
                                <label>Bus Transportation:</label>
                               <div class="checkbox">
                                  <label><input type="checkbox" name="bus_transportation" value="Yes">Yes</label>
                                </div>
                                <div class="checkbox">
                                  <label><input type="checkbox" name="bus_transportation" value="No">No</label>
                                </div>
                             </div>
                        </div>

                        <div id="fourth">
                            <!-- <div id="universalorlandoresorthotels"> -->
                                    <div class="form-group">
                                        <label>Universal Orlando Resort Hotels:</label>
                                        <select name="universal_orlando_resort_hotel" class="form-control" id="universal_orlando_resort_hotel" onchange="switchOrlandoDiv()">
                                            <option value="">Choose One....</option>
                                            <?php $__currentLoopData = $universalStudiosHotels; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($value->hotel_name); ?>"><?php echo e($value->hotel_name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                    </div>
                                <!-- </div>  -->

                                <div id="PartnersHotel">
                                    <div class="form-group">
                                        <label>Partners Hotel</label>
                                        <input type="text" name="partners_hotel" class="form-control">
                                    </div>
                                </div>
                        </div>

                        <div id="fifth">
                             <div class="form-group">
                                 <label>Ship Name:</label>
                                 <select name="ship_name1" class="form-control">
                                     <option value="">Choose One....</option>
                                     <optgroup label="XL Class">
                                     <?php $__currentLoopData = $xl; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($value->hotel_name); ?>"><?php echo e($value->hotel_name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </optgroup>
                                    <optgroup label="Vista Class">
                                   <?php $__currentLoopData = $vista; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($value->hotel_name); ?>"><?php echo e($value->hotel_name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </optgroup>
                                    <optgroup label="Sunshine Class">
                                    <?php $__currentLoopData = $sunshine; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($value->hotel_name); ?>"><?php echo e($value->hotel_name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </optgroup>
                                    <optgroup label="Dream Class">
                                    <?php $__currentLoopData = $dream; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($value->hotel_name); ?>"><?php echo e($value->hotel_name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </optgroup>
                                    <optgroup label="Splendor Class">
                                   <?php $__currentLoopData = $splendor; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($value->hotel_name); ?>"><?php echo e($value->hotel_name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </optgroup>
                                     <optgroup label="Conquest Class">
                                     <?php $__currentLoopData = $conquest; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($value->hotel_name); ?>"><?php echo e($value->hotel_name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </optgroup>
                                     <optgroup label="Spirit Class">
                                     <?php $__currentLoopData = $spirit; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($value->hotel_name); ?>"><?php echo e($value->hotel_name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </optgroup>
                                    <optgroup label="Fantasy Class">
                                    <?php $__currentLoopData = $fantasy; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($value->hotel_name); ?>"><?php echo e($value->hotel_name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </optgroup>
                                 </select>
                             </div>
                        
                        </div>
    

                         <div id="sixth">
                             <div class="form-group">
                                 <label>Ship Name:</label>
                                 <select name="ship_name" class="form-control">
                                     <option value="">Choose One....</option>
                                     <optgroup label="Oasis Class">
                                    <?php $__currentLoopData = $oasis; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($value->hotel_name); ?>"><?php echo e($value->hotel_name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </optgroup>
                                    <optgroup label="Quantum Class">
                                     <?php $__currentLoopData = $quantum; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($value->hotel_name); ?>"><?php echo e($value->hotel_name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </optgroup>
                                   <optgroup label="Freedom Class">
                                   <?php $__currentLoopData = $freedom; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($value->hotel_name); ?>"><?php echo e($value->hotel_name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </optgroup>
                                   <optgroup label="Voyager Class">
                                    <?php $__currentLoopData = $voyager; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($value->hotel_name); ?>"><?php echo e($value->hotel_name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </optgroup>
                                     <optgroup label="Radiance Class">
                                    <?php $__currentLoopData = $radiance; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($value->hotel_name); ?>"><?php echo e($value->hotel_name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </optgroup>
                                    <optgroup label="Vision Class">
                                     <?php $__currentLoopData = $vision; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($value->hotel_name); ?>"><?php echo e($value->hotel_name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </optgroup>
                                    <optgroup label="Other">
                                    <?php $__currentLoopData = $other; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($value->hotel_name); ?>"><?php echo e($value->hotel_name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </optgroup>
                                 </select>
                             </div>
                        
                        </div>
                            <div class="form-group">
                                <label>Insurance:</label>
                                <select name="insurance" class="form-control">
                                    <option value="">Choose One....</option>
                                    <option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Notes:</label>
                                <textarea name="notes" class="form-control" rows="4"></textarea>
                            </div>
                                                       
                            <button type="submit" class="btn btn-primary pull-right">Save & Next</button>
                        </form>
                    </div>
                </div>
            </div>
        </div><!--/.row-->
    </div>  <!--/.main-->


<script type="text/javascript">

hidedivs();

    function mycommission() {    
    var x = $('#commission').val();        
    var usercommission = $('#user_commission').val();

        if(x != "")
            $('#expected_commission').val(Math.round(parseFloat(x) * ((parseFloat(usercommission)/100) * 100)) / 100);

}

function hidedivs()
{
     
        var divEl=document.getElementById('first');
          divEl.style.display = 'none';

          var divE2=document.getElementById('second');
        divE2.style.display = 'none';

        var divE3=document.getElementById('third');
        divE3.style.display = 'none';

        var divE4=document.getElementById('fourth');
        divE4.style.display = 'none';  
        
        var divE5=document.getElementById('ticketdiv');
        divE5.style.display = 'none';  

        var divE6=document.getElementById('fifth');
        divE6.style.display = 'none';  

        var divE7=document.getElementById('sixth');
        divE7.style.display = 'none';  
}

function switchDivs()
{
    var selectedItem=document.getElementById('disney_select');

    if(selectedItem.value=="Walt Disney World")
    {
        document.getElementById('first').style.display='block';
        document.getElementById('worldHotels').style.display='block';
        document.getElementById('resortHotels').style.display='none';
        document.getElementById('second').style.display='none';
        document.getElementById('third').style.display='none';
        document.getElementById('fourth').style.display='none';
        document.getElementById('ticketdiv').style.display='block';
        document.getElementById('sixth').style.display='none';
        document.getElementById('fifth').style.display='none';
    }
    else if(selectedItem.value=="Disneyland Resort")
    {
        document.getElementById('second').style.display='block';
        document.getElementById('worldHotels').style.display='none';
        document.getElementById('resortHotels').style.display='block';
        document.getElementById('first').style.display='none';
        document.getElementById('third').style.display='none';
        document.getElementById('fourth').style.display='none';
        document.getElementById('ticketdiv').style.display='block';
        document.getElementById('fifth').style.display='none';
        document.getElementById('sixth').style.display='none';

    }
    else if(selectedItem.value=='Disney Cruise Line')
    {
        document.getElementById('first').style.display='none';
        document.getElementById('worldHotels').style.display='none';
        document.getElementById('resortHotels').style.display='none';
        document.getElementById('third').style.display='block';
        document.getElementById('second').style.display='none';
        document.getElementById('fourth').style.display='none';
        document.getElementById('ticketdiv').style.display='none';
        document.getElementById('fifth').style.display='none';
        document.getElementById('sixth').style.display='none';

    }
    else if(selectedItem.value=='Universal Studios Florida')
    {
        document.getElementById('first').style.display='none';
        document.getElementById('worldHotels').style.display='none';
        document.getElementById('resortHotels').style.display='none';
        document.getElementById('third').style.display='none';
        document.getElementById('second').style.display='none';
        document.getElementById('fourth').style.display='block';
        document.getElementById('ticketdiv').style.display='block';
        document.getElementById('fifth').style.display='none';
        document.getElementById('sixth').style.display='none';

    }
    else if(selectedItem.value=='Carnival Cruise Line')
    {
       
        document.getElementById('fifth').style.display='block';
        document.getElementById('first').style.display='none';
        document.getElementById('worldHotels').style.display='none';
        document.getElementById('resortHotels').style.display='none';
        document.getElementById('second').style.display='none';
        document.getElementById('third').style.display='none';
        document.getElementById('fourth').style.display='none';
        document.getElementById('ticketdiv').style.display='none';
        document.getElementById('sixth').style.display='none';
    }
    else if(selectedItem.value=='Royal Caribbean Cruise Line')
    {
        document.getElementById('sixth').style.display='block';
        document.getElementById('fifth').style.display='none';
        document.getElementById('first').style.display='none';
        document.getElementById('worldHotels').style.display='none';
        document.getElementById('resortHotels').style.display='none';
        document.getElementById('second').style.display='none';
        document.getElementById('third').style.display='none';
        document.getElementById('fourth').style.display='none';
        document.getElementById('ticketdiv').style.display='none';
    }
    else
    {
        document.getElementById('first').style.display='none';
        document.getElementById('worldHotels').style.display='none';
        document.getElementById('resortHotels').style.display='none';
        document.getElementById('second').style.display='none';
        document.getElementById('third').style.display='none';
        document.getElementById('fourth').style.display='none';
        document.getElementById('ticketdiv').style.display='none';
        document.getElementById('fifth').style.display='none';
        document.getElementById('sixth').style.display='none';
    }
}


</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>