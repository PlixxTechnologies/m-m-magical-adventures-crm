<?php $__env->startSection('content'); ?>
<!-- Datatables JS FILES -->

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.print.min.js"></script>

<!-- /Datatables JS FILES -->
<!-- Simple lists -->
<!-- <div class="row pull-right">
<input type="button" onclick="printDiv('printableArea')" value="Print" class="btn btn-primary btn-lg" />

    </div> -->
<div class="container-fluid">
<div id="printableArea">
  <div class="row">
    <div class="col-md-12">
      <!-- Simple list -->
        <div class="panel panel-flat">
          <div class="panel-heading">
              <center><h3 class="panel-title"><b><strong>Reason For Trip Cancellation</strong></b></h3></center>
          </div>

          <div class="panel-body">
            <div class="row">
              <div class="col-md-2"></div>
              <div class="col-md-8">
                <form action="<?php echo e(url('trip/updateTripStatus')); ?>" method="post">
                  <?php echo csrf_field(); ?>

                  <input type="hidden" value="<?php echo e($tripId); ?>" name="id">

                  <div class="form-group">
                      <label><span class="red">*</span>Reason:</label>
                      <input type="text" name="reason" class="form-control" required>
                  </div>

                  <input type="submit" name="" class="btn btn-primary btn-md" value="Submit">
                </form>
              </div>
              <div class="col-md-2"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
                                                   
                                           
                        
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.dashboardx', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>