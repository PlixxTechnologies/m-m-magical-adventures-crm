<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>M&M MAGICAL ADVENTURES</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="<?php echo e(url('assets/css/icons/icomoon/styles.css')); ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo e(url('assets/css/bootstrap.css')); ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo e(url('assets/css/core.css')); ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo e(url('assets/css/components.css')); ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo e(url('assets/css/colors.css')); ?>" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/loaders/pace.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/core/libraries/jquery.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/core/libraries/bootstrap.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/loaders/blockui.min.js')); ?>"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/tables/datatables/datatables.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/tables/datatables/extensions/buttons.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/forms/selects/select2.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js')); ?>"></script> 
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/tables/datatables/extensions/buttons.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/velocity/velocity.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/velocity/velocity.ui.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/ui/prism.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/forms/styling/uniform.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/notifications/pnotify.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/forms/selects/bootstrap_multiselect.js')); ?>"></script>

    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/forms/wizards/steps.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/forms/selects/select2.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/forms/styling/uniform.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/core/libraries/jasny_bootstrap.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/forms/validation/validate.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/extensions/cookie.js')); ?>"></script>

    <script type="text/javascript" src="<?php echo e(url('assets/js/core/app.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/pages/datatables_extension_buttons_init.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/pages/datatables_extension_buttons_html5.js')); ?>"></script>    
    <script type="text/javascript" src="<?php echo e(url('assets/js/pages/animations_velocity_examples.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/pages/form_multiselect.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/pages/wizard_steps.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/pages/user_pages_list.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/ui/ripple.min.js')); ?>"></script>
    <!-- /theme JS files -->

</head>

<body>

    <!-- Main navbar -->
    <div class="navbar navbar-inverse">
        <div class="navbar-header">
            <a href="<?php echo e(url('/home')); ?>" style="color: white;"><img src="<?php echo e(url('assets/images/Logo.png')); ?>" width="100" height="80"></a>

            <ul class="nav navbar-nav visible-xs-block">
                <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
                <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
            </ul>
        </div>

        <div class="navbar-collapse collapse" id="navbar-mobile">
            <ul class="nav navbar-nav">
                <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
            </ul>

            <div class="navbar-right">
                <ul class="nav navbar-nav">
                    <li class="dropdown dropdown-user">
                        <a class="dropdown-toggle" data-toggle="dropdown">
                            <span><?php echo e(Auth::user()->name); ?></span> 
                            <i class="caret"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li><a href="<?php echo e(url('logout')); ?>"><i class="icon-switch2"></i> Logout</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- /main navbar -->
    <!-- Page container -->
    <div class="page-container">
        <!-- Page content -->
        <div class="page-content">
            <!-- Main sidebar -->
            <div class="sidebar sidebar-main">
                <div class="sidebar-content">

                    <!-- User menu -->
                    <div class="sidebar-user">
                        <div class="category-content">
                            <div class="media">
                                <div class="media-body">
                                   <center><span class="media-heading text-semibold"><?php echo e(Auth::user()->name); ?></span></center>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /user menu -->
                     <!-- Main navigation -->
                    <div class="sidebar-category sidebar-category-visible">
                        <div class="category-content no-padding">
                            <ul class="navigation navigation-main navigation-accordion">
                                <!-- Main -->
                                <li><a href="<?php echo e(url('/home')); ?>"><i class="icon-home4"></i> <b>Dashboard</b></a></li>
                                <?php if(Auth::user()->role == 1): ?>
                                <li>
                                    <a href="#"><i class="icon-user"></i> <span>Agents</span></a>
                                    <ul>
                                        <li><a href="<?php echo e(url('user/list')); ?>">List Agents</a></li>
                                      <li><a href="<?php echo e(url('user/add')); ?>">Add Agent</a></li>
                                    </ul>
                                </li>                             
                                <?php endif; ?>
                                <li>
                                    <a href="#"><i class="icon-user"></i> <span>Customers</span></a>
                                    <ul>
                                        <li><a href="<?php echo e(url('customer/list')); ?>">List Customers</a></li>
                                        <li><a href="<?php echo e(url('customer/create')); ?>">Add Customer</a></li>
                                        <li><a href="<?php echo e(url('import-excel')); ?>">Customer Import</a></li>
                                        <li><a href="<?php echo e(url('sms-list')); ?>">List Customer Sms</a></li>
                                        <li><a href="<?php echo e(url('sms')); ?>">Customer Sms</a></li>
                                    </ul>
                                </li> 
                                <li>
                                    <a href="<?php echo e(url('todo/list')); ?>"><i class="icon-stack"></i> <span>To-Dos</span></a>
                                </li> 
                                <li>
                                    <a href="<?php echo e(url('trip/list')); ?>"><i class="icon-stack"></i> <span>Trips</span></a>
                                </li> 
                                <?php if(Auth::user()->role == 1): ?>
                                <li>
                                    <a href="#"><i class="icon-home"></i> <span>Destinations</span></a>
                                    <ul>
                                        <li><a href="<?php echo e(url('destination/list')); ?>">List Destinations</a></li>
                                      <li><a href="<?php echo e(url('destination/add')); ?>">Add Destination</a></li>
                                    </ul>
                                </li> 
                                <li><a href="<?php echo e(url('diningplan/list')); ?>" target="_blank"><i class="icon-stack"></i> <span>Disney Dining Plans</span></a></li>
                                <li><a href="<?php echo e(url('/dining-plan-calculator')); ?>"><i class="icon-calculator"></i> <span>Disney Dining Plan Calculator</span></a></li>
                                <li>
                                    <a href="#"><i class="icon-stack2"></i> <span>Reports</span></a>
                                    <ul>
                                        <li><a href="<?php echo e(url('commission-agents')); ?>">Add ALL Agent Report</a></li>
                                        <li><a href="<?php echo e(url('walt-disney-world-reports')); ?>">Walt Disney World Reports</a></li>
                                        <li><a href="<?php echo e(url('disneyland-resort-reports')); ?>">Disneyland Resort Reports</a></li>
                                        <li><a href="<?php echo e(url('disney-cruise-line-reports')); ?>">Disney Cruise Line Reports</a></li>
                                        <li><a href="<?php echo e(url('commission-destinations')); ?>">Other Destination Reports</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="<?php echo e(url('auditlogs/list')); ?>"><i class="icon-list"></i> <span>Audit Logs</span></a>
                                </li>
                                <li>
                                    <a href="<?php echo e(url('calenders-list')); ?>"><i class="icon-calculator"></i> <span>Calendar</span></a>
                                </li>                               
                                <?php endif; ?> 
                                <!-- /main -->
                            </ul>
                        </div>
                    </div>
                    <!-- /main navigation -->
                </div>
            </div>
            <!-- /main sidebar -->

            <!-- Main content -->
            <div class="content-wrapper">   
                <!-- Content area -->
                <div class="content">






 <!-- Page header -->
                <div class="page-header page-header-default">
                    <div class="page-header-content">
                        <div class="page-title">
                            <h4><span class="text-semibold">Audit Logs</span></h4>
                        </div>
                    </div>
                </div>
   <!-- /page header -->

<!-- Basic initialization -->
                    <div class="panel panel-flat">

                        <table class="table datatable-button-init-basic">
                            <thead>
                                <tr>
                                    <th>Action</th>
									<th data-sortable="true">Trip Id</th>
									<th data-sortable="true">Reservation Number</th>
									<th data-sortable="true">Date</th>
									<th data-sortable="true">Event</th>
									<th data-sortable="true">Agent Name</th>
								</tr>                            
                            </thead>
                            <tbody>
                                  <?php $__currentLoopData = $auditlog; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>             
                                <tr>
                                    <td>
                                        <form style="display:inline" method="post" action="<?php echo e(url('auditlog/delete')); ?>/<?php echo e($row['id']); ?>" onsubmit="return confirm('Are you sure you want to delete this Log?');">
                                            <?php echo csrf_field(); ?>

                                            <?php echo e(method_field('DELETE')); ?>

                                            <button type="submit" class="btn btn-danger btn-sm">DELETE</button>
                                        </form>
                                    </td>

                                    <?php if($row['tripId'] == null): ?>
                                    <td value="<?php echo e($row['id']); ?>">
                                         No Trip
                                    </td>
                                    <?php else: ?>
                                    <td value="<?php echo e($row['id']); ?>">
                                         <?php echo e($row['tripId']); ?>

                                    </td>
                                    <?php endif; ?>

                                    <?php if($row['reservation_number'] == null): ?>
                                    <td>
                                         No Reservation
                                    </td>
                                    <?php else: ?>
                                    <td>
                                         <?php echo e($row['reservation_number']); ?>

                                    </td>
                                    <?php endif; ?>
                                    <td>
                                        <?php echo e($row['change_date']); ?>

                                    </td>
                                    <td>
                                        <?php echo e($row['event']); ?>

                                    </td>
                                    <td>
                                        <?php echo e($row['agent_name']); ?>

                                    </td>
                                  </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                               
                            </tbody>
                        </table>
                    </div>
                    <!-- /basic initialization -->   
                    

 <!-- Footer -->
                      <div class="footer text-muted">
                        &nbsp &copy; 2019 Powered by <a href="https://www.nodlays.com/" target="_blank">Nodlays</a>
                    </div>
                    <!-- /footer -->

                </div>
                <!-- /content area -->

            </div>
            <!-- /main content -->

        </div>
        <!-- /page content -->

    </div>
    <!-- /page container -->

</body>
</html>
