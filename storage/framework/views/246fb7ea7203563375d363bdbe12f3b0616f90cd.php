<?php $__env->startSection('content'); ?>
<style>
.red{
    color: red;
}
</style>

<!-- Page header -->
                <div class="page-header page-header-default">
                    <div class="page-header-content">
                        <div class="page-title">
                            <h4><span class="text-semibold">Add Agent</span></h4>
                        </div>
                    </div>
                </div>
   <!-- /page header -->
    <div class="container">
        <br>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">

                        <form action="<?php echo e(url('user/save')); ?>" method="post">
                            <?php echo csrf_field(); ?>


                            <div class="form-group">
                                <label><span class="red">*</span>Name:</label>
                                <input type="text" name="name" value="" class="form-control" required>
                                <?php if($errors->has('name')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('name')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                            <div class="form-group">
                                <label><span class="red">*</span>Email:</label>
                                <input type="email" name="email" value="" class="form-control" required>
                                <?php if($errors->has('email')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('email')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                            <div class="form-group">
                                <label><span class="red">*</span>Password:</label>
                                <input type="password" name="password" value="" class="form-control" required>
                                <?php if($errors->has('password')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('password')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                            <div class="form-group">
                                <label><span class="red">*</span>Confirm Password:</label>
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>

                            </div>
                            <div class="form-group">
                                <label><span class="red">*</span>Phone Number:</label>
                                <input type="text" name="phone" value="" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label><span class="red">*</span>Address:</label>
                                <input type="text" name="address" value="" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label><span class="red">*</span>Contract Signed Date:</label>
                                <input type="date" name="contract_date" value="" class="form-control" required>
                            </div>                            
                            <div class="form-group">
                                <label><span class="red">*</span>Role:</label>
                                <select name="role" class="form-control" required>
                                    <option value=0>Agent</option>
                                    <option value=1>Admin</option>

                                </select>
                            </div>
                            <div class="form-group">
                                <label><span class="red">*</span>Commission:</label>
                                <select name="commission" class="form-control" required>
                                    <option value="">Choose One</option>
                                    <option value="60">60%</option>
                                    <option value="65">65%</option>
                                    <option value="70">70%</option>
                                    <option value="75">75%</option>
                                    <option value="80">80%</option>
                                    <option value="100">100%</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label><span class="red">*</span>Sales Goal:</label>
                                <input type="text" name="sales_goal" class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label><span class="red">*</span>My Anniversary Date :</label>
                                <input type="date" name="fdacs_date" class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label><span class="red">*</span>Status:</label>
                                <select name="status" class="form-control" required>
                                    <option disabled selected>--Select Status--</option>
                                    <option value="active">Active</option>
                                    <option value="inactive">Inactive</option>
                                    <option value="terminated">Terminated</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label><span class="red">*</span>Agent Mentor:</label>
                                <select name="user_id" class="form-control" required>
                                    <option disabled selected> --Select Mentor--</option>}
                                    
                                    <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    {
                                        <option value="<?php echo e($user->id); ?>"><?php echo e($user->name); ?></option>
                                    }

                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                </select>
                            </div>
                            

                            <h2>Add Destinations</h2>
                             <?php $__currentLoopData = $destinations; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $destination): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  
                          <!--   <?php echo e($x=0); ?>   -->                                
                                <div class="col-lg-6">
                                    
                                    <div class="form-group">
                                      
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" class="styled" name="destinations[]" value="<?php echo e($destination->id); ?>">
                                                <?php echo e($destination->name); ?>

                                            </label>
                                                 </div>
                                        
                                    </div>
                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            
                            <button type="submit" class="btn btn-primary pull-right">Save Agent</button>
                        </form>

                    </div>
                </div>
            </div>
        </div><!--/.row-->
    </div>	<!--/.main-->

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>