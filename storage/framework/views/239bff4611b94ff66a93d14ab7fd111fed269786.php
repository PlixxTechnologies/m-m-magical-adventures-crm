<?php $__env->startSection('content'); ?>
<!-- Datatables JS FILES -->

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.print.min.js"></script>

<!-- /Datatables JS FILES -->
<!-- Quick stats boxes -->

 <!-- Page header -->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="<?php echo e(url('/home')); ?>">
                    <em class="fa fa-home"></em>
                </a></li>
                <li class="active">Dashboard</li>
            </ol>
        </div><!--/.row-->
    <div class="container-fluid"> 
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Dashboard</h1>
            </div>
        </div><!--/.row-->    <!-- /page header -->
   </div>

<div class="container-fluid"> <!--Agent Sales Goals-->
     <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-body border-top-primary">
                <div class="text-center">
                    <h1 class="no-margin"><b>Agent Sales Goal</b></h1>
                    <h3 class="content-group-sm text-muted"><code><b>$<?php echo e(number_format(Auth::user()->sales_goal, 2)); ?></b></code></h4>                             
                </div>

                <div class="progress">
                    <div class="progress-bar bg-blue-1000" style="width: <?php echo e($agentsalespercentage); ?>%">
                        <b><?php echo e($agentsalespercentage); ?>% Complete</b>
                    </div>
                </div>
            </div>
     </div>
</div> <!--Agent Sales Goals End-->


<!-- CHART STARTS -->
<?php if(Auth::user()->role == 0): ?>
<div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Total Sales of Year
                        <!-- <ul class="pull-right panel-settings panel-button-tab-right">
                            <li class="dropdown"><a class="pull-right dropdown-toggle" data-toggle="dropdown" href="#">
                                <em class="fa fa-cogs"></em>
                            </a>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li>
                                        <ul class="dropdown-settings">
                                            <li><a href="#">
                                                <em class="fa fa-cog"></em> Settings 1
                                            </a></li>
                                            <li class="divider"></li>
                                            <li><a href="#">
                                                <em class="fa fa-cog"></em> Settings 2
                                            </a></li>
                                            <li class="divider"></li>
                                            <li><a href="#">
                                                <em class="fa fa-cog"></em> Settings 3
                                            </a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                        </ul> -->
                        <!-- <span class="pull-right clickable panel-toggle panel-button-tab-left"><em class="fa fa-toggle-up"></em></span> --></div>
                    <div class="panel-body">
                        <div class="canvas-wrapper">
                            <canvas class="main-chart" id="line-chart" height="200" width="600"></canvas>
                        </div>
                        <form  id="agentGraphId" action="<?php echo e(url('/home').'/'.Auth::user()->id); ?>" method="POST">
                           
                            <input type="hidden" name="agentId" id="agentId" value="<?php echo e(Auth::user()->id); ?> ">
                        </form>
                    </div>
                </div>
            </div>
        </div><!--/.row-->
        <?php endif; ?>
<!-- CHART END -->
<script type="text/javascript">
    var a = $('#agentId').val();
    if(a != ""){
        $('agentGraphId').submit();
    }
</script>

<div class="container-fluid"> <!--Datatable Ends-->

                      
<div class="row">
            <div style="padding-top:20px; padding-bottom:20px; padding-right:20px; padding-left:20px;"  class="panel panel-body">
                 <h1><b><center>Upcoming TO DO LIST</center></b></h1>
                <hr>
                <div style="overflow-x:auto;">
              <table id="example" class="display nowrap" style="width:100%">  
                    <thead>
                        <tr>
                            <th data-sortable="true">Customer Name</th>
                            <th data-sortable="true">Fast Pass Date</th>
                            <th data-sortable="true">Advanced Dining Reservations</th>
                            <th data-sortable="true">Itinerary Tip Sheets</th>
                            <th data-sortable="true">Final Payment Due</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if($todolistcount > 0): ?>
                             <?php $__currentLoopData = $todos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $todo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  
                                <tr>
                                    <?php if($todo->customer == null): ?>
                                        <td>
                                            No Customer
                                        </td>
                                    <?php else: ?>
                                        <td>
                                            <?php echo e($todo->customer->first_name); ?> <?php echo e($todo->customer->last_name); ?>

                                        </td>
                                    <?php endif; ?>
                                    
                                    <?php if($todo->trip == null): ?>
                                    <td>
                                        
                                    </td>
                                    <td>
                                        
                                    </td>
                                    <td>
                                        
                                    </td>
                                    <td>
                                        
                                    </td>
                                    <?php else: ?>
                                    <td>
                                        <?php echo e($todo->trip->fast_pass_date); ?>

                                    </td>
                                    <td>
                                        <?php echo e($todo->trip->advanced_dining_reservations); ?>

                                    </td>
                                    <td>
                                        <?php echo e($todo->trip->itinerary_tip_sheets); ?>

                                    </td>
                                    <td>
                                        <?php echo e($todo->trip->final_payment_due); ?>

                                    </td>
                                    <td>
                                        <a href="<?php echo e(url('/toggleTodo/'.$todo->id)); ?>" class="btn btn-success">Mark as Complete</a>
                                    </td>
                                    <?php endif; ?>
                                </tr>
                             <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endif; ?>   
                    </tbody>
                </table>
            </div>
            </div>
        </div>
</div> <!--Datatable Ends-->
<div class="container-fluid">
    <div class="row">
            <div class="panel panel-body"> 
                <h1><b><center>Agent Stats</center></b></h1>
                <hr>
            <div class="row">
                <div class="col-lg-4">
                    <h3><strong>This Month's Stats</strong></h3>
                </div>

                 <div class="col-lg-4">
                    <div class="panel bg-teal-400" style="background-color:#FFB53E">
                        <div class="panel-body">
                            <center><em class="fa fa-users fa-lg"></em></center>
                            <h4 class="no-margin"><center><?php echo e($customers_month); ?></center></h4>
                            <center>Customers</center>
                        </div>
                    </div>
                </div>                    

                <div class="col-lg-4">
                    <div class="panel bg-blue-400" style="background-color:#7FB7E6">
                        <div class="panel-body">
                            <center><em class="fa fa-suitcase fa-lg"></em></center>
                            <h4 class="no-margin"><center><?php echo e($trips_month); ?></center></h4>
                            <center>Trips</center>
                        </div>
                    </div>
                </div>                    
            </div>

            <div class="row">
                <div class="col-lg-4">
                     <h3><strong>Overall Stats</strong></h3>
                </div>

                <div class="col-lg-4">
                    <div class="panel bg-teal-400" style="background-color:#FFB53E">
                        <div class="panel-body">
                            <center><em class="fa fa-users fa-lg"></em></center>
                            <h4 class="no-margin"><center><?php echo e($total_customer); ?></center></h4>
                            <center>Customers</center>
                        </div>
                    </div>
                </div>             

                <div class="col-lg-4">
                    <div class="panel bg-blue-400" style="background-color:#7FB7E6">
                        <div class="panel-body">
                            <center><em class="fa fa-suitcase fa-lg"></em></center>
                            <h4 class="no-margin"><center><?php echo e($total_trips); ?></center></h4>
                            <center>Trips</center>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</div> <!--Main Div End-->



<div class="container-fluid"> <!--Counter 1-->
    <div class="panel panel-container"> 
        <h1><b><center>Commission</center></b></h1>
                <hr>
            <div class="row">
                <div class="col-xs-6 col-md-6 col-lg-6 no-padding">
                    <div class="panel panel-teal panel-widget border-right">
                        <div class="row no-padding"><em class="fa fa-xl fa-money color-blue"></em>
                            <div class="large">$<?php echo e(number_format($month_expected_commission, 2)); ?></div>
                            <div class="">This Month's Total Expected Commission</div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 col-md-6 col-lg-6 no-padding">
                    <div class="panel panel-blue panel-widget border-right">
                        <div class="row no-padding"><em class="fa fa-xl fa-money color-orange"></em>
                            <div class="large">$<?php echo e(number_format($year_total_commission, 2)); ?></div>
                            <div class="">Total Commission for <?php echo e(date("Y")); ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
</div> <!--Counter 1 Ends-->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.dashboardx', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>