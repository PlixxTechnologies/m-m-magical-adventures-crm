<?php $__env->startSection('content'); ?>
<!-- Datatables JS FILES -->

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.print.min.js"></script>

<!-- /Datatables JS FILES -->  
<style>
    .red{
color:red;
    }
</style>

<!-- Page header -->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="<?php echo e(url('/home')); ?>">
                    <em class="fa fa-home"></em>
                </a></li>
                <li class="active">My Commission Reports</li>
            </ol>
        </div><!--/.row-->
    <div class="container-fluid"> 
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">My Commission Reports</h1>
            </div>
        </div>  <!-- /page header -->
   </div>

<div class="container-fluid">

<div class="row">
    <form action="<?php echo e(url('my-commission')); ?>" method="get">
        <?php echo csrf_field(); ?>

        <input type="hidden" name="id" id="agentId" value="<?php echo e(Auth::user()->id); ?>">
        <div class="col-md-4 col-lg-4" style="padding: 20px;">
            <div class="form-group">
                <label>Travel Month</label>
                <select name="month" class="form-control">
                    <option value="">Select Travel Month</option>
                    <option value="January">January</option>
                    <option value="February">February</option>
                    <option value="March">March</option>
                    <option value="April">April</option>
                    <option value="May">May</option>
                    <option value="June">June</option>
                    <option value="July">July</option>
                    <option value="August">August</option>
                    <option value="September">September</option>
                    <option value="October">October</option>
                    <option value="November">November</option>
                    <option value="December">December</option>
                 </select> 
            </div>
        </div>

        <div class="col-md-4 col-lg-4" style="padding-top: 47px;"> 
            <button type="submit" class="btn btn-primary">Get Report</button>
        </div>
    </form>
</div>
<br>

<!-- Basic initialization -->
<div style="padding-top:20px; padding-bottom:20px; padding-right:20px; padding-left:20px; overflow-x: auto;" class="panel panel-flat">


    <table id="example" class="display nowrap" style="width:100%">
        <thead>
            <tr>
                <th data-sortable="true">Report ID</th>
                <th data-sortable="true">Agent Name</th>
                <th data-sortable="true">Travel Month</th>
                <th data-sortable="true">Report Creation Date</th>
                <th data-sortable="true">Export Date</th>
                <th data-sortable="true">Payment Status</th>
                <th data-sortable="true">Expected Commission</th>
                <th data-sortable="true">Commissions Paid</th>
                <th data-sortable="true">Cancel Report</th>
                <th data-sortable="true">Action</th>
            </tr>
        </thead>
        <tbody>
              <?php $__currentLoopData = $report; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $reports): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <?php if($reports->isActive != 0): ?>
              <tr>
                <td>
                    <?php echo e($reports->id); ?>

                </td>              
                <td>
                     <?php echo e(Auth::user()->name); ?>

                </td>
                <td>
                    <?php echo e($reports->travelmonth); ?>

                </td>
                <td>
                    <?php echo e(date('m-d-Y', strtotime($reports->creation_date))); ?>

                </td>
                <td>
                    <?php echo e(date('m-d-Y', strtotime($reports->export_date))); ?>

                </td>
                <?php if($reports->paymentstatus == null || $reports->paymentstatus == ""): ?>
                <td>
                    
                </td>
                <?php endif; ?>
                <?php if($reports->paymentstatus == 1): ?>
                <td>
                    <span class="bg-success p-5">Processed</span>
                </td>
                <?php endif; ?>
                <?php if($reports->paymentstatus == 2): ?>
                <td>
                    <span class="bg-primary p-5">Processing</span>
                </td>
                <?php endif; ?>
                <?php if($reports->paymentstatus == 3): ?>
                <td>
                    <span class="bg-danger p-5">Incomplete</span>
                </td>
                <?php endif; ?>
                <?php if($reports->paymentstatus == 4): ?>
                <td>
                    <span class="p-5" style="background:#f1f1f1;">Canceled</span>
                </td>
                <?php endif; ?>
                <?php if($reports->paymentstatus == 5): ?>
                <td>
                    <span class="bg-success p-5">Paid</span>
                </td>
                <?php endif; ?>
                <?php if($reports->paymentstatus == 6): ?>
                <td>
                    <span class="bg-warning p-5">Awaiting_Supplier</span>
                </td>
                <?php endif; ?>
                <td>
                   <strong>$</strong> <?php echo e(number_format($reports->expectedcommssion, 2)); ?>

                </td>
                <?php if($reports->paymentstatus == 5 || $reports->paymentstatus == 1): ?>
                <td>
                    <strong>$</strong><?php echo e(number_format($reports->expectedcommssion, 2)); ?>

                </td>
                <?php else: ?>
                <td>
                    
                </td>
                <?php endif; ?>
                <?php if($reports->paymentstatus != 4): ?>
                <td>
                    <form id="cancelrequestForm" method="post" action="<?php echo e(url('cancel-request').'/'.$reports->id); ?>">
                        <?php echo csrf_field(); ?>

                        <input type="hidden" name="role" id="agentIds">
                        <input type="hidden" name="startdate" id="startdates">
                        <input type="hidden" name="enddate" id="enddates">
                        <button class="btn btn-secondary btn-sm" onclick="CancelRequest('<?php echo e($reports->id); ?>')">Cancel Report</button>
                    </form>
                </td>
                <?php else: ?>
                <td>
                    
                </td>
                <?php endif; ?>

                <td>
                    <a class="btn" style="background: #f1f1f1; color: #000000" href="<?php echo e(url('commisions/history').'/'.$reports->id); ?>" target="_blank">History</a>
                </td>

              </tr>
              <?php endif; ?>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                               
        </tbody>                            
    </table>
</div>
<!-- /basic initialization -->


</div>
</div>


<script type="text/javascript">

    function CancelRequest(id){
        var objAgent = $('#agentId').val();
        var objstartdate = $('#startdate').val();
        var objenddate = $('#enddate').val();

        if(objAgent != "" && objAgent != undefined && objAgent != null &&
         objstartdate != "" && objstartdate != undefined && objstartdate != null &&
         objenddate != "" && objenddate != undefined && objenddate != null){
            $('#agentIds').val(objAgent);
            $('#startdates').val(objstartdate);
            $('#enddates').val(objenddate);
            $("#cancelrequestForm").submit();
        }
    }
</script>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('admin.layout.dashboardx', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>