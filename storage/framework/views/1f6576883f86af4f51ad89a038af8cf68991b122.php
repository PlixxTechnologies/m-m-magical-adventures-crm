<?php $__env->startSection('content'); ?>


<!-- Page header -->
                <div class="page-header page-header-default">
                    <div class="page-header-content">
                        <div class="page-title">
                            <h4><span class="text-semibold">Agent Commission Reports</span></h4>
                        </div>
                    </div>
                </div>
   <!-- /page header -->

<div class="container">
   
    <div class="row">
        <form action="<?php echo e(url('getcommission-reports')); ?>" method="post">
          <?php echo csrf_field(); ?>

          <input type="hidden" name="id" value="<?php echo e($id); ?>">
        <div class="col-md-4 col-lg-4" style="padding: 20px;">
            <div class="form-group">
                <label>*Start Date:</label>
                <input type="date" name="startdate" class="form-control" <?php if($startdate != null): ?> value="<?php echo e($startdate); ?>" <?php endif; ?> required>
            </div>
        </div>

        <div class="col-md-offset-2 col-md-4 col-lg-offset-2 col-lg-4" style="padding: 20px;">
            <div class="form-group">
                <label>*End Date:</label>
                <input type="date" name="enddate" class="form-control"  <?php if($enddate != null): ?> value="<?php echo e($enddate); ?>" <?php endif; ?> required>
            </div>
        </div> 
  </div>
    <button type="submit" class="btn btn-primary pull-right">Get Report</button>
  </form>
</div>

<br>

<?php if($trips != null): ?>
 <br><h3 class="box-title">Total Commission : <strong>$<?php echo e($commissionTotal); ?></strong></h3>
 <h3 class="box-title">Total Expected Commission : <strong>$<?php echo e($expectedCommissionTotal); ?></strong></h3>
<h3 class="box-title">Total Sales : <strong>$<?php echo e($TotalSales); ?></strong></h3>
 <!-- Basic initialization -->
                    <div class="panel panel-flat">
                        <div class="panel-heading">
                            <h5 class="panel-title">Customers List</h5>
                        </div>

                        <table class="table datatable-button-init-basic">
                            <thead>
                                <tr>
                                <th data-sortable="true">Agent Name</th>
                                <th data-sortable="true">Trip ID</th>
                                <th data-sortable="true">Customer Name</th>
                                <th data-sortable="true">Reservation Number</th>
                                <th data-sortable="true">Destination</th>
                                <th data-sortable="true">Booking Date</th>
                                <th data-sortable="true">Check In Date</th>
                                <th data-sortable="true">Check Out Date</th>
                                <th data-sortable="true">Commission</th>
                                <th data-sortable="true">Expected Commission</th>
                                <th data-sortable="true">Total Sale</th>



                                <!-- <th data-sortable="true">Email</th>
                                <th data-sortable="true">Phone Number</th>
                                                               
                                
                                <th data-sortable="true">Walt Disney World Hotel Name</th>
                                <th data-sortable="true">Disneyland Hotel Name</th>
                                <th data-sortable="true">Memory Maker</th>
                                <th data-sortable="true">Magical Express</th>
                                <th data-sortable="true">Ship Name</th>
                                <th data-sortable="true">Castaway Member</th>
                                <th data-sortable="true">Bus Transportation</th>
                                <th data-sortable="true">Insurance</th>
                                <th data-sortable="true">Travel With</th>
                                <th data-sortable="true">Special Occasions or Requests</th>
                                <th data-sortable="true">Dining Plan</th>
                                
                                
                                <th data-sortable="true">Notes</th>
                                <th data-sortable="true">Created At</th> -->
                            </tr>
                            </thead>
                            <tbody>
                                  <?php $__currentLoopData = $trips; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $trip): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                      
                                    <td>
                                         <?php echo e($trip->user->name); ?>

                                    </td>
                                    <td>
                                         <?php echo e($trip->id); ?>

                                    </td>
                                    <td>
                                        <?php echo e($trip->customer->first_name); ?> <?php echo e($trip->customer->last_name); ?>

                                    </td>

                                    <td>
                                        <?php echo e($trip->reservation_number); ?>

                                    </td>

                                     <td>
                                        <?php echo e($trip->destination); ?>

                                    </td>
                                    <td>
                                        <?php echo e($trip->booking_date); ?>

                                    </td>                                   
                                    <td>
                                        <?php echo e($trip->checkin_date); ?>

                                    </td>
                                   <td>
                                        <?php echo e($trip->checkout_date); ?>

                                   </td>

                                   <td>
                                        <?php echo e($trip->commission); ?>

                                    </td>
                                    <td>
                                        <?php echo e($trip->expected_commission); ?>

                                    </td>

                                    <td>
                                        <?php echo e($trip->total_sale); ?>

                                    </td>






                                   <!--  <td>
                                        <?php echo e($trip->customer->email); ?>

                                    </td>
                                    <td>
                                        <?php echo e($trip->customer->phone_no); ?>

                                    </td>
                                    
                                   
                                    <td>
                                        <?php echo e($trip->disneyworld_hotel_name); ?>

                                    </td>
                                    <td>
                                        <?php echo e($trip->disneyresort_hotel_name); ?>

                                    </td>
                                    <td>
                                        <?php echo e($trip->memory_maker); ?>

                                    </td>
                                    <td>
                                        <?php echo e($trip->magical_express); ?>

                                    </td>
                                    <td>
                                        <?php echo e($trip->ship_name); ?>

                                    </td>
                                    <td>
                                        <?php echo e($trip->castaway_member); ?>

                                    </td>
                                    <td>
                                        <?php echo e($trip->bus_transportation); ?>

                                    </td>
                                    <td>
                                        <?php echo e($trip->insurance); ?>

                                    </td>
                                    <td>
                                        <?php echo e($trip->travel_with); ?>

                                    </td>
                                    <td>
                                        <?php echo e($trip->special_request); ?>

                                    </td>
                                    <td>
                                        <?php echo e($trip->disney_dining_plan); ?>

                                    </td>
                                    
                                    
                                    <td>
                                        <?php echo e($trip->notes); ?>

                                    </td>
                                    <td>
                                        <?php echo e($trip->created_at->format('d M Y')); ?>

                                    </td> -->
                                  </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                               
                            </tbody>                            
                        </table>
                    </div>
                    <!-- /basic initialization -->
                    <?php endif; ?>

</div>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('admin.layout.dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>