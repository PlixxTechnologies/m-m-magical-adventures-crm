<?php $__env->startSection('content'); ?>

<!-- Datatables JS FILES -->

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.print.min.js"></script>

<!-- /Datatables JS FILES -->
<!-- Page header -->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="<?php echo e(url('/home')); ?>">
                    <em class="fa fa-home"></em>
                </a></li>
                <li class="active">View Customer</li>
            </ol>
        </div><!--/.row-->
    <div class="container-fluid"> 
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">View Customer</h1>
            </div>
        </div>  <!-- /page header -->
   </div>



<div class="container-fluid">
<!-- Simple lists -->
<div class="row pull-right">
<input type="button" onclick="printDiv('printableArea')" value="Print" class="btn btn-primary btn-lg" />

    </div>

<div id="printableArea">
    <div class="row" >
        <center>
            <h5 class="text-muted">Agent Name</h5>
                <h3 class="media-heading text-semibold"><?php echo e($customer->user->name); ?></h3>
        </center>  
    </div>
<br><br>



                    <div class="row">
                        <div class="col-md-6">
                            <!-- Simple list -->
                            <div class="panel panel-flat">
                                <div class="panel-heading">
                                    <br>
                                    <center><h3 class="panel-title"><b>Customer Information</b></h3></center>
                                </div>
<br>
                                <div class="panel-body">
                                    <!-- <ul class="media-list">
                                        <li class="media"> -->
                                            <div class="col-md-6">
                                                <span class="text-muted">First Name</span>
                                                <div class="media-heading text-semibold"><?php echo e($customer->first_name); ?></div>
                                            </div>  
                                            <div class="col-md-6">
                                                <span class="text-muted">Last Name</span>
                                                <div class="media-heading text-semibold"><?php echo e($customer->last_name); ?></div>
                                            </div>                                           
                                        <!-- </li>
                                        <br>
                                        <li class="media"> -->
                                            <div class="col-md-6">
                                                <span class="text-muted">Email</span>
                                                <div class="media-heading text-semibold"><?php echo e($customer->email); ?></div>
                                            </div>  
                                            <div class="col-md-6">
                                                <span class="text-muted">Country Code</span>
                                                <div class="media-heading text-semibold"><?php echo e($customer->country_code); ?></div>
                                            </div>                                           
                                        <!-- </li>
                                        <br>
                                        <li class="media"> -->
                                            <div class="col-md-6">
                                                <span class="text-muted">Phone Number</span>
                                                <div class="media-heading text-semibold"><?php echo e($customer->phone_no); ?></div>
                                            </div>  
                                            <div class="col-md-6">
                                                <span class="text-muted">Address</span>
                                                <div class="media-heading text-semibold"><?php echo e($customer->address1); ?></div>
                                            </div>                                           
                                        <!-- </li>
                                        <br>
                                        <li class="media"> -->
                                            <div class="col-md-6">
                                                <span class="text-muted">City</span>
                                                <div class="media-heading text-semibold"><?php echo e($customer->city); ?></div>
                                            </div>  
                                            <div class="col-md-6">
                                                <span class="text-muted">State</span>
                                                <div class="media-heading text-semibold"><?php echo e($customer->state); ?></div>
                                            </div>                                           
                                       <!--  </li>
                                        <br>
                                        <li class="media"> -->
                                            <div class="col-md-6">
                                                <span class="text-muted">Zip</span>
                                                <div class="media-heading text-semibold"><?php echo e($customer->zip); ?></div>
                                            </div>

                                            <?php if($customer->referral != null): ?>  
                                            <div class="col-md-6">
                                                <span class="text-muted">Referral</span>
                                                <div class="media-heading text-semibold"><?php echo e($customer->referral); ?></div>
                                            </div>
                                            <?php else: ?>
                                            <div class="col-md-6">
                                                <span class="text-muted">Referral</span>
                                                <div class="media-heading text-semibold"></div><br>
                                            </div>
                                            <?php endif; ?>                                           
                                        <!-- </li>
                                        <br>
                                        <li class="media"> -->
                                            <?php if($customer->disney_experience_username != null): ?> 
                                            <div class="col-md-6">
                                                <span class="text-muted">Disney Experience Username</span>
                                                <div class="media-heading text-semibold"><?php echo e($customer->disney_experience_username); ?></div>
                                            </div>
                                            <?php else: ?>
                                            <div class="col-md-6">
                                                <span class="text-muted">Disney Experience Username</span>
                                                <div class="media-heading text-semibold"></div><br>
                                            </div>
                                            <?php endif; ?>

                                            <?php if($customer->disney_experience_password != null): ?>  
                                            <div class="col-md-6">
                                                <span class="text-muted">Disney Experience Password</span>
                                                <div class="media-heading text-semibold"><?php echo e($customer->disney_experience_password); ?></div>
                                            </div>
                                            <?php else: ?>
                                            <div class="col-md-6">
                                                <span class="text-muted">Disney Experience Password</span>
                                                <div class="media-heading text-semibold"></div><br>
                                            </div>
                                            <?php endif; ?>
                                       <!--  </li>
                                        <br>
                                        <li class="media"> -->
                                            <?php if($customer->notes != null): ?>  
                                            <div class="col-md-6">
                                                <span class="text-muted">Notes</span>
                                                <div class="media-heading text-semibold"><?php echo e($customer->notes); ?></div>
                                            </div> 
                                            <?php else: ?>
                                            <div class="col-md-6">
                                                <span class="text-muted">Notes</span>
                                                <div class="media-heading text-semibold"></div><br>
                                            </div> 
                                            <?php endif; ?>

                                            <?php if($customer->user->name != null): ?>  
                                            <div class="col-md-6">
                                                <span class="text-muted">Agent Name</span>
                                                <div class="media-heading text-semibold"><?php echo e($customer->user->name); ?></div>
                                            </div> 
                                            <?php else: ?>
                                            <div class="col-md-6">
                                                <span class="text-muted">Agent Name</span>
                                                <div class="media-heading text-semibold">Agent has been deleted</div>
                                            </div> 
                                            <?php endif; ?> 
                                                                                      
                                        <!-- </li> -->

                                    </ul>
                                </div>
                            </div>
                            <!-- /simple list -->

                        </div>

                        <div class="col-md-6">
                            <!-- Simple list -->
                            <div class="panel panel-flat">
                                <div class="panel-heading">
                                    <br>
                                    <center><h3 class="panel-title"><b>Trip Information</b></h3></center>
                                </div>
                                <br>
                                <div class="panel-body">
                                    <ul class="media-list">
                                       <!--  <li class="media"> -->
                                            <div class="col-md-6">
                                                <span class="text-muted">Reservation Number</span>
                                                <div class="media-heading text-semibold"><a target="_blank" href="<?php echo e(url('trip/view').'/'.$customer->id); ?>"><?php echo e($customer->reservation_number); ?></a></div>
                                            </div>  
                                            <div class="col-md-6">
                                                <span class="text-muted">Booking Date</span>
                                                <div class="media-heading text-semibold"> <?php echo e(date("m", strtotime($customer->booking_date))); ?>-<?php echo e(date("d", strtotime($customer->booking_date))); ?>-<?php echo e(date("Y", strtotime($customer->booking_date))); ?></div>
                                            </div>                                           
                                        <!-- </li>
                                        <br>
                                        <li class="media"> -->
                                            <div class="col-md-6">
                                                <span class="text-muted">Check In Date</span>
                                                <div class="media-heading text-semibold">
                                                    <?php echo e(date("m", strtotime($customer->checkin_date))); ?>-<?php echo e(date("d", strtotime($customer->checkin_date))); ?>-<?php echo e(date("Y", strtotime($customer->checkin_date))); ?>

                                                </div>
                                            </div>  
                                            <div class="col-md-6">
                                                <span class="text-muted">Check Out Date</span>
                                                <div class="media-heading text-semibold"> <?php echo e(date("m", strtotime($customer->checkout_date))); ?>-<?php echo e(date("d", strtotime($customer->checkout_date))); ?>-<?php echo e(date("Y", strtotime($customer->checkout_date))); ?></div>
                                            </div>                                           
                                       <!--  </li>
                                        <br>
                                        <li class="media"> -->
                                            <div class="col-md-6">
                                                <span class="text-muted">Trip Status</span>
                                                <?php if($customer->status ==0 ): ?>
                                                    <div class="media-heading text-semibold"><?php echo e($customer->trip_status); ?></div>
                                                <?php else: ?>
                                                    <div class="media-heading text-semibold">Canceled</div>
                                                <?php endif; ?>
                                            </div>  
                                            <div class="col-md-6">
                                                <span class="text-muted">Total Sale</span>
                                                <div class="media-heading text-semibold">$<?php echo e(number_format($customer->total_sale,2)); ?></div>
                                            </div>                                           
                                       <!--  </li>
                                        <br>
                                        <li class="media"> -->
                                            <div class="col-md-6">
                                                <span class="text-muted">Total Commission</span>
                                                <div class="media-heading text-semibold">$<?php echo e(number_format($customer->commission,2)); ?></div>
                                            </div>  
                                            <div class="col-md-6">
                                                <span class="text-muted">Expected Commission</span>
                                                <div class="media-heading text-semibold">$<?php echo e(number_format($customer->expected_commission,2)); ?></div>
                                            </div>                                           
                                        <!-- </li>
                                        <br>
                                        <li class="media"> -->
                                            <?php if($customer->travel_with != null || $customer->travel_with != ""): ?>
                                            <?php if($flag == "true"): ?>
                                            <div class="col-md-6">
                                                <span class="text-muted">Travel With</span>
                                                <div class="media-heading text-semibold"><?php echo e(substr($customer->travel_with, 0,-1)); ?></div>
                                            </div>
                                            <?php else: ?> 
                                            <div class="col-md-6">
                                                <span class="text-muted">Travel With</span>
                                                <div class="media-heading text-semibold"><?php echo e($customer->travel_with); ?></div>
                                            </div>
                                            <?php endif; ?>
                                            <?php endif; ?>

                                            <?php if($customer->special_request != null || $customer->special_request != ""): ?>
                                             <div class="col-md-6">
                                                <span class="text-muted">Special Occasions or Requests</span>
                                                <div class="media-heading text-semibold"><?php echo e($customer->special_request); ?></div>
                                            </div>
                                            <?php endif; ?>                                         
                                        <!-- </li>
                                        <br>
                                        <li class="media"> -->
                                            <?php if($customer->destination != null || $customer->destination != ""): ?>
                                            <div class="col-md-6">
                                                <span class="text-muted">Destinations</span>
                                                <div class="media-heading text-semibold"><?php echo e($customer->destination); ?></div>
                                            </div>
                                            <?php endif; ?>

                                            <?php if($customer->disneyworld_hotel_name != null || $customer->disneyworld_hotel_name != ""): ?>  
                                            <div class="col-md-6">
                                                <span class="text-muted">Walt Disney World Hotel Name </span>
                                                <div class="media-heading text-semibold"><?php echo e($customer->disneyworld_hotel_name); ?></div>
                                            </div> 
                                            <?php endif; ?>                                       
                                        <!-- </li>
                                        <br>
                                        <li class="media"> -->
                                            <?php if($customer->magical_express != null || $customer->magical_express != ""): ?>
                                            <div class="col-md-6">
                                                <span class="text-muted">Magical Express</span>
                                                <div class="media-heading text-semibold"><?php echo e($customer->magical_express); ?></div>
                                            </div>  
                                            <?php endif; ?>

                                            <?php if($customer->memory_maker != null || $customer->memory_maker != ""): ?>
                                            <div class="col-md-6">
                                                <span class="text-muted">Memory Maker </span>
                                                <div class="media-heading text-semibold"><?php echo e($customer->memory_maker); ?></div>
                                            </div>  
                                            <?php endif; ?>                                      
                                        <!-- </li>
                                        <br>
                                        <li class="media"> -->
                                            <?php if($customer->disney_dining_plan != null || $customer->disney_dining_plan != ""): ?>
                                            <div class="col-md-6">
                                                <span class="text-muted">Dining Plan</span>
                                                <div class="media-heading text-semibold"><?php echo e($customer->disney_dining_plan); ?></div>
                                            </div>  
                                            <?php endif; ?>

                                            <?php if($customer->magic_band_color != null || $customer->magic_band_color != ""): ?>
                                            <div class="col-md-6">
                                                <span class="text-muted">Magic Band Color Selection </span>
                                                <div class="media-heading text-semibold"><?php echo e($customer->magic_band_color); ?></div>
                                            </div> 
                                            <?php endif; ?>                                       
                                        <!-- </li>
                                        <br>
                                        <li class="media"> -->
                                            <?php if($customer->disney_experience_username != null || $customer->disney_experience_username != ""): ?>
                                            <div class="col-md-6">
                                                <span class="text-muted">My Disney Experience User Name</span>
                                                <div class="media-heading text-semibold"><?php echo e($customer->disney_experience_username); ?></div>
                                            </div>
                                            <?php endif; ?>

                                            <?php if($customer->disney_experience_password != null || $customer->disney_experience_password != ""): ?> 
                                            <div class="col-md-6">
                                                <span class="text-muted">My Disney Experience Password </span>
                                                <div class="media-heading text-semibold"><?php echo e($customer->disney_experience_password); ?></div>
                                            </div>
                                            <?php endif; ?>                                        
                                        <!-- </li>
                                        <br>
                                        <li class="media"> -->
                                            <?php if($customer->disneyresort_hotel_name != null || $customer->disneyresort_hotel_name != ""): ?>
                                            <div class="col-md-6">
                                                <span class="text-muted">Disneyland Hotel Name</span>
                                                <div class="media-heading text-semibold"><?php echo e($customer->disneyresort_hotel_name); ?></div>
                                            </div>
                                            <?php endif; ?> 

                                            <?php if($customer->good_neighbor_hotel != null || $customer->good_neighbor_hotel != ""): ?> 
                                            <div class="col-md-6">
                                                <span class="text-muted">Good Neighbor Hotel </span>
                                                <div class="media-heading text-semibold"><?php echo e($customer->good_neighbor_hotel); ?></diV>
                                            </div>
                                            <?php endif; ?>                                        
                                        <!-- </li>
                                        <br>
                                        <li class="media"> -->
                                            <?php if($customer->ship_name != null || $customer->ship_name != ""): ?>
                                            <div class="col-md-6">
                                                <span class="text-muted">Ship Name</span>
                                                <div class="media-heading text-semibold"><?php echo e($customer->ship_name); ?></div>
                                            </div> 
                                            <?php endif; ?> 

                                            <?php if($customer->castaway_member != null || $customer->castaway_member != ""): ?>
                                            <div class="col-md-6">
                                                <span class="text-muted">Castaway Member </span>
                                                <div class="media-heading text-semibold"><?php echo e($customer->castaway_member); ?></div>
                                            </div>  
                                            <?php endif; ?>                                      
                                        <!-- </li>
                                        <br>
                                        <li class="media"> -->
                                            <?php if($customer->bus_transportation != null || $customer->bus_transportation != ""): ?>
                                            <div class="col-md-6">
                                                <span class="text-muted">Bus Transportation</span>
                                                <div class="media-heading text-semibold"><?php echo e($customer->bus_transportation); ?></div>
                                            </div> 
                                            <?php endif; ?>

                                            <?php if($customer->universal_orlando_resort_hotel != null || $customer->universal_orlando_resort_hotel != ""): ?> 
                                            <div class="col-md-6">
                                                <span class="text-muted">Universal Orlando Resort Hotels </span>
                                                <div class="media-heading text-semibold"><?php echo e($customer->universal_orlando_resort_hotel); ?></div>
                                            </div>  
                                            <?php endif; ?>                                      
                                        <!-- </li>
                                        <br>
                                        <li class="media"> -->
                                            <?php if($customer->partners_hotel != null || $customer->partners_hotel != ""): ?>
                                            <div class="col-md-6">
                                                <span class="text-muted">Partners Hotel</span>
                                                <div class="media-heading text-semibold"><?php echo e($customer->partners_hotel); ?></div>
                                            </div>  
                                            <?php endif; ?>

                                            <?php if($customer->ticket_type != null || $customer->ticket_type != ""): ?>
                                            <div class="col-md-6">
                                                <span class="text-muted">Ticket Only Sale </span>
                                                <div class="media-heading text-semibold"><?php echo e($customer->ticket_type); ?></div>
                                            </div>  
                                            <?php endif; ?>                                      
                                        <!-- </li>
                                        <br>
                                        <li class="media"> -->
                                            <?php if($customer->insurance != null || $customer->insurance != ""): ?>
                                            <div class="col-md-6">
                                                <span class="text-muted">Insurance</span>
                                                <div class="media-heading text-semibold"><?php echo e($customer->insurance); ?></div>
                                            </div> 
                                            <?php endif; ?>

                                            <?php if($customer->notes != null || $customer->notes != ""): ?> 
                                            <div class="col-md-6">
                                                <span class="text-muted">Notes </span>
                                                <div class="media-heading text-semibold"><?php echo e($customer->notes); ?></div>
                                            </div>   
                                            <?php endif; ?>                                     
                                        <!-- </li> -->

                                    </ul>
                                </div>
                            </div>
                            <!-- /simple list -->

                        </div>
                    </div>
                    <!-- /simple lists -->
</div>
</div>


                    <script type="text/javascript">
                        function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}
                    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.dashboardx', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>