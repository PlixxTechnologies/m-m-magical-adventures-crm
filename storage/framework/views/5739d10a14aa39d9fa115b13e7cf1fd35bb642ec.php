<?php $__env->startSection('content'); ?>


 <!-- Page header -->
             
   <!-- /page header -->

<!-- Basic initialization -->
                    <div class="panel panel-flat">
                         <a style="margin-left: 10px; margin-top: 10px" class="btn btn-primary btn-sm pull-left"  href="<?php echo e(url('customer/list')); ?>">
                                            Close
                                        </a>
                    
                       <center><h1>Customer Trip History</h1></center>

                       
                        <table class="table datatable-button-init-basic">
                            <thead>
                                <tr>
                               
                                
                                <th data-sortable="true">Customer Name</th>
                               
                                <th data-sortable="true">Reservation Number</th>                               
                                <th data-sortable="true">Destination</th>
                                <th data-sortable="true">Travel Date</th>
                                <th data-sortable="true">Total Sale</th>
                                                             
                            </tr>
                            </thead>
                            <tbody>
                                                                          
                                      <?php $__currentLoopData = $trips; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $trip): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>                                 

                                    <tr>

                                    <td><?php echo e($trip->customer->first_name); ?> <?php echo e($trip->customer->last_name); ?>  </td>
                                    <td>
                                            <a target="_blank" href="<?php echo e(url('trip/view').'/'.$trip->id); ?>">
                                            <?php echo e($trip->reservation_number); ?>

                                        </a>
                                        </td>
                                    <td><?php echo e($trip->destination); ?></td>
                                    <td>
                                    <?php echo e(date("m", strtotime($trip->checkin_date))); ?>-<?php echo e(date("d", strtotime($trip->checkin_date))); ?>-<?php echo e(date("Y", strtotime($trip->checkin_date))); ?>  
                                    </td>
                                    <td><?php echo e($trip->total_sale); ?></td>                
                                  </tr>
                                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>         
                            </tbody>


                        </table>
                        
                       

                        
                    </div>
                    <!-- /basic initialization -->   
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>