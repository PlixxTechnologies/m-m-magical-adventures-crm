 

<?php $__env->startSection('content'); ?>

<div class="container">
		<center>
			<h1><b>Customer Import</b></h1>
		</center>
		<br>
		<form style="border: 4px solid #a1a1a1;margin-top: 15px;padding: 10px;" action="<?php echo e(url('import-excel')); ?>" class="form-horizontal" method="post" enctype="multipart/form-data">
			 <?php echo e(csrf_field()); ?>

			<input type="file" name="import_file" /><br>
			<button class="btn btn-primary">Import File</button>
		</form>
	</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>