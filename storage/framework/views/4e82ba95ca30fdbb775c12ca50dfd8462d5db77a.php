<?php $__env->startSection('content'); ?>

<style>
    .red{
color:red;
    }
</style>
<!-- Page header -->
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><span class="text-semibold">Commission Payout</span></h4>
            </div>
        </div>
    </div>
<!-- /page header -->

<style type="text/css">
    .modal-content{
        width: 150%;
    }
</style>

<div class="container">

<?php if($Savereports != null || $Savereports != ""): ?>
    <?php if(Auth::user()->role == 1): ?>
        <!-- Basic initialization -->
        <div class="panel panel-flat">
            <div class="panel-heading">
                <div class="col-md-12">
                    <h5 class="panel-title">Edit Commission Payout</h5>
                </div>
            </div>

            <table class="table datatable-button-init-basic">
                <thead>
                    <tr>
                        <th data-sortable="true">Agent Name</th>
                        <th data-sortable="true">Customer Name</th>
                        <th data-sortable="true">Reservation Number</th>
                        <th data-sortable="true">Destination</th>
                        <th data-sortable="true">Check In Date</th>
                        <th data-sortable="true">Check Out Date</th>
                        <th data-sortable="true">Commission</th>
                        <th data-sortable="true">Expected Commission</th>
                        <th data-sortable="true">Set Status</th>
                        <th data-sortable="true">Set Status All Paid
                            <form id="CheckForm" action="<?php echo e(url('ChecktoPaid')); ?>" method="post" >
                                <?php echo csrf_field(); ?>

                                <input type="checkbox" id="checkAll">
                                <input type="hidden" name="savereportid" id="savereportid" value="<?php echo e($wajeeh); ?>">
                            </form>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php $__currentLoopData = $Savereports; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $reports): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            
                            <td>
                                <?php echo e($reports->agentName); ?>

                            </td>
                            <td>
                                <?php echo e($reports->CustomerName); ?>

                            </td>

                            <td>
                                <?php echo e($reports->reservation); ?>

                            </td>
                            <td>
                                <?php echo e($reports->destination); ?>

                            </td>

                            <td>
                                <?php echo e(date('m-d-Y', strtotime($reports->CheckinDate))); ?>

                            </td>
                            <td>
                                <?php echo e(date('m-d-Y', strtotime($reports->CheckoutDate))); ?>

                            </td>
                            <td>
                                <?php echo e($reports->ytdcommission); ?>

                            </td>
                            <td>
                                <?php echo e($reports->expectedcommsion); ?>

                            </td>
                            <td>
                                <form id="reportForm" action="<?php echo e(url('statusChange')); ?>" method="post" >
                                    <?php echo csrf_field(); ?>

                                    <?php
                                        $abc = "status".$reports->id;
                                     ?>
                                    <select onchange="Report('<?php echo e($reports->id); ?>')" id="<?php echo e($abc); ?>">
                                        <?php if($reports->paymentstatus == null): ?>
                                            <option value="" selected="selected">Status</option>
                                            <option value="1">Processed</option>
                                            <option value="2">Processing</option>
                                            <option value="3">Incomplete</option>
                                            <option value="4">Canceled</option>
                                            <option value="5">Paid</option>
                                            <option value="6">Awaiting Supplier</option>

                                        <?php endif; ?>
                                        
                                        <?php if($reports->paymentstatus == 1): ?>
                                            <option value="1" selected="selected">Processed</option>
                                            <option value="2">Processing</option>
                                            <option value="3">Incomplete</option>
                                            <option value="4">Canceled</option>
                                            <option value="5">Paid</option>
                                            <option value="6">Awaiting Supplier</option>
                                        <?php endif; ?>
                                        
                                        <?php if($reports->paymentstatus == 2): ?>
                                            <option value="1">Processed</option>
                                            <option value="2" selected="selected">Processing</option>
                                            <option value="3">Incomplete</option>
                                            <option value="4">Canceled</option>
                                            <option value="5">Paid</option>
                                            <option value="6">Awaiting Supplier</option>
                                        <?php endif; ?>

                                        <?php if($reports->paymentstatus == 3): ?>
                                            <option value="1">Processed</option>
                                            <option value="2">Processing</option>
                                            <option value="3" selected="selected">Incomplete</option>
                                            <option value="4">Canceled</option>
                                            <option value="5">Paid</option>
                                            <option value="6">Awaiting Supplier</option>
                                        <?php endif; ?>

                                        <?php if($reports->paymentstatus == 4): ?>
                                            <option value="1">Processed</option>
                                            <option value="2">Processing</option>
                                            <option value="3">Incomplete</option>
                                            <option value="4" selected="selected">Canceled</option>
                                            <option value="5">Paid</option>
                                            <option value="6">Awaiting Supplier</option>
                                        <?php endif; ?>

                                        <?php if($reports->paymentstatus == 5): ?>
                                            <option value="1">Processed</option>
                                            <option value="2">Processing</option>
                                            <option value="3">Incomplete</option>
                                            <option value="4">Canceled</option>
                                            <option value="5" selected="selected">Paid</option>
                                            <option value="6">Awaiting Supplier</option>
                                        <?php endif; ?>

                                        <?php if($reports->paymentstatus == 6): ?>
                                            <option value="1">Processed</option>
                                            <option value="2">Processing</option>
                                            <option value="3">Incomplete</option>
                                            <option value="4">Canceled</option>
                                            <option value="5">Paid</option>
                                            <option value="6" selected="selected">Awaiting Supplier</option>
                                        <?php endif; ?>
                                    </select>
                                    <input type="hidden" name="status" id="status1">
                                    <input type="hidden" name="hid" id="hid">
                                    <input type="hidden" name="actualid" value="<?php echo e($wajeeh); ?>">
                                </form>
                            </td>
                            <td>
                                <form id="StatusForm" action="<?php echo e(url('ChecktoPaid')); ?>" method="post" >
                                    <?php echo csrf_field(); ?>

                                    <?php
                                        $check = "check".$reports->id;
                                     ?>
                                    <input type="checkbox" name="allpaid" onclick="SingleRecord('<?php echo e($reports->id); ?>')" id="<?php echo e($check); ?>">
                                    <input type="hidden" name="check" id="check1">
                                    <input type="hidden" name="dataid" id="dataid">
                                    <input type="hidden" name="actualids" value="<?php echo e($wajeeh); ?>">
                                </form>
                            </td>
                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                               
                </tbody>                            
            </table>
        </div>
        <!-- /basic initialization -->
    <?php endif; ?>
<?php endif; ?>

<script type="text/javascript">
    $("#checkAll").click(function(){
        $('input:checkbox').not(this).prop('checked', this.checked);
        $("#CheckForm").submit();
    });
    
    function SingleRecord(getreportid){
        var check = $("#check"+getreportid).val();
        if(check != "" && check != undefined && check != null && getreportid != "" && getreportid != undefined && getreportid != null){
            $("#check1").val(check);
            $("#dataid").val(getreportid);
            //alert($("#check1").val());
            //alert($("#dataid").val());
            $("#StatusForm").submit();
        }
    }

    function Report(reportid) {
        var status = $('#status'+reportid).val();
        if(status != "" && status != undefined && status != null && reportid != "" && reportid != undefined && reportid != null){
            $("#hid").val(reportid);
            $("#status1").val(status);
            $("#reportForm").submit();
        }
        // $.ajaxSetup({
        //   headers: {
        //     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        //   }
        // });
        // var status = $('#status').val();
        // var reportid = $('#reportid').val();
        // console.log(status);

        // if(status != "" && reportid != ""){
        //         $.ajax({
        //             url: '/statusChange',
        //             type: "post",
        //             dataType: "json",
        //             cache:false,
        //             data: {
        //                 '_token': "<?php echo e(csrf_token()); ?>",
        //                 'Id': status,
        //                 'reportid': reportid,
        //             },
        //             success: function(response){// What to do if we succeed
        //                     alert(response);
        //                 },
        //             error: function(response){
        //                 alert('Error', response);
        //             }
        //         });
            
        // }
        
    }

    // $(document).ready(function() {
    //     $("").on('change', function() {
    //         var level = $(this).val();
    //         if(level){
    //             $.ajax({
    //                 url: 'statusChange',
    //                 type: "post",
    //                 data: {
    //                     val: level
    //                 },
    //                 success: function(response){// What to do if we succeed
    //                         alert(response);
    //                     }
    //             });
    //         }
    //     });
    // });
</script>
</div>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('admin.layout.dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>