<?php $__env->startSection('content'); ?>
<!-- Datatables JS FILES -->

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.print.min.js"></script>

<!-- /Datatables JS FILES -->
<!-- Simple lists -->
<!-- <div class="row pull-right">
<input type="button" onclick="printDiv('printableArea')" value="Print" class="btn btn-primary btn-lg" />

    </div> -->

    <!-- Page header -->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="<?php echo e(url('/home')); ?>">
                    <em class="fa fa-home"></em>
                </a></li>
                <li class="active">Add Payment</li>
            </ol>
        </div><!--/.row-->
    <div class="container-fluid"> 
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Add Payment</h1>
            </div>
        </div>  <!-- /page header -->
   </div>
<div class="container-fluid">
<div id="printableArea">
   

<?php $sum = 0; ?>

                    <div class="row">
                        <div class="col-md-12">
                            <!-- Simple list -->
                            <div class="panel panel-flat">
                                <div class="panel-heading">
                                    <center><h3 class="panel-title" style="margin-top:20px; margin-bottom:50px;"><b><strong>Client Payment History</strong></b></h3></center>
                                </div>

                                <div class="panel-body">
                                    <!-- <ul class="media-list">
                                        <li class="media"> -->
                                            <div class="col-md-4">
                                                <span ><h6><strong>Customer Name</strong></h6></span>
                                                <div class="media-heading "><?php echo e($trip->customer->first_name); ?> <?php echo e($trip->customer->last_name); ?></div>
                                            </div>  
                                            
                                            <div class="col-md-8"> 

                                                    <div class="col-md-4">
                                                         <span ><h6><strong>Total Sale</strong></h6></span>
                                               
                                                <div class="media-heading text-semibold">$<?php echo e($trip->total_sale); ?></div>

                                            </div>   

                                              <div class="col-md-4">
                                                <span ><h6><strong>Final Payment Date</strong></h6></span>
                                                
                                                <div class="media-heading text-semibold"><?php echo e($trip->final_payment_due); ?></div>
                                            </div> 

                                              </div>

                                                                                   
                                        <!-- </li>
                                        <br>
                                        <li class="media"> -->
                                            <div class="col-md-4">
                                                 <span ><h6><strong>Reservation Number</strong></h6></span>
                                               
                                                <div class="media-heading text-semibold"><?php echo e($trip->reservation_number); ?></div>
                                            </div> 
                                            <div  class="col-md-8">
                                                 <h6><b>Payment History</b></h2>
                                                <div style="height: 200px; overflow:auto " class="col-md-6">
                                                   
                                           
                        <table class="table ">
                            <thead>
                                <tr>
                                <th>Payment Date</th>
                                <th>Amount</th>
                                                               
                            </tr>
                            </thead>
                            <tbody>

                                <?php $__currentLoopData = $trippayments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $trippayment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                                <td><?php echo e($trippayment->payment_date); ?></td>
                                                <td>$<?php echo e($trippayment->amount); ?></td> 
                                                <td>
                                                  <form style="display:inline" method="post" action="<?php echo e(url('trip/RemovePaymentHistory')); ?>/<?php echo e($trippayment->id); ?>" onsubmit="return confirm('Are you sure you want to delete this payment?');">
                                                  <?php echo csrf_field(); ?>

                                                  <?php echo e(method_field('DELETE')); ?>

                                                  <button type="submit" class="btn btn-danger btn-sm">Remove</button>
                                        </form>
                                                </td>
                                    </tr>


                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
                                                     
                            </tbody>
                        </table>
                                                </div>

                                            </div> 
                                                      <br>                             
                                        <!-- </li>
                                        <br>
                                        <li class="media"> -->
                                            <div style="margin-top: 30px" class="col-md-4">
                                               <a onclick="showdiv()" class="btn btn-success btn-sm"  >
                                            ADD Payment
                                        </a>
                                            </div>  
                                                                                    
                                        <div style="margin-top: 50px" class="col-md-8">
                                              <div class="col-md-4">
                                                <span ><h6><strong>Balance Remaining</strong></h6></span>
                                                
                                                <div style="color: red" class="media-heading text-semibold">$<?php echo e($RemainingBalance); ?></div>
                                            </div> 

                                            <div class="col-md-4">
                                                
                                            </div>   
                                            </div> 

                                                <br>
                                            <form action="<?php echo e(url('trip/AddPaymentHistory')); ?>" method="post">  

                                                <?php echo csrf_field(); ?>

                                          <div id="pay" style="margin-top: 50px" class="col-md-4 ">
                                          <div class="form-group">
                                <label><span class="red">*</span>Payment Date:</label>
                                <input type="date" name="Payment_date" class="form-control" required>
                                  <input type="hidden"  name="trip_ID" value= <?php echo e($trip->id); ?> class="form-control" >
                            </div>     
                                            </div>  
                                                                                    
                                        <div id="pay1"  style="margin-top: 50px" class="col-md-8 ">
                                              <div class="col-md-4">
                                                <div class="form-group">
                                <label><span class="red">*</span>Amount:</label>
                                <input step="0.01" type="Number" name="amount" class="form-control" required>
                            </div>
                                            </div> 
                                            <div style="margin-top: 25px" class="col-md-4">
                                              <div>

                                                  
                                                 <button onclick="amount_limit()" type="submit" class="btn btn-primary pull-right">Apply Payment</button>
                                               
                                              </div>
                                            </div>   
                                            </div>

                                            
                                        </form>


                                  </ul>
                                 <br>
                                  <!--   <hr style="border: 2px solid black;margin-top: 100px"> -->
                                        <h3><strong>Send Client Payment Reminder</strong></h3>
                                        <p style="margin-top: -17px !important">(Client Cell Phone Must Be In Customer Profile)</p>


                                        <form action="<?php echo e(url('Remindersms')); ?>" method="post"> 
                                            
                                            <?php echo csrf_field(); ?>


                                    <div style="margin-top: 30px; margin-bottom:20px;" class="col-md-4">
                                              <input type="hidden" value="<?php echo e($trip->id); ?>" name="tripId">
                                        <button type="submit" class="btn btn-danger">SEND SMS NOW</button>
                                            </div>
                                        </form>


                                </div>

                                    
                                            
                                 
                            </div>
                            <!-- /simple list -->

                        </div>

                        
                    </div>
                
                    <!-- /simple lists -->
</div>
</div>

                    <script type="text/javascript">
                        function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}



document.getElementById('pay').style.display='none';
  
document.getElementById('pay1').style.display='none';

function showdiv()
{

var x=document.getElementById('pay');
  
var y=document.getElementById('pay1');

if (x.style.display === "none" && y.style.display === "none") {
    x.style.display = "block";
    y.style.display = "block";
  } else {
    x.style.display = "none";
    y.style.display = "none";
  }
  

}


                    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.dashboardx', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>