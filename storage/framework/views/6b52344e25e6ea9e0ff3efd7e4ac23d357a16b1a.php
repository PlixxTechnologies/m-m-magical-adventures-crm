 

<?php $__env->startSection('content'); ?>
<style>
.red{
    color: red;
}
</style>
 <!-- Page header -->
                <div class="page-header page-header-default">
                    <div class="page-header-content">
                        <div class="page-title">
                            <h4><span class="text-semibold">Add Customer</span></h4>
                        </div>
                    </div>
                </div>
   <!-- /page header -->

    <div class="container">
        <br>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">

                        <form action="<?php echo e(url('customer/save')); ?>" method="post" >
                            <?php echo csrf_field(); ?>


                            <?php if($user->role == 1): ?>
                                <div class="form-group">
                                    <label><span class="red">*</span>Agent Name:</label>
                                    <select name="userId" class="form-control" required>
                                        <option value="">Choose One</option>
                                    <?php $__currentLoopData = $allusers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>    
                                        <option value="<?php echo e($user->id); ?>"><?php echo e($user->name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                </div>
                            <?php else: ?>
                                <div class="form-group">
                                    <label>Agent Name:</label>
                                    <input type="text"  class="form-control" value="<?php echo e($user->name); ?>" readonly>
                                </div>
                            <?php endif; ?>                            


                            <div class="form-group">
                                <label><span class="red">*</span>First Name:</label>
                                <input type="text" name="first_name"  class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label><span class="red">*</span>Last Name:</label>
                                <input type="text" name="last_name"  class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label><span class="red">*</span>Email:</label>
                                <input type="email" name="email" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label>Country Code:</label>
                                <input type="text" value="+1" class="form-control" readonly="">
                            </div>
                            <div class="form-group">
                                <label><span class="red">*</span>Phone Number:</label>
                                <input type="text" name="phone_no" class="form-control" required>
                            </div>
                             <div class="form-group">
                                <label><span class="red">*</span>Address 1:</label>
                                <input type="text" name="address1" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label>Address 2:</label>
                                <input type="text" name="address2" class="form-control">
                            </div>
                            <div class="form-group">
                                <label><span class="red">*</span>City:</label>
                                <input type="text" name="city" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label><span class="red">*</span>State:</label>
                                <input type="text" maxlength="2" id="sessionNo" onkeypress="return isNumberKey(event)" name="state" class="form-control" required>

                            </div>
                            <div class="form-group">
                                <label><span class="red">*</span>Zip:</label>
                                <input type="text" name="zip" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label>Birthdate:</label>
                                <input type="date" name="birth_date" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Valid Passport:</label>
                                <select name="passport" class="form-control">
                                    <option value="">Choose One....</option>
                                    <option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                </select>
                            </div> 
                            <div class="form-group">
                                <label>Expiration Date:</label>
                                <input type="date" name="expire_date" class="form-control">
                            </div>
                            <br />
                            <!-- Add Guest -->
                            <input type="hidden" name="count" id="count" value=0 />
                            <div id="guestSection">
                                
                            </div>
                            <div class="form-group">
                                <button onclick="addGuest()" type="button" class="btn btn-success">Add Guest</button>
                            </div>   
                            <!--  <div class="form-group">
                                <label><span class="red">*</span>Booking Date:</label>
                                <input type="date" name="booking_date" class="form-control" required>
                            </div> -->
                            <div class="form-group">
                                <label>My Disney Experience User Name:</label>
                                <input type="text" name="disney_experience_username" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>My Disney Experience Password:</label>
                                <input type="text" name="disney_experience_password" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Referral:</label>
                                <input type="text" name="referral" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Notes:</label>
                                <input type="text" name="notes" class="form-control">
                            </div>

                            <button type="submit" class="btn btn-primary pull-right">Save Customer</button>
                        </form>
                    </div>
                </div>
            </div>
        </div><!--/.row-->
    </div>  <!--/.main-->

<script type="text/javascript">

   var guestCount = 1;
   var headingCount = 2;

   function addGuest(){

        var objTo = document.getElementById('guestSection');

        var guestDiv = document.createElement("div");
        guestDiv.setAttribute("class", "form-group removeclass" + guestCount);

        var removeGuestDiv = 'removeclass' + guestCount;
       
        var guestHeading = '<h3><strong>Guest No '+ headingCount +':</strong></h3>';

        var firstname = '<div class="form-group"><label><span class="red">*</span>First Name:</label><input type="text" name="guest_first_name'+ guestCount +'" id="guest_first_name'+ guestCount +'"  class="form-control" required></div>';

        var lastname = '<div class="form-group"><label><span class="red">*</span>Last Name:</label><input type="text" name="guest_last_name'+ guestCount +'" id="guest_last_name'+ guestCount +'"  class="form-control" required></div>';

        var birthdate = '<div class="form-group"><label>Birthdate:</label><input type="date" name="guest_birth_date'+ guestCount +'" id="guest_birth_date'+ guestCount +'"  class="form-control"></div>';

        var passport = '<div class="form-group"> <label>Valid Passport:</label><select name="guest_passport'+ guestCount +'" id="guest_passport'+ guestCount +'" class="form-control"><option value="">Choose One....</option><option value="Yes">Yes</option><option value="No">No</option></select></div>';


        var expirationdate = '<div class="form-group"><label>Expiration Date:</label><input type="date" name="guest_expire_date'+ guestCount +'" id="guest_expire_date'+ guestCount +'" class="form-control"></div>';

        var hiddenGuestId = '<input type="hidden" name="guest_no' + headingCount + '" id="guest_no' + headingCount + '" value='+ headingCount +' />';

        var deleteGuest = '<div class="form-group"><button onclick="removeGuest(' + guestCount + ');" type="button" class="btn btn-danger">Remove Guest</button></div>';

         guestDiv.innerHTML = guestHeading + firstname + lastname + birthdate + passport + expirationdate 
         + hiddenGuestId + '<br />';

        objTo.appendChild(guestDiv);

        document.getElementById('count').value = guestCount;

        guestCount++;
        headingCount++;

   }

   function removeGuest(gid){

         $('.removeclass' + gid).remove();
         guestCount--;
         headingCount--;
         document.getElementById('count').value = guestCount;
   }

</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>