<?php $__env->startSection('content'); ?>
<!-- Datatables JS FILES -->

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.print.min.js"></script>

<!-- /Datatables JS FILES -->
   <!-- Page header -->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="<?php echo e(url('/home')); ?>">
                    <em class="fa fa-home"></em>
                </a></li>
                <li class="active">DESTINATIONS & SUPPLIERS</li>
            </ol>
        </div><!--/.row-->
    <div class="container-fluid"> 
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">DESTINATIONS & SUPPLIERS</h1>
            </div>
        </div>  <!-- /page header -->
   </div>

<!-- Basic initialization -->
                    <div style="padding-top:20px; padding-bottom:20px; padding-right:20px; padding-left:20px; overflow-x:auto;" class="panel panel-flat">

                      <table id="example" class="display nowrap" style="width:100%">
                            <thead>
                                <tr>
                                <tr>
                               <!--  <th data-sortable="true" >ID</th> -->
                                <th data-sortable="true">Name</th>


                         <th data-sortable="true">Category</th>



                                <?php if($user->role == 1): ?>
                                    <th>Action</th>
                                    <?php endif; ?>
                            </tr>










                            </tr>
                            </thead>
                            <tbody>
                                 <?php $__currentLoopData = $destinations; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $destination): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <!-- <td id="order-id">
                                        <?php echo e($destination->id); ?>

                                    </td> -->
                                    <td>
                                         <?php echo e($destination->name); ?>

                                    </td>

                                    <td>
                                         <?php echo e($destination->destination_category); ?>

                                    </td>

                            <?php if($user->role == 1): ?>
                                    <td class="center">
                                        <a class="btn btn-info" href="<?php echo e(url('destination/edit').'/'.$destination->id); ?>">EDIT</a>
                                        <form style="display:inline" method="post" action="<?php echo e(url('destination/delete')); ?>/<?php echo e($destination->id); ?>" onsubmit="return confirm('Are you sure you want to delete this destination?');">
                                            <?php echo csrf_field(); ?>

                                            <?php echo e(method_field('DELETE')); ?>

                                            <button type="submit" class="btn btn-danger">DELETE</button>
                                        </form>
                                    </td>
                                <?php endif; ?>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                             
                            </tbody>
                        </table>
                    </div>
                </div>
                    <!-- /basic initialization -->
   
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout.dashboardx', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>