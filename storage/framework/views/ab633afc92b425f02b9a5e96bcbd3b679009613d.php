<?php $__env->startSection('content'); ?>

<style>
    .red{
color:red;
    }
</style>
<!-- Page header -->
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><span class="text-semibold">Commission Report History</span></h4>
            </div>
        </div>
    </div>
<!-- /page header -->

<div class="container">


<!-- Basic initialization -->
<div class="panel panel-flat">
    <div class="panel-heading">
        <div class="col-md-12">
            <!-- <h5 class="panel-title">Trips List</h5> -->
        </div>
    </div>

    <table class="table datatable-button-init-basic">
        <thead>
            <tr>
                <th data-sortable="true">Report ID</th>
                <th data-sortable="true">Agent Name</th>
                <th data-sortable="true">Report Creation Date</th>
                <th data-sortable="true">Export Date</th>
                <th data-sortable="true">Payment Status</th>
                <th data-sortable="true">Commissions Paid</th>
            </tr>
        </thead>
        <tbody>
              <?php $__currentLoopData = $report; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $reports): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <tr>
                <td>
                    <?php echo e($reports->id); ?>

                </td>              
                <td>
                     <?php echo e($reports->agentName); ?>

                </td>
                <td>
                    <?php echo e(date('m-d-Y', strtotime($reports->creation_date))); ?>

                </td>
                <td>
                    <?php echo e(date('m-d-Y', strtotime($reports->export_date))); ?>

                </td>
                <?php if($reports->paymentstatus == 1): ?>
                <td>
                    <span class="bg-success p-5">Processed</span>
                </td>
                <?php endif; ?>
                <?php if($reports->paymentstatus == 2): ?>
                <td>
                    <span class="bg-primary p-5">Processing</span>
                </td>
                <?php endif; ?>
                <?php if($reports->paymentstatus == 3): ?>
                <td>
                    <span class="bg-danger p-5">Incomplete</span>
                </td>
                <?php endif; ?>
                <?php if($reports->paymentstatus == 4): ?>
                <td>
                    <span class="p-5" style="background:#f1f1f1;">Canceled</span>
                </td>
                <?php endif; ?>
                <?php if($reports->paymentstatus == 5): ?>
                <td>
                    <span class="bg-success p-5">Processed</span>
                </td>
                <?php endif; ?>
                <?php if($reports->paymentstatus == 6): ?>
                <td>
                    <span class="bg-warning p-5">Awaiting Supplier</span>
                </td>
                <?php endif; ?>
                <?php if($reports->paymentstatus == 5 || $reports->paymentstatus == 1): ?>
                <td>
                    <strong>$</strong><?php echo e($reports->expectedcommssion); ?>

                </td>
                <?php else: ?>
                <td>
                    
                </td>
                <?php endif; ?>
              </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                               
        </tbody>                            
    </table>
</div>
<!-- /basic initialization -->


</div>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('admin.layout.dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>