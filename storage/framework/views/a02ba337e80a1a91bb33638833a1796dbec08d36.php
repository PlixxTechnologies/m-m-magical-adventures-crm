<?php $__env->startSection('content'); ?>
<!-- Datatables JS FILES -->

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.print.min.js"></script>

<!-- /Datatables JS FILES -->
<style>
.red{
    color: red;
}
</style>
<?php if(\Session::has('error')): ?>
    <div class="alert alert-danger">
        <ul>
            <li><?php echo \Session::get('error'); ?></li>
        </ul>
    </div>
<?php endif; ?>
<!-- Page header -->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="<?php echo e(url('/home')); ?>">
                    <em class="fa fa-home"></em>
                </a></li>
                <li class="active">Edit Agent Profile</li>
            </ol>
        </div><!--/.row-->
    <div class="container-fluid"> 
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Edit Agent Profile</h1>
            </div>
        </div>  <!-- /page header -->
   </div>
    <div class="container-fluid">
        <br>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">

                        <form action="<?php echo e(url('/user/p-edit-profile')); ?>" method="post">
                            <?php echo csrf_field(); ?>

                            
                            <div class="form-group">
                                <label><span class="red">*</span>Name:</label>
                                <input type="text" name="name" value="<?php echo e($user->name); ?>" class="form-control" required>
                                <?php if($errors->has('name')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('name')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                            <div class="form-group">
                                <label><span class="red">*</span>Email:</label>
                                <input type="email" name="email" value="<?php echo e($user->email); ?>" class="form-control" required>
                                <?php if($errors->has('email')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('email')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                            <div class="form-group">
                                <label><span class="red">*</span>Phone Number:</label>
                                <input type="text" name="phone" value="<?php echo e($user->phone); ?>" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label><span class="red">*</span>Address:</label>
                                <input type="text" name="address" value="<?php echo e($user->address); ?>" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label>Contract Signed Date:</label>
                                <input type="date" name="contract_date" value="<?php echo e($user->contract_date); ?>" class="form-control"  disabled="">
                            </div>
                                               

                            <div class="form-group">
                                <label>Status:</label>
                                <select name="status" class="form-control" disabled="">
                                    <option>--Select Status--</option>
                                    <option value="active" <?php if($user->status=="active"): ?>selected <?php endif; ?> >Active</option>
                                    <option value="inactive" <?php if($user->status=="inactive"): ?>selected <?php endif; ?>>Inactive</option>
                                    <option value="terminated" <?php if($user->status=="terminated"): ?>selected <?php endif; ?>>Terminated</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label>My Mentor:</label>
                                <select name="user_id" class="form-control" disabled="">
                                    <option> --Select Mentor--</option>}
                                    
                                    <?php if($agentmentor != null): ?>
                                    {
                                        <option value="<?php echo e($agentmentor->id); ?>" selected><?php echo e($agentmentor->name); ?></option>
                                    }

                                    <?php endif; ?>
                                </select>
                            </div>


                            <div class="form-group">
                                <label>My Commission:</label>
                                <select name="commission" class="form-control" disabled="">
                                    <option value="">Choose One</option>
                                    <option value="60" <?php if($user->commission == "60"): ?> selected <?php endif; ?>>60%</option>
                                    <option value="65"  <?php if($user->commission == "65"): ?> selected <?php endif; ?>>65%</option>
                                    <option value="70"  <?php if($user->commission == "70"): ?> selected <?php endif; ?>>70%</option>
                                    <option value="75"  <?php if($user->commission == "75"): ?> selected <?php endif; ?>>75%</option>
                                    <option value="80"  <?php if($user->commission == "80"): ?> selected <?php endif; ?>>80%</option>
                                    <option value="100"  <?php if($user->commission == "100"): ?> selected <?php endif; ?>>100%</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Sales Goal:</label>
                                <input type="text" name="sales_goal" value="<?php echo e($user->sales_goal); ?>" class="form-control" disabled="">
                            </div>


                            <div class="form-group">
                                <label>My Anniversary Date :</label>
                                <input type="date" name="fdacs_date" class="form-control" value="<?php echo e($user->fdacs_date); ?>" disabled="">
                            </div>



                            <div id="changePasswordFields">
                                
                            </div>


                            <button type="submit" class="btn btn-primary pull-right">Update Agent</button>
                            <button type="button" class="btn btn-danger pull-left mr-5" onclick="getPasswordFields()">Change Password</button>
                        </form>

                    </div>
                </div>
            </div>
        </div><!--/.row-->
    </div>  <!--/.main-->
</div>

<script type="text/javascript">
    function getPasswordFields()
    {
        $('#changePasswordFields').html("");
        var passwordFields=`<div class="form-group">
                                <label><span class="red">*</span>Password:</label>
                                <input type="password" name="password" value="" minlength='6' min='6' class="form-control" placeholder="Enter New Password" required>
                               
                            </div>
                            <div class="form-group">
                                <label><span class="red">*</span>Confirm Password:</label>
                                <input id="password-confirm" type="password" minlength='6' min='6'  class="form-control" placeholder="Confirm Password" name="password_confirmation" required>

                            </div>`;
        $('#changePasswordFields').append(passwordFields);
    }
</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.dashboardx', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>