<?php $__env->startSection('content'); ?>

<!-- Page header -->
                <div class="page-header page-header-default">
                    <div class="page-header-content">
                        <div class="page-title">
                            <h4><span class="text-semibold">Agency Total Revenue</span></h4>
                        </div>
                    </div>
                </div>
   <!-- /page header -->

    <div class="container">
       

        <div class="row">
        	<div class="col-lg-12">
        		<div class="col-lg-2">
                    
                 </div>

                 <div class="col-lg-2">
                    <label>Start Date</label>
                 </div>
                 <div class="col-lg-2">
                    <label>End Date</label>
                 </div>
                 <div class="col-lg-2">

                 </div>

              </div>
              <form action="<?php echo e(url('postagency-total-revenue')); ?>" method="post">
              	<?php echo csrf_field(); ?>

              
              <div class="col-lg-12">
              	<div class="col-lg-2">
                    
                 </div>

                 <div class="col-lg-2">
                    <input type="date" data-date-format='mm-dd-yy' name="startDate" id="startDate" class="form-control" required>
                 </div>
                 <div class="col-lg-2">
                    <input type="date" data-date-format='mm-dd-yy' name="endDate" id="endDate" class="form-control" required>
                 </div>
                 <div class="col-lg-2">
                    <button class="btn btn-info" type="submit">Get Report</button>
                 </div>

              </div>
              </form>

            

        </div>
       

   <div class="panel panel-flat mt-5">



        <table class="table datatable-button-init-basic">
            <thead>
               <tr>
                <th  data-sortable="true" >Destinations</th>
                <th  data-sortable="true">Q1 Totals</th>
                <th  data-sortable="true">Q2 Totals</th>
                <th  data-sortable="true">Q3 Totals</th>
                <th  data-sortable="true">Q4 Totals</th>
                <th  data-sortable="true">Total YTD</th>
                <th  data-sortable="true">Previous Year Totals</th>
                <th  data-sortable="true">Total YTD Commission</th>
                
            </tr>
            </thead>
            <tbody>

            	<?php if($object!=null): ?>
            	
            		<?php $__currentLoopData = $object; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $x): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            		<tr>
            			<td><?php echo e($x['destination']); ?></td>
            		
            			<td>$<?php echo e($x['q1']); ?></td>
            		
            			<td>$<?php echo e($x['q2']); ?></td>
            		
            			<td>$<?php echo e($x['q3']); ?></td>
            		
            			<td>$<?php echo e($x['q4']); ?></td>
            		
            			<td class="text-semibold">$<?php echo e($x['totalYTD']); ?></td>
            		
            			<td class="text-semibold">$<?php echo e($x['previousYearTotal']); ?></td>
            		
            			<td class="text-semibold">$<?php echo e($x['allTotal']); ?></td>
            		</tr>
            		<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

            	
            	<?php endif; ?>
               
                                  
            </tbody>
        </table>
    </div>

           
    </div>  <!--/.main-->

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>