<?php $__env->startSection('content'); ?>
<!-- Datatables JS FILES -->

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.print.min.js"></script>

<!-- /Datatables JS FILES -->

   <!-- Page header -->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="<?php echo e(url('/home')); ?>">
                    <em class="fa fa-home"></em>
                </a></li>
                <li class="active">Important Dates Calculator</li>
            </ol>
        </div><!--/.row-->
    <div class="container-fluid"> 
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Important Dates Calculator</h1>
            </div>
        </div>  <!-- /page header -->
   </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
             
            <h3><strong>Select a Destination:</strong></h3>
<div style="font-size: 13px">
                <div class="col-lg-2">
                    <input type="radio" name="radioGroup" id="dest1" value="Walt Disney World"><strong> Walt Disney World</strong>
                </div>

                <div class="col-lg-2">
                    <input type="radio" name="radioGroup" id="dest2" value="Disneyland Resort"><strong> Disneyland Resort</strong> 
                </div>
                <div class="col-lg-2">
                    <input type="radio" name="radioGroup" id="dest3" value="Disney Cruise Line"><strong> Disney Cruise Line</strong>
                </div>
                <div class="col-lg-2">
                    <input type="radio" name="radioGroup" id="dest4" value="Royal Caribbean"><strong> Royal Caribbean</strong>
                </div>
                <div class="col-lg-2">
                    <input type="radio" name="radioGroup" id="dest5" value="Carnival"><strong> Carnival</strong>
                </div>
                <br><br>
                     <h3 style="font-size: 23px"><strong>Enter travel date or select from the calender drop down:</strong></h3>

                
            </div>
          </div>
        </div><!--/.row-->

        <div class="row">
              <div class="col-lg-12">
              <!--   <div class="col-lg-4">
                </div> -->
                 <div class="col-lg-2">
                    <input type="date" data-date-format='mm-dd-yy' name="travel_date" id="travel_dete" class="form-control">
                 </div>
                 <div class="col-lg-2">
                   <button style="width:100%; margin-top:5px;" type="submit" id="Submit" onclick="hideshow()" class="btn btn-info">Calculate Dates</button>
                 </div>
            <!--      <div class="col-lg-4">
                </div> -->
   </div>
 </div>

        <div class="row">
              <div class="col-lg-12">

                 <div class="col-lg-2">
                 </div>
                 <div class="col-lg-4">
                    <center>

                    <div id="getDays">
                      
                    </div>
                    </center>
                 </div>
                 

              </div>
            

        </div>

   <div id="tbl1" class="panel panel-flat">

           <center><h3><strong>Walt Disney World Booking Date Calculator</strong></h3></center>


        <table class="table table-bordered table-responsive table-striped table-hover">
            <thead style="font-weight: 900;font-size: 18px">
               <tr>
                <th  data-sortable="false" >REMINDER</th>
                <th  data-sortable="false">DATE</th>
                
            </tr>
            </thead>
            <tbody  style="font-weight: 500;font-size: 16px">
               <tr>
                   <td>180 Days – Dining Reservations, Magical Extra Appointments and Tours</td>
                   <td id="tbl1_d1"></td>
                   
               </tr>
                <tr>
                   <td>60 Days – FastPass + on property</td>
                   <td id="tbl1_d2"></td>
                   
               </tr> 
               <tr>
                   <td>30 Days – Final payment and insurance due & FastPass off property</td>
                   <td id="tbl1_d3"></td>
                   
               </tr> 
                                  
            </tbody>
        </table>
    </div>

            <div id="tbl2" class="panel panel-flat">

                   <center><h3><strong>Disneyland Resort Booking Date Calculator</strong></h3></center>


                <table class="table table-bordered table-responsive table-striped table-hover">
                    <thead style="font-weight: 900;font-size: 18px">
                       <tr>
                        <th  data-sortable="false" >REMINDER</th>
                        <th  data-sortable="false">DATE</th>
                        
                    </tr>
                    </thead>
                    <tbody  style="font-weight: 500;font-size: 16px">
                       <tr>
                           <td>60 Days- Dining Reservations, Magical Extra Appointments and Tours</td>
                           <td id="tbl2_d1"></td>
                           
                       </tr>
                       <tr>
                           <td>30 Days - Final payment and insurance due.</td>
                           <td id="tbl2_d2"></td>
                           
                       </tr>
                      <!--   <tr>
                           <td>14 Days - Star Wars: Galaxy’s Edge (Oga’s Cantina, Droid Depot, and Sa’vi Workshop).</td>
                           <td id="tbl2_d3"></td>
                           
                       </tr> 
                         -->
                                          
                    </tbody>
                </table>
            </div>

                    <div id="tbl3" class="panel panel-flat">

                           <center><h3><strong>Disney Cruise Line Calculator</strong></h3></center>


                        <table class="table table-bordered table-responsive table-striped table-hover">
                            <thead style="font-weight: 900;font-size: 18px">
                               <tr>
                                <th  data-sortable="false" >REMINDER</th>
                                <th  data-sortable="false">DATE</th>
                                
                            </tr>
                            </thead>
                            <tbody  style="font-weight: 500;font-size: 16px">
                               <tr>
                                   <td>120 Days- Online Platinum level and Concierge.</td>
                                   <td id="tbl3_d1"></td>
                               </tr>
                               <tr>
                                   <td>105 Days- Gold level.</td>
                                   <td id="tbl3_d2"></td>
                               </tr>
                               <tr>
                                   <td>90 Days- Silver level.</td>
                                   <td id="tbl3_d3"></td>
                               </tr>
                               <tr>
                                   <td>75 Days- First time cruisers.</td>
                                   <td id="tbl3_d4"></td>
                               </tr>
                                
                                                  
                            </tbody>
                        </table>
                    </div>

                            <div id="tbl4" class="panel panel-flat">

                              <center><h3><strong>Royal Caribbean Booking Date Calculator</strong></h3></center>


                                <table class="table table-bordered table-responsive table-striped table-hover">
                                    <thead style="font-weight: 900;font-size: 18px">
                                       <tr>
                                        <th  data-sortable="false" >REMINDER</th>
                                        <th  data-sortable="false">DATE</th>
                                        
                                    </tr>
                                    </thead>
                                    <tbody  style="font-weight: 500;font-size: 16px">
                                       <tr>
                                           <td>90 Days- Online Check In and reserve embarkation time.</td>
                                           <td id="tbl4_d1"></td>
                                           
                                       </tr>
                                       
                                                          
                                    </tbody>
                                </table>
                            </div>
                                    <div id="tbl5" class="panel panel-flat">

                                           <center><h3><strong>Carnival Booking Date Calculator</strong></h3></center>


                                        <table class="table table-bordered table-responsive table-striped table-hover">
                                            <thead style="font-weight: 900;font-size: 18px">
                                               <tr>
                                                <th  data-sortable="false" >REMINDER</th>
                                                <th  data-sortable="false">DATE</th>
                                                
                                            </tr>
                                            </thead>
                                            <tbody  style="font-weight: 500;font-size: 16px">
                                               <tr>
                                                   <td>90 Days- Online Check In and reserve embarkation time.</td>
                                                   <td id="tbl5_d1"></td>
                                                   
                                               </tr>
                                                
                                                                  
                                            </tbody>
                                        </table>
                                    </div>



    </div>  <!--/.main-->
  </div>

<!--  <script>
 
 $("#tbl1").hide();    
$("#tbl2").hide();
$("#tbl3").hide();
$("#tbl4").hide();
$("#tbl5").hide();




   


function hideshow(){
  $('#getDays').html("");
    Date.prototype.toInputFormat = function() {
       var yyyy = this.getFullYear().toString();
       var mm = (this.getMonth()+1).toString(); // getMonth() is zero-based
       var dd  = this.getDate().toString();
       return (mm[1]?mm:"0"+mm[0])+ "-" + (dd[1]?dd:"0"+dd[0]) + "-" + yyyy; // padding
    }
   
        var y=$("#travel_dete").val();

     var date = new Date($("#travel_dete").val());

           days180 = parseInt(180, 10);
           days90 = parseInt(90, 10);
           days60 = parseInt(60, 10);
           days30 = parseInt(30, 10);
           days14 = parseInt(14, 10);
           days120 = parseInt(120, 10);
           days105 = parseInt(105, 10);
           days75 = parseInt(75, 10);

            var asiaTime = date.toLocaleString("en-US", {timeZone: "Asia/Karachi"});
  asiaTime = new Date(asiaTime);
  console.log('Asia time: '+asiaTime.toLocaleString())

  var usaTime = date.toLocaleString("en-US", {timeZone: "America/New_York" });
  usaTime = new Date(usaTime);
  console.log('USA time: '+usaTime.toLocaleString())

if($("#dest1").prop("checked") || $("#dest2").prop("checked") || $("#dest3").prop("checked") || $("#dest4").prop("checked") || $("#dest5").prop("checked")){

if(!isNaN(date.getTime())){

var milliseconds = date.getTime() - Date.now();

var day, hour, minute, seconds;
    seconds = Math.floor(milliseconds / 1000);
    minute = Math.floor(seconds / 60);
    seconds = seconds % 60;
    hour = Math.floor(minute / 60);
    minute = minute % 60;
    day = Math.floor(hour / 24);
    hour = hour % 24;

var months = ['January','February','March','April','May','June','July','August','September','October','November','December'];

  // console.log("Due Days: " + day);

  //console.log("Date: " + date);
  //console.log("Day: " + date.getDate());
  //console.log(months[date.getMonth()]);
  //console.log("Year: " + date.getFullYear());
  
  //console.log(new Date(Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate())));
  
 //for the bug: still one day off
 var includingCurrentDay = day+1; 

  $('#getDays').html("");

  // var textDays = `<h3 class="text-danger">`+day+` Days until this date</h3>`;
  
  var textDays = `<h3 class="text-danger">`+includingCurrentDay+` Days until this date</h3>`;
  $('#getDays').append(textDays);

            var date = new Date($("#travel_dete").val());
            date.setDate(date.getDate() - days180);
            var x180=date.toInputFormat();

            var date = new Date($("#travel_dete").val());
            date.setDate(date.getDate() - days90);
            var x90=date.toInputFormat();

            var date = new Date($("#travel_dete").val());
            date.setDate(date.getDate() - days60);
            var x60=date.toInputFormat();

            var date = new Date($("#travel_dete").val());
            date.setDate(date.getDate() - days30);
            var x30=date.toInputFormat();

            var date = new Date($("#travel_dete").val());
            date.setDate(date.getDate() - days14);
            var x14=date.toInputFormat();


            var date = new Date($("#travel_dete").val());
            date.setDate(date.getDate() - days120);
            var x120=date.toInputFormat();

            var date = new Date($("#travel_dete").val());
            date.setDate(date.getDate() - days105);
            var x105=date.toInputFormat();

            var date = new Date($("#travel_dete").val());
            date.setDate(date.getDate() - days75);
            var x75=date.toInputFormat();
     
       


if ($("#dest1").prop("checked"))
 {

$("#tbl1_d1").text(x180);
$("#tbl1_d2").text(x60);
$("#tbl1_d3").text(x30);

$("#tbl1").show();
$("#tbl2").hide();
$("#tbl3").hide();
$("#tbl4").hide();
$("#tbl5").hide();
   
}

if ($("#dest2").prop("checked"))
 {
$("#tbl2_d1").text(x60);
$("#tbl2_d2").text(x30);
$("#tbl2_d3").text(x14);

$("#tbl2").show();
$("#tbl1").hide();
$("#tbl3").hide();
$("#tbl4").hide();
$("#tbl5").hide();
   
}
if ($("#dest3").prop("checked"))
 {
    $("#tbl3_d1").text(x120);
    $("#tbl3_d2").text(x105);
    $("#tbl3_d3").text(x90);
    $("#tbl3_d4").text(x75);

$("#tbl3").show();
$("#tbl2").hide();
$("#tbl1").hide();
$("#tbl4").hide();
$("#tbl5").hide();
   
}
if ($("#dest4").prop("checked"))
 {
    $("#tbl4_d1").text(x90);

$("#tbl4").show();
$("#tbl2").hide();
$("#tbl3").hide();
$("#tbl1").hide();
$("#tbl5").hide();
   
}
if ($("#dest5").prop("checked"))
 {
    $("#tbl5_d1").text(x90);

$("#tbl5").show();
$("#tbl2").hide();
$("#tbl3").hide();
$("#tbl4").hide();
$("#tbl1").hide();
   
}
 } else {
            alert("Invalid Date"); 
                $("#tbl1").hide();    
                $("#tbl2").hide();
                $("#tbl3").hide();
                $("#tbl4").hide();
                $("#tbl5").hide();
 
        }


}else
{

}

}
 </script> -->


  <script>
 
$("#tbl1").hide();    
$("#tbl2").hide();
$("#tbl3").hide();
$("#tbl4").hide();
$("#tbl5").hide();




   


function hideshow(){
  $('#getDays').html("");
    Date.prototype.toInputFormat = function() {
       var yyyy = this.getFullYear().toString();
       var mm = (this.getMonth()+1).toString(); // getMonth() is zero-based
       var dd  = this.getDate().toString();
       return (mm[1]?mm:"0"+mm[0])+ "-" + (dd[1]?dd:"0"+dd[0]) + "-" + yyyy; // padding
    }
   
        var y=$("#travel_dete").val();
        
     var date = new Date($("#travel_dete").val());

//adding 1 extra day fo adjusting
           days180 = parseInt(179, 10);
           days90 = parseInt(89, 10);
           days60 = parseInt(59, 10);
           days30 = parseInt(29, 10);
           days14 = parseInt(13, 10);
           days120 = parseInt(119, 10);
           days105 = parseInt(104, 10);
           days75 = parseInt(74, 10);
           

           // days180 = parseInt(180, 10);
           // days90 = parseInt(90, 10);
           // days60 = parseInt(60, 10);
           // days30 = parseInt(30, 10);
           // days14 = parseInt(14, 10);
           // days120 = parseInt(120, 10);
           // days105 = parseInt(105, 10);
           // days75 = parseInt(75, 10);

  // var asiaTime = date.toLocaleString("en-US", {timeZone: "Asia/Karachi"});
  // asiaTime = new Date(asiaTime);
  // console.log('Asia time: '+asiaTime.toLocaleString())

  var usaTime = date.toLocaleString("en-US", {timeZone: "UTC" });
  usaTime = new Date(usaTime);
  console.log('USA Final time: '+usaTime.toLocaleString())

  var currentTime = new Date().toLocaleString("en-US", {timeZone: "UTC" });
  currentTime = new Date(currentTime);
  console.log('USA Current time: '+currentTime.toLocaleString())

  // var currentTime0 = date.toLocaleString("en-US", {timeZone: "Asia/Karachi" });
  // currentTime0 = new Date(currentTime0);
  // console.log('Lahore Final time: '+currentTime0.toLocaleString())

  // var currentTime1 = new Date().toLocaleString("en-US", {timeZone: "Asia/Karachi" });
  // currentTime1 = new Date(currentTime1);
  // console.log('Lahore Current time: '+currentTime1.toLocaleString())
  //console.log('Current time: '+currentTime1.getTime())


if($("#dest1").prop("checked") || $("#dest2").prop("checked") || $("#dest3").prop("checked") || $("#dest4").prop("checked") || $("#dest5").prop("checked")){

if(!isNaN(usaTime.getTime())){

var milliseconds = usaTime.getTime() - currentTime.getTime();

var day, hour, minute, seconds;
    seconds = Math.floor(milliseconds / 1000);
    minute = Math.floor(seconds / 60);
    seconds = seconds % 60;
    hour = Math.floor(minute / 60);
    minute = minute % 60;
    day = Math.floor(hour / 24);
    hour = hour % 24;

var months = ['January','February','March','April','May','June','July','August','September','October','November','December'];

  $('#getDays').html("");

  //for the bug: still one day off
  var includingCurrentDay = day+1; 

  // var textDays = `<h3 class="text-danger">`+day+` Days until this date</h3>`;
  
  var textDays = `<h3 class="text-danger">`+includingCurrentDay+` Days until this date</h3>`;
  
  $('#getDays').append(textDays);

            var date = new Date($("#travel_dete").val());
            date.setDate(date.getDate() - days180);
            var x180=date.toInputFormat();

            var date = new Date($("#travel_dete").val());
            date.setDate(date.getDate() - days90);
            var x90=date.toInputFormat();

            var date = new Date($("#travel_dete").val());
            date.setDate(date.getDate() - days60);
            var x60=date.toInputFormat();

            var date = new Date($("#travel_dete").val());
            date.setDate(date.getDate() - days30);
            var x30=date.toInputFormat();

            var date = new Date($("#travel_dete").val());
            date.setDate(date.getDate() - days14);
            var x14=date.toInputFormat();


            var date = new Date($("#travel_dete").val());
            date.setDate(date.getDate() - days120);
            var x120=date.toInputFormat();

            var date = new Date($("#travel_dete").val());
            date.setDate(date.getDate() - days105);
            var x105=date.toInputFormat();

            var date = new Date($("#travel_dete").val());
            date.setDate(date.getDate() - days75);
            var x75=date.toInputFormat();
     
       


if ($("#dest1").prop("checked"))
 {

$("#tbl1_d1").text(x180);
$("#tbl1_d2").text(x60);
$("#tbl1_d3").text(x30);

$("#tbl1").show();
$("#tbl2").hide();
$("#tbl3").hide();
$("#tbl4").hide();
$("#tbl5").hide();
   
}

if ($("#dest2").prop("checked"))
 {
$("#tbl2_d1").text(x60);
$("#tbl2_d2").text(x30);
$("#tbl2_d3").text(x14);

$("#tbl2").show();
$("#tbl1").hide();
$("#tbl3").hide();
$("#tbl4").hide();
$("#tbl5").hide();
   
}
if ($("#dest3").prop("checked"))
 {
    $("#tbl3_d1").text(x120);
    $("#tbl3_d2").text(x105);
    $("#tbl3_d3").text(x90);
    $("#tbl3_d4").text(x75);

$("#tbl3").show();
$("#tbl2").hide();
$("#tbl1").hide();
$("#tbl4").hide();
$("#tbl5").hide();
   
}
if ($("#dest4").prop("checked"))
 {
    $("#tbl4_d1").text(x90);

$("#tbl4").show();
$("#tbl2").hide();
$("#tbl3").hide();
$("#tbl1").hide();
$("#tbl5").hide();
   
}
if ($("#dest5").prop("checked"))
 {
    $("#tbl5_d1").text(x90);

$("#tbl5").show();
$("#tbl2").hide();
$("#tbl3").hide();
$("#tbl4").hide();
$("#tbl1").hide();
   
}
 } else {
            alert("Invalid Date"); 
                $("#tbl1").hide();    
                $("#tbl2").hide();
                $("#tbl3").hide();
                $("#tbl4").hide();
                $("#tbl5").hide();
 
        }


}else
{

}

}
 </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.dashboardx', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>