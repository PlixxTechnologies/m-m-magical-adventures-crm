<?php $__env->startSection('content'); ?>
<!-- Datatables JS FILES -->

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.print.min.js"></script>

<!-- /Datatables JS FILES -->
   <!-- Page header -->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="<?php echo e(url('/home')); ?>">
                    <em class="fa fa-home"></em>
                </a></li>
                <li class="active">Agency Total Revenue</li>
            </ol>
        </div><!--/.row-->
    <div class="container-fluid"> 
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Agency Total Revenue</h1>
            </div>
        </div>  <!-- /page header -->
   </div>

    <div class="container-fluid">
       

        <div class="row">
        	<!-- <div class="col-lg-12">
        	

                 <div class="col-lg-6">
                    <label>Start Date</label>
                 </div>
                 <div class="col-lg-6">
                    <label>End Date</label>
                 </div>
         

              </div> -->
              <form action="<?php echo e(url('postagency-total-revenue')); ?>" method="post">
              	<?php echo csrf_field(); ?>

              
              <div class="col-lg-12">
              	<!-- <div class="col-lg-2">
                    
                 </div> -->

                 <div class="col-lg-6">
                    <label>Start Date</label>
                    <input type="date" data-date-format='mm-dd-yy' name="startDate" id="startDate" class="form-control" required>
                 </div>
                 <div class="col-lg-6">
                    <label>End Date</label>
                    <input type="date" data-date-format='mm-dd-yy' name="endDate" id="endDate" class="form-control" required>
                 </div>
                 <div style="padding-top:10px;" class="col-lg-2">
                    <button class="btn btn-info" type="submit">Get Report</button>
                 </div>

              </div>
              </form>
 </div> <br>
       

   <div style="padding-top:20px; padding-bottom:20px; padding-right:20px; padding-left:20px; overflow-x:auto;" class="panel panel-flat mt-5">

        <table id="example" class="display nowrap" style="width:100%">
            <thead>
               <tr>
                <th  data-sortable="true" >Destinations</th>
                <th  data-sortable="true">Q1 Totals</th>
                <th  data-sortable="true">Q2 Totals</th>
                <th  data-sortable="true">Q3 Totals</th>
                <th  data-sortable="true">Q4 Totals</th>
                <th  data-sortable="true">Total YTD</th>
                <th  data-sortable="true">Previous Year Totals</th>
                <th  data-sortable="true">Total YTD Commission</th>
                
            </tr>
            </thead>
            <tbody>

            	<?php if($object!=null): ?>
            	
            		<?php $__currentLoopData = $object; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $x): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
             
            		<tr>
            			<td><?php echo e($x['destination']); ?></td>
            		
            			<td>$<?php echo e(number_format($x['q1'],2)); ?></td>
            		
            			<td>$<?php echo e(number_format($x['q2'],2)); ?></td>

            			<td>$<?php echo e(number_format($x['q3'],2)); ?></td>
            		
            			<td>$<?php echo e(number_format($x['q4'],2)); ?></td>
            
            			<td class="text-semibold">$<?php echo e(number_format($x['totalYTD'],2)); ?></td>
            		
            			<td class="text-semibold">$<?php echo e(number_format($x['previousYearTotal'],2)); ?></td>
            		
            			<td class="text-semibold">$<?php echo e(number_format($x['allTotal'],2)); ?></td>
            		</tr>
            		<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

            	
            	<?php endif; ?>
               
                                  
            </tbody>
        </table>
    </div>

           
    </div>  <!--/.main-->
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.dashboardx', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>