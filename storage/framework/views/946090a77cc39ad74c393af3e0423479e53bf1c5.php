<?php $__env->startSection('content'); ?>
<!-- Datatables JS FILES -->

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.print.min.js"></script>

<!-- /Datatables JS FILES -->

<style>
    .red{
color:red;
    }
</style>

   <!-- Page header -->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="<?php echo e(url('/home')); ?>">
                    <em class="fa fa-home"></em>
                </a></li>
                <li class="active">Date Traveled Commission Report</li>
            </ol>
        </div><!--/.row-->
    <div class="container-fluid"> 
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Date Traveled Commission Report</h1>
            </div>
        </div>  <!-- /page header -->
   </div>

<div class="container-fluid">

   <?php if(Auth::user()->role == 1): ?> 
        <div class="row">
            <form action="<?php echo e(url('trips-checkin')); ?>" method="post">
                <?php echo csrf_field(); ?>

                <input type="hidden" name="id" value="<?php echo e(Auth::user()->id); ?>">
                <div class="col-md-3 col-lg-3" style="padding: 20px;">
                    <div class="form-group">
                        <label><span class="red">*</span>Agent:</label>
                        <select name="role" id="agentId" onchange="agent()" class="form-control" required>
                            <option value="All" <?php if($selectedagentid == "All"): ?> selected <?php endif; ?>>All Agents</option>
                            <?php $__currentLoopData = $agents; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $agent): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($agent->id); ?>" <?php if($selectedagentid == $agent->id): ?> selected <?php endif; ?>><?php echo e($agent->name); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>  
                    </div>
                </div>

                <div class="col-md-3 col-lg-3" style="padding: 20px;">
                    <div class="form-group">
                        <label><span class="red">*</span>Check In Date:</label>
                        <input type="date" name="startdate" id="startdate" class="form-control" <?php if($startdate != null): ?> value="<?php echo e($startdate); ?>" <?php endif; ?> required>
                    </div>
                </div>

                <div class="col-md-3 col-lg-3" style="padding: 20px;">
                    <div class="form-group">
                        <label><span class="red">*</span>Check Out Date:</label>
                        <input type="date" name="enddate" id="enddate" class="form-control"  <?php if($enddate != null): ?> value="<?php echo e($enddate); ?>" <?php endif; ?> required>
                    </div>
                </div>

                <div class="col-md-3 col-lg-3" style="padding-top: 47px;">
                    <button type="submit" class="btn btn-primary">Get Report</button>
                </div>
            </form>
        </div>

    <?php else: ?>

        <div class="row">
            <form action="<?php echo e(url('trips-checkin')); ?>" method="post">
                <?php echo csrf_field(); ?>

                <input type="hidden" name="id" value="<?php echo e(Auth::user()->id); ?>">
                <div class="col-md-4 col-lg-4" style="padding: 20px;">
                    <div class="form-group">
                        <label><span class="red">*</span>Check In Date:</label>
                        <input type="date" name="startdate" id="startdateuser" class="form-control" <?php if($startdate != null): ?> value="<?php echo e($startdate); ?>" <?php endif; ?> required>
                    </div>
                </div>

                <div class="col-md-4 col-lg-4" style="padding: 20px;">
                    <div class="form-group">
                        <label><span class="red">*</span>Check Out Date:</label>
                        <input type="date" name="enddate" id="enddateuser" class="form-control"  <?php if($enddate != null): ?> value="<?php echo e($enddate); ?>" <?php endif; ?> required>
                    </div>
                </div> 
                <div class="col-md-4 col-lg-4" style="padding-top: 47px;">
                    <button type="submit" class="btn btn-primary">Get Report</button>
                </div>
            </form>
        </div>

    <?php endif; ?>
<br>

<?php if($trips != null): ?>
    <?php if($getSave == "Null"): ?>
        <style type="text/css">
            .dt-buttons{
                display: none;
            }
            .custom, .custom-text{
                display: block;
            }
        </style>
    <?php else: ?>
        <style type="text/css">
            .dt-buttons{
                display: block;
            }
            .custom, .custom-text{
                display: none;
            }
        </style>
    <?php endif; ?>
        
    <?php if(Auth::user()->role == 1): ?>
    
    <!-- Basic initialization -->
        <div style="padding-top:20px; padding-bottom:20px; padding-right:20px; padding-left:20px; overflow-x:auto;" class="panel panel-flat">
                <div class="panel-heading">
                    <div class="col-md-6">
                        <h5 class="panel-heading">Trips List</h5>
                    </div>
                    <div class="col-md-6">
                       <!--  <a class="btn btn-success pull-right" id="SaveReport"  onclick="Report(1)" style="display: block;">Save Report</a> -->

                       <div class="row">
                            <form action="<?php echo e(url('save-report')); ?>" method="post">
                                <?php echo csrf_field(); ?>

                                <input type="hidden" name="saveReportAgent" id="saveReportAgent">
                                <input type="hidden" name="saveReportStart" id="saveReportStart">
                                <input type="hidden" name="saveReportEnd" id="saveReportEnd">
                                <?php if($getSaveReportId != null): ?>
                                <input type="hidden" name="getSaveReportId" id="getSaveReportId" value="<?php echo e($getSaveReportId); ?>">
                                <?php endif; ?>
                                <div class="col-md-offset-9">
                                    <input type="submit" class="btn btn-success pull-right custom" id="SaveReport" value="Save Report">

                                </div>
                                <br><span class="red pull-right custom-text">Reports Must Be Saved Before Exporting</span>
                            </form>

                        </div>
                    </div>
                </div>

               <table id="example" class="display nowrap" style="width:100%"> 
                    <thead>
                        <tr>
                            <th data-sortable="true">Agent Name</th>
                            <th data-sortable="true">Customer Name</th>
                            <th data-sortable="true">Reservation Number</th>
                            <th data-sortable="true">Destination</th>
                            <th data-sortable="true">Check In Date</th>
                            <th data-sortable="true">Check Out Date</th>
                             <th data-sortable="true">Total Sale</th>
                            <th data-sortable="true">Total Commission</th>
                            <th data-sortable="true">Expected Commission</th>
                            <!--<th data-sortable="true">Created At</th>-->
                        </tr>
                    </thead>
                    <tbody>
                          <?php $__currentLoopData = $trips; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $trip): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                             <?php if(isset($trip->user)): ?>                
                            <td>
                                 <?php echo e($trip->user->name); ?>

                            </td>
                            <?php else: ?>
                            <td>
                                 Agent has been deleted
                            </td>
                            <?php endif; ?>
                            <?php if(isset($trip->customer)): ?>                
                            <td>
                                <?php echo e($trip->customer->first_name); ?> <?php echo e($trip->customer->last_name); ?>

                            </td>
                            <?php else: ?>
                            <td>
                                 Customer has been deleted
                            </td>
                            <?php endif; ?>
                            
                            <td>
                                <?php echo e($trip->reservation_number); ?>

                            </td>   
                            <td>
                                <?php echo e($trip->destination); ?>

                            </td>                                   
                            <td>
                                <?php echo e($trip->checkin_date); ?>

                            </td>
                           <td>
                                <?php echo e($trip->checkout_date); ?>

                           </td>
                            <td>
                                <?php echo e(number_format($trip->total_sale, 2)); ?>

                                <!--<?php echo e($trip->total_sale); ?> -->
                                
                            </td>
                             <td>
                                <?php echo e(number_format($trip->commission, 2)); ?>

                                <!--<?php echo e($trip->commission); ?> -->
                            </td>
                            <td>
                                <?php echo e(number_format($trip->expected_commission, 2)); ?> 
                                <!--<?php echo e($trip->expected_commission); ?> -->
                            </td>
                           <!-- <td>
                               <?php echo e(Carbon\Carbon::parse($trip->created_at)->format('d M Y')); ?>

                            </td> -->

                          </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                               
                    </tbody>                            
                </table>
            </div>
        <!-- /basic initialization -->
    <?php endif; ?>
    <?php if(Auth::user()->role != 1): ?>
        <!-- Basic initialization -->
        <div style="padding-top:20px; padding-bottom:20px; padding-right:20px; padding-left:20px;overflow-x: auto;" class="panel panel-flat">
            <div class="panel-heading">
                <div class="col-md-6">
                    <h5 class="panel-title">Trips List</h5>
                </div>
                <div class="col-md-6">
                   <div class="row">
                        <form action="<?php echo e(url('save-report')); ?>" method="post">
                            <?php echo csrf_field(); ?>

                            <input type="hidden" name="saveReportAgent" id="saveReportAgent">
                            <input type="hidden" name="saveReportStart" id="saveReportStarts">
                            <input type="hidden" name="saveReportEnd" id="saveReportEnds">
                            <?php if($getSaveReportId != "Null"): ?>
                                <input type="hidden" name="getSaveReportId" id="getSaveReportIds" value="<?php echo e($getSaveReportId); ?>">
                            <?php endif; ?>
                            <div class="col-md-offset-9">
                                <input type="submit" class="btn btn-success pull-right custom" id="SaveReport" value="Save Report">

                            </div>
                            <br><span class="red pull-right custom-text" style="margin-right: 20px;">Reports Must Be Saved Before Exporting</span>
                        </form>

                    </div>
                </div>
            </div>

            <table id="example" class="display nowrap" style="width:100%">
                <thead>
                    <tr>
                    <th data-sortable="true">Agent Name</th>
                    <th data-sortable="true">Customer Name</th>
                    <th data-sortable="true">Reservation Number</th>
                    <th data-sortable="true">Destination</th>
                    <th data-sortable="true">Check In Date</th>
                    <th data-sortable="true">Check Out Date</th>
                    <th data-sortable="true">Total Sale</th>
                    <th data-sortable="true">Total Commission</th>
                    <th data-sortable="true">Expected Commission</th>
                    <!--<th data-sortable="true">Created At</th>-->
                </tr>
                </thead>
                <tbody>
                    <?php $__currentLoopData = $trips; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $trip): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if(isset($trip->user)): ?>                
                        <td>
                            <?php echo e($trip->user->name); ?>

                        </td>
                        <?php else: ?>
                        <td>
                            Agent has been deleted
                        </td>
                        <?php endif; ?>
                        <?php if(isset($trip->customer)): ?>                
                        <td>
                            <?php echo e($trip->customer->first_name); ?> <?php echo e($trip->customer->last_name); ?>

                        </td>
                        <?php else: ?>
                        <td>
                            Customer has been deleted
                        </td>
                        <?php endif; ?>
                        
                        <td>
                            <?php echo e($trip->reservation_number); ?>

                        </td>   
                        <td>
                            <?php echo e($trip->destination); ?>

                        </td>                                   
                        <td>
                            <?php echo e($trip->checkin_date); ?>

                        </td>
                       <td>
                            <?php echo e($trip->checkout_date); ?>

                       </td>
                        <td>
                            <?php echo e(number_format($trip->total_sale, 2)); ?> 
                          <!--   <?php echo e($trip->total_sale); ?> -->
                        </td>
                         <td>
                            <?php echo e(number_format($trip->commission, 2)); ?> 
                            <!-- <?php echo e($trip->commission); ?>  -->
                        </td>
                        <td>
                            <?php echo e(number_format($trip->expected_commission, 2)); ?> 
                           <!--  <?php echo e($trip->expected_commission); ?> -->
                        </td>
                      </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                               
                </tbody>                            
            </table>
        </div>
        <!-- /basic initialization -->
    <?php endif; ?>

<?php endif; ?>
</div>


<?php if(Auth::user()->role == 1): ?>
<script type="text/javascript">
    // function Report(id) {
    //     var Rep = document.getElementById('SaveReport');
    //     var Buttons = document.getElementsByClassName('dt-buttons');
    //     // var csvButtons = document.getElementsByClassName('buttons-csv');
    //     // var pdfButtons = document.getElementsByClassName('buttons-pdf');
    //     // var excelButtons = document.getElementsByClassName('buttons-excel');
    //     var objAgent = $('#agentId').val();
    //     var objstartdate = $('#startdate').val();
    //     var objenddate = $('#enddate').val();
    //     // console.log(objAgent);
    //     // console.log(objstartdate);
    //     // console.log(objenddate);

    //     if(id == 1){
    //         if(Rep.style.display === "block"){
    //             Rep.style.display = "none";
    //             for(var i = 0; i< Buttons.length; i++){
    //                 Buttons[i].style.display = "block";
    //             }
    //             $.ajax({
    //                 url: 'savereports',
    //                 type: "get",
    //                 data: {
    //                     objagentsid:objAgent,
    //                     objsdate:objstartdate,
    //                     objedate:objenddate
    //                 },
    //                 success: function(response){// What to do if we succeed
    //                         //alert(response);
    //                         $('#savereportid').value(response['lastsavedata']);
    //                     }
    //             });
    //         }
    //     }
        
    // }

    // $(document).ready(function(){
    //     $("#SaveReport").click(function(){
    //         var Rep = document.getElementById('SaveReport');
    //         var Buttons = document.getElementsByClassName('dt-buttons');
    //         if(Rep.style.display === "block"){
    //             Rep.style.display = "none";
    //             for(var i = 0; i< Buttons.length; i++){
    //                 Buttons[i].style.display = "block";
    //             }
    //         }
    //     });
    // });

    window.onload = function (){
        var objAgentId = $('#agentId').val();
        var objstartdates = $('#startdate').val();
        var objenddates = $('#enddate').val();
        $('#saveReportAgent').val(objAgentId);
        $('#saveReportStart').val(objstartdates);
        $('#saveReportEnd').val(objenddates);

        if(objAgentId == 'All') {
           $('input[type="submit"]').attr('disabled' , true);
        }
    }
    
</script>
<?php else: ?>
<script type="text/javascript">
    window.onload = function () {
        var objstartdates = $('#startdateuser').val();
        var objenddates = $('#enddateuser').val();
        $('#saveReportStarts').val(objstartdates);
        $('#saveReportEnds').val(objenddates);
    }
</script>
<?php endif; ?>
</div>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('admin.layout.dashboardx', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>