<?php $__env->startSection('content'); ?>
<!-- Datatables JS FILES -->

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.print.min.js"></script>

<!-- /Datatables JS FILES -->
   <!-- Page header -->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="<?php echo e(url('/home')); ?>">
                    <em class="fa fa-home"></em>
                </a></li>
                <li class="active">Agent Commission Reports</li>
            </ol>
        </div><!--/.row-->
    <div class="container-fluid"> 
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Agent Commission Reports</h1>
            </div>
        </div>  <!-- /page header -->
   </div>

<div class="container-fluid">
   
    <div class="row">
        <form action="<?php echo e(url('getcommission-reports')); ?>" method="post">
          <?php echo csrf_field(); ?>

          <input type="hidden" name="id" value="<?php echo e($id); ?>">
        <div class="col-md-4 col-lg-4" style="padding: 20px;">
            <div class="form-group">
                <label>*Start Date:</label>
                <input type="date" name="startdate" class="form-control" <?php if($startdate != null): ?> value="<?php echo e($startdate); ?>" <?php endif; ?> required>
            </div>
        </div>

        <div class="col-md-offset-2 col-md-4 col-lg-offset-2 col-lg-4" style="padding: 20px;">
            <div class="form-group">
                <label>*End Date:</label>
                <input type="date" name="enddate" class="form-control"  <?php if($enddate != null): ?> value="<?php echo e($enddate); ?>" <?php endif; ?> required>
            </div>
        </div> 
  </div>
    <button type="submit" class="btn btn-primary pull-left">Get Report</button>
  </form>
</div>

<br>

<?php if($trips != null): ?>
 <br><h3 class="box-title">Total Commission : <strong>$<?php echo e(number_format($commissionTotal, 2)); ?></strong></h3>
 <h3 class="box-title">Total Expected Commission : <strong>$<?php echo e(number_format($expectedCommissionTotal, 2)); ?></strong></h3>
<h3 class="box-title">Total Sales : <strong>$<?php echo e(number_format($TotalSales, 2)); ?></strong></h3><br>
 <!-- Basic initialization -->
                    <div style="padding-top:20px; padding-bottom:20px; padding-right:20px; padding-left:20px;" class="panel panel-flat">
                        <div class="panel-heading">
                            <h5 class="panel-title">Customers List</h5>
                        </div>
<div style="overflow-x: auto;">
                       <table id="example" class="display nowrap" style="width:100%">
                            <thead>
                                <tr>
                                <th data-sortable="true">Agent Name</th>
                                <th data-sortable="true">Trip ID</th>
                                <th data-sortable="true">Customer Name</th>
                                <th data-sortable="true">Reservation Number</th>
                                <th data-sortable="true">Destination</th>
                                <th data-sortable="true">Booking Date</th>
                                <th data-sortable="true">Check In Date</th>
                                <th data-sortable="true">Check Out Date</th>
                                <th data-sortable="true">Commission</th>
                                <th data-sortable="true">Expected Commission</th>
                                <th data-sortable="true">Total Sale</th>



                                <!-- <th data-sortable="true">Email</th>
                                <th data-sortable="true">Phone Number</th>
                                                               
                                
                                <th data-sortable="true">Walt Disney World Hotel Name</th>
                                <th data-sortable="true">Disneyland Hotel Name</th>
                                <th data-sortable="true">Memory Maker</th>
                                <th data-sortable="true">Magical Express</th>
                                <th data-sortable="true">Ship Name</th>
                                <th data-sortable="true">Castaway Member</th>
                                <th data-sortable="true">Bus Transportation</th>
                                <th data-sortable="true">Insurance</th>
                                <th data-sortable="true">Travel With</th>
                                <th data-sortable="true">Special Occasions or Requests</th>
                                <th data-sortable="true">Dining Plan</th>
                                
                                
                                <th data-sortable="true">Notes</th>
                                <th data-sortable="true">Created At</th> -->
                            </tr>
                            </thead>
                            <tbody>
                                  <?php $__currentLoopData = $trips; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $trip): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                      
                                    <td>
                                         <?php echo e($trip->user->name); ?>

                                    </td>
                                    <td>
                                         <?php echo e($trip->id); ?>

                                    </td>
                                    <td>
                                        <?php echo e($trip->customer->first_name); ?> <?php echo e($trip->customer->last_name); ?>

                                    </td>

                                    <td>
                                        <?php echo e($trip->reservation_number); ?>

                                    </td>

                                     <td>
                                        <?php echo e($trip->destination); ?>

                                    </td>
                                    <td>
                                        <?php echo e($trip->booking_date); ?>

                                    </td>                                   
                                    <td>
                                        <?php echo e($trip->checkin_date); ?>

                                    </td>
                                   <td>
                                        <?php echo e($trip->checkout_date); ?>

                                   </td>

                                   <td>
                                        <?php echo e(number_format($trip->commission, 2)); ?>

                                    </td>
                                    <td>
                                        <?php echo e(number_format($trip->expected_commission, 2)); ?>

                                    </td>

                                    <td>
                                        <?php echo e(number_format($trip->total_sale, 2)); ?>

                                    </td>






                                   <!--  <td>
                                        <?php echo e($trip->customer->email); ?>

                                    </td>
                                    <td>
                                        <?php echo e($trip->customer->phone_no); ?>

                                    </td>
                                    
                                   
                                    <td>
                                        <?php echo e($trip->disneyworld_hotel_name); ?>

                                    </td>
                                    <td>
                                        <?php echo e($trip->disneyresort_hotel_name); ?>

                                    </td>
                                    <td>
                                        <?php echo e($trip->memory_maker); ?>

                                    </td>
                                    <td>
                                        <?php echo e($trip->magical_express); ?>

                                    </td>
                                    <td>
                                        <?php echo e($trip->ship_name); ?>

                                    </td>
                                    <td>
                                        <?php echo e($trip->castaway_member); ?>

                                    </td>
                                    <td>
                                        <?php echo e($trip->bus_transportation); ?>

                                    </td>
                                    <td>
                                        <?php echo e($trip->insurance); ?>

                                    </td>
                                    <td>
                                        <?php echo e($trip->travel_with); ?>

                                    </td>
                                    <td>
                                        <?php echo e($trip->special_request); ?>

                                    </td>
                                    <td>
                                        <?php echo e($trip->disney_dining_plan); ?>

                                    </td>
                                    
                                    
                                    <td>
                                        <?php echo e($trip->notes); ?>

                                    </td>
                                    <td>
                                        <?php echo e($trip->created_at->format('d M Y')); ?>

                                    </td> -->
                                  </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                               
                            </tbody>                            
                        </table>
                    </div>
                    </div>
                    <!-- /basic initialization -->
                    <?php endif; ?>

</div>
</div>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('admin.layout.dashboardx', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>