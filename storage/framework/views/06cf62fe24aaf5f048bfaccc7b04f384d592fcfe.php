<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>M&M MAGICAL ADVENTURES</title>
	<link href="<?php echo e(asset('proassests/css/bootstrap.min.css')); ?>" rel="stylesheet">
	<link href="<?php echo e(asset('proassests/css/font-awesome.min.css')); ?>" rel="stylesheet">
	<link href="<?php echo e(asset('proassests/css/datepicker3.css')); ?>" rel="stylesheet">
	<link href="<?php echo e(asset('proassests/css/styles.css')); ?>" rel="stylesheet">
	<link href="<?php echo e(asset('proassests/css/components.css')); ?>" rel="stylesheet">
	<link href="<?php echo e(url('assets/css/icons/icomoon/styles.css')); ?>" rel="stylesheet" type="text/css">



	<!--Custom Font-->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
	<!--[if lt IE 9]>
	<script src="js/html5shiv.js"></script>
	<script src="js/respond.min.js"></script>
	<![endif]-->

	<!-- Core JS files -->
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/loaders/pace.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/core/libraries/jquery.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/core/libraries/bootstrap.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/loaders/blockui.min.js')); ?>"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/tables/datatables/datatables.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/tables/datatables/extensions/buttons.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/forms/selects/select2.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js')); ?>"></script> 
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/tables/datatables/extensions/buttons.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/velocity/velocity.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/velocity/velocity.ui.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/ui/prism.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/forms/styling/uniform.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/notifications/pnotify.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/forms/selects/bootstrap_multiselect.js')); ?>"></script>

    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/forms/wizards/steps.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/core/libraries/jquery_ui/interactions.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/forms/selects/select2.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/forms/styling/uniform.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/core/libraries/jquery_ui/core.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/forms/selects/selectboxit.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/forms/inputs/typeahead/typeahead.bundle.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/forms/tags/tagsinput.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/forms/tags/tokenfield.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/forms/inputs/touchspin.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/forms/inputs/maxlength.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/forms/inputs/formatter.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/core/libraries/jasny_bootstrap.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/forms/validation/validate.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/extensions/cookie.js')); ?>"></script>

    <script type="text/javascript" src="<?php echo e(url('assets/js/core/app.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/pages/datatables_extension_buttons_init.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/pages/datatables_extension_buttons_html5.js')); ?>"></script>    
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/tables/datatables/extensions/responsive.min.js')); ?>"></script> 
    <script type="text/javascript" src="<?php echo e(url('assets/js/pages/form_select2.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/pages/form_multiselect.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/pages/wizard_steps.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/pages/user_pages_list.js')); ?>"></script>
     <script type="text/javascript" src="<?php echo e(url('assets/js/pages/datatables_advanced.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/ui/ripple.min.js')); ?>"></script>
   <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/ui/moment/moment.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/plugins/ui/fullcalendar/fullcalendar.min.js')); ?>"></script>

<!-- Select Picker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css" rel="stylesheet" />

<!-- CSS Datatable FIles -->
<link href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css" rel="stylesheet"/>
<link href="https://cdn.datatables.net/buttons/1.7.0/css/buttons.dataTables.min.css" rel="stylesheet" />



<!-- JS Datatable FILES -->
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.print.min.js"></script>

</head>
<body>
	<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse"><span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span></button>
				<a class="navbar-brand" href="#"><span>M&M MAGICAL</span> ADVENTURES</a>
			<!-- <a href="<?php echo e(url('/')); ?>"> <img src="<?php echo e(asset('proassests/media/Logo.png')); ?>" alt="logo"></a> -->
				<ul class="nav navbar-top-links navbar-right">
					<!-- <li class="dropdown"><a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
						<em class="fa fa-angle-down"></em><span class="label label-danger">15</span>
					</a>
						<ul class="dropdown-menu dropdown-messages">
							<li>
								<div class="dropdown-messages-box"><a href="profile.html" class="pull-left">
									<img alt="image" class="img-circle" src="http://placehold.it/40/30a5ff/fff">
									</a>
									<div class="message-body"><small class="pull-right">3 mins ago</small>
										<a href="#"><strong>John Doe</strong> commented on <strong>your photo</strong>.</a>
									<br /><small class="text-muted">1:24 pm - 25/03/2015</small></div>
								</div>
							</li>
							<li class="divider"></li>
							<li>
								<div class="dropdown-messages-box"><a href="profile.html" class="pull-left">
									<img alt="image" class="img-circle" src="http://placehold.it/40/30a5ff/fff">
									</a>
									<div class="message-body"><small class="pull-right">1 hour ago</small>
										<a href="#">New message from <strong>Jane Doe</strong>.</a>
									<br /><small class="text-muted">12:27 pm - 25/03/2015</small></div>
								</div>
							</li>
							<li class="divider"></li>
							<li>
								<div class="all-button"><a href="#">
									<em class="fa fa-inbox"></em> <strong>All Messages</strong>
								</a></div>
							</li>
						</ul>
					</li> -->
					<li class="dropdown"><a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
						<em class="fa fa-angle-down"></em><!-- <span class="label label-info">5</span> -->
					</a>
						<ul class="dropdown-menu dropdown-alerts">
							<li><a href="<?php echo e(url('/user/editProfile')); ?>">
								<div><em class="fa fa-pencil"></em>Edit Profile
							<!-- <span class="pull-right text-muted small">3 mins ago</span></div>
							</a> --></li>
							<li class="divider" style="width:100%"></li>
							<li><a href="<?php echo e(url('my-commission')); ?>">
								<div><em class="fa fa-money"></em>Commission Reports
								<!-- <span class="pull-right text-muted small">4 mins ago</span> --></div>
							</a></li>
							<li class="divider"></li>
							<li><a href="<?php echo e(url('logout')); ?>">
								<div><em class="icon-switch2"></em>Logout
								<!-- <span class="pull-right text-muted small">4 mins ago</span> --></div>
							</a></li>
<!-- 							<li class="divider"></li>
							<li><a href="#">
								<div><em class="fa fa-user"></em> 5 New Followers
									<span class="pull-right text-muted small">4 mins ago</span></div>
							</a></li> -->
						</ul>
					</li>
				</ul>
			</div>
		</div><!-- /.container-fluid -->
	</nav>
	<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<div class="profile-sidebar">
			<div class="profile-userpic">
				<!-- <img src="http://placehold.it/50/30a5ff/fff" class="img-responsive" alt=""> -->
			</div>
			<div class="profile-usertitle">
				<div class="profile-usertitle-name"><?php echo e(Auth::user()->name); ?></div>
				<div class="profile-usertitle-status"><span class="indicator label-success"></span>Online</div>
			</div>
			<div class="clear"></div>
		</div>
<!-- 		<div class="divider"></div>
		<form role="search">
			<div class="form-group">
				<input type="text" class="form-control" placeholder="Search">
			</div>
		</form> -->
		<ul class="nav menu">
		<li class="active"><a href="<?php echo e(url('/home')); ?>"><em class="fa fa-dashboard">&nbsp;</em>Dashboard</a></li>
		
			<?php if(Auth::user()->role == 1): ?>
			<li class="parent "><a data-toggle="collapse" href="#sub-item-1">
				<em class="fa fa-user-secret">&nbsp;</em> Agents<span data-toggle="collapse" href="#sub-item-1" class="icon pull-right"><em class="fa fa-plus"></em></span>
				</a>
				<ul class="children collapse" id="sub-item-1">
					<li><a class="" href="<?php echo e(url('user/list')); ?>">
						<span class="fa fa-arrow-right">&nbsp;</span>List Agents
					</a></li>
					<li><a class="" href="<?php echo e(url('user/add')); ?>">
						<span class="fa fa-arrow-right">&nbsp;</span>Add Agent
					</a></li>
				</ul>
			</li>
			 <?php endif; ?>

        <li class="parent "><a data-toggle="collapse" href="#sub-item-5">
				<em class="fa fa-bullhorn">&nbsp;</em>Leads<span data-toggle="collapse" href="#sub-item-5" class="icon pull-right"><em class="fa fa-plus"></em></span>
				</a>
				<ul class="children collapse" id="sub-item-5">
					<li><a class="" href="<?php echo e(url('lead/list')); ?>">
						<span class="fa fa-arrow-right">&nbsp;</span>List Leads
					</a></li>
					<li><a class="" href="<?php echo e(url('lead/create')); ?>">
						<span class="fa fa-arrow-right">&nbsp;</span>Add Leads
					</a></li></ul>
	     </li>

      <li><a href="<?php echo e(url('quote/list')); ?>"><em class="fa fa-paste">&nbsp;</em>Quotes</a></li>

			<li class="parent "><a data-toggle="collapse" href="#sub-item-2">
				<em class="fa fa-users">&nbsp;</em>Customers<span data-toggle="collapse" href="#sub-item-2" class="icon pull-right"><em class="fa fa-plus"></em></span>
				</a>
				<ul class="children collapse" id="sub-item-2">
					<li><a class="" href="<?php echo e(url('customer/list')); ?>">
						<span class="fa fa-arrow-right">&nbsp;</span>List Customers
					</a></li>
					<li><a class="" href="<?php echo e(url('customer/create')); ?>">
						<span class="fa fa-arrow-right">&nbsp;</span>Add Customer
					</a></li>
					<li><a class="" href="<?php echo e(url('import-excel')); ?>">
						<span class="fa fa-arrow-right">&nbsp;</span>Customer Import
					</a></li>
					<li><a class="" href="<?php echo e(url('sms-list')); ?>">
						<span class="fa fa-arrow-right">&nbsp;</span>List Customer Sms
					</a></li>
					<li><a class="" href="<?php echo e(url('reminder-sms-list')); ?>">
						<span class="fa fa-arrow-right">&nbsp;</span>List Payment Reminder Sms
					</a></li>
					<li><a class="" href="<?php echo e(url('sms')); ?>">
						<span class="fa fa-arrow-right">&nbsp;</span>Customer Sms
					</a></li>
				</ul>
			</li>

			<li><a href="<?php echo e(url('trip/list')); ?>"><em class="fa fa-suitcase">&nbsp;</em>Trips</a></li>

<?php if(Auth::user()->role == 0): ?>
<li><a href="<?php echo e(url('calenders-list')); ?>"><em class="fa fa-tasks">&nbsp;</em>Calendar</a></li>
<li><a href="<?php echo e(url('diningplan/list')); ?>"><em class="fa fa-copy">&nbsp;</em>Disney Dining Plans</a></li>
<li><a href="<?php echo e(url('/dining-plan-calculator')); ?>"><em class="fa fa-calculator">&nbsp;</em>Disney Dining Plan Calculator</a></li>
			<li><a href="<?php echo e(url('important-date-calculator')); ?>"><em class="fa fa-calculator">&nbsp;</em>Important Dates Calculator</a></li>
			<li class="parent "><a data-toggle="collapse" href="#sub-item-4">
				<em class="fa fa-bar-chart">&nbsp;</em>Reports <span data-toggle="collapse" href="#sub-item-4" class="icon pull-right"><em class="fa fa-plus"></em></span>
				</a>
				<ul class="children collapse" id="sub-item-4">
					<li><a class="" href="<?php echo e(url('trips-checkin')); ?>">
						<span class="fa fa-arrow-right">&nbsp;</span>Date Traveled Commission Report
					</a></li>
				</ul>
			</li>
				 <li><a href="<?php echo e(url('commisions-list')); ?>"><em class="fa fa-credit-card">&nbsp;</em>Commissions</a></li>
<?php endif; ?>


<?php if(Auth::user()->role == 1): ?>
			<li><a href="<?php echo e(url('calenders-list')); ?>"><em class="fa fa-calendar">&nbsp;</em>Calender</a></li>
<?php endif; ?>


			<li><a href="<?php echo e(url('todo/list')); ?>"><em class="fa fa-tasks">&nbsp;</em>TO-DO's</a></li>
			<?php if(Auth::user()->role == 0): ?><li><a href="<?php echo e(url('todo/todolists')); ?>"><em class="fa fa-tasks">&nbsp;</em>TO-DO List</a></li><?php endif; ?>


			<?php if(Auth::user()->role == 1): ?>
			<li class="parent "><a data-toggle="collapse" href="#sub-item-3">
				<em class="fa fa-map">&nbsp;</em>Destinations & Suppliers  <span data-toggle="collapse" href="#sub-item-3" class="icon pull-right"><em class="fa fa-plus"></em></span>
				</a>
				<ul class="children collapse" id="sub-item-3">
					<li><a class="" href="<?php echo e(url('destination/list')); ?>">
						<span class="fa fa-arrow-right">&nbsp;</span>List Destinations
					</a></li>
					<li><a class="" href="<?php echo e(url('destination/add')); ?>">
						<span class="fa fa-arrow-right">&nbsp;</span>Add Destination
					</a></li>
				</ul>
			</li>
			 
			<li><a href="<?php echo e(url('diningplan/list')); ?>"><em class="fa fa-copy">&nbsp;</em>Disney Dining Plans</a></li>
						<li><a href="<?php echo e(url('/dining-plan-calculator')); ?>"><em class="fa fa-calculator">&nbsp;</em>Disney Dining Plan Calculator</a></li>
			<li><a href="<?php echo e(url('important-date-calculator')); ?>"><em class="fa fa-calculator">&nbsp;</em>Important Dates Calculator</a></li>
			<?php endif; ?>


<!--  <?php if(Auth::user()->role == 0): ?>

 <?php endif; ?> -->





			<?php if(Auth::user()->role == 1): ?>
			<li class="parent "><a data-toggle="collapse" href="#sub-item-4">
				<em class="fa fa-bar-chart">&nbsp;</em>Reports <span data-toggle="collapse" href="#sub-item-4" class="icon pull-right"><em class="fa fa-plus"></em></span>
				</a>
				<ul class="children collapse" id="sub-item-4">
					<li><a class="" href="<?php echo e(url('trips-checkin')); ?>">
						<span class="fa fa-arrow-right">&nbsp;</span>Date Traveled Commission Report
					</a></li>
					<li><a class="" href="<?php echo e(url('commission-agents')); ?>">
						<span class="fa fa-arrow-right">&nbsp;</span>All Agent Reports
					</a></li>
					<li><a class="" href="<?php echo e(url('walt-disney-world-reports')); ?>">
						<span class="fa fa-arrow-right">&nbsp;</span>Walt Disney World Reports
					</a></li>
					<li><a class="" href="<?php echo e(url('disneyland-resort-reports')); ?>">
						<span class="fa fa-arrow-right">&nbsp;</span>Disneyland Resort Reports
					</a></li>
					<li><a class="" href="<?php echo e(url('disney-cruise-line-reports')); ?>">
						<span class="fa fa-arrow-right">&nbsp;</span>Disney Cruise Line Reports
					</a></li>
					<li><a class="" href="<?php echo e(url('commission-destinations')); ?>">
						<span class="fa fa-arrow-right">&nbsp;</span>Other Destination Reports
					</a></li>
					<li><a class="" href="<?php echo e(url('getagency-total-revenue')); ?>">
						<span class="fa fa-arrow-right">&nbsp;</span>Agency Total Revenue Report
					</a></li>
				</ul>
			</li>
						 <li><a href="<?php echo e(url('commisions-list')); ?>"><em class="fa fa-credit-card">&nbsp;</em>Commissions</a></li>
			 <li><a href="<?php echo e(url('auditlogs/list')); ?>"><em class="fa fa-list">&nbsp;</em>Audit Logs</a></li>
			
			 <?php endif; ?>
			<li><a href="<?php echo e(url('logout')); ?>"><em class="fa fa-power-off">&nbsp;</em> Logout</a></li>
		</ul>
	</div><!--/.sidebar-->


		


		<!-- FOOTER -->

		   <div class="content-wrapper">   
                <!-- Content area -->
                <div class="content">
                	<?php echo $__env->yieldContent('content'); ?>
			<div class="col-sm-12">
				<p class="back-link">© 2021 Powered by <a href="https://www.nodlays.com">Nodlays</a></p>
			</div>
		</div><!--/.row-->
	</div>	<!--/.main-->


	
<!-- <script src="<?php echo e(asset('proassests/js/jquery-1.11.1.min.js')); ?>"></script>
	<script src="<?php echo e(asset('proassests/js/bootstrap.min.js')); ?>"></script> -->
	<script src="<?php echo e(asset('proassests/js/chart.min.js')); ?>"></script>
	<script src="<?php echo e(asset('proassests/js/chart-data.js')); ?>"></script>
	<script src="<?php echo e(asset('proassests/js/easypiechart.js')); ?>"></script>
	<script src="<?php echo e(asset('proassests/js/easypiechart-data.js')); ?>"></script>
<!-- 	<script src="<?php echo e(asset('proassests/js/bootstrap-datepicker.js')); ?>"></script>
	<script src="<?php echo e(asset('proassests/js/custom.js')); ?>"></script>
	<script src="<?php echo e(asset('proassests/js/multipleselect.js')); ?>"></script>  -->





	<script>
		window.onload = function () {
	var chart1 = document.getElementById("line-chart").getContext("2d");
	window.myLine = new Chart(chart1).Line(lineChartData, {
	responsive: true,
	scaleLineColor: "rgba(0,0,0,.2)",
	scaleGridLineColor: "rgba(0,0,0,.05)",
	scaleFontColor: "#c5c7cc"
	});
};
	</script>

<!-- Datatables Script -->

	<script type="text/javascript"> 
    $(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'print'
        ]
    } );
} );


</script>
		
</body>
</html>