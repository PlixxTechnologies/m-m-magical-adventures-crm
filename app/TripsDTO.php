<?php
namespace App;

 class TripsDTO
 {
     public $start;
     public $end;
     public $title;
     public $color;
     public $id;


  // public function __construct(TripsDTO $trip)
  //  {
  //      $this->start = $trip->getStartDate();
  //      $this->end = $trip->getEndDate();
  //      $this->title = $trip->gettitle();
  //      $this->color = $trip->getColor();
  //      $this->id = $trip->getId();
  //  }

   public function getStartDate()
   {
       return $this->start;
   }

   public function setStartDate($startDate)
   {
        $this->start=$startDate;
   }

   public function getEndDate()
   {
       return $this->end;
   }

   public function setEndDate($endDate)
   {
        $this->end=$endDate;
   }

   public function gettitle()
   {
       return $this->title;
   }

   public function settitle($titles)
   {
        $this->title=$titles;
   }

   public function getColor()
   {
     return $this->color;
   }

   public function setColor($colors)
   {
        $this->color=$colors;
   }

   public function getId()
   {
       return $this->id;
   }

   public function setID($Ids)
   {
        $this->id=$Ids;
   }

 }


?>