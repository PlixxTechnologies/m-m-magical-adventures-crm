<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class lead extends Model
{
     public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function trips()
    {
        return $this->hasMany('App\Trip');
    }

    public function textmessages()
    {
        return $this->hasMany('App\TextMessage');
    }

    public function todos()
    {
        return $this->hasMany('App\Todo');
    }

    public function guests()
    {
        return $this->hasMany('App\Guest');
    }

    public function quotes() {
        return $this->hasMany('App\Quote');
    }
}
