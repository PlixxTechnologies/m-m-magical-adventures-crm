<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Destination extends Model
{
    public function hotels()
    {
        return $this->hasMany('App\Hotel');
    }

    public function hotelcategories()
    {
        return $this->hasMany('App\Hotel');
    }
}
