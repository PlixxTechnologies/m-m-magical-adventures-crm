<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SaveReport extends Model
{
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function savereportdatas()
    {
        return $this->belongsTo('App\SaveReportData');
    }
}
