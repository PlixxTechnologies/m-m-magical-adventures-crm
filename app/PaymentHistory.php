<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentHistory extends Model
{
    public function trip()
    {
        return $this->belongsTo('App\Trip');
    }
}
