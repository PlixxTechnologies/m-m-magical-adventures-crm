<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hotel extends Model
{
    public function destination()
    {
        return $this->belongsTo('App\Destination');
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }
}
