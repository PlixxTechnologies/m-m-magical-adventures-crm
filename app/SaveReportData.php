<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SaveReportData extends Model
{
	public function user()
    {
        return $this->belongsTo('App\User');
    }
    
    public function trips()
    {
        return $this->hasMany('App\Trip');
    }

    public function savereports()
    {
        return $this->hasMany('App\SaveReport');
    }
}
