<?php
namespace App;

 class ReportDTO
 {
     public $tripid;
     public $savereportId;
     public $expectedcommsion;
     public $ytdcommission;
     public $id;
     public $paymentstatus;
     public $CustomerName;
     public $agentName;
     public $CheckinDate;
     public $CheckoutDate;
     public $destination;
     public $reservation;
     public $export_date;
     public $creation_date;
     public $travelmonth;
     public $isActive;
     public $commissionTotal;
 }


?>