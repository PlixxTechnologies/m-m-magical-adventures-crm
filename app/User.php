<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function customers()
    {
        return $this->hasMany('App\Customer');
    }

    public function trips()
    {
        return $this->hasMany('App\Trip');
    }

    public function textmessages()
    {
        return $this->hasMany('App\TextMessage');
    }

    public function todos()
    {
        return $this->hasMany('App\Todo');
    }

    public function savereports()
    {
        return $this->hasMany('App\SaveReport');
    }

    public function savereportdatas()
    {
        return $this->hasMany('App\SaveReportData');
    }

    public function quotes()
    {
        return $this->hasMany('App\Quote');
    }
}
