<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trip extends Model
{
    public function customer()
    {
        return $this->belongsTo('App\Customer');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function todo()
    {
        return $this->hasOne('App\Todo');
    }

    public function paymenthistories()
    {
        return $this->hasMany('App\PaymentHistory');
    }

    public function savereportdatas()
    {
        return $this->belongsTo('App\SaveReportData');
    }
}
