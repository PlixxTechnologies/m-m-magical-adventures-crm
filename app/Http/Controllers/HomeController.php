<?php

namespace App\Http\Controllers;

use App\CalculatorYear;
use App\Agentdestination;
use App\Trip;
use App\Todo;
use App\User;
use App\DiningPlan;
use App\Customer;
use App\Guest;
use App\Hotel;
use App\HotelCategory;
use App\Destination;
use App\PaymentHistory;
use App\AuditLogs;
use App\SaveReport;
use App\ReportDTO;
use App\lead;
use App\LeadGuest;
use App\todolistings;
use App\quote;
use App\SaveReportData;
use App\categorynames;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Hash;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\NewTrip;
use DateTime;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    // public function todolists() {
    //     return view('admin/todo/todolists');
    // }

    public function index() {
        
        $current_month = date('Y-m');
        $current_year = date('Y');

        if(Auth::user()->role == 1)
        {
            $allcustomers = Customer::all();
            $monthcustomers = new Collection();
            $yearcustomers = new Collection();        
            foreach($allcustomers as $customer)
            {
                if(date("Y-m",strtotime($customer->created_at)) == $current_month)
                {
                    $monthcustomers->push($customer);
                }
                if(date("Y",strtotime($customer->created_at)) == $current_year)
                {
                    $yearcustomers->push($customer);
                }
            } 


            $alltrips = Trip::where('trip_status', '=', "Booked Deposit")->orWhere('trip_status', '=', "Paid in Full")->orWhere('trip_status', '=', "Complete – Commission Paid")->get();
            $monthtrips = new Collection();
            $yeartrips = new Collection();        
            foreach($alltrips as $trip)
            {
                if(date("Y-m",strtotime($trip->checkin_date)) == $current_month)
                {
                    $monthtrips->push($trip);
                }
                if(date("Y",strtotime($trip->checkin_date)) == $current_year)
                {
                    $yeartrips->push($trip);
                }
            }
            

            $mytrips = Trip::where('user_id', '=', Auth::user()->id)
                        ->where(function ($query) {
                           $query->where('trip_status', '=', "Booked Deposit")->orWhere('trip_status', '=', "Paid in Full")->orWhere('trip_status', '=', "Complete – Commission Paid");
                        })->get();


            $mymonthtrips = new Collection();
            $myyeartrips = new Collection();        
            foreach($mytrips as $trip)
            {
                if(date("Y-m",strtotime($trip->checkin_date)) == $current_month)
                {
                    $mymonthtrips->push($trip);
                }
                if(date("Y",strtotime($trip->checkin_date)) == $current_year)
                {
                    $myyeartrips->push($trip);
                }
            }
            
            $agentsales = 0.0;
            if($myyeartrips->count() > 0)
            {
                foreach($myyeartrips as $trip)
                {
                    $agentsales = $agentsales + $trip->total_sale;
                }
            }


            $agentsalespercentage = 0;
            if(Auth::user()->sales_goal != null)
            {
                 $agentsalespercentage = number_format((float)(($agentsales/Auth::user()->sales_goal) * 100), 1, '.', '');
            }
            

            // $todolist = Todo::where('user_id', '=', Auth::user()->id)->orderBy('due_date', 'desc')->take(10)->get();

            $todolist = Todo::where('isComplete', '=', 0)->orderBy('due_date', 'desc')->get();
           


            
            $data['todolist'] = $todolist;
            $data['todolistcount'] = $todolist->count();
            $data['agentsalespercentage'] = round($agentsalespercentage,2);
            $data['trips_month'] = $monthtrips->count();
            $data['customers_month'] = $monthcustomers->count();            
            $data['total_customer'] = Customer::all()->count();
            //$data['users_month'] = $monthusers->count();
            // $data['total_users'] = User::all()->count();    
            $data['total_trips'] = Trip::all()->count();   
            $data['first_name'] = Trip::all()->count(); 
            $data['fast_pass_date'] = Trip::all()->count();
            $data['final_payment_due'] = Trip::all()->count();
            $data['advanced_dining_reservations'] = Trip::all()->count();
            $data['itinerary_tip_sheets'] = Trip::all()->count();
            $data['month_expected_commission'] =  $mymonthtrips->sum('expected_commission');
            $data['year_total_commission'] = $myyeartrips->sum('commission');


            return view('home', $data);
        }
        else
        {
            $user = Auth::user();          
            $allcustomers = Customer::where('user_id', '=', Auth::user()->id)->get();
            $monthcustomers = new Collection();
            $yearcustomers = new Collection();        
            foreach($allcustomers as $customer)
            {
                if(date("Y-m",strtotime($customer->created_at)) == $current_month)
                {
                    $monthcustomers->push($customer);
                }
                if(date("Y",strtotime($customer->created_at)) == $current_year)
                {
                    $yearcustomers->push($customer);
                }
            } 

            $alltrips = new Collection();

            $alltrips = Trip::where('user_id', '=', $user->id)
                        ->where(function ($query) {
                           $query->where('trip_status', '=', "Booked Deposit")->orWhere('trip_status', '=', "Paid in Full")->orWhere('trip_status', '=', "Complete – Commission Paid");
                        })->get();


            $monthtrips = new Collection();
            $yeartrips = new Collection();        
            foreach($alltrips as $trip)
            {
                if(date("Y-m",strtotime($trip->checkin_date)) == $current_month)
                {
                    $monthtrips->push($trip);
                }
                if(date("Y",strtotime($trip->checkin_date)) == $current_year)
                {
                    $yeartrips->push($trip);
                }
            }
            
            $agentsales = 0.0;
            if($yeartrips->count() > 0)
            {
                foreach($yeartrips as $trip)
                {
                    $agentsales = $agentsales + $trip->total_sale;
                }
            }
            
             $agentsalespercentage = 0;
            if(Auth::user()->sales_goal != null)
            {
                 $agentsalespercentage = ($agentsales/Auth::user()->sales_goal) * 100;
            }            

            // $todolist = Todo::where('user_id', '=', Auth::user()->id)->orderBy('due_date', 'desc')->take(10)->get();

            $todolist = Todo::where('user_id', '=', Auth::user()->id)->where('isComplete', '=', 0)->orderBy('due_date', 'desc')->get();

            //Edited By Hassan
            $todolistings = Todolistings::where('user_id', '=', Auth::user()->id)->get();


            $data['todolist'] = $todolist;
            $data['todolistcount'] = $todolist->count();
            $data['agentsalespercentage'] = round($agentsalespercentage,2);
            $data['trips_month'] = $monthtrips->count();
            $data['customers_month'] = $monthcustomers->count();
            $data['total_customer'] = $allcustomers->count();  
            $data['total_trips'] = $alltrips->count(); 
            $data['month_expected_commission'] =  $monthtrips->sum('expected_commission');
            $data['year_total_commission'] = $yeartrips->sum('commission');
            //Edited By Hassan
            $data['todolistings'] = $todolistings;

            return view('home', $data);
        }
    }

    //TODO Lists
    public function showtodolist() {

        $current_month = date('Y-m');
        $current_year = date('Y');

        if(Auth::user()->role == 1)
        {
            $todolistings = Todolisting::all();
            $allcustomers = Customer::all();
            $monthcustomers = new Collection();
            $yearcustomers = new Collection();        
            foreach($allcustomers as $customer)
            {
                if(date("Y-m",strtotime($customer->created_at)) == $current_month)
                {
                    $monthcustomers->push($customer);
                }
                if(date("Y",strtotime($customer->created_at)) == $current_year)
                {
                    $yearcustomers->push($customer);
                }
            } 


            $alltrips = Trip::where('trip_status', '=', "Booked Deposit")->orWhere('trip_status', '=', "Paid in Full")->orWhere('trip_status', '=', "Complete – Commission Paid")->get();
            $monthtrips = new Collection();
            $yeartrips = new Collection();        
            foreach($alltrips as $trip)
            {
                if(date("Y-m",strtotime($trip->checkin_date)) == $current_month)
                {
                    $monthtrips->push($trip);
                }
                if(date("Y",strtotime($trip->checkin_date)) == $current_year)
                {
                    $yeartrips->push($trip);
                }
            }
            

            $mytrips = Trip::where('user_id', '=', Auth::user()->id)
                        ->where(function ($query) {
                           $query->where('trip_status', '=', "Booked Deposit")->orWhere('trip_status', '=', "Paid in Full")->orWhere('trip_status', '=', "Complete – Commission Paid");
                        })->get();


            $mymonthtrips = new Collection();
            $myyeartrips = new Collection();        
            foreach($mytrips as $trip)
            {
                if(date("Y-m",strtotime($trip->checkin_date)) == $current_month)
                {
                    $mymonthtrips->push($trip);
                }
                if(date("Y",strtotime($trip->checkin_date)) == $current_year)
                {
                    $myyeartrips->push($trip);
                }
            }
            
            $agentsales = 0.0;
            if($myyeartrips->count() > 0)
            {
                foreach($myyeartrips as $trip)
                {
                    $agentsales = $agentsales + $trip->total_sale;
                }
            }


            $agentsalespercentage = 0;
            if(Auth::user()->sales_goal != null)
            {
                 $agentsalespercentage = number_format((float)(($agentsales/Auth::user()->sales_goal) * 100), 1, '.', '');
            }
            

            // $todolist = Todo::where('user_id', '=', Auth::user()->id)->orderBy('due_date', 'desc')->take(10)->get();

            $todolist = Todo::where('user_id', '=', Auth::user()->id)->where('isComplete', '=', 0)->orderBy('due_date', 'desc')->get();
           


            
            $data['todolist'] = $todolist;
            $data['todolistcount'] = $todolist->count();
            $data['agentsalespercentage'] = round($agentsalespercentage,2);
            $data['trips_month'] = $monthtrips->count();
            $data['customers_month'] = $monthcustomers->count();            
            $data['total_customer'] = Customer::all()->count();
            //$data['users_month'] = $monthusers->count();
            // $data['total_users'] = User::all()->count();    
            $data['total_trips'] = Trip::all()->count();   
            $data['month_expected_commission'] =  $mymonthtrips->sum('expected_commission');
            $data['year_total_commission'] = $myyeartrips->sum('commission');

            return view('home', $data);
        }
        else
        {
            $user = Auth::user();  
            $todolistings = Todolistings::where('user_id', '=', Auth::user()->id)->get();        
            $allcustomers = Customer::where('user_id', '=', Auth::user()->id)->get();
            $monthcustomers = new Collection();
            $yearcustomers = new Collection();        
            foreach($allcustomers as $customer)
            {
                if(date("Y-m",strtotime($customer->created_at)) == $current_month)
                {
                    $monthcustomers->push($customer);
                }
                if(date("Y",strtotime($customer->created_at)) == $current_year)
                {
                    $yearcustomers->push($customer);
                }
            } 

            $alltrips = new Collection();

            $alltrips = Trip::where('user_id', '=', $user->id)
                        ->where(function ($query) {
                           $query->where('trip_status', '=', "Booked Deposit")->orWhere('trip_status', '=', "Paid in Full")->orWhere('trip_status', '=', "Complete – Commission Paid");
                        })->get();


            $monthtrips = new Collection();
            $yeartrips = new Collection();        
            foreach($alltrips as $trip)
            {
                if(date("Y-m",strtotime($trip->checkin_date)) == $current_month)
                {
                    $monthtrips->push($trip);
                }
                if(date("Y",strtotime($trip->checkin_date)) == $current_year)
                {
                    $yeartrips->push($trip);
                }
            }
            
            $agentsales = 0.0;
            if($yeartrips->count() > 0)
            {
                foreach($yeartrips as $trip)
                {
                    $agentsales = $agentsales + $trip->total_sale;
                }
            }
            
             $agentsalespercentage = 0;
            if(Auth::user()->sales_goal != null)
            {
                 $agentsalespercentage = ($agentsales/Auth::user()->sales_goal) * 100;
            }            

            // $todolist = Todo::where('user_id', '=', Auth::user()->id)->orderBy('due_date', 'desc')->take(10)->get();

            $todolist = Todo::where('user_id', '=', Auth::user()->id)->where('isComplete', '=', 0)->orderBy('due_date', 'desc')->get();


            $data['todolist'] = $todolist;
            $data['todolistings'] = $todolistings;
            $data['todolistcount'] = $todolist->count();
            $data['agentsalespercentage'] = round($agentsalespercentage,2);
            $data['trips_month'] = $monthtrips->count();
            $data['customers_month'] = $monthcustomers->count();
            $data['total_customer'] = $allcustomers->count();  
            $data['total_trips'] = $alltrips->count(); 
            $data['month_expected_commission'] =  $monthtrips->sum('expected_commission');
            $data['year_total_commission'] = $yeartrips->sum('commission');

            return view('/admin/todo/todolists' , $data);
        }
    }
        
    public function toggleTodo($id)
    {        
        $c = Todo::find($id);
        if($c->isComplete == NULL)
        {
            $c->isComplete = 0;
        }

        if($c->isComplete == 0)
        {
            $c->isComplete = 1;
        }
        else
        {
            $c->isComplete = 0;
        }           

        $c->save();

        return redirect('/home');
    }


    public function logout()
    {
        Auth::logout();

        return redirect('/');
    }

// User CRUD
    public function listUsers()
    {
        $users = User::all();

        for($i=0; $i<$users->count();$i++) 
        {
            $sdate=$users[$i]->contract_date;
                $sdate = strtotime($sdate);
                $sdate = date('m-d-Y',$sdate);
                $users[$i]->contract_date=$sdate;        
        }
        return view('admin/user/list', ['users'=>$users]);
    }

    public function addUser()
    {
        $destinations = Destination::where('id', '>', 0)->get();
        $users = User::where([['status','=','active']])->get();
        return view('admin/user/create',['destinations'=>$destinations,'users'=>$users]);
    }

    public function saveUser(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'role' => 'required',
        ]);

        $c = new User();

        $c->name = $request->input('name');
        $c->email = $request->input('email');
        $c->password = bcrypt($request->input('password'));
        $c->phone = $request->input('phone');
        $c->address = $request->input('address');
        $c->contract_date = $request->input('contract_date');
        $c->commission = $request->input('commission');
        $c->role = $request->input('role');
        $c->sales_goal = $request->input('sales_goal');
        $c->fdacs_date = $request->input('fdacs_date');
        $c->user_id = $request->input('user_id');
        $c->status = $request->input('status');

        $c->save();


          $destinations = $request->input('destinations');
          if(!empty($destinations))
          {
            $N = count($destinations);
            for($i=0; $i < $N; $i++)
            {
              $agentdestination = new Agentdestination();

              $agentdestination->destination_name = Destination::find($destinations[$i])->name;
              $agentdestination->user_id = $c->id;
              $agentdestination->destination_id = Destination::find($destinations[$i])->id;

              $agentdestination->save();
            }
          }


        return redirect('user/list');
    }

    public function editUser($id)
    {
        $user = User::find($id);
        $destinations = Destination::where('id', '>', 0)->get();
        $agentdestinations =Agentdestination::where('user_id','=', $user->id)->get();
        $all_users = User::where([['status','=','active']])->get();

        return view('admin/user/edit', ['user' => $user, 'destinations' => $destinations ,'agentdestinations'=>$agentdestinations,'all_users'=>$all_users]);
    }

    public function updateUser(Request $request, $id)
    {
        
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'role' => 'required',
        ]);

        $c = User::find($id);

        $c->name = $request->input('name');
        $c->email = $request->input('email');
        $c->phone = $request->input('phone');
        $c->address = $request->input('address');
        $c->contract_date = $request->input('contract_date');
        $c->role = $request->input('role');
        $c->commission = $request->input('commission');
        $c->sales_goal = $request->input('sales_goal');
        $c->fdacs_date = $request->input('fdacs_date');
        $c->user_id = $request->input('user_id');
        $c->status = $request->input('status');

        $password = $request->input('password');
        $confirmPassword = $request->input('password_confirmation');

        if($password!=null && $confirmPassword!=null)
        {
            if($password==$confirmPassword)
            {
                $c->password=bcrypt($password);
            }
            else
            {
                return redirect()->back()->with('error','New Password and Confirm Password didnt matched');
            }
        }


        $c->save();


        Agentdestination::where('user_id','=', $c->id)->delete();

        // $agentdestinations->delete();

        $destinations = $request->input('destinations');
          if(!empty($destinations))
          {
            $N = count($destinations);
            for($i=0; $i < $N; $i++)
            {
              $agentdestination = new Agentdestination();

              $agentdestination->destination_name = Destination::find($destinations[$i])->name;
              $agentdestination->user_id = $id;
              $agentdestination->destination_id = Destination::find($destinations[$i])->id;

              $agentdestination->save();
            }
          }



        return redirect('user/list');
    }

    public function deleteUser($id)
    {
        $c = User::find($id);
        $c->delete();

        return redirect('user/list');
    }

// Lead CRUDS
    public function listlead()
    {
        $user = Auth::user();
        if($user->role == 1)
        {
            $leads = Lead::all()->sortByDesc('created_at');
            $quotes = new Collection();

            foreach ($leads as $lead) {
                $quote = Quote::where('lead_id', '=', $lead->id)->get()->first();
                $quotes = $quotes->push($quote);
            }
        }
        else
        {
            $leads = Lead::where('user_id', '=', $user->id)->get()->sortByDesc('created_at');
            $quotes = new Collection();

            foreach ($leads as $lead) {
                $quote = Quote::where('lead_id', '=', $lead->id)->get()->last();
                $quotes = $quotes->push($quote);
            }
        }

           return view('/admin/lead/list', ['leads'=>$leads, 'user'=>$user, 'quotes'=>$quotes]);
    }

    public function addlead()
    {
        $destinations = Destination::where('id', '>', 3)->get();
        $user = Auth::user();
        $allusers = User::all();

        return view('/admin/lead/create', ['destinations' => $destinations, 'allusers' => $allusers, 'user' => $user]);
    }

    public function savelead(Request $request)
    {

       $this->validate($request, [
            'email' => 'email',
            'first_name' => 'required',
            'last_name' => 'required',
            'phone_no' => 'required',
            'address1' => 'required',
            'city' => 'required',
            'state' => 'required',
            'zip' => 'required',
        ]);

        $c = new Lead();

        $c->email = $request->input('email');
        $c->first_name = $request->input('first_name');
        $c->last_name = $request->input('last_name');
        $c->country_code = "+1";
        $c->phone_no = $request->input('phone_no');
        $c->address1 = $request->input('address1');
        $c->address2 = $request->input('address2');
        $c->city = $request->input('city');
        $c->state = $request->input('state');
        $c->zip = $request->input('zip'); 
        $c->birth_date = $request->input('birth_date');

        if(Auth::user()->role == 1)
        {
            $c->user_id = $request->input('userId');
        }
        else
        {
            $c->user_id = Auth::user()->id;
        }

        //saving leads
        $c->save();

        //Save Date in Audit Logs
        $requiredAgent = User::find($c->user_id);
         // print_r($requiredAgent->name);
         // exit;

        $al = new AuditLogs();

        $al->agent_name = $requiredAgent->name;
        
        $al->change_date = Carbon::now();
        $al->tripId = null;
        $al->reservation_number = null;
        $al->event = "New Lead Created";

        $al->save();

        //saving user guests
        $guestCount = $request->input('count');

        
        if($guestCount > 0){
            $GuestNo = 1;
            for($i=1; $i<=$guestCount; $i++){
                $guest = new LeadGuest();
                $guest->guest_no = $request->input('guest_no'.$i);
                $guest->guest_first_name = $request->input('guest_first_name'.$i);
                $guest->guest_last_name = $request->input('guest_last_name'.$i);
                $guest->guest_birth_date = $request->input('guest_birth_date'.$i);
                $guest->guest_passport = $request->input('guest_passport'.$i);
                $guest->guest_expire_date = $request->input('guest_expire_date'.$i);
                $guest->lead_id = $c->id;
                $guest->save();
                $GuestNo++;
            }
        }
         return redirect('lead/list');
    }

    public function editLead($id)
    {

        $lead = Lead::find($id);
        $user = User::find($lead->user_id);
        $authuser = Auth::user();
        $allusers = User::all();
        $noAllUserGuests = LeadGuest::where('lead_id', '=', $id)->get()->sortBy('id');
        
        $allUserGuests = new Collection();

        foreach($noAllUserGuests as $g){
            if($g->guest_passport == null){
                $g->guest_passport = "N/A";
            }


            if($g->guest_birth_date == null){
                $g->guest_birth_date = "N/A";
            }
            else{
               $g->guest_birth_date = date('m/d/Y', strtotime($g->guest_birth_date));
            }


            if($g->guest_expire_date == null){
                $g->guest_expire_date = "N/A";
            }
            else{
               $g->guest_expire_date = date('m/d/Y', strtotime($g->guest_expire_date));
            }

            $allUserGuests->push($g);
        }

        $count = 1;
        
        return view('admin/lead/edit', ['authuser' => $authuser, 'lead' => $lead, 'user' => $user, 'allusers' => $allusers, 'allUserGuests' => $allUserGuests, 
            'count' => $count]);
    }

    public function updateLead(Request $request, $id)
    {
        $this->validate($request, [
            'email' => 'email',
            'first_name' => 'required',
            'last_name' => 'required',
            'phone_no' => 'required',
            'address1' => 'required',
            'city' => 'required',
            'state' => 'required',
            'zip' => 'required',
        ]);

        $c = Lead::find($id);

        $c->email = $request->input('email');
        $c->first_name = $request->input('first_name');
        $c->last_name = $request->input('last_name');
        $c->country_code = "+1";
        $c->phone_no = $request->input('phone_no');
        $c->address1 = $request->input('address1');
        $c->address2 = $request->input('address2');
        $c->city = $request->input('city');
        $c->state = $request->input('state');
        $c->zip = $request->input('zip'); 
        $c->birth_date = $request->input('birth_date');

        if(Auth::user()->role == 1)
        {
            $c->user_id = $request->input('userId');
        }
        else
        {
            $c->user_id = Auth::user()->id;
        }

        //saving Lead
        $c->save();

        //saving user guests
        $guestCount = $request->input('count');

        
        if($guestCount > 0){
            
            $GuestNo = 2;
            for($i=1; $i<=$guestCount; $i++){
                $guest = new LeadGuest();
                $guest->guest_no = $request->input('guest_no'.$GuestNo);
                $guest->guest_first_name = $request->input('guest_first_name'.$i);
                $guest->guest_last_name = $request->input('guest_last_name'.$i);
                $guest->guest_birth_date = $request->input('guest_birth_date'.$i);
                $guest->guest_passport = $request->input('guest_passport'.$i);
                $guest->guest_expire_date = $request->input('guest_expire_date'.$i);
                $guest->lead_id = $c->id;
                $guest->save();
                $GuestNo++;
            }

        }

        return redirect('lead/list');
    }

    public function deleteLead($id)
    {
        $c = Lead::find($id);

      if ($c != null) {
        $c->delete();
         return redirect('lead/list');
        }
        // $c->delete();
        return redirect('home');
    }

// LEAD Cruds End


// Lead Guests Crud Start

    public function deleteLeadGuest(Request $request)
    {
         $this->validate($request, [
            'id' => 'required'
        ]);

        $id = $request->input('id');

        $g = LeadGuest::find($id);
        $lead_id = $g->lead_id;
        $g->delete();

        //redirecting to edit screen of customer
        $lead = Lead::find($lead_id);
        $user = User::find($lead->user_id);
        $authuser = Auth::user();
        $allusers = User::all();
        $noAllUserGuests = LeadGuest::where('lead_id', '=', $lead_id)->get()->sortBy('id');
        
        $allUserGuests = new Collection();

        foreach($noAllUserGuests as $g){
            if($g->guest_passport == null){
                $g->guest_passport = "N/A";
            } 

            if($g->guest_birth_date == null){
                $g->guest_birth_date = "N/A";
            }
            else{
               $g->guest_birth_date = date('m/d/Y', strtotime($g->guest_birth_date));
            }


            if($g->guest_expire_date == null){
                $g->guest_expire_date = "N/A";
            }
            else{
               $g->guest_expire_date = date('m/d/Y', strtotime($g->guest_expire_date));
            }

            $allUserGuests->push($g);
        }

        $count = 1;
        
        return view('admin/lead/edit', ['authuser' => $authuser, 'lead' => $lead, 'user' => $user, 'allusers' => $allusers, 'allUserGuests' => $allUserGuests, 
            'count' => $count]);
    }

    public function updateLeadGuest(Request $request)
    {
        $this->validate($request, [
            'guest_first_name' => 'required',
            'guest_last_name' => 'required',
            'id' => 'required'
        ]);

        $id = $request->input('id');

        $g = LeadGuest::find($id);

        $lead_id = $g->lead_id;

        $g->guest_first_name = $request->input('guest_first_name');
        $g->guest_last_name = $request->input('guest_last_name');
        $g->guest_birth_date = $request->input('guest_birth_date');
        $g->guest_passport = $request->input('guest_passport');
        $g->guest_expire_date = $request->input('guest_expire_date');
        $g->lead_id = $lead_id;

        //saving guest
         $g->save();

        //redirecting to edit screen of customer
        $lead = Lead::find($lead_id);
        $destinations = Destination::where('id', '>', 3)->get();
        $user = User::find($lead->user_id);
        $authuser = Auth::user();
        $allusers = User::all();
        $noAllUserGuests = LeadGuest::where('lead_id', '=', $lead_id)->get()->sortBy('id');
        
        $allUserGuests = new Collection();

        foreach($noAllUserGuests as $g){
            if($g->guest_passport == null){
                $g->guest_passport = "N/A";
            }
            
            if($g->guest_birth_date == null){
                $g->guest_birth_date = "N/A";
            }
            else{
               $g->guest_birth_date = date('m/d/Y', strtotime($g->guest_birth_date));
            }


            if($g->guest_expire_date == null){
                $g->guest_expire_date = "N/A";
            }
            else{
               $g->guest_expire_date = date('m/d/Y', strtotime($g->guest_expire_date));
            }

            $allUserGuests->push($g);
        }

        $count = 1;
        
        return view('admin/lead/edit', ['authuser' => $authuser, 'lead' => $lead, 'user' => $user, 'allusers' => $allusers, 'allUserGuests' => $allUserGuests, 
            'count' => $count]);
    }

// Lead Guests Crud End

// Quote Cruds Starts

    public function listQuotes()
    {
        $user = Auth::user();
        if($user->role == 1)
        {
            $Allquotes = Quote::where('Status', '!=', 2)->get()->sortByDesc('id');
        }
        else
        {
            $Allquotes = Quote::where([
                ['user_id', '=', $user->id],
                ['Status', '!=', 2],
            ])->get()->sortByDesc('id');
        }

        //filtering todo trips only       
        $todoTrips = new Collection();

        foreach($Allquotes as $quote){

            if($quote->todo == null){
                $todoTrips->push($quote);
            }
        }

        //filtering without todo trips
        $notTodoTrips = new Collection();

        foreach($Allquotes as $quote){

            if($quote->todo != null){
                $notTodoTrips->push($quote);
            }
        }
    
        $firstname = null;
        $lastname = null;
        $reservationnumber = null;
        $dateoftravel = null;
        
        //$Alltrips
        return view('admin/quote/list', ['quotes'=>$Allquotes, 'todoTrips'=>$todoTrips, 'user'=>$user, 'firstname' => $firstname, 'lastname' => $lastname, 'reservationnumber' => $reservationnumber, 'dateoftravel' => $dateoftravel]);
    }

    public function viewQuote($id)
    { 
        $quote = Quote::find($id);

        $travelWith = new Collection();

        $travelWith = explode(',',$quote->travel_with);
        $searchForValue = '~';

        if( strpos($quote->travel_with, $searchForValue) !== false ) {
            $flag = "true";
            $quote->travel_with = str_replace("~", ",", $quote->travel_with);
        }
        else{
            $flag = "false";

        }

        return view('admin/quote/view', ['quotes'=>$quote, 'flag' => $flag]);
    }

    public function addquote($id)
    {
        $lead = Lead::find($id);

        $name = $lead->first_name. ' ' .$lead->last_name;
        $alldestinations = Destination::where('id', '>', 4)->get();


        $guests = LeadGuest::where('lead_id', '=', $id)->get();
        $user = Auth::user();
       
         $agentdestinations =Agentdestination::where('user_id','=', $user->id)->orderBy('destination_name','asc')->get();
         $destinations= Destination::all();

        $universalStudiosHotels = new Collection();
        $disneyCruiseLineShips = new Collection();
        $disneylandResortHotels = new Collection();
        $waltDisneyWorldHotels = new Collection();
        $royalCaribbeanCruiseLineShips = new Collection();
        $carnivalCruiseLineShips = new Collection();


        $valueResorts = new Collection();
        $moderateResorts = new Collection();
        $deluxeResorts = new Collection();
        $deluxeVillas = new Collection();

        $oasis = new Collection();
        $quantum = new Collection();
        $freedom = new Collection();
        $voyager = new Collection();
        $radiance = new Collection();
        $vision = new Collection();
        $other = new Collection();

        $xl = new Collection();
        $sunshine = new Collection();
        $vista = new Collection();
        $dream = new Collection();
        $splendor = new Collection();
        $conquest = new Collection();
        $spirit = new Collection();
        $fantasy = new Collection();

        $disneyWaltHotelCategories = new Collection();
        $royalHotelCategories = new Collection();
        $carnivalHotelCategories = new Collection();

       // print_r($destinations->toArray()); exit; 


        foreach ($destinations as $destination) {
            if($destination->name == "Universal Studios Florida"){
                $universalStudiosHotels = Hotel::where('destination_id', '=', $destination->id)->get();
            }
            else if($destination->name == "Disney Cruise Line"){
                $disneyCruiseLineShips = Hotel::where('destination_id', '=', $destination->id)->get();
            }
            else if($destination->name == "Disneyland Resort"){
                $disneylandResortHotels = Hotel::where('destination_id', '=', $destination->id)->get();
            }
            else if($destination->name == "Walt Disney World"){

                $waltDisneyWorldHotels = Hotel::where('destination_id', '=', $destination->id)->get();

                $disneyWaltHotelCategories = HotelCategory::where('destination_id', '=', $destination->id)->get();

                foreach ($disneyWaltHotelCategories as $hotelCategory) {
                    if($hotelCategory->category_name == "Value Resorts"){
                        foreach($waltDisneyWorldHotels as $waltHotel){
                            if($waltHotel->category_id == $hotelCategory->id){
                                $valueResorts->push($waltHotel);
                            }
                        }
                    }
                    else if($hotelCategory->category_name == "Moderate Resorts"){
                       foreach($waltDisneyWorldHotels as $waltHotel){
                            if($waltHotel->category_id == $hotelCategory->id){
                                $moderateResorts->push($waltHotel);
                            }
                        }
                    }
                    else if($hotelCategory->category_name == "Deluxe Resorts"){
                        foreach($waltDisneyWorldHotels as $waltHotel){
                            if($waltHotel->category_id == $hotelCategory->id){
                                $deluxeResorts->push($waltHotel);
                            }
                        }
                    }
                    else if($hotelCategory->category_name == "Deluxe Villas"){
                        foreach($waltDisneyWorldHotels as $waltHotel){
                            if($waltHotel->category_id == $hotelCategory->id){
                                $deluxeVillas->push($waltHotel);
                            }
                        }
                    }
                }

            }
            else if($destination->name == "Royal Caribbean Cruise Line"){
                $royalCaribbeanCruiseLineShips = Hotel::where('destination_id', '=', $destination->id)->get();

                $royalHotelCategories = HotelCategory::where('destination_id', '=', $destination->id)->get();



                foreach ($royalHotelCategories as $hotelCategory) {
                    if($hotelCategory->category_name == "Oasis Class"){
                        foreach($royalCaribbeanCruiseLineShips as $royalHotel){
                            if($royalHotel->category_id == $hotelCategory->id){
                                 $oasis->push($royalHotel);
                            }
                        }
                       
                    }
                    else if($hotelCategory->category_name == "Quantum Class"){
                         foreach($royalCaribbeanCruiseLineShips as $royalHotel){
                            if($royalHotel->category_id == $hotelCategory->id){
                                $quantum->push($royalHotel);
                            }
                        }
                        
                    }
                    else if($hotelCategory->category_name == "Freedom Class"){
                        foreach($royalCaribbeanCruiseLineShips as $royalHotel){
                            if($royalHotel->category_id == $hotelCategory->id){
                                $freedom->push($royalHotel);
                            }
                        }
                    }
                    else if($hotelCategory->category_name == "Voyager Class"){
                        foreach($royalCaribbeanCruiseLineShips as $royalHotel){
                            if($royalHotel->category_id == $hotelCategory->id){
                                $voyager->push($royalHotel);
                            }
                        }
                    }
                    else if($hotelCategory->category_name == "Radiance Class"){
                        foreach($royalCaribbeanCruiseLineShips as $royalHotel){
                            if($royalHotel->category_id == $hotelCategory->id){
                                $radiance->push($royalHotel);
                            }
                        }
                    }
                    else if($hotelCategory->category_name == "Vision Class"){
                       foreach($royalCaribbeanCruiseLineShips as $royalHotel){
                            if($royalHotel->category_id == $hotelCategory->id){
                                $vision->push($royalHotel);
                            }
                        }
                    }
                    else if($hotelCategory->category_name == "Other"){
                        foreach($royalCaribbeanCruiseLineShips as $royalHotel){
                            if($royalHotel->category_id == $hotelCategory->id){
                                $other->push($royalHotel);
                            }
                        }
                    }
                }

                // print_r($quantum->toArray()); exit;
            }
            else if($destination->name == "Carnival Cruise Line"){
                $carnivalCruiseLineShips = Hotel::where('destination_id', '=', $destination->id)->get();

                $carnivalHotelCategories = HotelCategory::where('destination_id', '=', $destination->id)->get();

                foreach ($carnivalHotelCategories as $hotelCategory) {
                    if($hotelCategory->category_name == "XL Class"){
                        foreach($carnivalCruiseLineShips as $carnivalHotel){
                            if($carnivalHotel->category_id == $hotelCategory->id){
                                $xl->push($carnivalHotel);
                            }
                        }
                    }
                    else if($hotelCategory->category_name == "Sunshine Class"){
                        foreach($carnivalCruiseLineShips as $carnivalHotel){
                            if($carnivalHotel->category_id == $hotelCategory->id){
                                $sunshine->push($carnivalHotel);
                            }
                        }
                    }
                    else if($hotelCategory->category_name == "Vista Class"){
                        foreach($carnivalCruiseLineShips as $carnivalHotel){
                            if($carnivalHotel->category_id == $hotelCategory->id){
                                $vista->push($carnivalHotel);
                            }
                        }
                    }
                    else if($hotelCategory->category_name == "Dream Class"){
                       foreach($carnivalCruiseLineShips as $carnivalHotel){
                            if($carnivalHotel->category_id == $hotelCategory->id){
                                $dream->push($carnivalHotel);
                            }
                        }
                    }
                    else if($hotelCategory->category_name == "Splendor Class"){
                       foreach($carnivalCruiseLineShips as $carnivalHotel){
                            if($carnivalHotel->category_id == $hotelCategory->id){
                                $splendor->push($carnivalHotel);
                            }
                        }
                    }
                    else if($hotelCategory->category_name == "Conquest Class"){
                       foreach($carnivalCruiseLineShips as $carnivalHotel){
                            if($carnivalHotel->category_id == $hotelCategory->id){
                                $conquest->push($carnivalHotel);
                            }
                        }
                    }
                    else if($hotelCategory->category_name == "Spirit Class"){
                        foreach($carnivalCruiseLineShips as $carnivalHotel){
                            if($carnivalHotel->category_id == $hotelCategory->id){
                                $spirit->push($carnivalHotel);
                            }
                        }
                    }
                    else if($hotelCategory->category_name == "Fantasy Class"){
                       foreach($carnivalCruiseLineShips as $carnivalHotel){
                            if($carnivalHotel->category_id == $hotelCategory->id){
                                $fantasy->push($carnivalHotel);
                            }
                        }
                    }
                }
            }
        }

        return view('admin/quote/create', ['destinations'=>$destinations ,'agentdestinations' => $agentdestinations, 'user' => $user, 'lead' => $lead, 'name' => $name, 
            'guests' => $guests,
            'universalStudiosHotels' => $universalStudiosHotels,
            'disneyCruiseLineShips' => $disneyCruiseLineShips,
            'disneylandResortHotels' => $disneylandResortHotels,
            'waltDisneyWorldHotels' => $waltDisneyWorldHotels,
            'royalCaribbeanCruiseLineShips' => $royalCaribbeanCruiseLineShips,
            'carnivalCruiseLineShips' => $carnivalCruiseLineShips,
            'valueResorts' => $valueResorts,
            'moderateResorts' => $moderateResorts,
            'deluxeResorts' => $deluxeResorts,
            'deluxeVillas' => $deluxeVillas,
            'oasis' => $oasis,
            'quantum' => $quantum,
            'freedom' => $freedom,
            'voyager' => $voyager,
            'radiance' => $radiance,
            'vision' => $vision,
            'other' => $other,
            'xl' => $xl,
            'sunshine' => $sunshine,
            'dream' => $dream,
            'vista' => $vista,
            'splendor' => $splendor,
            'conquest' => $conquest,
            'spirit' => $spirit,
            'fantasy' => $fantasy]);
    }

    public function addquotes($id)
    {
        $customer = Customer::find($id);
        $name = $customer->first_name. ' ' .$customer->last_name;
        $alldestinations = Destination::where('id', '>', 4)->get();

        // print_r($disneyCruiseLineShips->toArray());

        $guests = Guest::where('customer_id', '=', $id)->get();
        $user = Auth::user();
       
         $agentdestinations =Agentdestination::where('user_id','=', $user->id)->orderBy('destination_name','asc')->get();
         $destinations= Destination::all();

        $universalStudiosHotels = new Collection();
        $disneyCruiseLineShips = new Collection();
        $disneylandResortHotels = new Collection();
        $waltDisneyWorldHotels = new Collection();
        $royalCaribbeanCruiseLineShips = new Collection();
        $carnivalCruiseLineShips = new Collection();


                $valueResorts = new Collection();
                $moderateResorts = new Collection();
                $deluxeResorts = new Collection();
                $deluxeVillas = new Collection();

                $oasis = new Collection();
                $quantum = new Collection();
                $freedom = new Collection();
                $voyager = new Collection();
                $radiance = new Collection();
                $vision = new Collection();
                $other = new Collection();


                $xl = new Collection();
                $sunshine = new Collection();
                $vista = new Collection();
                $dream = new Collection();
                $splendor = new Collection();
                $conquest = new Collection();
                $spirit = new Collection();
                $fantasy = new Collection();

                $disneyWaltHotelCategories = new Collection();
                $royalHotelCategories = new Collection();
                $carnivalHotelCategories = new Collection();

               // print_r($destinations->toArray()); exit; 


        foreach ($destinations as $destination) {
            if($destination->name == "Universal Studios Florida"){
               // print_r($destinations->toArray()); exit; 
                $universalStudiosHotels = Hotel::where('destination_id', '=', $destination->id)->get();
            }
            else if($destination->name == "Disney Cruise Line"){
                $disneyCruiseLineShips = Hotel::where('destination_id', '=', $destination->id)->get();
            }
            else if($destination->name == "Disneyland Resort"){
                $disneylandResortHotels = Hotel::where('destination_id', '=', $destination->id)->get();
            }
            else if($destination->name == "Walt Disney World"){

                $waltDisneyWorldHotels = Hotel::where('destination_id', '=', $destination->id)->get();

                $disneyWaltHotelCategories = HotelCategory::where('destination_id', '=', $destination->id)->get();

                  //print_r($waltDisneyWorldHotels->toArray()); exit; 
                foreach ($disneyWaltHotelCategories as $hotelCategory) {
                    if($hotelCategory->category_name == "Value Resorts"){
                        foreach($waltDisneyWorldHotels as $waltHotel){
                            if($waltHotel->category_id == $hotelCategory->id){
                                $valueResorts->push($waltHotel);
                            }
                        }
                    }
                    else if($hotelCategory->category_name == "Moderate Resorts"){
                       foreach($waltDisneyWorldHotels as $waltHotel){
                            if($waltHotel->category_id == $hotelCategory->id){
                                $moderateResorts->push($waltHotel);
                            }
                        }
                    }
                    else if($hotelCategory->category_name == "Deluxe Resorts"){
                        foreach($waltDisneyWorldHotels as $waltHotel){
                            if($waltHotel->category_id == $hotelCategory->id){
                                $deluxeResorts->push($waltHotel);
                            }
                        }
                    }
                    else if($hotelCategory->category_name == "Deluxe Villas"){
                        foreach($waltDisneyWorldHotels as $waltHotel){
                            if($waltHotel->category_id == $hotelCategory->id){
                                $deluxeVillas->push($waltHotel);
                            }
                        }
                    }
                }

            }
            else if($destination->name == "Royal Caribbean Cruise Line"){
                $royalCaribbeanCruiseLineShips = Hotel::where('destination_id', '=', $destination->id)->get();

                $royalHotelCategories = HotelCategory::where('destination_id', '=', $destination->id)->get();



                foreach ($royalHotelCategories as $hotelCategory) {
                    if($hotelCategory->category_name == "Oasis Class"){
                        foreach($royalCaribbeanCruiseLineShips as $royalHotel){
                            if($royalHotel->category_id == $hotelCategory->id){
                                 $oasis->push($royalHotel);
                            }
                        }
                       
                    }
                    else if($hotelCategory->category_name == "Quantum Class"){
                         foreach($royalCaribbeanCruiseLineShips as $royalHotel){
                            if($royalHotel->category_id == $hotelCategory->id){
                                $quantum->push($royalHotel);
                            }
                        }
                        
                    }
                    else if($hotelCategory->category_name == "Freedom Class"){
                        foreach($royalCaribbeanCruiseLineShips as $royalHotel){
                            if($royalHotel->category_id == $hotelCategory->id){
                                $freedom->push($royalHotel);
                            }
                        }
                    }
                    else if($hotelCategory->category_name == "Voyager Class"){
                        foreach($royalCaribbeanCruiseLineShips as $royalHotel){
                            if($royalHotel->category_id == $hotelCategory->id){
                                $voyager->push($royalHotel);
                            }
                        }
                    }
                    else if($hotelCategory->category_name == "Radiance Class"){
                        foreach($royalCaribbeanCruiseLineShips as $royalHotel){
                            if($royalHotel->category_id == $hotelCategory->id){
                                $radiance->push($royalHotel);
                            }
                        }
                    }
                    else if($hotelCategory->category_name == "Vision Class"){
                       foreach($royalCaribbeanCruiseLineShips as $royalHotel){
                            if($royalHotel->category_id == $hotelCategory->id){
                                $vision->push($royalHotel);
                            }
                        }
                    }
                    else if($hotelCategory->category_name == "Other"){
                        foreach($royalCaribbeanCruiseLineShips as $royalHotel){
                            if($royalHotel->category_id == $hotelCategory->id){
                                $other->push($royalHotel);
                            }
                        }
                    }
                }

                // print_r($quantum->toArray()); exit;
            }
            else if($destination->name == "Carnival Cruise Line"){
                $carnivalCruiseLineShips = Hotel::where('destination_id', '=', $destination->id)->get();

                $carnivalHotelCategories = HotelCategory::where('destination_id', '=', $destination->id)->get();

                foreach ($carnivalHotelCategories as $hotelCategory) {
                    if($hotelCategory->category_name == "XL Class"){
                        foreach($carnivalCruiseLineShips as $carnivalHotel){
                            if($carnivalHotel->category_id == $hotelCategory->id){
                                $xl->push($carnivalHotel);
                            }
                        }
                    }
                    else if($hotelCategory->category_name == "Sunshine Class"){
                        foreach($carnivalCruiseLineShips as $carnivalHotel){
                            if($carnivalHotel->category_id == $hotelCategory->id){
                                $sunshine->push($carnivalHotel);
                            }
                        }
                    }
                    else if($hotelCategory->category_name == "Vista Class"){
                        foreach($carnivalCruiseLineShips as $carnivalHotel){
                            if($carnivalHotel->category_id == $hotelCategory->id){
                                $vista->push($carnivalHotel);
                            }
                        }
                    }
                    else if($hotelCategory->category_name == "Dream Class"){
                       foreach($carnivalCruiseLineShips as $carnivalHotel){
                            if($carnivalHotel->category_id == $hotelCategory->id){
                                $dream->push($carnivalHotel);
                            }
                        }
                    }
                    else if($hotelCategory->category_name == "Splendor Class"){
                       foreach($carnivalCruiseLineShips as $carnivalHotel){
                            if($carnivalHotel->category_id == $hotelCategory->id){
                                $splendor->push($carnivalHotel);
                            }
                        }
                    }
                    else if($hotelCategory->category_name == "Conquest Class"){
                       foreach($carnivalCruiseLineShips as $carnivalHotel){
                            if($carnivalHotel->category_id == $hotelCategory->id){
                                $conquest->push($carnivalHotel);
                            }
                        }
                    }
                    else if($hotelCategory->category_name == "Spirit Class"){
                        foreach($carnivalCruiseLineShips as $carnivalHotel){
                            if($carnivalHotel->category_id == $hotelCategory->id){
                                $spirit->push($carnivalHotel);
                            }
                        }
                    }
                    else if($hotelCategory->category_name == "Fantasy Class"){
                       foreach($carnivalCruiseLineShips as $carnivalHotel){
                            if($carnivalHotel->category_id == $hotelCategory->id){
                                $fantasy->push($carnivalHotel);
                            }
                        }
                    }
                }
            }
        }



        /// print_r($agentdestinations->toArray());
         //exit;

        //print_r($agentdestinations->toArray());
        //exit;



        //          foreach($alldestinations as $destination){

        //                                     foreach($agentdestinations as $agentdestination){

        //                                             if($destination->id == $agentdestination->destination_id )
        //                                             {
        //                                                 $destinations->push($destination);
        //                                              } 

        //                                              if($agentdestination->destination_id <= 4)
        //                                              {
        // $destinations->push($destination);
        //                                              }

        //                                     }

        //                                 }  




             
        // $destinations= $destinations->unique();
         // print_r($waltDisneyWorldHotels->toArray()); exit; 

        return view('admin/quote/create', ['destinations'=>$destinations ,'agentdestinations' => $agentdestinations, 'user' => $user, 'customer' => $customer, 'name' => $name, 
            'guests' => $guests,
            'universalStudiosHotels' => $universalStudiosHotels,
            'disneyCruiseLineShips' => $disneyCruiseLineShips,
            'disneylandResortHotels' => $disneylandResortHotels,
            'waltDisneyWorldHotels' => $waltDisneyWorldHotels,
            'royalCaribbeanCruiseLineShips' => $royalCaribbeanCruiseLineShips,
            'carnivalCruiseLineShips' => $carnivalCruiseLineShips,
            'valueResorts' => $valueResorts,
            'moderateResorts' => $moderateResorts,
            'deluxeResorts' => $deluxeResorts,
            'deluxeVillas' => $deluxeVillas,
            'oasis' => $oasis,
            'quantum' => $quantum,
            'freedom' => $freedom,
            'voyager' => $voyager,
            'radiance' => $radiance,
            'vision' => $vision,
            'other' => $other,
            'xl' => $xl,
            'sunshine' => $sunshine,
            'dream' => $dream,
            'vista' => $vista,
            'splendor' => $splendor,
            'conquest' => $conquest,
            'spirit' => $spirit,
            'fantasy' => $fantasy]);
    }

    public function saveQuote(Request $request)
    {
        $this->validate($request, [
            'user_id' => 'required',
            'lead_id'=> 'required',
            'reservation_number' => 'required',
            'booking_date' => 'required',
            'trip_status' => 'required',
            'total_sale' => 'required',
            'commission' => 'required',
            'expected_commission' => 'required',
            'destination' => 'required',
            //'status' => 'required',

        ]);

      //  dd($request);

        try {

        $c = new Quote();

        $c->Referal = $request->input('Referal');
        $c->lead_id = $request->input('lead_id');
        $c->user_id = Auth::user()->id;
        $c->reservation_number = $request->input('reservation_number');
        $c->booking_date = $request->input('booking_date');

          
        $c->checkin_date = $request->input('checkin_date');
        if($request->input('checkout_date') == null || $request->input('checkout_date') == "")
        {
            $c->checkout_date = $request->input('checkin_date');
        }
        else
        {
            $c->checkout_date = $request->input('checkout_date');
        }
        $c->trip_status = $request->input('trip_status');
        $c->commission = $request->input('commission');
        $c->total_sale = $request->input('total_sale');
        $c->expected_commission = $request->input('expected_commission');
        $allTravelWith = $request->input('travel_with');
        $c->universal_studios_florida_hotel_names = $request->input('universal_studios_florida_hotel_names');
        $c->universal_dining_plan = $request->input('universal_dining_plan');
        $c->my_universal_photos = $request->input('my_universal_photos');
        $c->universal_super_shuttle = $request->input('universal_super_shuttle');
        $c->universal_express_pass = $request->input('universal_express_pass');
        $c->disneyworld_hotel_name = $request->input('disneyworld_hotel_name');
        $c->Referal = $request->input('Referal');
        $c->other = $request->input('other');



        $travel = null;
        if($allTravelWith == null){

            $c->travel_with = $travel;
        }
        else{

             foreach ($allTravelWith as $travelWith) {
               $travel = $travel.$travelWith.'~';
            }

            $c->travel_with =  $travel; //substr($travel, 0, -1);

        }
        
       
         // print_r($c->travel_with);
         // exit;
        $c->special_request = $request->input('special_request');

        if ($request->input('destination')=="Walt Disney World")
        {

            $c->destination = $request->input('destination');
            $c->disney_dining_plan = $request->input('disney_dining_plan');
            $c->memory_maker = $request->input('memory_maker');
            $c->magical_express = $request->input('magical_express');
            $c->magic_band_color = $request->input('magic_band_color');
            $c->disneyworld_hotel_name = $request->input('disneyworld_hotel_name');
            $c->ticket_type = $request->input('ticket_type');
            $c->insurance = $request->input('insurance');
            $c->notes = $request->input('notes');
            // $c->Referal = $request->input('Referal');
            // $c->other = $request->input('other');

        }
        else if ($request->input('destination')=="Disneyland Resort")
        {

            $c->destination = $request->input('destination');
            $c->good_neighbor_hotel = $request->input('good_neighbor_hotel');
            $c->disneyresort_hotel_name = $request->input('disneyresort_hotel_name');
            $c->ticket_type = $request->input('ticket_type');
            $c->insurance = $request->input('insurance');
            $c->notes = $request->input('notes');

   

        }
        else if ($request->input('destination')=="Disney Cruise Line")
        {
          
            $c->destination = $request->input('destination');
            $c->ship_name2 = $request->input('ship_name2');
            $c->stateroom_category = $request->input('stateroom_category');
            $c->castaway_member = $request->input('castaway_member');          
            $c->castaway_club_level = $request->input('castaway_club_level');
            $c->bus_transportation = $request->input('bus_transportation');
        }
        else if ($request->input('destination')=="Carnival Cruise Line")
        {
            $c->destination = $request->input('destination');
            $c->ship_name = $request->input('ship_name1');
            $c->insurance = $request->input('insurance');
            $c->notes = $request->input('notes');
        }
        else if ($request->input('destination')=="Royal Caribbean Cruise Line")
        {
          
            $c->destination = $request->input('destination');
            $c->ship_name = $request->input('ship_name');
            $c->stateroom_category_caribbean = $request->input('stateroom_category_caribbean');
            $c->crown_anchor_level = $request->input('crown_anchor_level');
            $c->insurance = $request->input('insurance');
            $c->notes = $request->input('notes');
               
        }
        else if ($request->input('destination')=="Universal Studios Florida")
        {
               $c->destination = $request->input('destination');
               $c->universal_studios_florida_hotel_names = $request->input('universal_studios_florida_hotel_names');
               $c->universal_dining_plan = $request->input('universal_dining_plan');
               $c->my_universal_photos = $request->input('my_universal_photos');
               $c->universal_super_shuttle = $request->input('universal_super_shuttle');
               $c->universal_express_pass = $request->input('universal_express_pass');
               $c->disneyworld_hotel_name = $request->input('disneyworld_hotel_name');
               $c->ticket_type = $request->input('ticket_type');
               // $c->insurance = $request->input('insurance');
               // $c->notes = $request->input('notes');
        }
        else
        {
            $c->destination = $request->input('destination');
            $c->insurance = $request->input('insurance');
            $c->Referal = $request->input('Referal');
            $c->other = $request->input('other');
            $c->notes = $request->input('notes');
        }


            $done =  $c->save();

            //$requiredTrip = users::where('id', '=', $c->id)->get();

            $requiredAgent = User::find($c->user_id);
            // exit;

            $al = new AuditLogs();

            $al->agent_name = $requiredAgent->name;

            $al->tripId = $c->id;
            $al->reservation_number = $request->input('reservation_number');
            $al->change_date = Carbon::now();
            $al->event = "New Quote Created";
        
            $al->save();
        }
        catch (\Illuminate\Database\QueryException $ex){ 
             dd($ex->getMessage()); }
        // Mail::to('bookings@MandMMagicalAdventures.com')->send(new NewTrip($c));
        
        return redirect('quote/list/');
    }

    public function phone_number_format($phone_no) {
      // Allow only Digits, remove all other characters.
      $number = preg_replace("/[^\d]/","",$phone_no);
     
      // get number length.
      $length = strlen($phone_no);
     
     // if number = 10
     if($length == 10) {
      $phone_no = preg_replace("/^1?(\d{3})(\d{3})(\d{4})$/", "$1-$2-$3", $phone_no);
     }
      
      return $phone_no;
     
    }

    public function editQuote($id)
    {
        $quote = Quote::find($id);
        $lead = Lead::where('id', '=', $quote->lead_id)->first(); 
        $guests = LeadGuest::where('lead_id', '=', $quote->lead_id)->get();       
        $name = $lead->first_name. ' ' .$lead->last_name;
        
        $travelWith = new Collection();
        $notTravelWith = new Collection();

        $stringVal = substr($quote->travel_with, 0,-1);
        $travelWith = explode('~',$stringVal);

        $searchForValue = '~';

        if( strpos($quote->travel_with, $searchForValue) !== false ) {
            $flag = "true";
        }
        else{
            $flag = "false";
        }


         // print_r($travelWith);
         // exit;

        $user_d  = $quote->user->id;
        
        $authuser= Auth::user();

        $users = User::all();
        
        $agentdestinations =Agentdestination::where('user_id','=', $user_d)->orderBy('destination_name','asc')->get();

        $destinations= Destination::all();

        $universalStudiosHotels = new Collection();
        $disneyCruiseLineShips = new Collection();
        $disneylandResortHotels = new Collection();
        $waltDisneyWorldHotels = new Collection();
        $royalCaribbeanCruiseLineShips = new Collection();
        $carnivalCruiseLineShips = new Collection();

        $valueResorts = new Collection();
        $moderateResorts = new Collection();
        $deluxeResorts = new Collection();
        $deluxeVillas = new Collection();

        $oasis = new Collection();
        $quantum = new Collection();
        $freedom = new Collection();
        $voyager = new Collection();
        $radiance = new Collection();
        $vision = new Collection();
        $other = new Collection();

        $xl = new Collection();
        $sunshine = new Collection();
        $vista = new Collection();
        $dream = new Collection();
        $splendor = new Collection();
        $conquest = new Collection();
        $spirit = new Collection();
        $fantasy = new Collection();

        $disneyWaltHotelCategories = new Collection();
        $royalHotelCategories = new Collection();
        $carnivalHotelCategories = new Collection();

               // print_r($destinations->toArray()); exit; 


        foreach ($destinations as $destination) {
            if($destination->name == "Universal Studios Florida"){
               // print_r($destinations->toArray()); exit; 
                $universalStudiosHotels = Hotel::where('destination_id', '=', $destination->id)->get();
            }
            else if($destination->name == "Disney Cruise Line"){
                $disneyCruiseLineShips = Hotel::where('destination_id', '=', $destination->id)->get();
            }
            else if($destination->name == "Disneyland Resort"){
                $disneylandResortHotels = Hotel::where('destination_id', '=', $destination->id)->get();
            }
            else if($destination->name == "Walt Disney World"){

                $waltDisneyWorldHotels = Hotel::where('destination_id', '=', $destination->id)->get();

                $disneyWaltHotelCategories = HotelCategory::where('destination_id', '=', $destination->id)->get();

                  
                foreach ($disneyWaltHotelCategories as $hotelCategory) {
                    if($hotelCategory->category_name == "Value Resorts"){
                        foreach($waltDisneyWorldHotels as $waltHotel){
                            if($waltHotel->category_id == $hotelCategory->id){
                                $valueResorts->push($waltHotel);
                            }
                        }
                    }
                    else if($hotelCategory->category_name == "Moderate Resorts"){
                       foreach($waltDisneyWorldHotels as $waltHotel){
                            if($waltHotel->category_id == $hotelCategory->id){
                                $moderateResorts->push($waltHotel);
                            }
                        }
                    }
                    else if($hotelCategory->category_name == "Deluxe Resorts"){
                        foreach($waltDisneyWorldHotels as $waltHotel){
                            if($waltHotel->category_id == $hotelCategory->id){
                                $deluxeResorts->push($waltHotel);
                            }
                        }
                    }
                    else if($hotelCategory->category_name == "Deluxe Villas"){
                        foreach($waltDisneyWorldHotels as $waltHotel){
                            if($waltHotel->category_id == $hotelCategory->id){
                                $deluxeVillas->push($waltHotel);
                            }
                        }
                    }
                }

            }
            else if($destination->name == "Royal Caribbean Cruise Line"){
                $royalCaribbeanCruiseLineShips = Hotel::where('destination_id', '=', $destination->id)->get();

                $royalHotelCategories = HotelCategory::where('destination_id', '=', $destination->id)->get();



                foreach ($royalHotelCategories as $hotelCategory) {
                    if($hotelCategory->category_name == "Oasis Class"){
                        foreach($royalCaribbeanCruiseLineShips as $royalHotel){
                            if($royalHotel->category_id == $hotelCategory->id){
                                 $oasis->push($royalHotel);
                            }
                        }
                       
                    }
                    else if($hotelCategory->category_name == "Quantum Class"){
                         foreach($royalCaribbeanCruiseLineShips as $royalHotel){
                            if($royalHotel->category_id == $hotelCategory->id){
                                $quantum->push($royalHotel);
                            }
                        }
                        
                    }
                    else if($hotelCategory->category_name == "Freedom Class"){
                        foreach($royalCaribbeanCruiseLineShips as $royalHotel){
                            if($royalHotel->category_id == $hotelCategory->id){
                                $freedom->push($royalHotel);
                            }
                        }
                    }
                    else if($hotelCategory->category_name == "Voyager Class"){
                        foreach($royalCaribbeanCruiseLineShips as $royalHotel){
                            if($royalHotel->category_id == $hotelCategory->id){
                                $voyager->push($royalHotel);
                            }
                        }
                    }
                    else if($hotelCategory->category_name == "Radiance Class"){
                        foreach($royalCaribbeanCruiseLineShips as $royalHotel){
                            if($royalHotel->category_id == $hotelCategory->id){
                                $radiance->push($royalHotel);
                            }
                        }
                    }
                    else if($hotelCategory->category_name == "Vision Class"){
                       foreach($royalCaribbeanCruiseLineShips as $royalHotel){
                            if($royalHotel->category_id == $hotelCategory->id){
                                $vision->push($royalHotel);
                            }
                        }
                    }
                    else if($hotelCategory->category_name == "Other"){
                        foreach($royalCaribbeanCruiseLineShips as $royalHotel){
                            if($royalHotel->category_id == $hotelCategory->id){
                                $other->push($royalHotel);
                            }
                        }
                    }
                }

                // print_r($quantum->toArray()); exit;
            }
            else if($destination->name == "Carnival Cruise Line"){
                $carnivalCruiseLineShips = Hotel::where('destination_id', '=', $destination->id)->get();

                $carnivalHotelCategories = HotelCategory::where('destination_id', '=', $destination->id)->get();

                foreach ($carnivalHotelCategories as $hotelCategory) {
                    if($hotelCategory->category_name == "XL Class"){
                        foreach($carnivalCruiseLineShips as $carnivalHotel){
                            if($carnivalHotel->category_id == $hotelCategory->id){
                                $xl->push($carnivalHotel);
                            }
                        }
                    }
                    else if($hotelCategory->category_name == "Sunshine Class"){
                        foreach($carnivalCruiseLineShips as $carnivalHotel){
                            if($carnivalHotel->category_id == $hotelCategory->id){
                                $sunshine->push($carnivalHotel);
                            }
                        }
                    }
                    else if($hotelCategory->category_name == "Vista Class"){
                        foreach($carnivalCruiseLineShips as $carnivalHotel){
                            if($carnivalHotel->category_id == $hotelCategory->id){
                                $vista->push($carnivalHotel);
                            }
                        }
                    }
                    else if($hotelCategory->category_name == "Dream Class"){
                       foreach($carnivalCruiseLineShips as $carnivalHotel){
                            if($carnivalHotel->category_id == $hotelCategory->id){
                                $dream->push($carnivalHotel);
                            }
                        }
                    }
                    else if($hotelCategory->category_name == "Splendor Class"){
                       foreach($carnivalCruiseLineShips as $carnivalHotel){
                            if($carnivalHotel->category_id == $hotelCategory->id){
                                $splendor->push($carnivalHotel);
                            }
                        }
                    }
                    else if($hotelCategory->category_name == "Conquest Class"){
                       foreach($carnivalCruiseLineShips as $carnivalHotel){
                            if($carnivalHotel->category_id == $hotelCategory->id){
                                $conquest->push($carnivalHotel);
                            }
                        }
                    }
                    else if($hotelCategory->category_name == "Spirit Class"){
                        foreach($carnivalCruiseLineShips as $carnivalHotel){
                            if($carnivalHotel->category_id == $hotelCategory->id){
                                $spirit->push($carnivalHotel);
                            }
                        }
                    }
                    else if($hotelCategory->category_name == "Fantasy Class"){
                       foreach($carnivalCruiseLineShips as $carnivalHotel){
                            if($carnivalHotel->category_id == $hotelCategory->id){
                                $fantasy->push($carnivalHotel);
                            }
                        }
                    }
                }
            }
        }

        
        return view('admin/quote/edit', ['quote' => $quote,'authuser' => $authuser, 'users' => $users, 'lead' => $lead, 'destinations' => $agentdestinations, 'name' => $name, 
            'travelWith' => $travelWith, 'guests' => $guests,
            'universalStudiosHotels' => $universalStudiosHotels,
            'disneyCruiseLineShips' => $disneyCruiseLineShips,
            'disneylandResortHotels' => $disneylandResortHotels,
            'waltDisneyWorldHotels' => $waltDisneyWorldHotels,
            'royalCaribbeanCruiseLineShips' => $royalCaribbeanCruiseLineShips,
            'carnivalCruiseLineShips' => $carnivalCruiseLineShips,
            'valueResorts' => $valueResorts,
            'moderateResorts' => $moderateResorts,
            'deluxeResorts' => $deluxeResorts,
            'deluxeVillas' => $deluxeVillas,
            'oasis' => $oasis,
            'quantum' => $quantum,
            'freedom' => $freedom,
            'voyager' => $voyager,
            'radiance' => $radiance,
            'vision' => $vision,
            'other' => $other,
            'xl' => $xl,
            'sunshine' => $sunshine,
            'dream' => $dream,
            'vista' => $vista,
            'splendor' => $splendor,
            'conquest' => $conquest,
            'spirit' => $spirit,
            'fantasy' => $fantasy,
            'flag' => $flag]);
    }

    public function updateQuote(Request $request, $id)
    {
    
        $this->validate($request, [
            'user_id' => 'required',
            'lead_id'=> 'required',
            'reservation_number' => 'required',
            'trip_status' => 'required',
            'total_sale' => 'required',
            'commission' => 'required',
            'expected_commission' => 'required',
            'destination' => 'required',
        ]);

        $c = Quote::find($id);

        $c->lead_id = $request->input('lead_id');
        $c->Referal = $request->input('Referal');

        $c->user_id =  $request->input('user_id');
        // Auth::user()->id;
        $c->reservation_number = $request->input('reservation_number');
        $c->booking_date = $request->input('booking_date');
        $c->checkin_date = $request->input('checkin_date');

        if($request->input('checkout_date') == null || $request->input('checkout_date') == "")
        {
            $c->checkout_date = $request->input('checkin_date');
        }
        else
        {
            $c->checkout_date = $request->input('checkout_date');
        }

        $c->trip_status = $request->input('trip_status');
        $c->commission = $request->input('commission');
        $c->total_sale = $request->input('total_sale');
        $c->expected_commission = $request->input('expected_commission');
        $allTravelWith = $request->input('travel_with');
        $c->Referal = $request->input('Referal');

        $travel = null;
        if($allTravelWith == null){

             //print_r($allTravelWith); exit;

            $c->travel_with = $travel;
        }
        //checking if it is an array then new feature else it is an old record
        else if(is_array($allTravelWith)){

            //print_r($allTravelWith); exit;

           foreach ($allTravelWith as $travelWith) {
               $travel = $travel.$travelWith.'~';
            }

            $c->travel_with = $travel; //substr($travel, 0, -1);

        }
        else{

           // print_r($allTravelWith); exit;

            $c->travel_with =  $allTravelWith;
        }
        
        $c->special_request = $request->input('special_request');

        if ($request->input('destination')=="Walt Disney World")
        {

            $c->destination = $request->input('destination');
            $c->disney_dining_plan = $request->input('disney_dining_plan');
            $c->memory_maker = $request->input('memory_maker');
            $c->magical_express = $request->input('magical_express');
            $c->magic_band_color = $request->input('magic_band_color');
            $c->disneyworld_hotel_name = $request->input('disneyworld_hotel_name');
            $c->ticket_type = $request->input('ticket_type');
            $c->insurance = $request->input('insurance');
            $c->Referal = $request->input('Referal');
            $c->notes = $request->input('notes');
        
            $c->disneyresort_hotel_name = null;
            $c->ship_name = null;
            $c->castaway_member = null;
            $c->bus_transportation = null;
            $c->promo_applied = null;
            $c->good_neighbor_hotel = null;
            $c->universal_orlando_resort_hotel = null;
            $c->partners_hotel =null;

        }
        elseif ($request->input('destination')=="Disneyland Resort")
        {

            $c->destination = $request->input('destination');
            $c->good_neighbor_hotel = $request->input('good_neighbor_hotel');
            $c->disneyresort_hotel_name = $request->input('disneyresort_hotel_name');
            $c->ticket_type = $request->input('ticket_type');
            $c->insurance = $request->input('insurance');
            $c->Referal = $request->input('Referal');
            $c->notes = $request->input('notes');

            $c->disneyworld_hotel_name = null;
            $c->memory_maker = null;
            $c->magical_express = null;
            $c->ship_name =null;
            $c->castaway_member =null;
            $c->bus_transportation = null;
            $c->disney_dining_plan = null;
            $c->promo_applied = null;
            $c->magic_band_color = null;
            $c->universal_orlando_resort_hotel =null;
            $c->partners_hotel = null;

        }
        elseif ($request->input('destination')=="Disney Cruise Line")
        {
            $c->destination = $request->input('destination');
            $c->ship_name2 = $request->input('ship_name2');
            $c->castaway_member = $request->input('castaway_member');
            $c->bus_transportation = $request->input('bus_transportation');
            $c->stateroom_category = $request->input('stateroom_category');        
            $c->castaway_club_level = $request->input('castaway_club_level');
            $c->insurance = $request->input('insurance');
            $c->Referal = $request->input('Referal');
            $c->notes = $request->input('notes');


            $c->disneyworld_hotel_name = null;
            $c->disneyresort_hotel_name = null;
            $c->memory_maker = null;
            $c->magical_express = null;
            $c->disney_dining_plan = null;
            $c->ticket_type = null;
            $c->promo_applied = null;
            $c->magic_band_color = null;
            $c->good_neighbor_hotel = null;
            $c->universal_orlando_resort_hotel = null;
            $c->partners_hotel = null;


        }
        elseif ($request->input('destination')=="Universal Studios Florida")
        {
            $c->destination = $request->input('destination');
            // $c->universal_orlando_resort_hotel = $request->input('universal_orlando_resort_hotel');
            // $c->partners_hotel = $request->input('partners_hotel');
            $c->ticket_type = $request->input('ticket_type');
            $c->insurance = $request->input('insurance');
            $c->Referal = $request->input('Referal');
            $c->notes = $request->input('notes');

            $c->disneyworld_hotel_name = null;
            $c->disneyresort_hotel_name = null;
            $c->memory_maker = null;
            $c->magical_express = null;
            $c->ship_name =null;
            $c->castaway_member =null;
            $c->bus_transportation = null;
            $c->disney_dining_plan =null;
            $c->promo_applied = null;
            $c->magic_band_color = null;
            $c->good_neighbor_hotel =null;
    
   
        }
        else if ($request->input('destination')=="Carnival Cruise Line")
        {
            $c->destination = $request->input('destination');
            $c->ship_name = $request->input('ship_name1');
            $c->insurance = $request->input('insurance');
            $c->Referal = $request->input('Referal');
            $c->notes = $request->input('notes');

            $c->universal_orlando_resort_hotel = null;
            $c->partners_hotel = null;
            $c->ticket_type = null;
            $c->disneyworld_hotel_name = null;
            $c->disneyresort_hotel_name = null;
            $c->memory_maker = null;
            $c->magical_express = null;
            $c->castaway_member =null;
            $c->bus_transportation = null;
            $c->disney_dining_plan =null;
            $c->promo_applied = null;
            $c->magic_band_color = null;
            $c->good_neighbor_hotel =null;
        }

        else if ($request->input('destination')=="Royal Caribbean Cruise Line")
        {
            $c->destination = $request->input('destination');
            $c->ship_name = $request->input('ship_name');
            $c->stateroom_category_caribbean = $request->input('stateroom_category_caribbean');
            $c->crown_anchor_level = $request->input('crown_anchor_level');
            $c->insurance = $request->input('insurance');
            $c->Referal = $request->input('Referal');
            $c->notes = $request->input('notes');

            $c->universal_orlando_resort_hotel = null;
            $c->partners_hotel = null;
            $c->ticket_type = null;
            $c->disneyworld_hotel_name = null;
            $c->disneyresort_hotel_name = null;
            $c->memory_maker = null;
            $c->magical_express = null;
            $c->castaway_member =null;
            $c->bus_transportation = null;
            $c->disney_dining_plan =null;
            $c->promo_applied = null;
            $c->magic_band_color = null;
            $c->good_neighbor_hotel =null;
        }
        else
        {
            $c->Referal = $request->input('Referal');
            $c->destination = $request->input('destination');
            $c->insurance = $request->input('insurance');
            $c->Referal = $request->input('Referal');
            $c->notes = $request->input('notes');


            $c->universal_orlando_resort_hotel = null;
            $c->partners_hotel = null;
            $c->ticket_type = null;
            $c->disneyworld_hotel_name = null;
            $c->disneyresort_hotel_name = null;
            $c->memory_maker = null;
            $c->magical_express = null;
            $c->ship_name =null;
            $c->castaway_member =null;
            $c->bus_transportation = null;
            $c->disney_dining_plan =null;
            $c->promo_applied = null;
            $c->magic_band_color = null;
            $c->good_neighbor_hotel =null;
        }

        $c->save();

        return redirect('quote/list');
    }

    public function deleteQuote($id)
    {
        $c = Quote::find($id);

        $todos = TODO::where('trip_id', '=', $c->trip_id)->get();

        if($todos != null)
        {
            foreach ($todos as $todo) 
            {
                $todo->delete();
            }
        }
        
        $c->delete();

        return redirect('quote/list');
    }

    public function QuoteHistory($id)
    {
        $quotes = Quote::where('lead_id', '=', $id)->get()->sortByDesc('id');
        return view('admin/quote/QuoteHistory',['quotelist'=>$quotes]);
    }

    public function ConverttoTrip($id){
        
        //save in Trips
        $findquote = Quote::find($id);
        // echo $findquote;
        // exit();

        //save in customer
        $findlead = $lead = Lead::where('id', '=', $findquote->lead_id)->first();
        // echo $findlead;
        // exit();

        //save in Guests
        $findGuests = LeadGuest::where('lead_id', '=', $findlead->id)->get();
        // echo $findGuests;
        // exit();

        $addCustomer = new Customer();

        $addCustomer->first_name = $findlead->first_name;
        $addCustomer->last_name = $findlead->last_name;
        $addCustomer->email = $findlead->email;
        $addCustomer->country_code = $findlead->country_code;
        $addCustomer->phone_no = $findlead->phone_no;
        $addCustomer->address1 = $findlead->address1;
        $addCustomer->address2 = $findlead->address2;
        $addCustomer->city = $findlead->city;
        $addCustomer->state = $findlead->state;
        $addCustomer->zip = $findlead->zip;
        $addCustomer->birth_date = $findlead->birth_date;
        $addCustomer->user_id = $findlead->user_id;

        $addCustomer->save();
        $guest = 2;
        if($checklength = count($findGuests) > 0){
            foreach($findGuests as $guests){
                $addGuests = new Guest();
                $addGuests->guest_no = $guest;
                $addGuests->guest_first_name = $guests->guest_first_name;
                $addGuests->guest_last_name = $guests->guest_last_name;
                $addGuests->guest_birth_date = $guests->guest_birth_date;
                $addGuests->guest_passport = $guests->guest_passport;
                $addGuests->guest_expire_date = $guests->guest_expire_date;
                $addGuests->customer_id  = $addCustomer->id;
                $addGuests->save();
                $guest++;
            }
        }
        // else{
        //     echo "string";
        // }
        $addTrip = new Trip();

        $addTrip->booking_date = $findquote->booking_date;
        $addTrip->trip_status = $findquote->trip_status;
        $addTrip->total_sale = $findquote->total_sale;
        $addTrip->ticket_type = $findquote->ticket_type;
        $addTrip->destination = $findquote->destination;
        $addTrip->reservation_number = $findquote->reservation_number;
        $addTrip->disneyworld_hotel_name = $findquote->disneyworld_hotel_name;
        $addTrip->disneyresort_hotel_name = $findquote->disneyresort_hotel_name;
        $addTrip->disney_dining_plan = $findquote->disney_dining_plan;
        $addTrip->memory_maker = $findquote->memory_maker;
        $addTrip->magical_express = $findquote->magical_express;
        $addTrip->checkin_date = $findquote->checkin_date;
        $addTrip->checkout_date = $findquote->checkout_date;
        $addTrip->ship_name = $findquote->ship_name;
        $addTrip->castaway_member = $findquote->castaway_member;
        $addTrip->bus_transportation = $findquote->bus_transportation;
        $addTrip->insurance = $findquote->insurance;
        $addTrip->travel_with = $findquote->travel_with;
        $addTrip->special_request = $findquote->special_request;
        $addTrip->notes = $findquote->notes;
        $addTrip->commission = $findquote->commission;
        $addTrip->expected_commission = $findquote->expected_commission;
        $addTrip->customer_id = $addCustomer->id;
        $addTrip->user_id = $findquote->user_id;
        $addTrip->promo_applied = $findquote->promo_applied;
        $addTrip->advanced_dining_reservations = $findquote->advanced_dining_reservations;
        $addTrip->fast_pass_date = $findquote->fast_pass_date;
        $addTrip->final_payment_due = $findquote->final_payment_due;
        $addTrip->magic_band_color = $findquote->magic_band_color;
        $addTrip->reminder = $findquote->reminder;
        $addTrip->magical_express_completed = $findquote->magical_express_completed;
        $addTrip->call_room_requests = $findquote->call_room_requests;
        $addTrip->itinerary_tip_sheets = $findquote->itinerary_tip_sheets;
        $addTrip->good_neighbor_hotel = $findquote->good_neighbor_hotel;
        $addTrip->universal_orlando_resort_hotel = $findquote->universal_orlando_resort_hotel;
        $addTrip->partners_hotel = $findquote->partners_hotel;
        $addTrip->promo_code = $findquote->promo_code;
        $addTrip->is_active = $findquote->is_active;
        $addTrip->status = $findquote->status;
        $addTrip->reason = $findquote->reason;
        $addTrip->magicalexpresscalender = $findquote->magicalexpresscalender;
        $addTrip->other = $findquote->other;
        $addTrip->universal_dining_plan = $findquote->universal_dining_plan;
        $addTrip->my_universal_photos = $findquote->my_universal_photos;
        $addTrip->universal_super_shuttle = $findquote->universal_super_shuttle;
        $addTrip->universal_express_pass = $findquote->universal_express_pass;
        $addTrip->universal_studios_florida_hotel_names = $findquote->universal_studios_florida_hotel_names;
        $addTrip->Referal = $findquote->Referal;
        $addTrip->ship_name2 = $findquote->ship_name2;
        $addTrip->stateroom_category = $findquote->stateroom_category;
        $addTrip->castaway_club_level = $findquote->castaway_club_level;
        $addTrip->crown_anchor_level = $findquote->crown_anchor_level;
        $addTrip->stateroom_category_caribbean = $findquote->stateroom_category_caribbean;
        $addTrip->ship_name_caribbean = $findquote->ship_name_caribbean;

        $addTrip->save();
        return redirect('customer/list');
        exit();

    }

// Quote Cruds End


// Customer CRUD
    public function listCustomers()
    {
        $user = Auth::user();

        if($user->role == 1)
        {
            $customers = Customer::all()->sortByDesc('created_at');
            $trips = new Collection();

            foreach ($customers as $customer) {
                $trip = Trip::where('customer_id', '=', $customer->id)->get()->first();

                $trips = $trips->push($trip);
            }
        }
        else
        {
            $customers = Customer::where('user_id', '=', $user->id)->get()->sortByDesc('created_at');

            $trips = new Collection();

            foreach ($customers as $customer) {
                $trip = Trip::where('customer_id', '=', $customer->id)->get()->last();

                $trips = $trips->push($trip);
            }
        }
               
        return view('admin/customer/list', ['customers'=>$customers, 'user'=>$user, 'trips'=>$trips]);
    }

    public function addCustomer()
    {
        $destinations = Destination::where('id', '>', 3)->get();
        $user = Auth::user();
        $allusers = User::all();

        return view('admin/customer/create', ['destinations' => $destinations, 'allusers' => $allusers, 'user' => $user]);
    }

    public function saveCustomer(Request $request)
    {
        //print_r($request->toArray());
        //exit;

        $this->validate($request, [
            'email' => 'email',
            'first_name' => 'required',
            'last_name' => 'required',
            'phone_no' => 'required',
            'address1' => 'required',
            'city' => 'required',
            'state' => 'required',
            'zip' => 'required',
        ]);

        $c = new Customer();

        $c->email = $request->input('email');
        $c->first_name = $request->input('first_name');
        $c->last_name = $request->input('last_name');
        $c->country_code = "+1";
        $c->phone_no = $request->input('phone_no');
        $c->address1 = $request->input('address1');
        $c->address2 = $request->input('address2');
        $c->city = $request->input('city');
        $c->state = $request->input('state');
        $c->zip = $request->input('zip'); 
        $c->birth_date = $request->input('birth_date');
        $c->passport = $request->input('passport');
        $c->expire_date = $request->input('expire_date');
        $c->referral = $request->input('referral');   
        $c->disney_experience_username = $request->input('disney_experience_username');     
        $c->disney_experience_password = $request->input('disney_experience_password');  
        $c->notes = $request->input('notes');

        if(Auth::user()->role == 1)
        {
            $c->user_id = $request->input('userId');
        }
        else
        {
            $c->user_id = Auth::user()->id;
        }

        //saving customer
        $c->save();

        //Save Date in Audit Logs
        $requiredAgent = User::find($c->user_id);
         // print_r($requiredAgent->name);
         // exit;

        $al = new AuditLogs();

        $al->agent_name = $requiredAgent->name;
        
        $al->change_date = Carbon::now();
        $al->tripId = null;
        $al->reservation_number = null;
        $al->event = "New Customer Created";

        $al->save();

        //saving user guests
        $guestCount = $request->input('count');

        //print_r('guest_first_name'.$guestCount);
        //exit;
        
        if($guestCount > 0){
            
            $GuestNo = 2;
            for($i=1; $i<=$guestCount; $i++){
                $guest = new Guest();
                $guest->guest_no = $request->input('guest_no'.$GuestNo);
                $guest->guest_first_name = $request->input('guest_first_name'.$i);
                $guest->guest_last_name = $request->input('guest_last_name'.$i);
                $guest->guest_birth_date = $request->input('guest_birth_date'.$i);
                $guest->guest_passport = $request->input('guest_passport'.$i);
                $guest->guest_expire_date = $request->input('guest_expire_date'.$i);
                $guest->customer_id = $c->id;
                $guest->save();
                $GuestNo++;
            }

        }

        return redirect('customer/list');
    }

    public function editCustomer($id)
    {

        $customer = Customer::find($id);
        $destinations = Destination::where('id', '>', 3)->get();
        $user = User::find($customer->user_id);
        $authuser = Auth::user();
        $allusers = User::all();
        $noAllUserGuests = Guest::where('customer_id', '=', $id)->get()->sortBy('id');
        
        $allUserGuests = new Collection();

        foreach($noAllUserGuests as $g){
            if($g->guest_passport == null){
                $g->guest_passport = "N/A";
            }


            if($g->guest_birth_date == null){
                $g->guest_birth_date = "N/A";
            }
            else{
               $g->guest_birth_date = date('m/d/Y', strtotime($g->guest_birth_date));
            }


            if($g->guest_expire_date == null){
                $g->guest_expire_date = "N/A";
            }
            else{
               $g->guest_expire_date = date('m/d/Y', strtotime($g->guest_expire_date));
            }

            $allUserGuests->push($g);
        }

        $count = 1;
        
        return view('admin/customer/edit', ['authuser' => $authuser, 'customer' => $customer, 'destinations' => $destinations, 'user' => $user, 'allusers' => $allusers, 'allUserGuests' => $allUserGuests, 
            'count' => $count]);
    }

    public function updateCustomer(Request $request, $id)
    {
        $this->validate($request, [
            'email' => 'email',
            'first_name' => 'required',
            'last_name' => 'required',
            'phone_no' => 'required',
            'address1' => 'required',
            'city' => 'required',
            'state' => 'required',
            'zip' => 'required',
        ]);

        $c = Customer::find($id);

        $c->email = $request->input('email');
        $c->first_name = $request->input('first_name');
        $c->last_name = $request->input('last_name');
        $c->country_code = "+1";
        $c->phone_no = $request->input('phone_no');
        $c->address1 = $request->input('address1');
        $c->address2 = $request->input('address2');
        $c->city = $request->input('city');
        $c->state = $request->input('state');
        $c->zip = $request->input('zip'); 
        $c->birth_date = $request->input('birth_date');
        $c->passport = $request->input('passport');
        $c->expire_date = $request->input('expire_date');
        $c->referral = $request->input('referral');   
        $c->disney_experience_username = $request->input('disney_experience_username');     
        $c->disney_experience_password = $request->input('disney_experience_password');  
        $c->notes = $request->input('notes');
        if(Auth::user()->role == 1)
        {
            $c->user_id = $request->input('userId');
        }
        else
        {
            $c->user_id = Auth::user()->id;
        }

        //saving customer
        $c->save();

        //saving user guests
        $guestCount = $request->input('count');

        
        if($guestCount > 0){
            
            $GuestNo = 2;
            for($i=1; $i<=$guestCount; $i++){
                $guest = new Guest();
                $guest->guest_no = $request->input('guest_no'.$GuestNo);
                $guest->guest_first_name = $request->input('guest_first_name'.$i);
                $guest->guest_last_name = $request->input('guest_last_name'.$i);
                $guest->guest_birth_date = $request->input('guest_birth_date'.$i);
                $guest->guest_passport = $request->input('guest_passport'.$i);
                $guest->guest_expire_date = $request->input('guest_expire_date'.$i);
                $guest->customer_id = $c->id;
                $guest->save();
                $GuestNo++;
            }

        }

        return redirect('customer/list');
    }

    public function deleteCustomer($id)
    {
        $c = Customer::find($id);
        $c->delete();
        return redirect('customer/list');
    }

    public function viewcustomer($id)
    { 
        $customer = Customer::find($id);

        $travelWith = new Collection();

        $travelWith = explode(',',$customer->travel_with);
        $searchForValue = '~';

        if( strpos($customer->travel_with, $searchForValue) !== false ) {
            $flag = "true";
            $customer->travel_with = str_replace("~", ",", $customer->travel_with);
        }
        else{
            $flag = "false";

        }

        $getGuests = Guest::where('customer_id', '=', $customer->id)->get();

        $trip = Trip::where('customer_id', '=', $customer->id)->get();

        return view('admin/customer/view', ['customer'=>$customer, 'flag' => $flag, 'trip'=>$trip,
         'guests'=>$getGuests]);
    }

//Customer End

//Guest CRUD
    public function deleteGuest(Request $request)
    {
         $this->validate($request, [
            'id' => 'required'
        ]);

        $id = $request->input('id');

        $g = Guest::find($id);
        $customer_id = $g->customer_id;
        $g->delete();

        //redirecting to edit screen of customer
        $customer = Customer::find($customer_id);
        $destinations = Destination::where('id', '>', 3)->get();
        $user = User::find($customer->user_id);
        $authuser = Auth::user();
        $allusers = User::all();
        $noAllUserGuests = Guest::where('customer_id', '=', $customer_id)->get()->sortBy('id');
        
        $allUserGuests = new Collection();

        foreach($noAllUserGuests as $g){
            if($g->guest_passport == null){
                $g->guest_passport = "N/A";
            } 

            if($g->guest_birth_date == null){
                $g->guest_birth_date = "N/A";
            }
            else{
               $g->guest_birth_date = date('m/d/Y', strtotime($g->guest_birth_date));
            }


            if($g->guest_expire_date == null){
                $g->guest_expire_date = "N/A";
            }
            else{
               $g->guest_expire_date = date('m/d/Y', strtotime($g->guest_expire_date));
            }

            $allUserGuests->push($g);
        }

        $count = 1;
        
        return view('admin/customer/edit', ['authuser' => $authuser, 'customer' => $customer, 'destinations' => $destinations, 'user' => $user, 'allusers' => $allusers, 'allUserGuests' => $allUserGuests, 
            'count' => $count]);
    }

    public function updateGuest(Request $request)
    {
        $this->validate($request, [
            'guest_first_name' => 'required',
            'guest_last_name' => 'required',
            'id' => 'required'
        ]);

        $id = $request->input('id');

        $g = Guest::find($id);

        $customer_id = $g->customer_id;

        $g->guest_first_name = $request->input('guest_first_name');
        $g->guest_last_name = $request->input('guest_last_name');
        $g->guest_birth_date = $request->input('guest_birth_date');
        $g->guest_passport = $request->input('guest_passport');
        $g->guest_expire_date = $request->input('guest_expire_date');
        $g->customer_id = $customer_id;

        //saving guest
         $g->save();

        //redirecting to edit screen of customer
        $customer = Customer::find($customer_id);
        $destinations = Destination::where('id', '>', 3)->get();
        $user = User::find($customer->user_id);
        $authuser = Auth::user();
        $allusers = User::all();
        $noAllUserGuests = Guest::where('customer_id', '=', $customer_id)->get()->sortBy('id');
        
        $allUserGuests = new Collection();

        foreach($noAllUserGuests as $g){
            if($g->guest_passport == null){
                $g->guest_passport = "N/A";
            }
            
            if($g->guest_birth_date == null){
                $g->guest_birth_date = "N/A";
            }
            else{
               $g->guest_birth_date = date('m/d/Y', strtotime($g->guest_birth_date));
            }


            if($g->guest_expire_date == null){
                $g->guest_expire_date = "N/A";
            }
            else{
               $g->guest_expire_date = date('m/d/Y', strtotime($g->guest_expire_date));
            }

            $allUserGuests->push($g);
        }

        $count = 1;
        
        return view('admin/customer/edit', ['authuser' => $authuser, 'customer' => $customer, 'destinations' => $destinations, 'user' => $user, 'allusers' => $allusers, 'allUserGuests' => $allUserGuests, 
            'count' => $count]);
    }

    public function guestUpdate($id){
        $guest = Guest::find($id);
        return Response::json($guest);
    }
//Guest End


// Trips CRUD
    public function listTrips()
    {
        // print_r('abc');
        // exit();
        $user = Auth::user();

        if($user->role == 1)
        {
            // $Alltrips = Trip::all()->sortByDesc('id');
            $Alltrips = Trip::where('Status', '!=', 2)->get()->sortByDesc('id');
        }
        else
        {
            $Alltrips = Trip::where([
                ['user_id', '=', $user->id],
                ['Status', '!=', 2],
            ])->get()->sortByDesc('id');
        }

       // print_r($Alltrips->toArray());
       // exit;

        //filtering todo trips only       
        $todoTrips = new Collection();

        foreach($Alltrips as $trip){

            if($trip->todo == null){
                $todoTrips->push($trip);
            }
        }

        //filtering without todo trips
        $notTodoTrips = new Collection();

        foreach($Alltrips as $trip){

            if($trip->todo != null){
                $notTodoTrips->push($trip);
            }

        }
    
        $firstname = null;
        $lastname = null;
        $reservationnumber = null;
        $dateoftravel = null;
                                                 //$Alltrips
        return view('admin/trip/list', ['trips'=>$Alltrips, 'todoTrips'=>$todoTrips, 'user'=>$user, 'firstname' => $firstname, 'lastname' => $lastname, 'reservationnumber' => $reservationnumber, 'dateoftravel' => $dateoftravel]);
    }


    public function viewTrip($id)
    { 
        $trip = Trip::find($id);

        $travelWith = new Collection();

        $travelWith = explode(',',$trip->travel_with);
        $searchForValue = '~';

        if( strpos($trip->travel_with, $searchForValue) !== false ) {
            $flag = "true";
            $trip->travel_with = str_replace("~", ",", $trip->travel_with);
        }
        else{
            $flag = "false";

        }

        return view('admin/trip/view', ['trip'=>$trip, 'flag' => $flag]);
    }

    // public function viewSinglecustomer($id)
    // { 
    //     $trip = Trip::find($id);

    //     $travelWith = new Collection();

    //     $travelWith = explode(',',$trip->travel_with);
    //     $searchForValue = '~';

    //     if( strpos($trip->travel_with, $searchForValue) !== false ) {
    //         $flag = "true";
    //         $trip->travel_with = str_replace("~", ",", $trip->travel_with);
    //     }
    //     else{
    //         $flag = "false";

    //     }

    //     return view('admin/trip/singlecustomerview', ['trip'=>$trip, 'flag' => $flag]);
    // }

    public function AddPayment($id)
    { 
        $trip = Trip::find($id);
            $trippayments =PaymentHistory::where('trip_id','=', $trip->id)->get();
            $sum= $trippayments->sum('amount');

            $RemainingBalance=  $trip->total_sale - $sum;

        return view('admin/trip/AddPayment', ['trip'=>$trip, 'trippayments'=> $trippayments, 'RemainingBalance'=>$RemainingBalance]);
    }

    public function cancelTrips($id)
    {

        // $this->validate($request, [
        //     'Reason' => 'required',
        // ]);

        // print_r($id);
        // exit;

       // $trip = Trip::find($id);
        //$tripp = Trips::where('trip_id','=', $trip->id)->get();
       // $trip->Reason=$request->input('Reason');
        //$trip->status= 1;

        //$trip->save();


      return view('admin/trip/CancelTrip', ['tripId'=>$id]);
    }

    public function updateTripStatus(Request $request)
    {
        $user_id = Auth::user()->id;
        $this->validate($request, [
            'id' => 'required',
            'reason' => 'required',
        ]);

        // print_r($request->input('id'));
        // print_r($request->input('Reason'));
        // exit;

        $trip = Trip::find($request->input('id'));
        $trip->reason=$request->input('reason');
        $trip->status= 1;

        $trip->save();

        $requiredAgent = User::find($user_id);

        $al = new AuditLogs();

        $al->agent_name = $requiredAgent->name;

        $al->tripId = $trip->id;
        $al->reservation_number = $trip->reservation_number;
        $al->change_date = Carbon::now();
        $al->event = "Trip Status Updated";
        
        $al->save();

      return redirect()->action(
                'HomeController@listTrips'
            );
    }

    public function TripHistory($id)
    {
        $trips = Trip::where('customer_id', '=', $id)->get()->sortByDesc('created_at');
        return view('admin/customer/TripHistory',['trips'=>$trips]);
    }


    public function addTrip($id)
    {
        $customer = Customer::find($id);
        $name = $customer->first_name. ' ' .$customer->last_name;
        $alldestinations = Destination::where('id', '>', 4)->get();

        // print_r($disneyCruiseLineShips->toArray())

        $guests = Guest::where('customer_id', '=', $id)->get();
        $user = Auth::user();
       
         $agentdestinations =Agentdestination::where('user_id','=', $user->id)->orderBy('destination_name','asc')->get();
         $destinations= Destination::all();

        $universalStudiosHotels = new Collection();
        $disneyCruiseLineShips = new Collection();
        $disneylandResortHotels = new Collection();
        $waltDisneyWorldHotels = new Collection();
        $royalCaribbeanCruiseLineShips = new Collection();
        $carnivalCruiseLineShips = new Collection();


                $valueResorts = new Collection();
                $moderateResorts = new Collection();
                $deluxeResorts = new Collection();
                $deluxeVillas = new Collection();

                $oasis = new Collection();
                $quantum = new Collection();
                $freedom = new Collection();
                $voyager = new Collection();
                $radiance = new Collection();
                $vision = new Collection();
                $other = new Collection();


                $xl = new Collection();
                $sunshine = new Collection();
                $vista = new Collection();
                $dream = new Collection();
                $splendor = new Collection();
                $conquest = new Collection();
                $spirit = new Collection();
                $fantasy = new Collection();

                $disneyWaltHotelCategories = new Collection();
                $royalHotelCategories = new Collection();
                $carnivalHotelCategories = new Collection();

               // print_r($destinations->toArray()); exit; 


        foreach ($destinations as $destination) {
            if($destination->name == "Universal Studios Florida"){
               // print_r($destinations->toArray()); exit; 
                $universalStudiosHotels = Hotel::where('destination_id', '=', $destination->id)->get();
            }
            else if($destination->name == "Disney Cruise Line"){
                $disneyCruiseLineShips = Hotel::where('destination_id', '=', $destination->id)->get();
            }
            else if($destination->name == "Disneyland Resort"){
                $disneylandResortHotels = Hotel::where('destination_id', '=', $destination->id)->get();
            }
            else if($destination->name == "Walt Disney World"){

                $waltDisneyWorldHotels = Hotel::where('destination_id', '=', $destination->id)->get();

                $disneyWaltHotelCategories = HotelCategory::where('destination_id', '=', $destination->id)->get();

                  //print_r($waltDisneyWorldHotels->toArray()); exit; 
                foreach ($disneyWaltHotelCategories as $hotelCategory) {
                    if($hotelCategory->category_name == "Value Resorts"){
                        foreach($waltDisneyWorldHotels as $waltHotel){
                            if($waltHotel->category_id == $hotelCategory->id){
                                $valueResorts->push($waltHotel);
                            }
                        }
                    }
                    else if($hotelCategory->category_name == "Moderate Resorts"){
                       foreach($waltDisneyWorldHotels as $waltHotel){
                            if($waltHotel->category_id == $hotelCategory->id){
                                $moderateResorts->push($waltHotel);
                            }
                        }
                    }
                    else if($hotelCategory->category_name == "Deluxe Resorts"){
                        foreach($waltDisneyWorldHotels as $waltHotel){
                            if($waltHotel->category_id == $hotelCategory->id){
                                $deluxeResorts->push($waltHotel);
                            }
                        }
                    }
                    else if($hotelCategory->category_name == "Deluxe Villas"){
                        foreach($waltDisneyWorldHotels as $waltHotel){
                            if($waltHotel->category_id == $hotelCategory->id){
                                $deluxeVillas->push($waltHotel);
                            }
                        }
                    }
                }

            }
            else if($destination->name == "Royal Caribbean Cruise Line"){
                $royalCaribbeanCruiseLineShips = Hotel::where('destination_id', '=', $destination->id)->get();

                $royalHotelCategories = HotelCategory::where('destination_id', '=', $destination->id)->get();



                foreach ($royalHotelCategories as $hotelCategory) {
                    if($hotelCategory->category_name == "Oasis Class"){
                        foreach($royalCaribbeanCruiseLineShips as $royalHotel){
                            if($royalHotel->category_id == $hotelCategory->id){
                                 $oasis->push($royalHotel);
                            }
                        }
                       
                    }
                    else if($hotelCategory->category_name == "Quantum Class"){
                         foreach($royalCaribbeanCruiseLineShips as $royalHotel){
                            if($royalHotel->category_id == $hotelCategory->id){
                                $quantum->push($royalHotel);
                            }
                        }
                        
                    }
                    else if($hotelCategory->category_name == "Freedom Class"){
                        foreach($royalCaribbeanCruiseLineShips as $royalHotel){
                            if($royalHotel->category_id == $hotelCategory->id){
                                $freedom->push($royalHotel);
                            }
                        }
                    }
                    else if($hotelCategory->category_name == "Voyager Class"){
                        foreach($royalCaribbeanCruiseLineShips as $royalHotel){
                            if($royalHotel->category_id == $hotelCategory->id){
                                $voyager->push($royalHotel);
                            }
                        }
                    }
                    else if($hotelCategory->category_name == "Radiance Class"){
                        foreach($royalCaribbeanCruiseLineShips as $royalHotel){
                            if($royalHotel->category_id == $hotelCategory->id){
                                $radiance->push($royalHotel);
                            }
                        }
                    }
                    else if($hotelCategory->category_name == "Vision Class"){
                       foreach($royalCaribbeanCruiseLineShips as $royalHotel){
                            if($royalHotel->category_id == $hotelCategory->id){
                                $vision->push($royalHotel);
                            }
                        }
                    }
                    else if($hotelCategory->category_name == "Other"){
                        foreach($royalCaribbeanCruiseLineShips as $royalHotel){
                            if($royalHotel->category_id == $hotelCategory->id){
                                $other->push($royalHotel);
                            }
                        }
                    }
                }

                // print_r($quantum->toArray()); exit;
            }
            else if($destination->name == "Carnival Cruise Line"){
                $carnivalCruiseLineShips = Hotel::where('destination_id', '=', $destination->id)->get();

                $carnivalHotelCategories = HotelCategory::where('destination_id', '=', $destination->id)->get();

                foreach ($carnivalHotelCategories as $hotelCategory) {
                    if($hotelCategory->category_name == "XL Class"){
                        foreach($carnivalCruiseLineShips as $carnivalHotel){
                            if($carnivalHotel->category_id == $hotelCategory->id){
                                $xl->push($carnivalHotel);
                            }
                        }
                    }
                    else if($hotelCategory->category_name == "Sunshine Class"){
                        foreach($carnivalCruiseLineShips as $carnivalHotel){
                            if($carnivalHotel->category_id == $hotelCategory->id){
                                $sunshine->push($carnivalHotel);
                            }
                        }
                    }
                    else if($hotelCategory->category_name == "Vista Class"){
                        foreach($carnivalCruiseLineShips as $carnivalHotel){
                            if($carnivalHotel->category_id == $hotelCategory->id){
                                $vista->push($carnivalHotel);
                            }
                        }
                    }
                    else if($hotelCategory->category_name == "Dream Class"){
                       foreach($carnivalCruiseLineShips as $carnivalHotel){
                            if($carnivalHotel->category_id == $hotelCategory->id){
                                $dream->push($carnivalHotel);
                            }
                        }
                    }
                    else if($hotelCategory->category_name == "Splendor Class"){
                       foreach($carnivalCruiseLineShips as $carnivalHotel){
                            if($carnivalHotel->category_id == $hotelCategory->id){
                                $splendor->push($carnivalHotel);
                            }
                        }
                    }
                    else if($hotelCategory->category_name == "Conquest Class"){
                       foreach($carnivalCruiseLineShips as $carnivalHotel){
                            if($carnivalHotel->category_id == $hotelCategory->id){
                                $conquest->push($carnivalHotel);
                            }
                        }
                    }
                    else if($hotelCategory->category_name == "Spirit Class"){
                        foreach($carnivalCruiseLineShips as $carnivalHotel){
                            if($carnivalHotel->category_id == $hotelCategory->id){
                                $spirit->push($carnivalHotel);
                            }
                        }
                    }
                    else if($hotelCategory->category_name == "Fantasy Class"){
                       foreach($carnivalCruiseLineShips as $carnivalHotel){
                            if($carnivalHotel->category_id == $hotelCategory->id){
                                $fantasy->push($carnivalHotel);
                            }
                        }
                    }
                }
            }
        }



        /// print_r($agentdestinations->toArray());
         //exit;

        //print_r($agentdestinations->toArray());
        //exit;



        //          foreach($alldestinations as $destination){

        //                                     foreach($agentdestinations as $agentdestination){

        //                                             if($destination->id == $agentdestination->destination_id )
        //                                             {
        //                                                 $destinations->push($destination);
        //                                              } 

        //                                              if($agentdestination->destination_id <= 4)
        //                                              {
        // $destinations->push($destination);
        //                                              }

        //                                     }

        //                                 }  




             
        // $destinations= $destinations->unique();
         // print_r($waltDisneyWorldHotels->toArray()); exit; 

        return view('admin/trip/create', ['destinations'=>$destinations ,'agentdestinations' => $agentdestinations, 'user' => $user, 'customer' => $customer, 'name' => $name, 
            'guests' => $guests,
            'universalStudiosHotels' => $universalStudiosHotels,
            'disneyCruiseLineShips' => $disneyCruiseLineShips,
            'disneylandResortHotels' => $disneylandResortHotels,
            'waltDisneyWorldHotels' => $waltDisneyWorldHotels,
            'royalCaribbeanCruiseLineShips' => $royalCaribbeanCruiseLineShips,
            'carnivalCruiseLineShips' => $carnivalCruiseLineShips,
            'valueResorts' => $valueResorts,
            'moderateResorts' => $moderateResorts,
            'deluxeResorts' => $deluxeResorts,
            'deluxeVillas' => $deluxeVillas,
            'oasis' => $oasis,
            'quantum' => $quantum,
            'freedom' => $freedom,
            'voyager' => $voyager,
            'radiance' => $radiance,
            'vision' => $vision,
            'other' => $other,
            'xl' => $xl,
            'sunshine' => $sunshine,
            'dream' => $dream,
            'vista' => $vista,
            'splendor' => $splendor,
            'conquest' => $conquest,
            'spirit' => $spirit,
            'fantasy' => $fantasy]);
    }

    public function saveTrip(Request $request)
    {



        $this->validate($request, [
            'user_id' => 'required',
            'customer_id'=> 'required',
            'reservation_number' => 'required',
            'booking_date' => 'required',
            'trip_status' => 'required',
            'total_sale' => 'required',
            'commission' => 'required',
            'expected_commission' => 'required',
            'destination' => 'required',
            //'status' => 'required',

        ]);

      //  dd($request);

        try {

          

        $c = new Trip();

        // $c->destination = Destination::where('id', '=', $request->input('destination'))->orWhere('name', '=', $request->input('destination'))->first()->name;

        $c->Referal = $request->input('Referal');
        $c->customer_id = $request->input('customer_id');
        $c->user_id = Auth::user()->id;
        $c->reservation_number = $request->input('reservation_number');
        $c->booking_date = $request->input('booking_date');

          
        $c->checkin_date = $request->input('checkin_date');
        if($request->input('checkout_date') == null || $request->input('checkout_date') == "")
        {
            $c->checkout_date = $request->input('checkin_date');
        }
        else
        {
            $c->checkout_date = $request->input('checkout_date');
        }
        $c->trip_status = $request->input('trip_status');
        $c->commission = $request->input('commission');
        $c->total_sale = $request->input('total_sale');
        $c->expected_commission = $request->input('expected_commission');
        $allTravelWith = $request->input('travel_with');
        $c->universal_studios_florida_hotel_names = $request->input('universal_studios_florida_hotel_names');
        $c->universal_dining_plan = $request->input('universal_dining_plan');
        $c->my_universal_photos = $request->input('my_universal_photos');
        $c->universal_super_shuttle = $request->input('universal_super_shuttle');
        $c->universal_express_pass = $request->input('universal_express_pass');
        $c->disneyworld_hotel_name = $request->input('disneyworld_hotel_name');
        $c->Referal = $request->input('Referal');
        $c->other = $request->input('other');



        $travel = null;
        if($allTravelWith == null){

            $c->travel_with = $travel;
        }
        else{

             foreach ($allTravelWith as $travelWith) {
               $travel = $travel.$travelWith.'~';
            }

            $c->travel_with =  $travel; //substr($travel, 0, -1);

        }
        
       
         // print_r($c->travel_with);
         // exit;
        $c->special_request = $request->input('special_request');

        if ($request->input('destination')=="Walt Disney World")
        {

            $c->destination = $request->input('destination');
            $c->disney_dining_plan = $request->input('disney_dining_plan');
            $c->memory_maker = $request->input('memory_maker');
            $c->magical_express = $request->input('magical_express');
            $c->magic_band_color = $request->input('magic_band_color');
            $c->disneyworld_hotel_name = $request->input('disneyworld_hotel_name');
            $c->ticket_type = $request->input('ticket_type');
            $c->insurance = $request->input('insurance');
            $c->notes = $request->input('notes');
            // $c->Referal = $request->input('Referal');
            // $c->other = $request->input('other');

            }
            else if ($request->input('destination')=="Disneyland Resort")
            {

                $c->destination = $request->input('destination');
                $c->good_neighbor_hotel = $request->input('good_neighbor_hotel');
                $c->disneyresort_hotel_name = $request->input('disneyresort_hotel_name');
                $c->ticket_type = $request->input('ticket_type');
                $c->insurance = $request->input('insurance');
                $c->notes = $request->input('notes');

       

            }
            else if ($request->input('destination')=="Disney Cruise Line")
             {
              
                $c->destination = $request->input('destination');
                $c->ship_name2 = $request->input('ship_name2');
                $c->stateroom_category = $request->input('stateroom_category');
                $c->castaway_member = $request->input('castaway_member');          
                $c->castaway_club_level = $request->input('castaway_club_level');
                $c->bus_transportation = $request->input('bus_transportation');
            }
            else if ($request->input('destination')=="Carnival Cruise Line")
            {
                $c->destination = $request->input('destination');
                $c->ship_name = $request->input('ship_name1');
                $c->insurance = $request->input('insurance');
                $c->notes = $request->input('notes');
            }
            else if ($request->input('destination')=="Royal Caribbean Cruise Line")
            {
              

                $c->destination = $request->input('destination');
                $c->ship_name = $request->input('ship_name');
                $c->stateroom_category_caribbean = $request->input('stateroom_category_caribbean');
                $c->crown_anchor_level = $request->input('crown_anchor_level');
                $c->insurance = $request->input('insurance');
                $c->notes = $request->input('notes');
                  
                   
            }
            else if ($request->input('destination')=="Universal Studios Florida")
            {
                   $c->destination = $request->input('destination');
                   
                   $c->universal_studios_florida_hotel_names = $request->input('universal_studios_florida_hotel_names');
                   $c->universal_dining_plan = $request->input('universal_dining_plan');
                   $c->my_universal_photos = $request->input('my_universal_photos');
                   $c->universal_super_shuttle = $request->input('universal_super_shuttle');
                   $c->universal_express_pass = $request->input('universal_express_pass');
                   $c->disneyworld_hotel_name = $request->input('disneyworld_hotel_name');
                   $c->ticket_type = $request->input('ticket_type');
                   // $c->insurance = $request->input('insurance');
                   // $c->notes = $request->input('notes');
       
            }
            else
            {
                $c->destination = $request->input('destination');
                $c->insurance = $request->input('insurance');
                $c->Referal = $request->input('Referal');
                $c->other = $request->input('other');
                $c->notes = $request->input('notes');
            }


            $done =  $c->save();

            //$requiredTrip = users::where('id', '=', $c->id)->get();

            $requiredAgent = User::find($c->user_id);
            // exit;

            $al = new AuditLogs();

            $al->agent_name = $requiredAgent->name;

            $al->tripId = $c->id;
            $al->reservation_number = $request->input('reservation_number');
            $al->change_date = Carbon::now();
            $al->event = "New Trip Created";
        
            $al->save();
        }
        catch (\Illuminate\Database\QueryException $ex){ 
             dd($ex->getMessage()); }
        // Mail::to('bookings@MandMMagicalAdventures.com')->send(new NewTrip($c));
        
        return redirect('todo/create/'.$c->id);
    }

   public function AddPaymentHistory(Request $request)
    {
     $this->validate($request, [
            'trip_ID' => 'required',
            'Payment_date'=> 'required',
            'amount' => 'required',
            
        ]);

        $obj =new PaymentHistory();
        $obj->trip_id=$request->input('trip_ID');
        $obj->payment_date=$request->input('Payment_date');
        $obj->amount=$request->input('amount');

        $obj->save();
        return redirect('trip/AddPayment/' .$request->input('trip_ID'));
   }

   public function RemovePaymentHistory($id)
   {
       $payment = PaymentHistory::find($id);

       $trip_id = $payment->trip_id;

       $payment->delete();

       return redirect('trip/AddPayment/' .$trip_id);
   }
   

    public function editTrip($id)
    {
        // $destinations = Destination::where('id', '>', 4)->get();
        $trip = Trip::find($id);
        $customer = Customer::where('id', '=', $trip->customer_id)->first(); 
        $guests = Guest::where('customer_id', '=', $trip->customer_id)->get();       
        $name = $customer->first_name. ' ' .$customer->last_name;
        
        $travelWith = new Collection();
        $notTravelWith = new Collection();

        $stringVal = substr($trip->travel_with, 0,-1);
        $travelWith = explode('~',$stringVal);

        $searchForValue = '~';

        if( strpos($trip->travel_with, $searchForValue) !== false ) {
            $flag = "true";
        }
        else{
            $flag = "false";
        }


         // print_r($travelWith);
         // exit;

        $user_d  = $trip->user->id;
        
        $authuser= Auth::user();

        $users = User::all();
         $agentdestinations =Agentdestination::where('user_id','=', $user_d)->orderBy('destination_name','asc')->get();

         $destinations= Destination::all();

        $universalStudiosHotels = new Collection();
        $disneyCruiseLineShips = new Collection();
        $disneylandResortHotels = new Collection();
        $waltDisneyWorldHotels = new Collection();
        $royalCaribbeanCruiseLineShips = new Collection();
        $carnivalCruiseLineShips = new Collection();
        $Referal = new Collection;

                $valueResorts = new Collection();
                $moderateResorts = new Collection();
                $deluxeResorts = new Collection();
                $deluxeVillas = new Collection();

                $oasis = new Collection();
                $quantum = new Collection();
                $freedom = new Collection();
                $voyager = new Collection();
                $radiance = new Collection();
                $vision = new Collection();
                $other = new Collection();


                $xl = new Collection();
                $sunshine = new Collection();
                $vista = new Collection();
                $dream = new Collection();
                $splendor = new Collection();
                $conquest = new Collection();
                $spirit = new Collection();
                $fantasy = new Collection();

                $disneyWaltHotelCategories = new Collection();
                $royalHotelCategories = new Collection();
                $carnivalHotelCategories = new Collection();

               // print_r($destinations->toArray()); exit; 


        foreach ($destinations as $destination) {
            if($destination->name == "Universal Studios Florida"){
               // print_r($destinations->toArray()); exit; 
                $universalStudiosHotels = Hotel::where('destination_id', '=', $destination->id)->get();
            }
            else if($destination->name == "Disney Cruise Line"){
                $disneyCruiseLineShips = Hotel::where('destination_id', '=', $destination->id)->get();
            }
            else if($destination->name == "Disneyland Resort"){
                $disneylandResortHotels = Hotel::where('destination_id', '=', $destination->id)->get();
            }
            else if($destination->name == "Walt Disney World"){

                $waltDisneyWorldHotels = Hotel::where('destination_id', '=', $destination->id)->get();

                $disneyWaltHotelCategories = HotelCategory::where('destination_id', '=', $destination->id)->get();

                  //print_r($waltDisneyWorldHotels->toArray()); exit; 
                foreach ($disneyWaltHotelCategories as $hotelCategory) {
                    if($hotelCategory->category_name == "Value Resorts"){
                        foreach($waltDisneyWorldHotels as $waltHotel){
                            if($waltHotel->category_id == $hotelCategory->id){
                                $valueResorts->push($waltHotel);
                            }
                        }
                    }
                    else if($hotelCategory->category_name == "Moderate Resorts"){
                       foreach($waltDisneyWorldHotels as $waltHotel){
                            if($waltHotel->category_id == $hotelCategory->id){
                                $moderateResorts->push($waltHotel);
                            }
                        }
                    }
                    else if($hotelCategory->category_name == "Deluxe Resorts"){
                        foreach($waltDisneyWorldHotels as $waltHotel){
                            if($waltHotel->category_id == $hotelCategory->id){
                                $deluxeResorts->push($waltHotel);
                            }
                        }
                    }
                    else if($hotelCategory->category_name == "Deluxe Villas"){
                        foreach($waltDisneyWorldHotels as $waltHotel){
                            if($waltHotel->category_id == $hotelCategory->id){
                                $deluxeVillas->push($waltHotel);
                            }
                        }
                    }
                }

            }
            else if($destination->name == "Royal Caribbean Cruise Line"){
                $royalCaribbeanCruiseLineShips = Hotel::where('destination_id', '=', $destination->id)->get();

                $royalHotelCategories = HotelCategory::where('destination_id', '=', $destination->id)->get();



                foreach ($royalHotelCategories as $hotelCategory) {
                    if($hotelCategory->category_name == "Oasis Class"){
                        foreach($royalCaribbeanCruiseLineShips as $royalHotel){
                            if($royalHotel->category_id == $hotelCategory->id){
                                 $oasis->push($royalHotel);
                            }
                        }
                       
                    }
                    else if($hotelCategory->category_name == "Quantum Class"){
                         foreach($royalCaribbeanCruiseLineShips as $royalHotel){
                            if($royalHotel->category_id == $hotelCategory->id){
                                $quantum->push($royalHotel);
                            }
                        }
                        
                    }
                    else if($hotelCategory->category_name == "Freedom Class"){
                        foreach($royalCaribbeanCruiseLineShips as $royalHotel){
                            if($royalHotel->category_id == $hotelCategory->id){
                                $freedom->push($royalHotel);
                            }
                        }
                    }
                    else if($hotelCategory->category_name == "Voyager Class"){
                        foreach($royalCaribbeanCruiseLineShips as $royalHotel){
                            if($royalHotel->category_id == $hotelCategory->id){
                                $voyager->push($royalHotel);
                            }
                        }
                    }
                    else if($hotelCategory->category_name == "Radiance Class"){
                        foreach($royalCaribbeanCruiseLineShips as $royalHotel){
                            if($royalHotel->category_id == $hotelCategory->id){
                                $radiance->push($royalHotel);
                            }
                        }
                    }
                    else if($hotelCategory->category_name == "Vision Class"){
                       foreach($royalCaribbeanCruiseLineShips as $royalHotel){
                            if($royalHotel->category_id == $hotelCategory->id){
                                $vision->push($royalHotel);
                            }
                        }
                    }
                    else if($hotelCategory->category_name == "Other"){
                        foreach($royalCaribbeanCruiseLineShips as $royalHotel){
                            if($royalHotel->category_id == $hotelCategory->id){
                                $other->push($royalHotel);
                            }
                        }
                    }
                }

                // print_r($quantum->toArray()); exit;
            }
            else if($destination->name == "Carnival Cruise Line"){
                $carnivalCruiseLineShips = Hotel::where('destination_id', '=', $destination->id)->get();

                $carnivalHotelCategories = HotelCategory::where('destination_id', '=', $destination->id)->get();

                foreach ($carnivalHotelCategories as $hotelCategory) {
                    if($hotelCategory->category_name == "XL Class"){
                        foreach($carnivalCruiseLineShips as $carnivalHotel){
                            if($carnivalHotel->category_id == $hotelCategory->id){
                                $xl->push($carnivalHotel);
                            }
                        }
                    }
                    else if($hotelCategory->category_name == "Sunshine Class"){
                        foreach($carnivalCruiseLineShips as $carnivalHotel){
                            if($carnivalHotel->category_id == $hotelCategory->id){
                                $sunshine->push($carnivalHotel);
                            }
                        }
                    }
                    else if($hotelCategory->category_name == "Vista Class"){
                        foreach($carnivalCruiseLineShips as $carnivalHotel){
                            if($carnivalHotel->category_id == $hotelCategory->id){
                                $vista->push($carnivalHotel);
                            }
                        }
                    }
                    else if($hotelCategory->category_name == "Dream Class"){
                       foreach($carnivalCruiseLineShips as $carnivalHotel){
                            if($carnivalHotel->category_id == $hotelCategory->id){
                                $dream->push($carnivalHotel);
                            }
                        }
                    }
                    else if($hotelCategory->category_name == "Splendor Class"){
                       foreach($carnivalCruiseLineShips as $carnivalHotel){
                            if($carnivalHotel->category_id == $hotelCategory->id){
                                $splendor->push($carnivalHotel);
                            }
                        }
                    }
                    else if($hotelCategory->category_name == "Conquest Class"){
                       foreach($carnivalCruiseLineShips as $carnivalHotel){
                            if($carnivalHotel->category_id == $hotelCategory->id){
                                $conquest->push($carnivalHotel);
                            }
                        }
                    }
                    else if($hotelCategory->category_name == "Spirit Class"){
                        foreach($carnivalCruiseLineShips as $carnivalHotel){
                            if($carnivalHotel->category_id == $hotelCategory->id){
                                $spirit->push($carnivalHotel);
                            }
                        }
                    }
                    else if($hotelCategory->category_name == "Fantasy Class"){
                       foreach($carnivalCruiseLineShips as $carnivalHotel){
                            if($carnivalHotel->category_id == $hotelCategory->id){
                                $fantasy->push($carnivalHotel);
                            }
                        }
                    }
                }
            }
        }

        
        return view('admin/trip/edit', ['trip' => $trip, 'authuser' => $authuser, 'users' => $users, 'customer' => $customer, 'destinations' => $agentdestinations, 'name' => $name, 
            'travelWith' => $travelWith, 'guests' => $guests,
            'universalStudiosHotels' => $universalStudiosHotels,
            'disneyCruiseLineShips' => $disneyCruiseLineShips,
            'disneylandResortHotels' => $disneylandResortHotels,
            'waltDisneyWorldHotels' => $waltDisneyWorldHotels,
            'royalCaribbeanCruiseLineShips' => $royalCaribbeanCruiseLineShips,
            'carnivalCruiseLineShips' => $carnivalCruiseLineShips,
            'valueResorts' => $valueResorts,
            'moderateResorts' => $moderateResorts,
            'deluxeResorts' => $deluxeResorts,
            'deluxeVillas' => $deluxeVillas,
            'oasis' => $oasis,
            'quantum' => $quantum,
            'freedom' => $freedom,
            'voyager' => $voyager,
            'radiance' => $radiance,
            'vision' => $vision,
            'other' => $other,
            'xl' => $xl,
            'sunshine' => $sunshine,
            'dream' => $dream,
            'vista' => $vista,
            'splendor' => $splendor,
            'conquest' => $conquest,
            'spirit' => $spirit,
            'fantasy' => $fantasy,
            'flag' => $flag]);
    }

    public function updateTrip(Request $request, $id)
    {
    
    $this->validate($request, [
            'user_id' => 'required',
            'customer_id'=> 'required',
            'reservation_number' => 'required',
            'trip_status' => 'required',
            'total_sale' => 'required',
            'commission' => 'required',
            'expected_commission' => 'required',
            'destination' => 'required',
        ]);
    
        // Destination::where('id', '=', $request->input('destination'))->orWhere('name', '=', $request->input('destination'))->first()->name;

        $c = Trip::find($id);


        // $c->customer_id = $request->input('customer_id');
        // $c->user_id = $request->input('user_id');
        // $c->destination = $request->input('destination');
        // $c->reservation_number = $request->input('reservation_number');
        // if($request->input('booking_date') != null || $request->input('booking_date') != "")
        // {
        //     $c->booking_date = $request->input('booking_date');
        // }        
        // $c->trip_status = $request->input('trip_status');
        // $c->total_sale = $request->input('total_sale');
        // $c->disneyworld_hotel_name = $request->input('disneyworld_hotel_name');
        // $c->disneyresort_hotel_name = $request->input('disneyresort_hotel_name');
        // $c->memory_maker = $request->input('memory_maker');
        // $c->magical_express = $request->input('magical_express');
        // $c->checkin_date = $request->input('checkin_date');
        // if($request->input('checkout_date') == null || $request->input('checkout_date') == "")
        // {
        //     $c->checkout_date = $request->input('checkin_date');
        // }
        // else
        // {
        //     $c->checkout_date = $request->input('checkout_date');
        // }
        // $c->ship_name = $request->input('ship_name');
        // $c->castaway_member = $request->input('castaway_member');
        // $c->bus_transportation = $request->input('bus_transportation');
        // $c->commission = $request->input('commission');
        // $c->expected_commission = $request->input('expected_commission');
        // $c->travel_with = $request->input('travel_with');
        // $c->insurance = $request->input('insurance');
        // $c->disney_dining_plan = $request->input('disney_dining_plan');
        // $c->special_request = $request->input('special_request');
        // $c->notes = $request->input('notes');
        // $c->ticket_type = $request->input('ticket_type');
        // $c->promo_applied = $request->input('promo_applied');
        
        // $c->magic_band_color = $request->input('magic_band_color');
        // $c->good_neighbor_hotel = $request->input('good_neighbor_hotel');
        // $c->universal_orlando_resort_hotel = $request->input('universal_orlando_resort_hotel');
        // $c->partners_hotel = $request->input('partners_hotel');  

       $c->customer_id = $request->input('customer_id');
        $c->Referal = $request->input('Referal');
        //
         $c->user_id =  $request->input('user_id');
         // Auth::user()->id;
         $c->reservation_number = $request->input('reservation_number');
          $c->booking_date = $request->input('booking_date');
          $c->checkin_date = $request->input('checkin_date');
          if($request->input('checkout_date') == null || $request->input('checkout_date') == "")
        {
            $c->checkout_date = $request->input('checkin_date');
        }
        else
        {
            $c->checkout_date = $request->input('checkout_date');
        }
        $c->trip_status = $request->input('trip_status');
        $c->commission = $request->input('commission');
        $c->total_sale = $request->input('total_sale');
        $c->expected_commission = $request->input('expected_commission');
        $allTravelWith = $request->input('travel_with');
         $c->Referal = $request->input('Referal');

        $travel = null;
        if($allTravelWith == null){

             //print_r($allTravelWith); exit;

            $c->travel_with = $travel;
        }
        //checking if it is an array then new feature else it is an old record
        else if(is_array($allTravelWith)){

            //print_r($allTravelWith); exit;

           foreach ($allTravelWith as $travelWith) {
               $travel = $travel.$travelWith.'~';
            }

            $c->travel_with = $travel; //substr($travel, 0, -1);

        }
        else{

           // print_r($allTravelWith); exit;

            $c->travel_with =  $allTravelWith;
        }
        $c->special_request = $request->input('special_request');

            if ($request->input('destination')=="Walt Disney World")
            {

                 $c->destination = $request->input('destination');
                  $c->disney_dining_plan = $request->input('disney_dining_plan');
                   $c->memory_maker = $request->input('memory_maker');
                   $c->magical_express = $request->input('magical_express');
                   $c->magic_band_color = $request->input('magic_band_color');
                   $c->disneyworld_hotel_name = $request->input('disneyworld_hotel_name');
                   $c->ticket_type = $request->input('ticket_type');
                   $c->insurance = $request->input('insurance');
                    $c->Referal = $request->input('Referal');
                   $c->notes = $request->input('notes');


        
        $c->disneyresort_hotel_name = null;
        $c->ship_name = null;
        $c->castaway_member = null;
        $c->bus_transportation = null;
        $c->promo_applied = null;
        $c->good_neighbor_hotel = null;
        $c->universal_orlando_resort_hotel = null;
        $c->partners_hotel =null;



            }elseif ($request->input('destination')=="Disneyland Resort")
            {

                  $c->destination = $request->input('destination');
                  $c->good_neighbor_hotel = $request->input('good_neighbor_hotel');
                  $c->disneyresort_hotel_name = $request->input('disneyresort_hotel_name');
                   $c->ticket_type = $request->input('ticket_type');
                   $c->insurance = $request->input('insurance');
                    $c->Referal = $request->input('Referal');
                   $c->notes = $request->input('notes');

        $c->disneyworld_hotel_name = null;
        $c->memory_maker = null;
        $c->magical_express = null;
        $c->ship_name =null;
        $c->castaway_member =null;
        $c->bus_transportation = null;
        $c->disney_dining_plan = null;
        $c->promo_applied = null;
        $c->magic_band_color = null;
        $c->universal_orlando_resort_hotel =null;
        $c->partners_hotel = null;

            }elseif ($request->input('destination')=="Disney Cruise Line")
             {
              

                 $c->destination = $request->input('destination');
                 $c->ship_name2 = $request->input('ship_name2');
                 $c->castaway_member = $request->input('castaway_member');
                 $c->bus_transportation = $request->input('bus_transportation');
                 $c->stateroom_category = $request->input('stateroom_category');        
                 $c->castaway_club_level = $request->input('castaway_club_level');
                 $c->insurance = $request->input('insurance');
                  $c->Referal = $request->input('Referal');
                 $c->notes = $request->input('notes');


        $c->disneyworld_hotel_name = null;
        $c->disneyresort_hotel_name = null;
        $c->memory_maker = null;
        $c->magical_express = null;
        $c->disney_dining_plan = null;
        $c->ticket_type = null;
        $c->promo_applied = null;
        $c->magic_band_color = null;
        $c->good_neighbor_hotel = null;
        $c->universal_orlando_resort_hotel = null;
        $c->partners_hotel = null;


            }elseif ($request->input('destination')=="Universal Studios Florida")
             {
                  $c->destination = $request->input('destination');
                  // $c->universal_orlando_resort_hotel = $request->input('universal_orlando_resort_hotel');
                   // $c->partners_hotel = $request->input('partners_hotel');
                    $c->ticket_type = $request->input('ticket_type');
                   $c->insurance = $request->input('insurance');
                    $c->Referal = $request->input('Referal');
                   $c->notes = $request->input('notes');


       $c->disneyworld_hotel_name = null;
        $c->disneyresort_hotel_name = null;
        $c->memory_maker = null;
        $c->magical_express = null;
         $c->ship_name =null;
        $c->castaway_member =null;
        $c->bus_transportation = null;
        $c->disney_dining_plan =null;
        $c->promo_applied = null;
        $c->magic_band_color = null;
        $c->good_neighbor_hotel =null;
        
       
            }
             else if ($request->input('destination')=="Carnival Cruise Line")
             {
              

                $c->destination = $request->input('destination');
                 $c->ship_name = $request->input('ship_name1');
                 $c->insurance = $request->input('insurance');
                  $c->Referal = $request->input('Referal');
                   $c->notes = $request->input('notes');

  $c->universal_orlando_resort_hotel = null;
        $c->partners_hotel = null;
        $c->ticket_type = null;
        $c->disneyworld_hotel_name = null;
        $c->disneyresort_hotel_name = null;
        $c->memory_maker = null;
        $c->magical_express = null;
        $c->castaway_member =null;
        $c->bus_transportation = null;
        $c->disney_dining_plan =null;
        $c->promo_applied = null;
        $c->magic_band_color = null;
        $c->good_neighbor_hotel =null;
            }

             else if ($request->input('destination')=="Royal Caribbean Cruise Line")
             {

                $c->destination = $request->input('destination');
                $c->ship_name = $request->input('ship_name');
                $c->stateroom_category_caribbean = $request->input('stateroom_category_caribbean');
                $c->crown_anchor_level = $request->input('crown_anchor_level');
                $c->insurance = $request->input('insurance');
                 $c->Referal = $request->input('Referal');
                $c->notes = $request->input('notes');

  $c->universal_orlando_resort_hotel = null;
        $c->partners_hotel = null;
        $c->ticket_type = null;
        $c->disneyworld_hotel_name = null;
        $c->disneyresort_hotel_name = null;
        $c->memory_maker = null;
        $c->magical_express = null;
        $c->castaway_member =null;
        $c->bus_transportation = null;
        $c->disney_dining_plan =null;
        $c->promo_applied = null;
        $c->magic_band_color = null;
        $c->good_neighbor_hotel =null;
            }
            else
            {
                 $c->Referal = $request->input('Referal');
                  $c->destination = $request->input('destination');
                  $c->insurance = $request->input('insurance');
                   $c->Referal = $request->input('Referal');
                   $c->notes = $request->input('notes');

       
        $c->universal_orlando_resort_hotel = null;
        $c->partners_hotel = null;
        $c->ticket_type = null;
        $c->disneyworld_hotel_name = null;
        $c->disneyresort_hotel_name = null;
        $c->memory_maker = null;
        $c->magical_express = null;
         $c->ship_name =null;
        $c->castaway_member =null;
        $c->bus_transportation = null;
        $c->disney_dining_plan =null;
        $c->promo_applied = null;
        $c->magic_band_color = null;
        $c->good_neighbor_hotel =null;
            }


   

        $c->save();

        return redirect('trip/list');
    }

    public function deleteTrip($id)
    {
        $c = Trip::find($id);

        $todos = TODO::where('trip_id', '=', $c->trip_id)->get();

        if($todos != null)
        {
            foreach ($todos as $todo) 
            {
                $todo->delete();
            }
        }
        
        $c->delete();

        return redirect('trip/list');
    }


// TODOs CRUD
    public function listTodos()
    {
        $user = Auth::user();

        if(Auth::user()->role == 1)
        {
            $todos = TODO::where('isComplete', '=', 1)->orderBy('due_date', 'desc')->get();
           
        $firstname = null;
        $lastname = null;
        $reservationnumber = null;
        $dateoftravel = null;

        return view('admin/todo/list', ['todos'=>$todos, 'user'=>$user, 'firstname' => $firstname, 'lastname' => $lastname, 'reservationnumber' => $reservationnumber, 'dateoftravel' => $dateoftravel]);
        }
        else
        {
            $todos = TODO::where('user_id', '=', $user->id)->where('isComplete', '=', 1)->orderBy('due_date', 'desc')->get();
           
        $firstname = null;
        $lastname = null;
        $reservationnumber = null;
        $dateoftravel = null;

        return view('admin/todo/list', ['todos'=>$todos, 'user'=>$user, 'firstname' => $firstname, 'lastname' => $lastname, 'reservationnumber' => $reservationnumber, 'dateoftravel' => $dateoftravel]);
        }                
    }


    public function addTodo($id)
    {
        $trip = Trip::find($id);
        $customer = Customer::find($trip->customer_id);
        $name = $customer->first_name. ' ' .$customer->last_name;
        $user = Auth::user();

        return view('admin/todo/create', ['user' => $user, 'customer' => $customer, 'name' => $name, 'trip' => $trip]);
    }

    public function saveTodo(Request $request, $id)
    {
        $this->validate($request, [
            'user_id' => 'required',
            'customer_id'=> 'required',
            'trip_id' => 'required',
            'final_payment_due' => 'required',
            'todonotification' => 'required',
        ]);

        $c = Trip::find($id);


        $c->promo_code = $request->input('promo_code');
        $c->final_payment_due = $request->input('final_payment_due');
        $c->advanced_dining_reservations = $request->input('advanced_dining_reservations');
        $c->itinerary_tip_sheets = $request->input('itinerary_tip_sheets');
        $c->magical_express_completed = $request->input('magical_express_completed');
        $c->reminder = $request->input('reminder');
        $c->call_room_requests = $request->input('call_room_requests');
        $c->fast_pass_date = $request->input('fast_pass_date');
        $c->magicalexpresscalender = $request->input('magicalexpresscalender');

        $c->save();

        $todo = new Todo();
        
        // $todo->name = $request->input('name');
        $todo->customer_id = $request->input('customer_id');
        $todo->user_id = $request->input('user_id');
        $todo->trip_id = $request->input('trip_id');
        $todo->is_active = 1;
        $todo->due_date = $request->input('final_payment_due');
        $todo->todonotification = $request->input('todonotification');

        $todo->save();

        return redirect('todo/list');
    }

    public function editTodo($id)
    {
        $destinations = Destination::where('id', '>', 3)->get();
        $todo = Todo::find($id);
        $trip = Trip::find($todo->trip_id);

        $customer = Customer::where('id', '=', $todo->customer_id)->first();
        if($customer == null)
        {
            $name = "Customer has been deleted.";
        } 
        else
        {
            $name = $customer->first_name. ' ' .$customer->last_name;       
        }
        $user = Auth::user();
       
        return view('admin/todo/edit', ['customer_id' => $todo->customer_id, 'todo' => $todo, 'user' => $user, 'customer' => $customer, 'destinations' => $destinations, 'name' => $name, 'trip' => $trip]);
    }

    public function updateTodo(Request $request, $id)
    {
        $this->validate($request, [
            'user_id' => 'required',
            'customer_id'=> 'required',
            'trip_id' => 'required',
            'final_payment_due' => 'required',
            'todonotification' => 'required',
        ]);
        
        $todo = Todo::find($id);

        // $todo->name = $request->input('name');
        $todo->customer_id = $request->input('customer_id');
        $todo->user_id = $request->input('user_id');
        $todo->trip_id = $request->input('trip_id');
        $todo->is_active = 1;
        $todo->due_date = $request->input('final_payment_due');
        $todo->todonotification = $request->input('todonotification');

        $todo->save();

        $c = Trip::find($request->input('trip_id'));

        $c->promo_code = $request->input('promo_code');
        $c->final_payment_due = $request->input('final_payment_due');
        $c->advanced_dining_reservations = $request->input('advanced_dining_reservations');
        $c->itinerary_tip_sheets = $request->input('itinerary_tip_sheets');
        $c->magical_express_completed = $request->input('magical_express_completed');
        $c->reminder = $request->input('reminder');
        $c->call_room_requests = $request->input('call_room_requests');    
        $c->fast_pass_date = $request->input('fast_pass_date');
        $c->magicalexpresscalender = $request->input('magicalexpresscalender');

        $c->save();

        return redirect('todo/list');
    }

    public function deleteTodo($id)
    {
        $c = Todo::find($id);
        $c->delete();
        return redirect('todo/list');
    }



// Destinations CRUD
    public function listDestinations()
    {
        $destinations = Destination::all();
        return view('admin/destination/list', ['destinations'=>$destinations, 'user' => Auth::user() ]);
    }

    public function addDestination()
    {

        $categorynames = categorynames::all();
        return view('admin/destination/create' , ['categorynames'=>$categorynames]);
    }

    public function saveDestination(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'destination_category' => 'required|string|max:255'
        ]);



        $c = new Destination();

        $c->name = $request->input('name');
        $c->destination_category = $request->input('destination_category');

        $c->save();

        return redirect('destination/list');
    }

    public function saveCategory1(Request $request)
    {
        $this->validate($request, [
            'category_name' => 'required|string|max:255',
        ]);

         $this->validate($request, [
            'destination_name' => 'required|string|max:255',
        ]);


        $c = new Category();

        $c->category_name = $request->input('category_name');
        $c->destination_name = $request->input('destination_name');

    


        $c->save();

        return redirect('destination/list');
    }


    public function editDestination($id)
    {
        $destination = Destination::find($id);
        $categorynames = categorynames::all();

        //getting list of hotels or ships
        $hotels = Hotel::where('destination_id', '=', $id)->get()->sortByDesc('id');

        $categories = HotelCategory::where('destination_id', '=', $id)->get()->sortByDesc('id');

        return view('admin/destination/edit', ['destination' => $destination, 'categorynames' => $categorynames]);
    }

    public function updateDestination(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
        ]);

        $c = Destination::find($id);

        $c->name = $request->input('name');
        $c->destination_category = $request->input('destination_category');

        $c->save();

        return redirect('destination/list');
    }

    public function deleteDestination($id)
    {
        $c = Destination::find($id);
        $c->delete();

        return redirect('destination/list');
    }

    //Hotel CRUD
    public function saveHotel(Request $request)
    {
        $this->validate($request, [
            'hotel_name' => 'required',
            'destination_id' => 'required'
        ]);

        $g = new Hotel();
        $category_id = $request->input('category_id');
        $g->category_id = $category_id;
        $destination_id = $request->input('destination_id');
        $g->hotel_name = $request->input('hotel_name');
        $g->destination_id = $destination_id;
     
        //saving hotel
         $g->save();

       $destination = Destination::find($destination_id);

        //getting list of hotels or ships
        $hotels = Hotel::where('destination_id', '=', $destination_id)->get()->sortByDesc('id');

        $categories = HotelCategory::where('destination_id', '=', $destination_id)->get()->sortByDesc('id');

        return view('admin/destination/edit', ['destination' => $destination, 'hotels' => $hotels, 
            'categories' => $categories]);
    }
    
    public function deleteHotel(Request $request)
    {
         $this->validate($request, [
            'id' => 'required'
        ]);

        $id = $request->input('id');

        $g = Hotel::find($id);
        $destination_id = $g->destination_id;
        $g->delete();

        $destination = Destination::find($destination_id);

        //getting list of hotels or ships
        $hotels = Hotel::where('destination_id', '=', $destination_id)->get()->sortByDesc('id');

        $categories = HotelCategory::where('destination_id', '=', $destination_id)->get()->sortByDesc('id');

        return view('admin/destination/edit', ['destination' => $destination, 'hotels' => $hotels, 
            'categories' => $categories]);
    }

    public function editHotel($id){

        $g = Hotel::find($id);

        $destination_id = $g->destination_id;

        $destination = Destination::find($destination_id);

        //getting list of hotels or ships
        $hotels = Hotel::where('destination_id', '=', $destination_id)->get()->sortByDesc('id');

        $categories = HotelCategory::where('destination_id', '=', $destination_id)->get()->sortByDesc('id');

        return view('admin/hotel/edit', ['destination' => $destination, 'hotel' => $g, 
            'categories' => $categories]);
    }

    public function updateHotel(Request $request)
    {
        $this->validate($request, [
            'hotel_name' => 'required',
            'id' => 'required'
        ]);

        $id = $request->input('id');

        $g = Hotel::find($id);
        $destination_id = $g->destination_id;

        $g->hotel_name = $request->input('hotel_name');
        $g->destination_id = $destination_id;
        $category_id = $request->input('category_id');
        $g->category_id = $category_id;

        //saving hotel
         $g->save();

       $destination = Destination::find($destination_id);

        //getting list of hotels or ships
        $hotels = Hotel::where('destination_id', '=', $destination_id)->get()->sortByDesc('id');

        $categories = HotelCategory::where('destination_id', '=', $destination_id)->get()->sortByDesc('id');

        return view('admin/destination/edit', ['destination' => $destination, 'hotels' => $hotels, 
            'categories' => $categories]);
    }


    // Destination Category
          
   public function savedestinationcategory(Request $request)
            {
            $this->validate($request, [
                'destination_category' => 'required|string|max:255'
            ]);

            $c = new categorynames();

            $c->destination_category = $request->input('destination_category');

            $c->save();

            return redirect('destination/add');
        }

     //Destination Category Ends 

    //Category CRUD
    public function saveCategory(Request $request)
    {
        $this->validate($request, [
            'category_name' => 'required',
            'destination_id' => 'required'
        ]);

        $g = new HotelCategory();
        $destination_id = $request->input('destination_id');
        $g->category_name = $request->input('category_name');
        $g->destination_id = $destination_id;
     
        //saving category
         $g->save();

       $destination = Destination::find($destination_id);

        //getting list of hotels or ships
        $hotels = Hotel::where('destination_id', '=', $destination_id)->get()->sortByDesc('id');

        $categories = HotelCategory::where('destination_id', '=', $destination_id)->get()->sortByDesc('id');

        return view('admin/destination/edit', ['destination' => $destination, 'hotels' => $hotels, 
            'categories' => $categories]);
    }
    
    public function deleteCategory(Request $request)
    {
         $this->validate($request, [
            'id' => 'required'
        ]);

        $id = $request->input('id');

        $g = HotelCategory::find($id);
        $destination_id = $g->destination_id;
        $g->delete();

        $destination = Destination::find($destination_id);

        //getting list of hotels or ships
        $hotels = Hotel::where('destination_id', '=', $destination_id)->get()->sortByDesc('id');

        $categories = HotelCategory::where('destination_id', '=', $destination_id)->get()->sortByDesc('id');

        return view('admin/destination/edit', ['destination' => $destination, 'hotels' => $hotels, 
            'categories' => $categories]);
    }

    public function updateCategory(Request $request)
    {
        $this->validate($request, [
            'category_name' => 'required',
            'id' => 'required'
        ]);

        $id = $request->input('id');

        $g = HotelCategory::find($id);
        $destination_id = $g->destination_id;

        $g->category_name = $request->input('category_name');
        $g->destination_id = $destination_id;
        $g->save();
        

        //saving hotel
         $g->save();

       $destination = Destination::find($destination_id);

        //getting list of hotels or ships
        $hotels = Hotel::where('destination_id', '=', $destination_id)->get()->sortByDesc('id');

        $categories = HotelCategory::where('destination_id', '=', $destination_id)->get()->sortByDesc('id');

        return view('admin/destination/edit', ['destination' => $destination, 'hotels' => $hotels, 
            'categories' => $categories
    ]);
    }

   //Todolistings CRUD

   public function savetodolistings(Request $request)
    {
        $user = Auth::user();

        if($user->role == 0)
        {


        $this->validate($request, [
            'tasks' => 'required|string|max:255'
        ]);

        $c = new todolistings();

        $c->tasks = $request->input('tasks');
        $c->user_id = $user->id;
        $c->save();

}
        return redirect('home');
    }

 public function deletetasks($id)
    {

        $c = Todolistings::find($id);
        $c->delete();
        return redirect('/home');
    }


    public function commissionAgents()
    {
        $admins = User::where('role', '=', 1)->orderBy('name')->get();
        $agents = User::where('role', '=', 0)->orderBy('name')->get();
        $user = Auth::user();

        return view("admin/reports/commission-agents", ['user' => $user, 'agents' => $agents, 'admins' => $admins]);
    }


    public function commissionReports(Request $request)
    {
        $id = $request->id;
        $startdate = null;
        $enddate = null;
        $trips = null;
        $commissionTotal = null;
        $expectedCommissionTotal = null;
        
        return view("admin/reports/commission-reports", ['startdate' => $startdate, 'enddate'=> $enddate, 'id' => $id, 'trips' => $trips,
                                                            'commissionTotal' => $commissionTotal, 'expectedCommissionTotal' => $expectedCommissionTotal]);
    }

    public function getcommissionReports(Request $request)
    {
        $startdate = date($request->startdate).' 00:00:00';
        $enddate = date($request->enddate).' 23:59:59';


     $trips = Trip::where('user_id', '=', $request->id)->whereBetween('checkin_date', array($startdate, $enddate))
                        ->where(function ($query) {
                           $query->where('trip_status', '=', "Booked Deposit")->orWhere('trip_status', '=', "Paid in Full")->orWhere('trip_status', '=', "Complete – Commission Paid");
                        })->get();



        $commissionTotal = $trips->sum('commission');
        $expectedCommissionTotal = $trips->sum('expected_commission');
        $TotalSales = $trips->sum('total_sale');
        

                            foreach($trips as $trip)
                        {
                            $sdate=$trip->checkin_date;
                                $sdate = strtotime($sdate);
                                 $sdate = date('m-d-Y',$sdate);

                            $endate=$trip->checkout_date;
                                $endate = strtotime($endate);
                                 $endate = date('m-d-Y',$endate);
                               
                            $bdate=$trip->booking_date;
                                $bdate = strtotime($bdate);
                                 $bdate = date('m-d-Y',$bdate);

                                $trip->checkin_date=$sdate;
                                $trip->checkout_date=$endate;
                                $trip->booking_date=$bdate;

                                 
                        }



        return view("admin/reports/commission-reports", ['TotalSales' => $TotalSales, 'startdate' => $request->startdate, 'enddate'=> $request->enddate, 'id' => $request->id, 'trips' => $trips,
                                                            'commissionTotal' => $commissionTotal, 'expectedCommissionTotal' => $expectedCommissionTotal]);
    }

    public function waltDisneyWorld()
    {

        $startdate = null;
        $enddate = null;
        $trips = null;
        $commissionTotal = null;
        $expectedCommissionTotal = null;


    return view("admin/reports/waltDisneyWorld", ['startdate' => $startdate, 'enddate'=> $enddate, 'trips' => $trips,
                                                            'commissionTotal' => $commissionTotal, 'expectedCommissionTotal' => $expectedCommissionTotal]);
    }

    public function getwaltDisneyWorldcommissions(Request $request)
    {
        $startdate = date( $request->startdate).' 00:00:00';
        $enddate = date($request->enddate).' 23:59:59';

        $trips = Trip::where('destination', '=', 'Walt Disney World')->whereBetween('checkin_date', array($startdate, $enddate))
                        ->where(function ($query) {
                           $query->where('trip_status', '=', "Booked Deposit")->orWhere('trip_status', '=', "Paid in Full")->orWhere('trip_status', '=', "Complete – Commission Paid");
                        })->get();
        $commissionTotal = $trips->sum('commission');
        $expectedCommissionTotal = $trips->sum('expected_commission');
        $TotalSales = $trips->sum('total_sale');


                                    for($i=0; $i<$trips->count();$i++) 
                        {
                            $sdate=$trips[$i]->checkin_date;
                                $sdate = strtotime($sdate);
                                 $sdate = date('m-d-Y',$sdate);

                            $endate=$trips[$i]->checkout_date;
                                $endate = strtotime($endate);
                                 $endate = date('m-d-Y',$endate);
                               
                            $bdate=$trips[$i]->booking_date;
                                $bdate = strtotime($bdate);
                                 $bdate = date('m-d-Y',$bdate);

                                $trips[$i]->checkin_date=$sdate;
                                $trips[$i]->checkout_date=$endate;
                                $trips[$i]->booking_date=$bdate;

                                 
                        }


        return view("admin/reports/waltDisneyWorld", ['TotalSales' => $TotalSales, 'startdate' => $request->startdate, 'enddate'=> $request->enddate, 'trips' => $trips,
                                                            'commissionTotal' => $commissionTotal, 'expectedCommissionTotal' => $expectedCommissionTotal]);
    }


     public function disneylandResort()
     {

        $startdate = null;
        $enddate = null;
        $trips = null;
        $commissionTotal = null;
        $expectedCommissionTotal = null;


    return view("admin/reports/disneylandResort", ['startdate' => $startdate, 'enddate'=> $enddate, 'trips' => $trips,
                                                            'commissionTotal' => $commissionTotal, 'expectedCommissionTotal' => $expectedCommissionTotal]);
     }

    public function getdisneylandResortcommissions(Request $request)
    {

        $startdate = date( $request->startdate).' 00:00:00';
        $enddate = date($request->enddate).' 23:59:59';

        $trips = Trip::where('destination', '=', 'Disneyland Resort')->whereBetween('checkin_date', array($startdate, $enddate))
                        ->where(function ($query) {
                           $query->where('trip_status', '=', "Booked Deposit")->orWhere('trip_status', '=', "Paid in Full")->orWhere('trip_status', '=', "Complete – Commission Paid");
                        })->get();
        $commissionTotal = $trips->sum('commission');
        $expectedCommissionTotal = $trips->sum('expected_commission');
        $TotalSales = $trips->sum('total_sale');

                        for($i=0; $i<$trips->count();$i++) 
                        {
                            $sdate=$trips[$i]->checkin_date;
                                $sdate = strtotime($sdate);
                                 $sdate = date('m-d-Y',$sdate);

                            $endate=$trips[$i]->checkout_date;
                                $endate = strtotime($endate);
                                 $endate = date('m-d-Y',$endate);
                               
                            $bdate=$trips[$i]->booking_date;
                                $bdate = strtotime($bdate);
                                 $bdate = date('m-d-Y',$bdate);

                                $trips[$i]->checkin_date=$sdate;
                                $trips[$i]->checkout_date=$endate;
                                $trips[$i]->booking_date=$bdate;

                                 
                        }


        return view("admin/reports/disneylandResort", ['TotalSales' => $TotalSales, 'startdate' => $request->startdate, 'enddate'=> $request->enddate, 'trips' => $trips,
                                                            'commissionTotal' => $commissionTotal, 'expectedCommissionTotal' => $expectedCommissionTotal]);
    }


    public function disneyCruiseLine()
    {

        $startdate = null;
        $enddate = null;
        $trips = null;
        $commissionTotal = null;
        $expectedCommissionTotal = null;


    return view("admin/reports/disneyCruiseLine", ['startdate' => $startdate, 'enddate'=> $enddate, 'trips' => $trips,
                                                            'commissionTotal' => $commissionTotal, 'expectedCommissionTotal' => $expectedCommissionTotal]);
    }

    public function getdisneyCruiseLinecommissions(Request $request)
    {
        $startdate = date( $request->startdate).' 00:00:00';
        $enddate = date($request->enddate).' 23:59:59';

        $trips = Trip::where('destination', '=', 'Disney Cruise Line')->whereBetween('checkin_date', array($startdate, $enddate))
                        ->where(function ($query) {
                           $query->where('trip_status', '=', "Booked Deposit")->orWhere('trip_status', '=', "Paid in Full")->orWhere('trip_status', '=', "Complete – Commission Paid");
                        })->get();
        
        $commissionTotal = $trips->sum('commission');
        $expectedCommissionTotal = $trips->sum('expected_commission');
        $TotalSales = $trips->sum('total_sale');


                         for($i=0; $i<$trips->count();$i++) 
                        {
                            $sdate=$trips[$i]->checkin_date;
                                $sdate = strtotime($sdate);
                                 $sdate = date('m-d-Y',$sdate);

                            $endate=$trips[$i]->checkout_date;
                                $endate = strtotime($endate);
                                 $endate = date('m-d-Y',$endate);
                               
                            $bdate=$trips[$i]->booking_date;
                                $bdate = strtotime($bdate);
                                 $bdate = date('m-d-Y',$bdate);

                                $trips[$i]->checkin_date=$sdate;
                                $trips[$i]->checkout_date=$endate;
                                $trips[$i]->booking_date=$bdate;

                                 
                        }


        return view("admin/reports/disneyCruiseLine", ['TotalSales' => $TotalSales, 'startdate' => $request->startdate, 'enddate'=> $request->enddate, 'trips' => $trips,
                                                            'commissionTotal' => $commissionTotal, 'expectedCommissionTotal' => $expectedCommissionTotal]);
    }


    public function commissionDestinations()
    {
        $destinations = Destination::where('id', '>', 3)->orderBy('name')->get();

        return view("admin/reports/commissiondestinations", ['destinations' => $destinations]);
    }


    public function commissionDestinationReports(Request $request)
    {
        $id = $request->id;

        $startdate = null;
        $enddate = null;
        $trips = null;
        $commissionTotal = null;
        $expectedCommissionTotal = null;
        
        return view("admin/reports/getcommissionDestinationReports", ['startdate' => $startdate, 'enddate'=> $enddate, 'id' => $id, 'trips' => $trips,
                                                            'commissionTotal' => $commissionTotal, 'expectedCommissionTotal' => $expectedCommissionTotal]);
    }

    public function getcommissionDestinationReports(Request $request)
    {
        //dd($request->startdate);  exit;
        $destination = Destination::find($request->id)->name;
        $startdate = date( $request->startdate).' 00:00:00';
        $enddate = date($request->enddate).' 23:59:59';

        $trips = Trip::where('destination', '=', $destination)->whereBetween('checkin_date', array($startdate, $enddate))
                        ->where(function ($query) {
                           $query->where('trip_status', '=', "Booked Deposit")->orWhere('trip_status', '=', "Paid in Full")->orWhere('trip_status', '=', "Complete – Commission Paid");
                        })->get();

                        for($i=0; $i<$trips->count();$i++) 
                        {
                            $sdate=$trips[$i]->checkin_date;
                                $sdate = strtotime($sdate);
                                 $sdate = date('m-d-Y',$sdate);

                            $endate=$trips[$i]->checkout_date;
                                $endate = strtotime($endate);
                                 $endate = date('m-d-Y',$endate);
                               
                            $bdate=$trips[$i]->booking_date;
                                $bdate = strtotime($bdate);
                                 $bdate = date('m-d-Y',$bdate);

                                $trips[$i]->checkin_date=$sdate;
                                $trips[$i]->checkout_date=$endate;
                                $trips[$i]->booking_date=$bdate;

                                 
                        }


        $commissionTotal = $trips->sum('commission');
        $expectedCommissionTotal = $trips->sum('expected_commission');
        $TotalSales = $trips->sum('total_sale');
        
        return view("admin/reports/getcommissionDestinationReports", ['TotalSales' => $TotalSales, 'startdate' => $request->startdate, 'enddate'=> $request->enddate, 'id' => $request->id,
         'trips' => $trips, 'commissionTotal' => $commissionTotal, 'expectedCommissionTotal' => $expectedCommissionTotal]);
    }


// Dining Plans CRUD
    public function listdiningplans()
    {
        $diningplans = DB::table('dining_plans')->orderBy('year', 'asc')->get();
        
        return view("admin/calculator/listdiningplans", ['diningplans' => $diningplans]);
    }

    public function adddiningplan()
    {
        $message = "";
        return view("admin/calculator/creatediningplans", ['message' => $message]);
    }

    public function savediningplan(Request $request)
    {
        $this->validate($request, [
            'year' => 'required',
            'name' => 'required',
            'adultprice' => 'required',
            'childprice' => 'required',
        ]);             
 

        $alldiningplans = DiningPlan::all();

        foreach ($alldiningplans as $dp) 
        {            
            if($dp->name == $request->name && $dp->year == $request->year)
            {
                $message = "Dining Plan for entered year already exists!";
                return view("admin/calculator/creatediningplans", ['message' => $message]);
            }
        }

        $c = new DiningPlan();

        $c->name = $request->input('name');
        $c->year = $request->input('year');

        if(is_numeric($request->adultprice) == true || is_float($request->adultprice) == true)
        {
            $c->adultprice = $request->input('adultprice');
        }
        else
        {
                $message = "Price for the adult is not a valid number!";
                return view("admin/calculator/creatediningplans", ['message' => $message]);
        }

        if(is_numeric($request->childprice) == true || is_float($request->childprice) == true)
        {
            $c->childprice = $request->input('childprice');
        } 
        else
        {
                $message = "Price for the child is not a valid number!";
                return view("admin/calculator/creatediningplans", ['message' => $message]);
        }          
        

        $c->save();

        return redirect('diningplan/list');
    }

    public function editdiningplan($id)
    {
        $diningplan = DiningPlan::find($id);
        $years = DB::table('calculator_years')->orderBy('year', 'asc')->get();
        
        return view("admin/calculator/editdiningplan", ['diningplan' => $diningplan, 'years' => $years]);
    }

    public function updatediningplan(Request $request, $id)
    {
        $this->validate($request, [
            'year' => 'required',
            'name' => 'required',
            'adultprice' => 'required',
            'childprice' => 'required',
        ]);
        
        $c = DiningPlan::find($id);

        $c->name = $request->input('name');
        $c->year = $request->input('year');
        $c->adultprice = $request->input('adultprice');
        $c->childprice = $request->input('childprice');

        $c->save();

        return redirect('diningplan/list');
    }

    public function deletediningplan($id)
    {
        $c = DiningPlan::find($id);
        $c->delete();
        return redirect('diningplan/list');
    }

    public function diningplancalculator()
    {
        $years = CalculatorYear::all(); 
        $diningplans = DB::table('dining_plans')->select('year')->groupBy('year')->get();


        $noofadults = null;
        $noofchildren = null;
        $noofnights = null;
        $selectedyear = null;

        $quickserviceadultperday =  null;
        $quickservicechildperday =  null;
        $quickservicetotalperday =  null;
        $quickserviceadulttotal =  null;
        $quickservicechildtotal =  null;
        $quickservicetotal =  null;


        $disneydiningadultperday = null;
        $disneydiningchildperday =  null;
        $disneydiningtotalperday =  null;
        $disneydiningadulttotal =  null;
        $disneydiningchildtotal =  null;
        $disneydiningtotal =  null;

        $deluxediningadultperday =  null;
        $deluxediningchildperday =  null;
        $deluxediningtotalperday =  null;
        $deluxediningadulttotal =  null;
        $deluxediningchildtotal =  null;
        $deluxediningtotal =  null;
        

        return view("admin/calculator/diningplancalculator", ['selectedyear' => $selectedyear, 'noofadults' => $noofadults, 'noofchildren'=> $noofchildren, 'noofnights' => $noofnights, 'years' => $years,
            'quickserviceadultperday' => $quickserviceadultperday, 'quickservicechildperday' => $quickservicechildperday, 'quickservicetotalperday' => $quickservicetotalperday,
            'disneydiningadultperday' => $disneydiningadultperday, 'disneydiningchildperday' => $disneydiningchildperday, 'disneydiningtotalperday' => $disneydiningtotalperday,
            'deluxediningadultperday' => $deluxediningadultperday, 'deluxediningchildperday' => $deluxediningchildperday, 'deluxediningtotalperday' => $deluxediningtotalperday,
            'quickserviceadulttotal' => $quickserviceadulttotal, 'quickservicechildtotal' => $quickservicechildtotal, 'quickservicetotal' => $quickservicetotal,
            'disneydiningadulttotal' => $disneydiningadulttotal, 'disneydiningchildtotal' => $disneydiningchildtotal, 'disneydiningtotal' => $disneydiningtotal,
            'deluxediningadulttotal' => $deluxediningadulttotal, 'deluxediningchildtotal' => $deluxediningchildtotal, 'deluxediningtotal' => $deluxediningtotal]);
    }

    public function postdiningplancalculator(Request $request)
    {

        $years = CalculatorYear::all(); 
        $diningplans = DB::table('dining_plans')->select('year')->groupBy('year')->get();



        $selectedyear = $request->year;
        $noofadults = floatval($request->noofadults);
        $noofchildren = floatval($request->noofchildren);
        $noofnights = floatval($request->noofnights);
        $year = floatval($request->year);

        $quickservice = DiningPlan::where('name', '=', 'Quick Service Dining')->where('year', '=', $year)->first();
        $disneydining = DiningPlan::where('name', '=', 'Disney Dining Plan')->where('year', '=', $year)->first();
        $deluxedining = DiningPlan::where('name', '=', 'Deluxe Dining Plan')->where('year', '=', $year)->first();
        

        $quickserviceadultperday =  $noofadults * (floatval($quickservice->adultprice));
        $quickservicechildperday =  $noofchildren * (floatval($quickservice->childprice));
        $quickservicetotalperday =  $quickserviceadultperday + $quickservicechildperday;
        $quickserviceadulttotal =  $noofnights * $noofadults * (floatval($quickservice->adultprice));
        $quickservicechildtotal =  $noofnights * $noofchildren * (floatval($quickservice->childprice));
        $quickservicetotal =  $quickserviceadulttotal + $quickservicechildtotal;


        $disneydiningadultperday =  $noofadults * (floatval($disneydining->adultprice));
        $disneydiningchildperday =  $noofchildren * (floatval($disneydining->childprice));
        $disneydiningtotalperday =  $disneydiningadultperday + $disneydiningchildperday;
        $disneydiningadulttotal =  $noofnights * $noofadults * (floatval($disneydining->adultprice));
        $disneydiningchildtotal =  $noofnights * $noofchildren * (floatval($disneydining->childprice));
        $disneydiningtotal =  $disneydiningadulttotal + $disneydiningchildtotal;

        $deluxediningadultperday =  $noofadults * (floatval($deluxedining->adultprice));
        $deluxediningchildperday =  $noofchildren * (floatval($deluxedining->childprice));
        $deluxediningtotalperday =  $deluxediningadultperday + $deluxediningchildperday;
        $deluxediningadulttotal =  $noofnights * $noofadults * (floatval($deluxedining->adultprice));
        $deluxediningchildtotal =  $noofnights * $noofchildren * (floatval($deluxedining->childprice));
        $deluxediningtotal =  $deluxediningadulttotal + $deluxediningchildtotal;
        

        return view("admin/calculator/diningplancalculator", ['years' => $years, 'selectedyear' => $selectedyear, 'noofadults' => $noofadults, 'noofchildren'=> $noofchildren, 'noofnights' => $noofnights,
            'quickserviceadultperday' => $quickserviceadultperday, 'quickservicechildperday' => $quickservicechildperday, 'quickservicetotalperday' => $quickservicetotalperday,
            'disneydiningadultperday' => $disneydiningadultperday, 'disneydiningchildperday' => $disneydiningchildperday, 'disneydiningtotalperday' => $disneydiningtotalperday,
            'deluxediningadultperday' => $deluxediningadultperday, 'deluxediningchildperday' => $deluxediningchildperday, 'deluxediningtotalperday' => $deluxediningtotalperday,
            'quickserviceadulttotal' => $quickserviceadulttotal, 'quickservicechildtotal' => $quickservicechildtotal, 'quickservicetotal' => $quickservicetotal,
            'disneydiningadulttotal' => $disneydiningadulttotal, 'disneydiningchildtotal' => $disneydiningchildtotal, 'disneydiningtotal' => $disneydiningtotal,
            'deluxediningadulttotal' => $deluxediningadulttotal, 'deluxediningchildtotal' => $deluxediningchildtotal, 'deluxediningtotal' => $deluxediningtotal]);
    }


    public function customSearch(Request $request)
    {

        if($request->firstname == null && $request->lastname == null && $request->reservationnumber == null && $request->dateoftravel == null) 
            {
                    return redirect('trip/list');
            }
        else
            {

            $user = Auth::user();

            $trips = Trip::all();


            if($request->firstname != null)
            {
                $trips = $trips->where('customerfirstname', strtoupper($request->firstname));
            }

            if($request->lastname != null)
            {
                $trips = $trips->where('customerlastname', strtoupper($request->lastname));
            }

            if($request->reservationnumber != null)
            {
                $trips = $trips->where('reservation_number', $request->reservationnumber);                
            }

            if($request->dateoftravel != null)
            {
                $trips = $trips->where('checkin_date', $request->dateoftravel);
            }


            return view('admin/trip/list', ['trips'=>$trips, 'user'=>$user, 'firstname' => $request->firstname, 'lastname' => $request->lastname,'reservationnumber' => $request->reservationnumber, 'dateoftravel' => $request->dateoftravel]);
        }
    }

public function ImportantDateCalculator()
{
   
    return view("admin/calculator/importantdatecalculator");
}

public function addCalculatorYear()
{
    $calculatoryears = CalculatorYear::all();
    return view("admin/calculator/calculatoryear", ['calculatoryears' => $calculatoryears]);
}


public function postCalculatorYear(Request $request)
    {
        $this->validate($request, [
            'year' => 'required',
        ]);

        $c = new CalculatorYear();

        $c->year = $request->input('year');

        $c->save();

        return redirect('calculator-year');
    }


public function deleteCalculatorYear($id)
    {
        $c = CalculatorYear::find($id);
        $c->delete();

        return redirect('calculator-year');
    }



//trips check in (date travelled commission report)

    public function getTripCheckInReport()
    {

        $startdate = null;
        $enddate = null;
        $trips = null;
        $selectedagentid = null;
        $agents = User::all();

    return view("admin/reports/tripCheckInReport", ['startdate' => $startdate, 'enddate'=> $enddate, 'trips' => $trips, 'agents' => $agents, 'selectedagentid' => $selectedagentid]);
    }

    public function postTripCheckInReport(Request $request)
    {
        $startdate = date( $request->startdate).' 00:00:00';
        $enddate = date($request->enddate).' 23:59:59';
        $agents = User::all();
        $id = $request->input('role');

      //  dd($id); exit;
        $selectedagentid = $id;
        $trips = new Collection();

        if (Auth::User()->role == 1) {
            if($id == "All")
            {
                $checkintrips = Trip::whereBetween('checkin_date', array($startdate, $enddate))
                        ->where(function ($query) {
                           $query->where('trip_status', '=', "Booked Deposit")->orWhere('trip_status', '=', "Paid in Full")->orWhere('trip_status', '=', "Complete – Commission Paid");
                        })->get();
                        

                $checkouttrips = Trip::whereBetween('checkout_date', array($startdate, $enddate))
                        ->where(function ($query) {
                           $query->where('trip_status', '=', "Booked Deposit")->orWhere('trip_status', '=', "Paid in Full")->orWhere('trip_status', '=', "Complete – Commission Paid");
                        })->get();     

                foreach ($checkintrips as $checkintrip) {
                              $trips->push($checkintrip);
                          }

                foreach ($checkouttrips as $checkouttrip) {
                            $trips->push($checkouttrip);                
                }

                $trips = $trips->unique();
                
            }
            else
            {

               $checkintrips = Trip::where('user_id', '=', $id)->whereBetween('checkin_date', array($startdate, $enddate))
                        ->where(function ($query) {
                           $query->where('trip_status', '=', "Booked Deposit")->orWhere('trip_status', '=', "Paid in Full")->orWhere('trip_status', '=', "Complete – Commission Paid");
                        })->get();
                        

                $checkouttrips = Trip::where('user_id', '=', $id)->whereBetween('checkout_date', array($startdate, $enddate))
                        ->where(function ($query) {
                           $query->where('trip_status', '=', "Booked Deposit")->orWhere('trip_status', '=', "Paid in Full")->orWhere('trip_status', '=', "Complete – Commission Paid");
                        })->get();     

                foreach ($checkintrips as $checkintrip) {
                              $trips->push($checkintrip);
                          }

                foreach ($checkouttrips as $checkouttrip) {
                            $trips->push($checkouttrip);

                        }                              
                 $trips = $trips->unique();   
            }
        }
    
        else
        {
            $checkintrips = Trip::where('user_id', '=', Auth::User()->id)->whereBetween('checkin_date', array($startdate, $enddate))
                        ->where(function ($query) {
                           $query->where('trip_status', '=', "Booked Deposit")->orWhere('trip_status', '=', "Paid in Full")->orWhere('trip_status', '=', "Complete – Commission Paid");
                        })->get();

            $checkouttrips = Trip::where('user_id', '=', Auth::User()->id)->whereBetween('checkout_date', array($startdate, $enddate))
                        ->where(function ($query) {
                           $query->where('trip_status', '=', "Booked Deposit")->orWhere('trip_status', '=', "Paid in Full")->orWhere('trip_status', '=', "Complete – Commission Paid");
                        })->get();

                 
            foreach ($checkintrips as $checkintrip) {
                $trips->push($checkintrip);
            }

            foreach ($checkouttrips as $checkouttrip) {
                $trips->push($checkouttrip);
            }                              
            $trips = $trips->unique(); 
        }
        

        foreach($trips as $tripss)
        {
            $sdate=$tripss->checkin_date;
            $sdate = strtotime($sdate);
            $sdate = date('m-d-Y',$sdate);

            $endate=$tripss->checkout_date;
            $endate = strtotime($endate);
            $endate = date('m-d-Y',$endate);

            $bdate=$tripss->booking_date;
            $bdate = strtotime($bdate);
            $bdate = date('m-d-Y',$bdate);

            $tripss->checkin_date=$sdate;
            $tripss->checkout_date=$endate;
            $tripss->booking_date=$bdate;

                                 
        }

        $getSave = "Null";
        $data = "Null";

        return view("admin/reports/tripCheckInReport", ['startdate' => $request->startdate, 'enddate'=> $request->enddate, 'trips' => $trips, 'agents' => $agents, 'selectedagentid' => $selectedagentid, 'getSave' => $getSave, 'getSaveReportId' => $data ]);
    }  

    public function editProfile()
    {
        $user = User::Find(Auth::user()->id);

        if($user->user_id != null)
        {
            $agentmentor = User::Find($user->user_id);
        }
        else
        {
            $agentmentor = null;
        }


        return view("admin/user/editProfile",['user'=>$user, 'agentmentor'=>$agentmentor ]);
    }

    public function PEditProfile(Request $request)
    {
        $c = User::find(Auth::user()->id);

        $c->name = $request->input('name');
        $c->email = $request->input('email');
        $c->phone = $request->input('phone');
        $c->address = $request->input('address');

        if(($request->input('password') != null && $request->input('password') != "") && ($request->input('password_confirmation') != null && $request->input('password_confirmation') != ""))
        {
            if($request->input('password') == $request->input('password_confirmation'))
            {
                 $c->password = $request->input('password');
            }
        }
       
        
        $c->save();

        return redirect('/user/editProfile');
    }

    public function changePassword()
    {
        return view("admin/user/changePassword");
    }

    public function PChangePassword(Request $request)
    {

        $oldPassword = $request->input('oldPassword');
        $newPassword = $request->input('newPassword');
        $confirmPassword = $request->input('confirmPassword');

        if($newPassword==$confirmPassword)
        {
            if(Hash::check($oldPassword, Auth::user()->password))
            {
                $c=User::Find(Auth::user()->id);
                $c->password=bcrypt($newPassword);
                $c->save();
                return redirect()->back()->with('success','Password Successfully Changed');
            }
            else
            {
                 return redirect()->back()->with('error','Password didnt matched');   
            }
        }
        else
        {
            return redirect()->back()->with('error','New Password and Old Password didnt matched');
        }
    }


    public function getAgencyTotalRevenue()
    {
        $object=null;
        return view("admin/reports/agencyRevenueReport",['object'=>$object]);
    }

    public function postAgencyTotalRevenue(Request $request)
    {
        $startdate = date($request->startDate).' 00:00:00';
        $enddate = date($request->endDate).' 23:59:59';
        

        $object=array('destination','q1','q2','q3','q4','totalYTD','previousYearTotal','allTotal');

        $dest = Destination::all();
        $x=0;
        foreach($dest as $d)
        {
            $a = Carbon::createFromFormat('Y-m-d H:i:s', $enddate)->year;
            $Q1 = Trip::where('destination','=',$d->name)->whereBetween('booking_date', array($a.'-01-01 00:00:00', $a.'-03-30 23:59:59'))->sum('total_sale');
            $Q2 = Trip::where('destination','=',$d->name)->whereBetween('booking_date', array($a.'-04-01 00:00:00', $a.'-06-30 23:59:59'))->sum('total_sale');
            $Q3 = Trip::where('destination','=',$d->name)->whereBetween('booking_date', array($a.'-07-01 00:00:00', $a.'-09-30 23:59:59'))->sum('total_sale');
            $Q4 = Trip::where('destination','=',$d->name)->whereBetween('booking_date', array($a.'-10-01 00:00:00', $a.'-12-31 23:59:59'))->sum('total_sale');
            $a=$a-1;
            $previousYearTotal = Trip::where('destination','=',$d->name)->whereBetween('booking_date', array($a.'-10-01 00:00:00', $a.'-12-31 23:59:59'))->sum('total_sale');
            $allTotal = Trip::where('destination','=',$d->name)->whereBetween('booking_date', array($startdate, $enddate))->sum('commission');
            $totalYTD = $Q1+$Q2+$Q3+$Q4;
            $object[$x]=array('destination'=>$d->name,'q1'=>$Q1,'q2'=>$Q2,'q3'=>$Q3,'q4'=>$Q4,'totalYTD'=>$totalYTD,'previousYearTotal'=>$previousYearTotal,'allTotal'=>$allTotal);
            $x++;
        }
        return view("admin/reports/agencyRevenueReport",['object'=>$object]);
    }

    public function listAuditLogs()
    {
        //$auditlog = AuditLogs::all()->toArray();
        $auditlog = AuditLogs::all()->sortByDesc('created_at');
        return View('admin/auditlogs/list', compact('auditlog'), ['AuditLogs'=>$auditlog]);

        // $auditlog = AuditLogs::all()->where('change_date', '>', Carbon::now())->orderBy('change_date', 'desc')->get();
        // $auditlog = $auditlog->reverse();
        // return View('admin/auditlogs/list', compact('auditlog'), ['AuditLogs'=>$auditlog]);

    }

    public function delete($id)
    {
        $findauditlog = AuditLogs::find($id);
         // print_r($Id);
         // exit();
         $findauditlog = AuditLogs::where('id' , $findauditlog->id)->delete();
         return redirect('auditlogs/list');
    }

    // public function abc(){
    //     //return View('admin/auditlogs/abc');
    //     return view('admin/calenders/list');
    //     //return View('admin/auditlogs/abc');
    // }

    // public function getAjaxTrips(){
    //     $user = Auth::user();

    //     if($user->role == 1)
    //     {

    //         $Alltrips = Trip::where('Status', '=', 0)->get()->sortByDesc('id');
    //     }
    //     else
    //     {
    //         $Alltrips = Trip::where([
    //             ['user_id', '=', $user->id],
    //             ['Status', '=', 0],
    //         ])->get()->sortByDesc('id');
    //     }

    //     $todoTrips = new Collection();


    //   //  $obj = new TripsDTO();
    //     $list = new Collection();
    //     $i = 0;

    //     $data = [];
    //     $titles = "";
    //         foreach ($Alltrips as $trip)
    //         {

    //             $data[] = [
    //                 'start' => $trip->checkin_date,
    //                 'end' => $trip->checkout_date,
    //                 'title' => $trip->customer['first_name'].' '.$trip->customer['last_name']. "\n". $trip->destination. "\n" .$trip->reservation_number,
    //                 'color' => "#A8D2E0",
    //             ];
    //         }

    //     return Response::json($data);
    // }

    public function getcalenders(Request $request)
    {
        $dd = $request->input('userId');
        // if($dd!="" && $dd!=null){
        //       print_r($dd);
        // exit();
        // }
      
        $user = Auth::user();

        if($user->role == 1)
        {
            // $Alltrips = Trip::all()->sortByDesc('id');
            $Alltrips = Trip::where('Status', '=', 0)->get()->sortByDesc('id');
        }
        else
        {
            $Alltrips = Trip::where([
                ['user_id', '=', $user->id],
                ['Status', '=', 0],
            ])->get()->sortByDesc('id');
        }

        $todoTrips = new Collection();

        foreach($Alltrips as $trip){

            if($trip->todo == null){
                $todoTrips->push($trip);
            }

        }

        //filtering without todo trips
        $notTodoTrips = new Collection();

        foreach($Alltrips as $trip){

            if($trip->todo != null){
                $notTodoTrips->push($trip);
            }

        }


        //$use = DB::table('user')->lists('name');
        //$use = user::all();
        $use = user::all('id','name');
        $firstname = null;
        $lastname = null;
        $reservationnumber = null;
        $dateoftravel = null;
        $fastpastdate = null;
        $advanced_dining_reservations = null;
        $FinalPaymentDate = null;
        //$Alltrips
        
        return view('admin/calenders/list', ['trips'=>$Alltrips, 'trips'=>$Alltrips, 'todoTrips'=>$todoTrips, 'user'=>$user,   'use'=>$use, 'firstname' => $firstname, 'lastname' => $lastname, 'reservationnumber' => $reservationnumber, 'dateoftravel' => $dateoftravel, 'fast_pass_date' => $fastpastdate, 'advanced_dining_reservations' => $advanced_dining_reservations, 'final_payment_due' => $FinalPaymentDate, 'agent_id'=>$dd]);
        // return view();

        //return View('admin/calenders/list');
    }

    public function savereports(Request $request)
    {

         $agentId= $request->Id;
         return response()->json($agentId);
        // $objcheckindate = $request->objsdate;
        // $objcheckoutdate = $request->objedate;

      //   $startdate = date( $request->startdate).' 00:00:00';
      //   $enddate = date($request->enddate).' 23:59:59';
      //   $agents = User::all();
      //   $id = $request->input('role');

      // //  dd($id); exit;
      //   $selectedagentid = $id;
      //   $trips = new Collection();

      //   if (Auth::User()->role == 1) {
      //       if($id == "All")
      //       {
      //           $checkintrips = Trip::whereBetween('checkin_date', array($startdate, $enddate))
      //                   ->where(function ($query) {
      //                      $query->where('trip_status', '=', "Booked Deposit")->orWhere('trip_status', '=', "Paid in Full")->orWhere('trip_status', '=', "Complete – Commission Paid");
      //                   })->get();
                        

      //           $checkouttrips = Trip::whereBetween('checkout_date', array($startdate, $enddate))
      //                   ->where(function ($query) {
      //                      $query->where('trip_status', '=', "Booked Deposit")->orWhere('trip_status', '=', "Paid in Full")->orWhere('trip_status', '=', "Complete – Commission Paid");
      //                   })->get();     

      //           foreach ($checkintrips as $checkintrip) {
      //                         $trips->push($checkintrip);
      //                     }

      //           foreach ($checkouttrips as $checkouttrip) {
      //                       $trips->push($checkouttrip);                
      //           }

      //           $trips = $trips->unique();

      //           print_r($trips);
      //           exit();
                
      //       }
      //       else
      //       {

      //          $checkintrips = Trip::where('user_id', '=', $id)->whereBetween('checkin_date', array($startdate, $enddate))
      //                   ->where(function ($query) {
      //                      $query->where('trip_status', '=', "Booked Deposit")->orWhere('trip_status', '=', "Paid in Full")->orWhere('trip_status', '=', "Complete – Commission Paid");
      //                   })->get();
                        

      //           $checkouttrips = Trip::where('user_id', '=', $id)->whereBetween('checkout_date', array($startdate, $enddate))
      //                   ->where(function ($query) {
      //                      $query->where('trip_status', '=', "Booked Deposit")->orWhere('trip_status', '=', "Paid in Full")->orWhere('trip_status', '=', "Complete – Commission Paid");
      //                   })->get();     

      //           foreach ($checkintrips as $checkintrip) {
      //                         $trips->push($checkintrip);
      //                     }

      //           foreach ($checkouttrips as $checkouttrip) {
      //                       $trips->push($checkouttrip);

      //                   }                              
      //            $trips = $trips->unique();   
      //       }
      //   }

        

        //$data = $agentId + " " + $objcheckindate + " " + $objcheckoutdate;
        //return response()->json($data);

        // foreach($trips as $tripss)
        // {
        //     $sdate=$tripss->checkin_date;
        //     $sdate = strtotime($sdate);
        //     $sdate = date('m-d-Y',$sdate);

        //     $endate=$tripss->checkout_date;
        //     $endate = strtotime($endate);
        //     $endate = date('m-d-Y',$endate);

        //     $bdate=$tripss->booking_date;
        //     $bdate = strtotime($bdate);
        //     $bdate = date('m-d-Y',$bdate);

        //     $tripss->checkin_date=$sdate;
        //     $tripss->checkout_date=$endate;
        //     $tripss->booking_date=$bdate;

                                 
        // }


       //  $getdate = Carbon::now();
       //  $monthname = date('F', strtotime($getdate));
        
       //  $saveReport = new SaveReport();

       //  $saveReport->agentId = $id;
       //  $saveReport->reportcreation = $getdate;
       //  $saveReport->isActive = 1;
       //  $saveReport->paymentstatus = null;
       //  $saveReport->travelmonth = $monthname;
       // // $saveReport->checkin_date = $trips->checkin_date;
       // // $saveReport->checkout_date = $trips->checkout_date;

       //  $saveReport->save();
       //  $data = $saveReport->savereportid;


       //  return view("admin/reports/tripCheckInReport", ['startdate' => $request->startdate, 'enddate'=> $request->enddate, 'trips' => $trips, 'agents' => $agents, 'selectedagentid' => $selectedagentid, 'lastsavedata' => $data]);

    }

    public function postTripsSaveReport(Request $request)
    {
        $startdate = date( $request->saveReportStart).' 00:00:00';
        $enddate = date($request->saveReportEnd).' 23:59:59';
        $agents = User::all();
        $id = $request->input('saveReportAgent');

        //dd($id); exit;
        $selectedagentid = $id;
        $trips = new Collection();

        if (Auth::User()->role == 1) {
            if($id == "All")
            {
                $checkintrips = Trip::whereBetween('checkin_date', array($startdate, $enddate))
                        ->where(function ($query) {
                           $query->where('trip_status', '=', "Booked Deposit")->orWhere('trip_status', '=', "Paid in Full")->orWhere('trip_status', '=', "Complete – Commission Paid");
                        })->get();
                        

                $checkouttrips = Trip::whereBetween('checkout_date', array($startdate, $enddate))
                        ->where(function ($query) {
                           $query->where('trip_status', '=', "Booked Deposit")->orWhere('trip_status', '=', "Paid in Full")->orWhere('trip_status', '=', "Complete – Commission Paid");
                        })->get();     

                foreach ($checkintrips as $checkintrip) {
                              $trips->push($checkintrip);
                          }

                foreach ($checkouttrips as $checkouttrip) {
                            $trips->push($checkouttrip);                
                }

                $trips = $trips->unique();
                
            }
            else
            {
                // print_r($id);
                // exit();
               $checkintrips = Trip::where('user_id', '=', $id)->whereBetween('checkin_date', array($startdate, $enddate))
                        ->where(function ($query) {
                           $query->where('trip_status', '=', "Booked Deposit")->orWhere('trip_status', '=', "Paid in Full")->orWhere('trip_status', '=', "Complete – Commission Paid");
                        })->get();
                        

                $checkouttrips = Trip::where('user_id', '=', $id)->whereBetween('checkout_date', array($startdate, $enddate))
                        ->where(function ($query) {
                           $query->where('trip_status', '=', "Booked Deposit")->orWhere('trip_status', '=', "Paid in Full")->orWhere('trip_status', '=', "Complete – Commission Paid");
                        })->get();     

                foreach ($checkintrips as $checkintrip) {
                              $trips->push($checkintrip);
                          }

                foreach ($checkouttrips as $checkouttrip) {
                            $trips->push($checkouttrip);

                        }                              
                $trips = $trips->unique();   
            }
        }
        else
        {
            $id = Auth::user()->id;
            $selectedagentid = $id;
            $checkintrips = Trip::where('user_id', '=', Auth::User()->id)->whereBetween('checkin_date', array($startdate, $enddate))
                        ->where(function ($query) {
                           $query->where('trip_status', '=', "Booked Deposit")->orWhere('trip_status', '=', "Paid in Full")->orWhere('trip_status', '=', "Complete – Commission Paid");
                        })->get();

            $checkouttrips = Trip::where('user_id', '=', Auth::User()->id)->whereBetween('checkout_date', array($startdate, $enddate))
                        ->where(function ($query) {
                           $query->where('trip_status', '=', "Booked Deposit")->orWhere('trip_status', '=', "Paid in Full")->orWhere('trip_status', '=', "Complete – Commission Paid");
                        })->get();

                 
            foreach ($checkintrips as $checkintrip) {
                $trips->push($checkintrip);
            }

            foreach ($checkouttrips as $checkouttrip) {
                $trips->push($checkouttrip);
            }                              
            $trips = $trips->unique();
        }
        
        $sum = 0;
        foreach ($trips as $expected) { 
            $sum = $sum + $expected->expected_commission;
        }
        $newYear = new Carbon('first day of January 2020');
        $Date = $newYear->toDateString();
        $ytdDate = Trip::where([
            ['user_id', '=', $selectedagentid],
            ['created_at', '>=', $newYear],
        ])->get();

        $PaidSum = 0;
        foreach ($ytdDate as $paid) { 
            // print_r($paid->total_sale);
            $PaidSum = $PaidSum + $paid->total_sale;
            // $PaidSum = $PaidSum + 0;
        }

        // print_r($ytdDate);
        // exit();

        if($sum == $PaidSum){
            $status = 1; //Processing
        }
        if($sum != $PaidSum && $sum != 0){
            $status = 2; // Processed
        }
        if($sum == 0){
            $status = 3; //Incomplete
        }

        $getdate = Carbon::now();
        $onlydate = $getdate->toDateString();
        $monthname = date('F', strtotime($getdate));
        
        $saveReport = new SaveReport();

        $saveReport->agentId = $id;
        $saveReport->reportcreation = $onlydate;
        $saveReport->isActive = 1;
        $saveReport->paymentstatus = $status;
        $saveReport->travelmonth = $monthname;
        $saveReport->checkin_date = $startdate;
        $saveReport->checkout_date = $enddate;
        $saveReport->exportdate = $onlydate;
        $saveReport->ytd_commission = $PaidSum;
        $saveReport->save();

        $data = $saveReport->id;

        foreach($trips as $tripss)
        {
            $sdate=$tripss->checkin_date;
            $sdate = strtotime($sdate);
            $sdate = date('m-d-Y',$sdate);

            $endate=$tripss->checkout_date;
            $endate = strtotime($endate);
            $endate = date('m-d-Y',$endate);

            $bdate=$tripss->booking_date;
            $bdate = strtotime($bdate);
            $bdate = date('m-d-Y',$bdate);

            $tripss->checkin_date=$sdate;
            $tripss->checkout_date=$endate;
            $tripss->booking_date=$bdate;

            $saveReportData = new SaveReportData();
            $saveReportData->savereportId = $data;
            $saveReportData->tripid = $tripss->id;
            $saveReportData->payment_status = 2;

            $saveReportData->save();

        }

        $getSave = "Data";
        return view("admin/reports/tripCheckInReport", ['startdate' => $request->saveReportStart, 'enddate'=> $request->saveReportEnd, 'trips' => $trips, 'agents' => $agents, 'selectedagentid' => $selectedagentid, 'getSave' => $getSave, 'getSaveReportId' => $data, 'SumofPaid' => $PaidSum, 'SumofExpected' => $sum ]);
    }

    public function getCommision()
    {
        $startdate = null;
        $enddate = null;
        $report = null;
        $selectedagentid = null;
        //$agents = User::all();

        $agents = User::where('status', '=', 'Active')->get();
        return view("admin/commisions/list", ['startdate' => $startdate, 'enddate'=> $enddate, 'report' => $report, 'agents' => $agents, 'selectedagentid' => $selectedagentid]);
    }

    public function postCommision(Request $request)
    {
        $startdate = date( $request->startdate).' 00:00:00';
        $enddate = date($request->enddate).' 23:59:59';
        $agents = User::all();
        $id = $request->input('role');

        $selectedagentid = $id;

        $report = new Collection();

        if (Auth::User()->role == 1) {
            if($id == "All")
            {
                $checkinreports = SaveReport::where([
                    ['checkin_date', '>=', $startdate],
                    ['checkout_date', '<=', $enddate]
                ])->orderBy('created_at', 'desc')->get();

                $report = new Collection;
               foreach ($checkinreports as $key) {
                    $object = new \App\ReportDTO;
                    $getAgent = User::where('id', '=', $key->agentId)->first();
                    $sad = SaveReportData::where('savereportId', '=', $key->savereportId)
                    ->get();
                    $texpectcom = 0;
                    foreach($sad as $sadKey){
                        $getTrip = Trip::where('id', '=', $sadKey->tripid)->first();
                        $texpectcom = $texpectcom + $getTrip->expected_commission;
                    }

                    $object->id = $key->savereportId;
                    $object->expectedcommssion = $texpectcom;
                    $object->agentName = $getAgent->name;
                    $object->CheckinDate = $key->checkin_date;
                    $object->CheckoutDate = $key->checkout_date;
                    $object->export_date = $key->exportdate;
                    $object->creation_date = $key->reportcreation;
                    $object->paymentstatus = $key->paymentstatus;
                    $object->ytdcommission = $key->ytd_commission;
                    $object->isActive = $key->isActive;
                    $report->push($object);
               }

                // $newYear = new Carbon('first day of January 2020');
                // $newDate = $newYear->toDateString();

                 $PaidCommission = 0;
                // $getyTrip = Trip::where([
                //         ['user_id','=',$getTrip->user_id],
                //         ['created_at', '>=', $newDate]
                // ])->get();
                
                // foreach ($getyTrip as $YTDkey) {
                //     $PaidCommission = $PaidCommission + $YTDkey->total_sale;
                // }


               // $ytdDate = Trip::where('created_at', '>=', $newYear)->get();
               // $PaidCommission = 0;

               // $expectedCommission = 0;
               
               // foreach ($sad as $data) {
               //     $getTrip = Trip::where('id', '=', $data->tripid)->first();
               //     $expectedCommission = $expectedCommission + $getTrip->expected_commission;
               // }

               //calculating ytd
               // foreach($ytdDate as $getYTD){
               //      $PaidCommission = $PaidCommission + $getYTD->total_sale;
               //  }

                // foreach ($report as $reportkey) {
                //     $reportkey->ytdcommission = $PaidCommission;
                // }
                
            }
            else
            {
               $checkinreports = SaveReport::where([
                    ['agentId', '=', $id],
                    ['checkin_date', '>=', $startdate],
                    ['checkout_date', '<=', $enddate]
                ])->orderBy('created_at', 'desc')->get();

               $report = new Collection;
               foreach ($checkinreports as $key) {
                    $object = new \App\ReportDTO;
                    $getAgent = User::where('id', '=', $key->agentId)->first();
                    $sad = SaveReportData::where('savereportId', '=', $key->savereportId)
                    ->get();
                    $texpectcom = 0;
                    foreach($sad as $sadKey){
                        $getTrip = Trip::where('id', '=', $sadKey->tripid)->first();
                        $texpectcom = $texpectcom + $getTrip->expected_commission;
                    }

                    $object->id = $key->savereportId;
                    $object->expectedcommssion = $texpectcom;
                    $object->agentName = $getAgent->name;
                    $object->CheckinDate = $key->checkin_date;
                    $object->CheckoutDate = $key->checkout_date;
                    $object->export_date = $key->exportdate;
                    $object->creation_date = $key->reportcreation;
                    $object->paymentstatus = $key->paymentstatus;
                    $object->ytdcommission = $key->ytd_commission;
                    $object->isActive = $key->isActive;
                    $report->push($object);
               }


               // $newYear = new Carbon('first day of January 2020');
               // $ytdDate = Trip::where('created_at', '>=', $newYear)->get();
               $PaidCommission = 0;

               // $expectedCommission = 0;
               
               // foreach ($sad as $data) {
               //     $getTrip = Trip::where('id', '=', $data->tripid)->first();
               //     $expectedCommission = $expectedCommission + $getTrip->expected_commission;
               // }

               //calculating ytd
               // foreach($ytdDate as $getYTD){
               //      $PaidCommission = $PaidCommission + $getYTD->total_sale;
               //  }

               //  foreach ($report as $reportkey) {
               //      $reportkey->ytdcommission = $PaidCommission;
               //  }
            }

        }
    
        else
        {
            $checkinreports = SaveReport::where([
                    ['agentId', '=', Auth::user()->id],
                    // ['checkin_date', '>=', $startdate],
                    // ['checkout_date', '<=', $enddate]
                ])->orderBy('created_at', 'desc')->get();


               $report = new Collection;
               foreach ($checkinreports as $key) {
                    $object = new \App\ReportDTO;
                    $getAgent = User::where('id', '=', $key->agentId)->first();
                    $sad = SaveReportData::where('savereportId', '=', $key->savereportId)
                    ->get();
                    $texpectcom = 0;
                    foreach($sad as $sadKey){
                        $getTrip = Trip::where('id', '=', $sadKey->tripid)->first();
                        $texpectcom = $texpectcom + $getTrip->expected_commission;
                    }

                    $object->id = $key->savereportId;
                    $object->expectedcommssion = $texpectcom;
                    $object->agentName = $getAgent->name;
                    $object->CheckinDate = $key->checkin_date;
                    $object->CheckoutDate = $key->checkout_date;
                    $object->export_date = $key->exportdate;
                    $object->creation_date = $key->reportcreation;
                    $object->paymentstatus = $key->paymentstatus;
                    $object->ytdcommission = $key->ytd_commission;
                    $object->travelmonth = $key->travelmonth;
                    $object->isActive = $key->isActive;
                    $report->push($object);
               }

               $PaidCommission = 0;

        }

        return view("admin/commisions/list", ['startdate' => $request->startdate, 'enddate'=> $request->enddate, 'report' => $report, 'agents' => $agents, 'selectedagentid' => $selectedagentid, 'SumofPaid' => $PaidCommission]);
    }

    public function PostMyCommission(Request $request){
        $getMonth = $request->month;


        $agents = User::all();
        $id = Auth::user()->id;
        $selectedagentid = $id;

        $report = new Collection();

        if($getMonth != "" || $getMonth != null){
            $checkinreports = SaveReport::where([
                ['agentId', '=', Auth::user()->id],
                ['travelmonth', '=', $getMonth],
            ])->orderBy('created_at', 'desc')->get();

            $report = new Collection;
            foreach ($checkinreports as $key) {
                $object = new \App\ReportDTO;
                $getAgent = User::where('id', '=', $key->agentId)->first();
                $sad = SaveReportData::where('savereportId', '=', $key->savereportId)
                ->get();
                $texpectcom = 0;
                foreach($sad as $sadKey){
                    $getTrip = Trip::where('id', '=', $sadKey->tripid)->first();
                    $texpectcom = $texpectcom + $getTrip->expected_commission;
                }

                $object->id = $key->savereportId;
                $object->expectedcommssion = $texpectcom;
                $object->agentName = $getAgent->name;
                $object->CheckinDate = $key->checkin_date;
                $object->CheckoutDate = $key->checkout_date;
                $object->export_date = $key->exportdate;
                $object->creation_date = $key->reportcreation;
                $object->paymentstatus = $key->paymentstatus;
                $object->ytdcommission = $key->ytd_commission;
                $object->travelmonth = $key->travelmonth;
                $object->isActive = $key->isActive;
                $report->push($object);
            }

        }
        else{
            $checkinreports = SaveReport::where([
                ['agentId', '=', Auth::user()->id],
            ])->orderBy('created_at', 'desc')->get();


            $report = new Collection;
            foreach ($checkinreports as $key) {
                $object = new \App\ReportDTO;
                $getAgent = User::where('id', '=', $key->agentId)->first();
                $sad = SaveReportData::where('savereportId', '=', $key->savereportId)
                ->get();
                $texpectcom = 0;
                foreach($sad as $sadKey){
                    $getTrip = Trip::where('id', '=', $sadKey->tripid)->first();
                    $texpectcom = $texpectcom + $getTrip->expected_commission;
                }

                $object->id = $key->savereportId;
                $object->expectedcommssion = $texpectcom;
                $object->agentName = $getAgent->name;
                $object->CheckinDate = $key->checkin_date;
                $object->CheckoutDate = $key->checkout_date;
                $object->export_date = $key->exportdate;
                $object->creation_date = $key->reportcreation;
                $object->paymentstatus = $key->paymentstatus;
                $object->ytdcommission = $key->ytd_commission;
                $object->travelmonth = $key->travelmonth;
                $object->isActive = $key->isActive;
                $report->push($object);
            }
        }

       return view("admin/commisions/my-commission", ['report' => $report, 'agents' => $agents, 'selectedagentid' => $selectedagentid]);
    }

    public function deleteReport($id)
    {        
        //$findReportId = SaveReport::where('savereportId' , $id)->delete();
        DB::table('save_reports')->where('savereportId', $id)->update(array('isActive'=>0));
        return redirect('commisions-list');
    }

    public function editReports($id)
    {
        $wajeeh = $id;
        $agents = User::all();
        $Savereports = SaveReportData::where('savereportId', '=', $id)->get();


        $DTO = new Collection();
        
        foreach ($Savereports as $data) {
            $obj = new \App\ReportDTO;

            $getTrip = Trip::where('id', '=', $data->tripid)->first();
            
            $getCustomer = Customer::where('id', '=', $getTrip->customer_id)->first();
            
            $getagent = User::where('id', '=', $getTrip->user_id)->first();
            
            $obj->expectedcommsion = $getTrip->expected_commission;
            $obj->ytdcommission = $getTrip->commission;
            $obj->paymentstatus = $data->payment_status;
            $obj->savereportid = $data->savereportid;
            $obj->id = $data->Id;
            $obj->CustomerName = $getCustomer->first_name;
            $obj->agentName = $getagent->name;
            $obj->CheckinDate = $getTrip->checkin_date;
            $obj->CheckoutDate = $getTrip->checkout_date;
            $obj->destination = $getTrip->destination;
            $obj->reservation = $getTrip->reservation_number;
            $DTO->push($obj);
        }

        $getpay = 0;

        return view("admin/commisions/edit", ['Savereports' => $DTO, 'agents' => $agents, 'trips'=> $getTrip, 'wajeeh' => $wajeeh, 'getpay' => $getpay]);
    }

    public function ViewReports($id)
    {
        $wajeeh = $id;
        $agents = User::all();
        $Savereports = SaveReportData::where('savereportId', '=', $id)->get();


        $DTO = new Collection();
        
        foreach ($Savereports as $data) {
            $obj = new \App\ReportDTO;

            $getTrip = Trip::where('id', '=', $data->tripid)->first();
            
            $getCustomer = Customer::where('id', '=', $getTrip->customer_id)->first();
            
            $getagent = User::where('id', '=', $getTrip->user_id)->first();
            
            $obj->expectedcommsion = $getTrip->expected_commission;
            $obj->ytdcommission = $getTrip->commission;
            $obj->paymentstatus = $data->payment_status;
            $obj->savereportid = $data->savereportid;
            $obj->id = $data->Id;
            $obj->CustomerName = $getCustomer->first_name;
            $obj->agentName = $getagent->name;
            $obj->CheckinDate = $getTrip->checkin_date;
            $obj->CheckoutDate = $getTrip->checkout_date;
            $obj->destination = $getTrip->destination;
            $obj->reservation = $getTrip->reservation_number;
            $DTO->push($obj);
        }



        // print_r($DTO);
        // exit();
        $getpay = 0;

        return view("admin/commisions/saved-reports", ['Savereports' => $DTO, 'agents' => $agents, 'trips'=> $getTrip, 'wajeeh' => $wajeeh, 'getpay' => $getpay]);
    }

    public function PoststatusChange(Request $request)
    {
        $value = $request->status;
        $reportids = $request->hid;
        $actualid = $request->actualid;
        $wajeeh = $actualid;
        DB::table('save_report_datas')->where('id', $reportids)->update(array('payment_status'=>$value));

        $saveid = SaveReportData::find($reportids);
        if($value == 5){
            $updateStatus = Trip::where('id', '=', $saveid->tripid)->first();

            $updateStatus->trip_status = "Complete – Commission Paid";
            $updateStatus->save();
        }
        $getpay = $saveid->payment_status;
        
        $agents = User::all();
        $Savereports = SaveReportData::where('savereportId', '=', $actualid)->get();

        $DTO = new Collection();
        
        foreach ($Savereports as $data) {
            $obj = new \App\ReportDTO;

            $getTrip = Trip::where('id', '=', $data->tripid)->first();
            
            $getCustomer = Customer::where('id', '=', $getTrip->customer_id)->first();
            
            $getagent = User::where('id', '=', $getTrip->user_id)->first();
            
            $obj->expectedcommsion = $getTrip->expected_commission;
            $obj->ytdcommission = $getTrip->commission;
            $obj->paymentstatus = $data->payment_status;
            $obj->savereportid = $data->savereportid;
            $obj->id = $data->Id;
            $obj->CustomerName = $getCustomer->first_name;
            $obj->agentName = $getagent->name;
            $obj->CheckinDate = $getTrip->checkin_date;
            $obj->CheckoutDate = $getTrip->checkout_date;
            $obj->destination = $getTrip->destination;
            $obj->reservation = $getTrip->reservation_number;
            $DTO->push($obj);
            
        }

        return view("admin/commisions/edit", ['Savereports' => $DTO, 'agents' => $agents, 'trips'=> $getTrip, 'wajeeh' => $wajeeh, 'getpay' => $getpay]);
    }

    public function Postcancelrequest($id, Request $request)
    {
        $value = 4;
        DB::table('save_reports')->where('savereportid', $id)->update(array('paymentstatus'=>$value));
        // print_r($id);
        // exit();
       //  $startdate = date( $request->startdate).' 00:00:00';

       //  $enddate = date($request->enddate).' 23:59:59';
       //  $agents = User::all();
       //  $id = $request->input('role');

       //  $selectedagentid = $id;

       //  $report = new Collection();
       //  $checkinreports = SaveReport::where([
       //      ['agentId', '=', Auth::user()->id],
       //      ['checkin_date', '>=', $startdate],
       //      ['checkout_date', '<=', $enddate]
       //  ])->get();

       // $report = new Collection;
       // foreach ($checkinreports as $key) {
       //      $object = new \App\ReportDTO;
       //      $getAgent = User::where('id', '=', $key->agentId)->first();
       //      $sad = SaveReportData::where('savereportId', '=', $key->savereportId)
       //      ->get();
       //      $texpectcom = 0;
       //      foreach($sad as $sadKey){
       //          $getTrip = Trip::where('id', '=', $sadKey->tripid)->first();
       //          $texpectcom = $texpectcom + $getTrip->expected_commission;
       //      }

       //      $object->id = $key->savereportId;
       //      $object->expectedcommssion = $texpectcom;
       //      $object->agentName = $getAgent->name;
       //      $object->CheckinDate = $key->checkin_date;
       //      $object->CheckoutDate = $key->checkout_date;
       //      $object->export_date = $key->exportdate;
       //      $object->creation_date = $key->reportcreation;
       //      $object->paymentstatus = $key->paymentstatus;
       //      $object->ytdcommission = $key->ytd_commission;
       //      $report->push($object);
       // }
       // $PaidCommission = 0;

       //  return view("admin/commisions/list", ['startdate' => $request->startdate, 'enddate'=> $request->enddate, 'report' => $report, 'agents' => $agents, 'selectedagentid' => $selectedagentid, 'SumofPaid' => $PaidCommission]);
        return redirect('commisions-list');
    }

    public function AdminstatusChange(Request $request)
    {
        $value = $request->status;

        $reportids = $request->hid;
        DB::table('save_reports')->where('savereportId', $reportids)->update(array('paymentstatus'=>$value));

        $UpdatePaymentStatus = SaveReport::where('savereportId', '=', $reportids)->first();
        $GetTripPaymentStatus = SaveReportData::where('savereportId', '=', $UpdatePaymentStatus->savereportId)->get();
        
        if($value == 5){
            foreach($GetTripPaymentStatus as $GetKey){
                $value = 1;
                $GetReport = $GetKey->Id;
                DB::table('save_report_datas')->where('Id', $GetReport)->update(array('payment_status'=>$value));
                $updateStatus = Trip::where('id', '=', $GetKey->tripid)->first();
                $updateStatus->trip_status = "Complete – Commission Paid";
                $updateStatus->save();
            }
        }
        else{
            
            foreach($GetTripPaymentStatus as $GetKey){
                $GetReport = $GetKey->Id;
                DB::table('save_report_datas')->where('Id', $GetReport)->update(array('payment_status'=>$value));

            }
        }
        $startdate = date( $request->startdate).' 00:00:00';
        $enddate = date($request->enddate).' 23:59:59';
        $agents = User::all();
        $id = $request->input('role');

        $selectedagentid = $id;

        $report = new Collection();

        if (Auth::User()->role == 1) {
            if($id == "All")
            {
                $checkinreports = SaveReport::where([
                    ['checkin_date', '>=', $startdate],
                    ['checkout_date', '<=', $enddate]
                ])->orderBy('created_at', 'desc')->get();

                $report = new Collection;
               foreach ($checkinreports as $key) {
                    $object = new \App\ReportDTO;
                    $getAgent = User::where('id', '=', $key->agentId)->first();
                    $sad = SaveReportData::where('savereportId', '=', $key->savereportId)
                    ->get();
                    $texpectcom = 0;
                    foreach($sad as $sadKey){
                        $getTrip = Trip::where('id', '=', $sadKey->tripid)->first();
                        $texpectcom = $texpectcom + $getTrip->expected_commission;
                    }

                    $object->id = $key->savereportId;
                    $object->expectedcommssion = $texpectcom;
                    $object->agentName = $getAgent->name;
                    $object->CheckinDate = $key->checkin_date;
                    $object->CheckoutDate = $key->checkout_date;
                    $object->export_date = $key->exportdate;
                    $object->creation_date = $key->reportcreation;
                    $object->paymentstatus = $key->paymentstatus;
                    $object->ytdcommission = $key->ytd_commission;
                    $report->push($object);
               }

                 $PaidCommission = 0;
                
            }
            else
            {
               $checkinreports = SaveReport::where([
                    ['agentId', '=', $id],
                    ['checkin_date', '>=', $startdate],
                    ['checkout_date', '<=', $enddate]
                ])->orderBy('created_at', 'desc')->get();

               $report = new Collection;
               foreach ($checkinreports as $key) {
                    $object = new \App\ReportDTO;
                    $getAgent = User::where('id', '=', $key->agentId)->first();
                    $sad = SaveReportData::where('savereportId', '=', $key->savereportId)
                    ->get();
                    $texpectcom = 0;
                    foreach($sad as $sadKey){
                        $getTrip = Trip::where('id', '=', $sadKey->tripid)->first();
                        $texpectcom = $texpectcom + $getTrip->expected_commission;
                    }

                    $object->id = $key->savereportId;
                    $object->expectedcommssion = $texpectcom;
                    $object->agentName = $getAgent->name;
                    $object->CheckinDate = $key->checkin_date;
                    $object->CheckoutDate = $key->checkout_date;
                    $object->export_date = $key->exportdate;
                    $object->creation_date = $key->reportcreation;
                    $object->paymentstatus = $key->paymentstatus;
                    $object->ytdcommission = $key->ytd_commission;
                    $report->push($object);
               }

               $PaidCommission = 0;

            }
        }

        return view("admin/commisions/list", ['startdate' => $request->startdate, 'enddate'=> $request->enddate, 'report' => $report, 'agents' => $agents, 'selectedagentid' => $selectedagentid, 'SumofPaid' => $PaidCommission]);
    }

    public function HistoryReports(Request $request, $id)
    {
        $v = $id;
        $getData = SaveReport::where('savereportId', '=', $id)->first();
        $getId = $getData->agentId;
        $agents = User::all();
        $selectedagentid = $id;

        $GetAgent = SaveReport::where([
            ['agentId', '=', $getId]
        ])->orderBy('created_at', 'desc')->get();

            
            $report = new Collection();

            $report = new Collection;
            foreach ($GetAgent as $key) {
                $object = new \App\ReportDTO;
                $getAgent = User::where('id', '=', $key->agentId)->first();
                $sad = SaveReportData::where('savereportId', '=', $key->savereportId)
                ->get();
                
                $texpectcom = 0;
                foreach($sad as $sadKey){
                    $getTrip = Trip::where('id', '=', $sadKey->tripid)->first();
                    $texpectcom = $texpectcom + $getTrip->expected_commission;
                }

                $object->id = $key->savereportId;
                $object->expectedcommssion = $texpectcom;
                $object->agentName = $getAgent->name;
                $object->CheckinDate = $key->checkin_date;
                $object->CheckoutDate = $key->checkout_date;
                $object->export_date = $key->exportdate;
                $object->creation_date = $key->reportcreation;
                $object->paymentstatus = $key->paymentstatus;
                $object->ytdcommission = $key->ytd_commission;
                $object->travelmonth = $key->travelmonth;
                $report->push($object);
        }


       return view("admin/commisions/history", ['report' => $report, 'agents' => $agents, 'selectedagentid' => $selectedagentid]);
    }

    public function PostChecktoPaid(Request $request)
    {
        $SingleCheck = $request->dataid;
        $Paid = $request->savereportid;
        $reportids = $request->savereportid;
        $actualid = $request->actualids;
        $wajeeh = $actualid;
        $agents = User::all();
        $getpay = 0;
        $CheckStatus = SaveReport::where('savereportId', '=', $Paid)->first();
        
        if ($SingleCheck == null || $SingleCheck == "") {
                $GetCheckStatus = SaveReportData::where([
                ['savereportId', '=', $CheckStatus->savereportId],
            ])->get();
        
            foreach($GetCheckStatus as $GetKey){
                $value = 1;
                $GetReport = $GetKey->Id;
                DB::table('save_report_datas')->where('Id', $GetReport)->update(array('payment_status'=>$value));
               
                $updateStatus = Trip::where('id', '=', $GetKey->tripid)->first();
                $updateStatus->trip_status = "Complete – Commission Paid";
                $updateStatus->save();
            }
        
            
            $Savereports = SaveReportData::where('savereportId', '=', $Paid)->get();

            $DTO = new Collection();
            
            foreach ($Savereports as $data) {
                $obj = new \App\ReportDTO;

                $getTrip = Trip::where('id', '=', $data->tripid)->first();
                
                $getCustomer = Customer::where('id', '=', $getTrip->customer_id)->first();
                
                $getagent = User::where('id', '=', $getTrip->user_id)->first();
                
                $obj->expectedcommsion = $getTrip->expected_commission;
                $obj->ytdcommission = $getTrip->commission;
                $obj->paymentstatus = $data->payment_status;
                $obj->savereportid = $data->savereportid;
                $obj->id = $data->Id;
                $obj->CustomerName = $getCustomer->first_name;
                $obj->agentName = $getagent->name;
                $obj->CheckinDate = $getTrip->checkin_date;
                $obj->CheckoutDate = $getTrip->checkout_date;
                $obj->destination = $getTrip->destination;
                $obj->reservation = $getTrip->reservation_number;
                $DTO->push($obj);
                
            }
        }
        else
        {

            $SingleRecord = SaveReportData::where('Id', '=', $SingleCheck)->first();
            $value = 1;
            $GetReport = $SingleRecord->Id;
            DB::table('save_report_datas')->where('Id', $GetReport)->update(array('payment_status'=>$value));

            $updateStatus = Trip::where('id', '=', $SingleRecord->tripid)->first();
            $updateStatus->trip_status = "Complete – Commission Paid";
            $updateStatus->save();

            $Savereports = SaveReportData::where('savereportId', '=', $actualid)->get();

            $DTO = new Collection();
            
            foreach ($Savereports as $data) {
                $obj = new \App\ReportDTO;

                $getTrip = Trip::where('id', '=', $data->tripid)->first();
                
                $getCustomer = Customer::where('id', '=', $getTrip->customer_id)->first();
                
                $getagent = User::where('id', '=', $getTrip->user_id)->first();
                
                $obj->expectedcommsion = $getTrip->expected_commission;
                $obj->ytdcommission = $getTrip->commission;
                $obj->paymentstatus = $data->payment_status;
                $obj->savereportid = $data->savereportid;
                $obj->id = $data->Id;
                $obj->CustomerName = $getCustomer->first_name;
                $obj->agentName = $getagent->name;
                $obj->CheckinDate = $getTrip->checkin_date;
                $obj->CheckoutDate = $getTrip->checkout_date;
                $obj->destination = $getTrip->destination;
                $obj->reservation = $getTrip->reservation_number;
                $DTO->push($obj);
                
            }

        }
        return view("admin/commisions/edit", ['Savereports' => $DTO, 'agents' => $agents, 'trips'=> $getTrip, 'wajeeh' => $wajeeh, 'getpay' => $getpay]);
    }

    public function getCurrentYearSales(){
        // $current_year = date('Y');
        if(Auth::user()->role == 0){
            $agentid = Auth::user()->id;
            $agentGrapg = user::where('id', '=', $agentid);
            $allcustomers =  Customer::all();
            print_r($agentGrapg);
            exit();
        }
    }

    //Graph Function
    public function GetData(){
        $user = Auth::user();
        $id = $user->id;
        $lastyear = date("Y") - 1;
        $startdate = $lastyear."-01-01".' 00:00:00';
        $enddate =  $lastyear."-12-31".' 23:59:59';

        $LastYearData = Trip::where([
                    ['checkin_date', '>=', $startdate],
                    ['checkin_date', '<=', $enddate]
                ])->orderBy('created_at', 'desc')->get();

        $sum = 0;
        $CurrentTotal = 0;
        $Object = array();
        $Objects2 = array();
        
        for ($i=01; $i <= 12; $i++) { 
            foreach($LastYearData as $item){
                $date = new \DateTime($item->checkin_date);
                $month = $date->format('m');
                if($month == $i){
                    $sum = $sum + $item->total_sale;
                }
            }
            $Object[] = (object)[
                "month" => $i,
                "total" => $sum 
            ];
            $Objects2[] = (object)[
                "month" => $i,
                "total" => $sum 
            ];
        }

        $Object3 = array();
        for ($i=1; $i < count($Object) ; $i++) { 
            $PreviousTotal = $Object[$i]->total;
            if(!empty($Objects2[$i])){
                $CurrentTotal = $Objects2[$i]->total;
            }
            $Object3[] = (object)[
                "month" => $i,
                "Pre" => $PreviousTotal,
                "Current" => $CurrentTotal
            ];
        }
        return $Object3;
    }

}
