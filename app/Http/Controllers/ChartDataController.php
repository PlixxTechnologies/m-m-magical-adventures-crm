<?php

namespace App\Http\Controllers;

use App\CalculatorYear;
use App\Agentdestination;
use App\Trip;
use App\Todo;
use App\User;
use App\DiningPlan;
use App\Customer;
use App\Guest;
use App\Hotel;
use App\HotelCategory;
use App\Destination;
use App\PaymentHistory;
use App\AuditLogs;
use App\SaveReport;
use App\ReportDTO;
use App\lead;
use App\todolistings;
use App\quote;
use App\SaveReportData;
use App\categorynames;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Hash;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\NewTrip;
use DateTime;
use App\Http\Controllers\Controller;

class ChartDataController extends Controller
{
    function getPreviousYear(){
        $user = Auth::user();
        $id = $user->id;
        $montharray = array();
        $lastyear = date("Y") - 1;
        $startdate = $lastyear."-01-01".' 00:00:00';
        $enddate =  $lastyear."-12-31".' 23:59:59';

        $LastYearData = Trip::where([
                    ['checkin_date', '>=', $startdate],
                    ['checkin_date', '<=', $enddate]
                ])->orderBy('created_at', 'desc')->get();
        
        $LastYearData = json_decode($LastYearData);

        if(!empty($LastYearData)){
            foreach($LastYearData as $data){
                $date = new \DateTime($data->checkin_date);
                $month = $date->format('m');
                $month_name = $date->format('M');
                // $montharray[$month] = $month_name;
                $montharray[$month_name] = $month;
            }
        }
        return $montharray;
    }

    function getMonthlyCount($month){
        $lastyear = date("Y") - 1;
        $gettingData = $lastyear."-".$month."-"."01";
        $lastDateOfMonth = date("Y-m-t", strtotime($gettingData));
        $monthly_count = Trip::whereYear('checkin_date', $lastyear)->whereMonth('checkin_date', $month)->get()->count();
        return $monthly_count;
    }

    function getMonthlyData(){
        
        $monthly_data_count_array = array();
        $month_array = $this->getPreviousYear();
        // $month_no_array = array();
        if(!empty($month_array)){
            
            foreach ($month_array as $month_no) {
                $getCount = $this->getMonthlyCount($month_no);
                array_push($monthly_data_count_array, $getCount);
            }
        }
        // return $monthly_data_count_array;
        $max_no = max($monthly_data_count_array);
        $max = round(($max_no + 10/2) /10 )*10;
        $monthly_post_data = array(
            'months' => $month_array,
            'data' => $monthly_data_count_array,
            'max' => $max,
        );

        return $monthly_post_data;
    }
}
