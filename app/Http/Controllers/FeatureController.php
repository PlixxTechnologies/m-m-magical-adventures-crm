<?php

namespace App\Http\Controllers;

use App\Todo;
use App\Trip;
use App\User;
use App\TextMessage;
use App\DiningPlan;
use App\Customer;
use App\Destination;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Hash;
use Illuminate\Support\Collection;
use DB;
use Excel;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Twilio\Rest\Client;
use Twilio\Jwt\ClientToken;

class FeatureController extends Controller
{

    public function importExcel()
    {
        return view('feature/importExcel');
    }

    public function postimportExcel()
    {
        if(Input::hasFile('import_file'))
        {
            $path = Input::file('import_file')->getRealPath();
            $data = Excel::load($path, function($reader) {
            })->get();
            

            if(!empty($data) && $data->count())
            {
                foreach ($data as $key => $value)
                 {                   


                    $c = new Customer();
                    $c->user_id = $value->user_id;
                    $c->first_name = $value->first_name;
                    $c->last_name = $value->last_name;
                    $c->email = $value->email;
                    $c->country_code = '+1';
                    $c->phone_no = $value->phone_number;
                    $c->address1 = $value->address1;
                    $c->city = $value->city;
                    $c->state = $value->state;
                    $c->zip = $value->zip;
                    $c->disney_experience_username = $value->my_disney_experience_user_name;
                    $c->disney_experience_password = $value->my_disney_experience_password;
                    $c->referral = $value->referral;
                    $c->save();

                    $trip = new Trip();
                    $trip->user_id = $value->user_id;
                    $trip->customer_id = $c->id;
                    $trip->reservation_number = $value->reservation_number;
                    $trip->booking_date = $value->booking_date;
                    $trip->checkin_date = $value->check_in_calendar;
                    $trip->checkout_date = $value->check_out_calendar;
                    $trip->trip_status = $value->trip_status;
                    $trip->total_sale = $value->total_sale;
                    $trip->commission = $value->total_commission;
                    $trip->expected_commission = $value->expected_commission;
                    $trip->travel_with = $value->travel_with;
                    $trip->special_request = $value->special_occasions_or_request;
                    $trip->destination = $value->destination;
                    $trip->reminder = $value->calendar_reminders_set;
                    $trip->advanced_dining_reservations = $value->advanced_dining_reservations_bbbtours;
                    $trip->fast_pass_date = $value->fast_pass_date;
                    $trip->promo_applied = $value->promo_applied;
                    $trip->magic_band_color = $value->magic_band_color_selection;
                    $trip->magical_express_completed = $value->magical_express_completed;
                    $trip->final_payment_due = $value->final_payment_due;
                    $trip->call_room_requests = $value->call_in_room_requests;
                    $trip->itinerary_tip_sheets = $value->itinerary_and_tip_sheets_sent;

                    $trip->save();
                }
          } 
       }
       return redirect('customer/list');
    }


    public function listSms()
    {
        $user = Auth::user();


        if($user->role == 1)
        {
            $textmessages = TextMessage::all();
            $textmessages = $textmessages->sortByDesc('id');
        }
        else
        {
            $textmessages = TextMessage::where('user_id', '=', $user->id)->get();
            $textmessages = $textmessages->sortByDesc('id');
        }
          
        return view('admin/customer/listsms', ['textmessages'=>$textmessages, 'user'=>$user]);
    }
      public function listReminderSms()
    {
        $user = Auth::user();


        if($user->role == 1)
        {
            $textmessages = TextMessage::all()->where('reminder','=','1');
            $textmessages = $textmessages->sortByDesc('id');
        }
        else
        {
            $textmessages = TextMessage::where('user_id', '=', $user->id)->where('reminder','=','1')->get();
            $textmessages = $textmessages->sortByDesc('id');
        }
          
        return view('admin/customer/listremindersms', ['textmessages'=>$textmessages, 'user'=>$user]);
    }


    public function Sms()
    {
        $user = Auth::user();

        if($user->role == 1)
        {
            $customers = Customer::all();
        }
        else
        {
            $customers = Customer::where('user_id', '=', $user->id)->get();
        }
          
        return view('admin/customer/sms', ['customers'=>$customers, 'user'=>$user]);
    }

    public function sendSms(Request $request)
    {
        //$accountSid = config('app.twilio')['TWILIO_ACCOUNT_SID'];
        //$authToken  = config('app.twilio')['TWILIO_AUTH_TOKEN'];
        //dd($request->msg); exit;
        
        $accountSid = 'ACd08621776ec3ae661d938e955e1a2b7e';
        $authToken  = 'c5ed7991777e7a6157314fbe93ff4620';
        
        $client = new Client($accountSid, $authToken);
        try
        {
            if(sizeof($request->customer) > 0)
            {
                for ($i=0; $i < sizeof($request->customer); $i++) 
                { 
                    $customer = Customer::find($request->customer[$i]);

                    $number = $customer->country_code.$customer->phone_no;

                    // Use the client to do fun stuff like send text messages!
                    $client->messages->create(
                    // the number you'd like to send the message to
                        $number,
                   array(
                         // A Twilio phone number you purchased at twilio.com/console
                         'from' => '+19728489657',
                         // the body of the text message you'd like to send
                         'body' => $request->msg
                        )
                    );


                    $textmessage = new TextMessage();
                    $textmessage->message = $request->msg;
                    $textmessage->customer_id = $customer->id;
                    $textmessage->user_id = Auth::user()->id;
                    $textmessage->country_code = $customer->country_code;
                    $textmessage->number = $customer->phone_no;

                    $textmessage->save();
                }
            }   

            return redirect('sms-list');

        }
        catch (Exception $e)
        {
            echo "Error: " . $e->getMessage();
        }
    }
    

    // Reminder 


     public function sendReminderSms(Request $request)
    {
        
        $accountSid = 'ACd08621776ec3ae661d938e955e1a2b7e';
        $authToken  = 'c5ed7991777e7a6157314fbe93ff4620';
        
        $client = new Client($accountSid, $authToken);
        try
        {   
            $trip = Trip::find($request->tripId);
            $agent = User::find(Auth::user()->id);

                $msg= "Hello ".$trip->customer->first_name. " ".$trip->customer->last_name."
This is a reminder from ".$agent->name." that a payment is due. Please contact ".$agent->name." at ".$agent->phone." or ".$agent->email." to make a payment.
Thank you!
".$agent->name." ";
                                           
                       
                  $customer = Customer::find($trip->customer->id);

                    $number = $customer->country_code.$customer->phone_no;         



                    // Use the client to do fun stuff like send text messages!
                    $client->messages->create(
                    // the number you'd like to send the message to
                        $number,
                   array(
                         // A Twilio phone number you purchased at twilio.com/console
                         'from' => '+19728489657',
                         // the body of the text message you'd like to send
                         'body' => $msg
                        )
                    );


                    $textmessage = new TextMessage();
                    $textmessage->message = $msg;
                    $textmessage->customer_id = $customer->id;
                    $textmessage->user_id = Auth::user()->id;
                    $textmessage->country_code = $customer->country_code;
                    $textmessage->number = $customer->phone_no;
                    $textmessage->reminder = 1;

                    $textmessage->save();
                
               

            return redirect('reminder-sms-list');

        }
        catch (Exception $e)
        {
            echo "Error: " . $e->getMessage();
        }
    }
    
    
    public function sendAgentsAlerts()
    {
        $accountSid = 'ACd08621776ec3ae661d938e955e1a2b7e';
        $authToken  = 'c5ed7991777e7a6157314fbe93ff4620';
        
        $client = new Client($accountSid, $authToken);
        try
        {
            $alltodos = Todo::all();
            
            $month = date("m");
            $day = date("d");
            $year = date("Y");
            $from_unix_time = mktime(0, 0, 0, $month, $day, $year);
            $datenow = strtotime(date("Y-m-d", $from_unix_time));            


            foreach($alltodos as $todo)
            {
                $deadline = strtotime(date($todo->due_date));
                $datediff = $deadline - $datenow;


                $deadline1 = strtotime(date($todo->trip->final_payment_due));
                $datediff1 = $deadline1 - $datenow;


                $deadline2 = strtotime(date($todo->trip->advanced_dining_reservations));
                $datediff2 = $deadline2 - $datenow;


                $deadline3 = strtotime(date($todo->trip->fast_pass_date));
                $datediff3 = $deadline3 - $datenow;

 
                if($datediff/(60 * 60 * 24) == 1)
                {
                    $number = $todo->user->phone;

                    $message = $todo->user->name + " Customer" + $todo->customer->first_name." ".$todo->customer->last_name." To-do due tomorrow.";                    
                        // Use the client to do fun stuff like send text messages!
                    $client->messages->create(
                        // the number you'd like to send the message to
                    $number,
                    array(
                             // A Twilio phone number you purchased at twilio.com/console
                            'from' => '+19728489657',
                             // the body of the text message you'd like to send
                            'body' =>  $message
                        )
                    );
                }



                if($datediff1/(60 * 60 * 24) == 1)
                {
                    $number = $todo->user->phone;

                    $message = $todo->user->name." Customer " .$todo->customer->first_name." ".$todo->customer->last_name." Final Payment Date due tomorrow.";
                        // Use the client to do fun stuff like send text messages!
                    $client->messages->create(
                        // the number you'd like to send the message to
                    $number,
                    array(
                             // A Twilio phone number you purchased at twilio.com/console
                            'from' => '+19728489657',
                             // the body of the text message you'd like to send
                            'body' =>  $message
                        )
                    );
                }


                if($datediff2/(60 * 60 * 24) == 1)
                {
                    $number = $todo->user->phone;

                    $message = $todo->user->name." Customer " .$todo->customer->first_name." ".$todo->customer->last_name." Advanced Dining Reservations due tomorrow.";
                        // Use the client to do fun stuff like send text messages!
                    $client->messages->create(
                        // the number you'd like to send the message to
                    $number,
                    array(
                             // A Twilio phone number you purchased at twilio.com/console
                            'from' => '+19728489657',
                             // the body of the text message you'd like to send
                            'body' =>  $message
                        )
                    );
                }

                if($datediff3/(60 * 60 * 24) == 1)
                {
                    $number = $todo->user->phone;

                    $message = $todo->user->name." Customer " .$todo->customer->first_name." ".$todo->customer->last_name." Fast Pass Date due tomorrow.";
                        // Use the client to do fun stuff like send text messages!
                    $client->messages->create(
                        // the number you'd like to send the message to
                    $number,
                    array(
                             // A Twilio phone number you purchased at twilio.com/console
                            'from' => '+19728489657',
                             // the body of the text message you'd like to send
                            'body' =>  $message
                        )
                    );
                }
   
            }

            return null;

        }
        catch (Exception $e)
        {
            echo "Error: " . $e->getMessage();
        }
    }
}