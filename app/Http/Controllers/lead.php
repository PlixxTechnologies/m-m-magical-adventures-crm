<?php

namespace App\Http\Controllers;

use App\lead;
use Illuminate\Http\Request;


class lead extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user_add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $res = new category_name;
        $res->category_name=$request->input('category_name');
        $res->save();

        $request->session()->flash('msg', 'Data Submitted Sucessfuly');
        return redirect('user_show');


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\category_name  $category_name
     * @return \Illuminate\Http\Response
     */
    public function listlead(lead $lead)
    {
        $user = Auth::user();
        return view('/admin/lead/list')->with('leadArr' , lead::all());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\category_name  $category_name
     * @return \Illuminate\Http\Response
     */
    public function edit(category_name $category_name, $id)
    {
         return view('user_edit')->with('category_nameArr' , category_name::find($id));
    }

    /**
     * Update the specified resource in storage.
     *s
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\category_name  $category_name
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, category_name $category_name, $id)
    {
        $res = category_name::find($request->id);
        $res->category_name=$request->input('category_name');
        $category_name->save();


// $category_name = $request->input('category_name');
// $category_name = category_name::find($id);

        $request->session()->flash('msg', 'Data Updated Sucessfuly');

        return redirect('user_show');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\category_name  $category_name
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request , category_name $category_name , $id)
    {
        category_name::destroy(array('id' , $id));

        $request->session()->flash('msgdel', 'Data Deleted Sucessfuly');
        return redirect('user_show');
    }
}
