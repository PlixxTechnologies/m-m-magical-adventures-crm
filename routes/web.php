<?php

use App\Guest;
use App\LeadGuest;
use App\Hotel;
use App\HotelCategory;
use App\Trip;
use App\TripsDTO;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Input;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('send-email', 'HomeController@sendEmail');




    Route::get('/', function () {
        return view('auth/login');
    });

// Route::get('/', function () {
//     return view('admin/commisions/my-commission');
// });

    Route::get('logout', 'HomeController@logout');

    Route::get('dining-plan-calculator', 'HomeController@diningplancalculator');
    Route::post('dining-plan-calculator', 'HomeController@postdiningplancalculator');


    Route::get('send-agents-alert', 'FeatureController@sendAgentsAlerts');


    Route::group(['middleware' => ['web','auth']], function () {

    Route::get('/todo/todolists','HomeController@showtodolist');

    Route::get('/toggleTodo/{id}', 'HomeController@toggleTodo');    

    Route::get('/home', 'HomeController@index')->name('home');

// LEADS ROUTE

    Route::get('lead/list','HomeController@listlead');
    Route::get('lead/create','HomeController@addlead');
    Route::post('lead/save', 'HomeController@savelead');
    Route::get('lead/edit/{id}', 'HomeController@editLead');
    Route::post('lead/edit/{id}', 'HomeController@updateLead');
    Route::delete('lead/delete/{id}', 'HomeController@deleteLead');
    Route::post('guest/edit', 'HomeController@updateGuest');
    Route::post('leadguest/edit', 'HomeController@updateLeadGuest');
    Route::delete('leadguest/delete/', 'HomeController@deleteLeadGuest');

    Route::get('/ajax-leadguestUpdate',function(){
        $id = Input::get('guest_id');
        $guest = LeadGuest::find($id);
        return Response::json($guest);
    });

// LEADS ROUTE ENDS


// TODOLISTINGS START

Route::post('todolistings/save', 'HomeController@savetodolistings');
Route::delete('delete/{id}', 'HomeController@deletetasks');

//TODOLISTINGS END 



// QUOTES ROUTE
    Route::post('quote/creates','HomeController@addquotes');
    Route::get('quote/create/{id}','HomeController@addquote');
    Route::post('quote/save','HomeController@saveQuote');

    Route::get('quote/list', 'HomeController@listQuotes');
    Route::get('quote/view/{id}', 'HomeController@viewQuote');
    Route::get('quote/AddPayment/{id}', 'HomeController@AddPayment');
    
    Route::post('trip/AddPaymentHistory', 'HomeController@AddPaymentHistory');
    Route::delete('trip/RemovePaymentHistory/{id}', 'HomeController@RemovePaymentHistory');

    Route::get('quote/edit/{id}', 'HomeController@editQuote');
    Route::post('quote/edit/{id}', 'HomeController@updateQuote');
    Route::delete('quote/delete/{id}', 'HomeController@deleteQuote');

    Route::get('quote/QuoteHistory/{id}', 'HomeController@QuoteHistory');

    Route::get('quote/convert/{id}', 'HomeController@ConverttoTrip');

    

  // QUOTE ROUTE ENDS

    Route::get('customer/list', 'HomeController@listCustomers');
    Route::get('customer/create', 'HomeController@addCustomer');
    Route::post('customer/save', 'HomeController@saveCustomer');
    Route::get('customer/edit/{id}', 'HomeController@editCustomer');
    Route::post('customer/edit/{id}', 'HomeController@updateCustomer');
    Route::delete('customer/delete/{id}', 'HomeController@deleteCustomer');
    Route::get('customer/view/{id}', 'HomeController@viewcustomer');
    Route::post('guest/edit', 'HomeController@updateGuest');
    Route::delete('guest/delete', 'HomeController@deleteGuest');

    // Route::get('show/calender', 'HomeController@deleteGuest');

    Route::get('/ajax-guestUpdate',function(){
        $id = Input::get('guest_id');
        $guest = Guest::find($id);
        return Response::json($guest);
    });


    
    // Route::get('singlecustomer/view/{id}', 'HomeController@viewSinglecustomer');

    Route::get('trip/list', 'HomeController@listTrips');
    Route::get('trip/view/{id}', 'HomeController@viewTrip');
    Route::get('trip/AddPayment/{id}', 'HomeController@AddPayment');
    
    Route::post('trip/AddPaymentHistory', 'HomeController@AddPaymentHistory');
    Route::delete('trip/RemovePaymentHistory/{id}', 'HomeController@RemovePaymentHistory');
    Route::get('trip/create/{id}', 'HomeController@addTrip');

    Route::post('trip/save', 'HomeController@saveTrip');

    Route::get('trip/edit/{id}', 'HomeController@editTrip');
    Route::post('trip/edit/{id}', 'HomeController@updateTrip');
    Route::delete('trip/delete/{id}', 'HomeController@deleteTrip');
    //Route::get('trip/CancelTrip/{id}', 'HomeController@cancelTrips');
    Route::post('trip/CancelTrip/{id}', 'HomeController@cancelTrips');
    Route::post('trip/updateTripStatus', 'HomeController@updateTripStatus');
    Route::get('customer/TripHistory/{id}', 'HomeController@TripHistory');

    Route::get('sms-list', 'FeatureController@listSms');
    Route::get('sms', 'FeatureController@Sms');
    Route::post('sms', 'FeatureController@sendSms');
    Route::post('Remindersms', 'FeatureController@sendReminderSms');
    Route::get('reminder-sms-list', 'FeatureController@listReminderSms');

    Route::get('todo/list', 'HomeController@listTodos');
    Route::get('todo/create/{id}', 'HomeController@addTodo');
    Route::post('todo/save/{id}', 'HomeController@saveTodo');
    Route::get('todo/edit/{id}', 'HomeController@editTodo');
    Route::post('todo/update/{id}', 'HomeController@updateTodo');
    Route::delete('todo/delete/{id}', 'HomeController@deleteTodo');

    Route::get('important-date-calculator', 'HomeController@ImportantDateCalculator');

    Route::get('import-excel', 'FeatureController@importExcel');
    Route::post('import-excel', 'FeatureController@postimportExcel');


    Route::get('trips-checkin', 'HomeController@getTripCheckInReport');
    Route::post('trips-checkin', 'HomeController@postTripCheckInReport');   

    Route::get('user/editProfile', 'HomeController@editProfile');
    Route::post('/user/p-edit-profile',"HomeController@PEditProfile");

    Route::get('changePassword', 'HomeController@changePassword');
    Route::post('p-changePassword',"HomeController@PChangePassword");

    // Calender Route
    Route::get('calenders-list', "HomeController@getcalenders");
    Route::get('get-ajaxTrips', "HomeController@getAjaxTrips");

    Route::get('/ajax-GetTrips',function(){
        $uid = Input::get('u_I_d');
        $fpd = Input::get('fpd');
        $Adr = Input::get('adr');
        $fpdate = Input::get('FpDate');
        $user = Auth::user();
        $search = Input::get('search');
        

        if($user->role == 1)
        {
            if($uid!="" && $uid!=null)
            {
                if($search != "" && $search != null){
                    $Alltrips = Trip::where([
                        ['user_id', '=', $uid],
                        ['Status', '=', 0],
                        ['reservation_number', '=', $search],
                    ])->orWhere([
                        ['user_id', '=', $uid],
                        ['Status', '=', 0],
                        ['customer_Id', '=', $search],
                    ])->get()->sortByDesc('id');
                }
                else{
                    if($fpd == "1" && $Adr == "true" && $fpdate == "true"){
                        $Alltrips = Trip::where([
                            ['user_id', '=', $uid],
                            ['Status', '=', 0],
                            ['fast_pass_date', '!=', NULL],
                            ['advanced_dining_reservations', '!=', NULL],
                            ['final_payment_due', '!=', NULL]
                        ])->get()->sortByDesc('id');
                    }

                    if($fpd == "0" && $Adr == "false" && $fpdate == "false"){
                        $Alltrips = Trip::where([
                            ['user_id', '=', $uid],
                            ['Status', '=', 0],
                            ['fast_pass_date', '=', NULL],
                            ['advanced_dining_reservations', '=', NULL],
                            ['final_payment_due', '=', NULL]
                        ])->get()->sortByDesc('id');
                    }

                    if($fpd == "1" && $Adr == "false" && $fpdate == "false"){
                        $Alltrips = Trip::where([
                            ['user_id', '=', $uid],
                            ['Status', '=', 0],
                            ['fast_pass_date', '!=', NULL],
                            ['advanced_dining_reservations', '=', NULL],
                            ['final_payment_due', '=', NULL]
                        ])->get()->sortByDesc('id');
                    }

                    if($fpd == "1" && $Adr == "true" && $fpdate == "false"){
                        $Alltrips = Trip::where([
                            ['user_id', '=', $uid],
                            ['Status', '=', 0],
                            ['fast_pass_date', '!=', NULL],
                            ['advanced_dining_reservations', '!=', NULL],
                            ['final_payment_due', '=', NULL]
                        ])->get()->sortByDesc('id');
                    }

                    if($fpd == "1" && $Adr == "false" && $fpdate == "true"){
                        $Alltrips = Trip::where([
                            ['user_id', '=', $uid],
                            ['Status', '=', 0],
                            ['fast_pass_date', '!=', NULL],
                            ['advanced_dining_reservations', '=', NULL],
                            ['final_payment_due', '!=', NULL]
                        ])->get()->sortByDesc('id');
                    }

                    if($fpd == "0" && $Adr == "false" && $fpdate == "true"){
                        $Alltrips = Trip::where([
                            ['user_id', '=', $uid],
                            ['Status', '=', 0],
                            ['fast_pass_date', '=', NULL],
                            ['advanced_dining_reservations', '=', NULL],
                            ['final_payment_due', '!=', NULL]
                        ])->get()->sortByDesc('id');
                    }

                    if($fpd == "0" && $Adr == "true" && $fpdate == "true"){
                        $Alltrips = Trip::where([
                            ['user_id', '=', $uid],
                            ['Status', '=', 0],
                            ['fast_pass_date', '=', NULL],
                            ['advanced_dining_reservations', '!=', NULL],
                            ['final_payment_due', '!=', NULL]
                        ])->get()->sortByDesc('id');
                    }

                    if($fpd == "0" && $Adr == "true" && $fpdate == "false"){
                        $Alltrips = Trip::where([
                            ['user_id', '=', $uid],
                            ['Status', '=', 0],
                            ['fast_pass_date', '=', NULL],
                            ['advanced_dining_reservations', '!=', NULL],
                            ['final_payment_due', '=', NULL]
                        ])->get()->sortByDesc('id');
                    }
                }

            }
            else
            {
                if($search != "" && $search != null){
                    $Alltrips = Trip::where([
                        ['Status', '=', 0],
                        ['reservation_number', '=', $search],
                    ])->orWhere([
                        ['Status', '=', 0],
                        ['customer_Id', '=', $search],
                    ])->get()->sortByDesc('id');
                }
                else{

                    if($fpd == "1" && $Adr == "true" && $fpdate == "true"){
                        $Alltrips = Trip::where([
                            ['Status', '=', 0],
                            ['fast_pass_date', '!=', NULL],
                            ['advanced_dining_reservations', '!=', NULL],
                            ['final_payment_due', '!=', NULL]
                        ])->get()->sortByDesc('id');
                    }

                    if($fpd == "0" && $Adr == "false" && $fpdate == "false"){
                        $Alltrips = Trip::where([
                            ['Status', '=', 0],
                            ['fast_pass_date', '=', NULL],
                            ['advanced_dining_reservations', '=', NULL],
                            ['final_payment_due', '=', NULL]
                        ])->get()->sortByDesc('id');
                    }

                    if($fpd == "1" && $Adr == "false" && $fpdate == "false"){
                        $Alltrips = Trip::where([
                            ['Status', '=', 0],
                            ['fast_pass_date', '!=', NULL],
                            ['advanced_dining_reservations', '=', NULL],
                            ['final_payment_due', '=', NULL]
                        ])->get()->sortByDesc('id');
                    }

                    if($fpd == "1" && $Adr == "true" && $fpdate == "false"){
                        $Alltrips = Trip::where([
                            ['Status', '=', 0],
                            ['fast_pass_date', '!=', NULL],
                            ['advanced_dining_reservations', '!=', NULL],
                            ['final_payment_due', '=', NULL]
                        ])->get()->sortByDesc('id');
                    }

                    if($fpd == "1" && $Adr == "false" && $fpdate == "true"){
                        $Alltrips = Trip::where([
                            ['Status', '=', 0],
                            ['fast_pass_date', '!=', NULL],
                            ['advanced_dining_reservations', '=', NULL],
                            ['final_payment_due', '!=', NULL]
                        ])->get()->sortByDesc('id');
                    }

                    if($fpd == "0" && $Adr == "false" && $fpdate == "true"){
                        $Alltrips = Trip::where([
                            ['Status', '=', 0],
                            ['fast_pass_date', '=', NULL],
                            ['advanced_dining_reservations', '=', NULL],
                            ['final_payment_due', '!=', NULL]
                        ])->get()->sortByDesc('id');
                    }

                    if($fpd == "0" && $Adr == "true" && $fpdate == "true"){
                        $Alltrips = Trip::where([
                            ['Status', '=', 0],
                            ['fast_pass_date', '=', NULL],
                            ['advanced_dining_reservations', '!=', NULL],
                            ['final_payment_due', '!=', NULL]
                        ])->get()->sortByDesc('id');
                    }

                    if($fpd == "0" && $Adr == "true" && $fpdate == "false"){
                        $Alltrips = Trip::where([
                            ['Status', '=', 0],
                            ['fast_pass_date', '=', NULL],
                            ['advanced_dining_reservations', '!=', NULL],
                            ['final_payment_due', '=', NULL]
                        ])->get()->sortByDesc('id');
                    }
                }
            }
        }
        else
        {
            if($fpd == "1" && $Adr == "true" && $fpdate == "true"){
                    $Alltrips = Trip::where([
                        ['user_id', '=', $user->id],
                        ['Status', '=', 0],
                        ['fast_pass_date', '!=', NULL],
                        ['advanced_dining_reservations', '!=', NULL],
                        ['final_payment_due', '!=', NULL]
                    ])->get()->sortByDesc('id');
                }

                if($fpd == "0" && $Adr == "false" && $fpdate == "false"){
                    $Alltrips = Trip::where([
                        ['user_id', '=', $user->id],
                        ['Status', '=', 0],
                        ['fast_pass_date', '=', NULL],
                        ['advanced_dining_reservations', '=', NULL],
                        ['final_payment_due', '=', NULL]
                    ])->get()->sortByDesc('id');
                }

                if($fpd == "1" && $Adr == "false" && $fpdate == "false"){
                    $Alltrips = Trip::where([
                        ['user_id', '=', $user->id],
                        ['Status', '=', 0],
                        ['fast_pass_date', '!=', NULL],
                        ['advanced_dining_reservations', '=', NULL],
                        ['final_payment_due', '=', NULL]
                    ])->get()->sortByDesc('id');
                }

                if($fpd == "1" && $Adr == "true" && $fpdate == "false"){
                    $Alltrips = Trip::where([
                        ['user_id', '=', $user->id],
                        ['Status', '=', 0],
                        ['fast_pass_date', '!=', NULL],
                        ['advanced_dining_reservations', '!=', NULL],
                        ['final_payment_due', '=', NULL]
                    ])->get()->sortByDesc('id');
                }

                if($fpd == "1" && $Adr == "false" && $fpdate == "true"){
                    $Alltrips = Trip::where([
                        ['user_id', '=', $user->id],
                        ['Status', '=', 0],
                        ['fast_pass_date', '!=', NULL],
                        ['advanced_dining_reservations', '=', NULL],
                        ['final_payment_due', '!=', NULL]
                    ])->get()->sortByDesc('id');
                }

                if($fpd == "0" && $Adr == "false" && $fpdate == "true"){
                    $Alltrips = Trip::where([
                        ['user_id', '=', $user->id],
                        ['Status', '=', 0],
                        ['fast_pass_date', '=', NULL],
                        ['advanced_dining_reservations', '=', NULL],
                        ['final_payment_due', '!=', NULL]
                    ])->get()->sortByDesc('id');
                }

                if($fpd == "0" && $Adr == "true" && $fpdate == "true"){
                    $Alltrips = Trip::where([
                        ['user_id', '=', $user->id],
                        ['Status', '=', 0],
                        ['fast_pass_date', '=', NULL],
                        ['advanced_dining_reservations', '!=', NULL],
                        ['final_payment_due', '!=', NULL]
                    ])->get()->sortByDesc('id');
                }

                if($fpd == "0" && $Adr == "true" && $fpdate == "false"){
                    $Alltrips = Trip::where([
                        ['user_id', '=', $user->id],
                        ['Status', '=', 0],
                        ['fast_pass_date', '=', NULL],
                        ['advanced_dining_reservations', '!=', NULL],
                        ['final_payment_due', '=', NULL]
                    ])->get()->sortByDesc('id');
                }
        }

        $todoTrips = new Collection();

        $list = new Collection();
        $i = 0;

        $data = [];
        $titles = "";

            foreach ($Alltrips as $trip)
            {

                $data[] = [
                    'start' => $trip->checkin_date,
                    'end' => $trip->checkout_date,
                    'id' => $trip->id,
                    'title' => $trip->customer['first_name'].' '.$trip->customer['last_name']. "\n". $trip->destination. "\n" .$trip->reservation_number. "\n". $trip->fast_pass_date ."\n". $trip->advanced_dining_reservations. "\n". $trip->final_payment_due,
                    'color' => "#A8D2E0",
                    'reservation_number' => $trip->reservation_number,
                    'customer_name' => $trip->customer['first_name'].' '.$trip->customer['last_name'],
                    'customer_Id' => $trip->customer['id'],
                    'fastpassdate' => $trip->fast_pass_date,
                    'advancediningreservations' => $trip->advanced_dining_reservations,
                    'finalduedate' => $trip->final_payment_due,
                ];
            }

        return Response::json($data);
    });

    //ajaxcall route taking from tripCheckInReport.blade.php
    Route::get('savereports','HomeController@savereports');
    Route::get('saveexportreport','HomeController@saveexportreport');

    Route::post('save-report', 'HomeController@postTripsSaveReport');

    Route::get('commisions-list', 'HomeController@getCommision');
    Route::post('commisions-list', 'HomeController@postCommision');
    
    Route::delete('commisions/delete/{id}', 'HomeController@deleteReport');

    Route::get('commisions/saved-reports/{id}', 'HomeController@ViewReports');
    Route::get('commisions/edit/{id}', 'HomeController@editReports');
    
    Route::post('statusChange','HomeController@PoststatusChange');
    Route::post('AdminstatusChange','HomeController@AdminstatusChange');
    Route::post('cancel-request/{id}', 'HomeController@Postcancelrequest');
    Route::get('my-commission', 'HomeController@PostMyCommission');

    Route::get('commisions/history/{id}', 'HomeController@HistoryReports');

    Route::post('ChecktoPaid', 'HomeController@PostChecktoPaid');

    // Route::get('auditlogs/abc', 'HomeController');
});




Route::group(['middleware' => ['web','App\Http\Middleware\Admin']], function () {


    Route::get('user/list', 'HomeController@listUsers');
    Route::get('user/add', 'HomeController@addUser');
    Route::post('user/save', 'HomeController@saveUser');
    Route::get('user/edit/{id}', 'HomeController@editUser');
    Route::post('user/update/{id}', 'HomeController@updateUser');
    Route::delete('user/delete/{id}', 'HomeController@deleteUser');

    Route::get('commission-agents', 'HomeController@commissionAgents');
    Route::get('commission-reports', 'HomeController@commissionReports');    
    Route::post('getcommission-reports', 'HomeController@getcommissionReports');
    
    Route::get('walt-disney-world-reports', 'HomeController@waltDisneyWorld');
    Route::post('walt-disney-world-commission', 'HomeController@getwaltDisneyWorldcommissions');

    Route::get('disneyland-resort-reports', 'HomeController@disneylandResort');
    Route::post('disneyland-resort-commission', 'HomeController@getdisneylandResortcommissions');

    Route::get('disney-cruise-line-reports', 'HomeController@disneyCruiseLine');
    Route::post('disney-cruise-line-commission', 'HomeController@getdisneyCruiseLinecommissions');

    Route::get('commission-destinations', 'HomeController@commissionDestinations');
    Route::post('commission-destination-reports', 'HomeController@commissionDestinationReports');    
    Route::post('getcommission-destination-reports', 'HomeController@getcommissionDestinationReports');


    Route::get('destination/list', 'HomeController@listDestinations');
    Route::get('destination/add', 'HomeController@addDestination');


    Route::post('destination/save', 'HomeController@saveDestination');

     // Route::post('destination/save', 'HomeController@saveCategorydestination');

     Route::post('destination/saveCategory', 'HomeController@savedestinationcategory');

    // GraphRoute
     // Route::get('/home', 'HomeController@getCurrentYearSales');

    Route::get('destination/edit/{id}', 'HomeController@editDestination');
    Route::post('destination/update/{id}', 'HomeController@updateDestination');
    Route::delete('destination/delete/{id}', 'HomeController@deleteDestination');
    Route::post('hotel/save', 'HomeController@saveHotel');
    Route::get('hotel/edit/{id}', 'HomeController@editHotel');
    Route::post('hotel/edit', 'HomeController@updateHotel');
    Route::delete('hotel/delete', 'HomeController@deleteHotel');

    Route::get('auditlogs/list', 'HomeController@listAuditLogs');
    Route::delete('auditlog/delete/{id}', 'HomeController@delete');


    Route::get('/ajax-hotelUpdate',function(){
        $id = Input::get('hotel_id');
        $hotel = Hotel::find($id);
        $categories = HotelCategory::where('destination_id', '=', $hotel->destination_id);
        return Response::json($hotel);
    });


    Route::post('category/save', 'HomeController@saveCategory');
    Route::post('category/edit', 'HomeController@updateCategory');
    Route::delete('category/delete', 'HomeController@deleteCategory');

    Route::get('/ajax-categoryUpdate',function(){
        $id = Input::get('category_id');
        $category = HotelCategory::find($id);
        return Response::json($category);
    });


    Route::get('diningplan/list', 'HomeController@listdiningplans');
    Route::get('diningplan/edit/{id}', 'HomeController@editdiningplan');
    Route::get('diningplan/edit/{id}', 'HomeController@editdiningplan');
    Route::delete('diningplan/delete/{id}', 'HomeController@deletediningplan');


    Route::get('calculator-year', 'HomeController@addCalculatorYear');
    Route::post('calculator-year', 'HomeController@postCalculatorYear');
    Route::delete('calculator-year/delete/{id}', 'HomeController@deleteCalculatorYear');

    Route::get('getagency-total-revenue', 'HomeController@getAgencyTotalRevenue');
    Route::post('postagency-total-revenue', 'HomeController@postAgencyTotalRevenue');

    // Route::get('auditlogs/abc', 'HomeController@abc');
    // //Route::post('auditlogs/abc', 'HomeController@abc');

    // //Route::post('calender', "HomeController@Calenders");
    // Route::get('calenders-list', "HomeController@getcalenders");
    // Route::get('get-ajaxTrips', "HomeController@getAjaxTrips");

    // //Route::get('/ajax-GetTrips',function(){
    // Route::get('/ajax-GetTrips',function(){
    //    // print_r("Call Recieved!");
    //    // exit();
    //     $user = Auth::user();

    //     if($user->role == 1)
    //     {

    //         $Alltrips = Trip::where('Status', '=', 0)->get()->sortByDesc('id');
    //     }
    //     else
    //     {
    //         $Alltrips = Trip::where([
    //             ['user_id', '=', $user->id],
    //             ['Status', '=', 0],
    //         ])->get()->sortByDesc('id');
    //     }

    //     $todoTrips = new Collection();


    //     //$obj = new TripsDTO();
    //     $list = new Collection();
    //     $i = 0;

    //     $data = [];
    //     $titles = "";
    //         foreach ($Alltrips as $trip)
    //         {

    //             $data[] = [
    //                 'start' => $trip->checkin_date,
    //                 'end' => $trip->checkout_date,
    //                 'title' => $trip->customer['first_name'].' '.$trip->customer['last_name']. "\n". $trip->destination. "\n" .$trip->reservation_number,
    //                 'color' => "#A8D2E0",
    //             ];
    //         }

    //     return Response::json($data);
    // });

});