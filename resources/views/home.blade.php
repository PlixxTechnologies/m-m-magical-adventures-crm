@extends('admin.layout.dashboardx')

@section('content')
<!-- Datatables JS FILES -->

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.print.min.js"></script>

<style type="text/css">
    .todo-list{
        margin: 5px;
    }
    /*.navbar-brand{
        float: none;
        line-height: 0px;
        padding-top: 30px;
    }*/
</style>

<!-- /Datatables JS FILES -->
<!-- Quick stats boxes -->

 <!-- Page header -->
<div class="col-sm-12 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="{{ url('/home') }}">
                <em class="fa fa-home"></em>
            </a></li>
            <li class="active">Dashboard</li>
        </ol>
    </div>

    <div class="container-fluid"> 
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Dashboard</h1>
            </div>
        </div>
   </div>

   <!-- My TODO Lists -->
   @if(Auth::user()->role == 0)
   <div class="container-fluid no-padding">
        <div class="panel panel-default">
            <div class="panel-heading">
               My To-do List
                <span class="pull-right clickable panel-toggle panel-button-tab-left">
                    <em class="fa fa-toggle-up" id="toggle-up" onclick="toggle()"></em>
                </span>
            </div>

            <div class="panel-body" id="toggle-body" style="display:block;">
                @foreach($todolistings as $todolisting)
                <ul class="todo-list">
                    <li class="todo-list-item">
                        <div class="checkbox">
                            <label for="checkbox-1">{{ $todolisting->id }}</label>
                            <label for="checkbox-2">{{ $todolisting->tasks }}</label>
                        </div>
                        <div class="pull-right action-buttons">
                            <form style="display:inline" method="post" action="{{ url('delete') }}/{{$todolisting->id }}" onsubmit="return confirm('Are you sure you want to delete this Task?');">
                                {!! csrf_field() !!}
                                {{ method_field('DELETE') }}
                                <button type="submit" class="btn-danger btn icon-bin"></button>
                            </form>
                        </div>
                    </li>
                </ul>
                @endforeach
            </div>
            
            <form action="{{ url('todolistings/save') }}" method="post">
                {!! csrf_field() !!}
                <div class="panel-footer">
                    <div class="input-group">
                        <input id="btn-input" type="text" name="tasks" class="form-control" placeholder="Add new task" />
                        <span class="input-group-btn">
                            <button type="submit" class="btn btn-primary btn-md">Add</button>
                        </span>
                    </div>
                </div>
            </form>
        </div>
    </div>
   @endif
   <!-- /My TODO Lists -->

    <!--Agent Sales Goals-->
    <div class="container-fluid no-padding"> 
         <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-body border-top-primary">
                    <div class="text-center">
                        <h1 class="no-margin"><b>Agent Sales Goal</b></h1>
                        <h3 class="content-group-sm text-muted"><code><b>${{ number_format(Auth::user()->sales_goal, 2) }}</b></code></h3>                             
                    </div>

                    <div class="progress">
                        <div class="progress-bar bg-blue-1000" style="width: {{ $agentsalespercentage }}%">
                            <b>{{ $agentsalespercentage }}% Complete</b>
                        </div>
                    </div>
                </div>
            </div>
         </div>
    </div> 
    <!--Agent Sales Goals End-->


    <!-- CHART STARTS -->
    @if(Auth::user()->role == 0)
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Total Sales of Year
                </div>
                <div class="panel-body">
                    <!-- <div class="canvas-wrapper">
                        <canvas class="main-chart" id="line-chart" height="200" width="100%"></canvas>
                    </div>
                    <form  id="agentGraphId" action="{{ url('/home').'/'.Auth::user()->id }}" method="POST">
                        <input type="hidden" name="agentId" id="agentId" value="{{ Auth::user()->id }} ">
                    </form> -->
                    <div class="chart-container">
                        <div class="chart" id="google-line"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
    <!-- CHART END -->


    <div class="container-fluid">
        <div class="row">
            <div style="padding-top:20px; padding-bottom:20px; padding-right:20px; padding-left:20px;"  class="panel panel-body">
                <h1><b><center>Upcoming TO DO LIST</center></b></h1>
                <hr>
                <div style="overflow-x:auto;">
                    <table id="example" class="display nowrap" style="width:100%">  
                        <thead>
                            <tr>
                                <th data-sortable="true">Customer Name</th>
                                <th data-sortable="true">Fast Pass Date</th>
                                <th data-sortable="true">Advanced Dining Reservations</th>
                                <th data-sortable="true">Itinerary Tip Sheets</th>
                                <th data-sortable="true">Final Payment Due</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($todolistcount > 0)
                                 @foreach($todolist as $todo)  
                                    <tr>
                                        @if($todo->customer == null)
                                            <td>
                                                No Customer
                                            </td>
                                        @else
                                            <td>
                                                {{ $todo->customer->first_name }} {{ $todo->customer->last_name }}
                                            </td>
                                        @endif
                                        
                                        @if($todo->trip == null)
                                        <td>
                                            
                                        </td>
                                        <td>
                                            
                                        </td>
                                        <td>
                                            
                                        </td>
                                        <td>
                                            
                                        </td>
                                        @else
                                        <td>
                                            {{ $todo->trip->fast_pass_date }}
                                        </td>
                                        <td>
                                            {{ $todo->trip->advanced_dining_reservations }}
                                        </td>
                                        <td>
                                            {{ $todo->trip->itinerary_tip_sheets }}
                                        </td>
                                        <td>
                                            {{ $todo->trip->final_payment_due }}
                                        </td>
                                        <td>
                                            <a href="{{ url('/toggleTodo/'.$todo->id) }}" class="btn btn-success">Mark as Complete</a>
                                        </td>
                                        @endif
                                    </tr>
                                 @endforeach
                            @endif   
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div> 

    <div class="container-fluid">
        <div class="row">
            <div class="panel panel-body"> 
                <h1><b><center>Agent Stats</center></b></h1>
                <hr>
                <div class="row">
                    <div class="col-lg-4">
                        <h3><strong>This Month's Stats</strong></h3>
                    </div>

                     <div class="col-lg-4">
                        <div class="panel bg-teal-400" style="background-color:#FFB53E">
                            <div class="panel-body">
                                <center><em class="fa fa-users fa-lg"></em></center>
                                <h4 class="no-margin"><center>{{ $customers_month }}</center></h4>
                                <center>Customers</center>
                            </div>
                        </div>
                    </div>                    

                    <div class="col-lg-4">
                        <div class="panel bg-blue-400" style="background-color:#7FB7E6">
                            <div class="panel-body">
                                <center><em class="fa fa-suitcase fa-lg"></em></center>
                                <h4 class="no-margin"><center>{{ $trips_month }}</center></h4>
                                <center>Trips</center>
                            </div>
                        </div>
                    </div>                    
                </div>

                <div class="row">
                    <div class="col-lg-4">
                         <h3><strong>Overall Stats</strong></h3>
                    </div>

                    <div class="col-lg-4">
                        <div class="panel bg-teal-400" style="background-color:#FFB53E">
                            <div class="panel-body">
                                <center><em class="fa fa-users fa-lg"></em></center>
                                <h4 class="no-margin"><center>{{ $total_customer }}</center></h4>
                                <center>Customers</center>
                            </div>
                        </div>
                    </div>             

                    <div class="col-lg-4">
                        <div class="panel bg-blue-400" style="background-color:#7FB7E6">
                            <div class="panel-body">
                                <center><em class="fa fa-suitcase fa-lg"></em></center>
                                <h4 class="no-margin"><center>{{ $total_trips }}</center></h4>
                                <center>Trips</center>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <!--Counter 1-->
    <div class="container-fluid no-padding"> 
        <div class="panel panel-container"> 
            <h1>
                <b>
                    <center>Commission</center>
                </b>
            </h1>
            <hr>
            <div class="row">
                <div class="col-sm-12 col-md-6 col-lg-6 no-padding">
                    <div class="panel panel-teal panel-widget border-right">
                        <div class="row no-padding"><em class="fa fa-xl fa-money color-blue"></em>
                            <div class="large">${{ number_format($month_expected_commission, 2) }}</div>
                            <div class="" style="overflow:auto;">This Month's Total Expected Commission</div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-6 no-padding">
                    <div class="panel panel-blue panel-widget border-right">
                        <div class="row no-padding"><em class="fa fa-xl fa-money color-orange"></em>
                            <div class="large">${{ number_format($year_total_commission, 2) }}</div>
                            <div class="">Total Commission for {{ date("Y") }}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
    </div> 
<!--Counter 1 Ends-->

</div>

<!-- Javascript -->

<script type="text/javascript">
    var a = $('#agentId').val();
    if(a != ""){
        $('agentGraphId').submit();
    }
</script>

<!-- TODO List -->
<script type="text/javascript">
    function toggle(){
        var x = document.getElementById('toggle-up');
        var y = document.getElementById('toggle-body');
        if(y.style.display === 'block'){
            y.style.display = 'none';
        }
        else{
            y.style.display = 'block';
        }
    }
</script>

<!-- Graph -->


@endsection