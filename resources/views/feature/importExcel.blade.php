 @extends('admin.layout.dashboardx')

@section('content')

<!-- Page header -->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="#">
                    <em class="fa fa-home"></em>
                </a></li>
                <li class="active">Dashboard</li>
            </ol>
        </div><!--/.row-->
    <div class="container-fluid"> 
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Customer Import</h1>
            </div>
        </div>  <!-- /page header -->
   </div>

<div class="container-fluid">
		<form style="border: 4px solid #FFB53E;margin-top: 15px;padding: 10px; border-radius:12px;" action="{{ url('import-excel') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
			 {{ csrf_field() }}
			<input type="file" name="import_file" /><br>
			<button class="btn btn-primary">Import File</button>
		</form>
		<br><br>
	</div>
</div>
@endsection