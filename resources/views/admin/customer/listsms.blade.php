@extends('admin.layout.dashboardx')
@section('content')
<!-- Datatables JS FILES -->

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.print.min.js"></script>

<!-- /Datatables JS FILES -->
<!-- Page header -->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="{{ url('/home') }}">
                    <em class="fa fa-home"></em>
                </a></li>
                <li class="active">List Text Messages</li>
            </ol>
        </div><!--/.row-->
    <div class="container-fluid"> 
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">List Text Messages</h1>
            </div>
        </div>  <!-- /page header -->
   </div>

<!-- Basic initialization -->
                    <div style="padding-top:20px; padding-bottom:20px; padding-right:20px; padding-left:20px; overflow-x: auto;" class="panel panel-flat">

                       <table id="example" class="display nowrap" style="width:100%">
                            <thead>
                                <tr>
                                <th data-sortable="true">Agent Name</th>
                                <th data-sortable="true">Customer Name</th>
                                <th data-sortable="true">Country Code</th>
                                <th data-sortable="true">Phone Number</th>
                                <th data-sortable="true">Message</th>
                            </tr>
                            </thead>
                            <tbody>
                                  @foreach($textmessages as $textmessage)
                                                      
                                <tr>
                                    @if($textmessage->user == null)
                                    <td>
                                         No Agent
                                    </td>
                                    @else
                                    <td>
                                         {{ $textmessage->user->name }}
                                    </td>
                                    @endif

                                    @if($textmessage->customer == null)
                                        <td>
                                            No Customer
                                        </td>
                                    @else
                                        <td>
                                            {{ $textmessage->customer->first_name }} {{ $textmessage->customer->last_name }}
                                        </td>
                                    @endif
                                    <td>
                                        {{ $textmessage->country_code }}
                                    </td>
                                   
                                    <td>
                                        {{ $textmessage->number }}
                                    </td>
                                    <td>
                                        {{ $textmessage->message }}
                                    </td>
                                  </tr>
                            @endforeach                               
                            </tbody>
                        </table>
                    </div>
                </div>
                    <!-- /basic initialization -->   
@endsection