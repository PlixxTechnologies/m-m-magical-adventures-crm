@extends('admin.layout.dashboardx')

@section('content')
<!-- Datatables JS FILES -->

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.print.min.js"></script>

<!-- /Datatables JS FILES -->
<style>
.red{
    color: red;
}
</style>
<!-- Page header -->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="{{ url('/home') }}">
                    <em class="fa fa-home"></em>
                </a></li>
                <li class="active">Edit Customer</li>
            </ol>
        </div><!--/.row-->
    <div class="container-fluid"> 
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Edit Customer</h1>
            </div>
        </div>  <!-- /page header -->
   </div>

<!--Delete Guest modal -->
    <div id="deleteGuestModal" class="modal">
        <div class="modal-dialog">
            <form  method="post" action="{{ url('guest/delete') }}"> 
                {!! csrf_field() !!}
                {{ method_field('DELETE') }}
                <div class="modal-content">
                    <div class="modal-header bg-danger">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h6 class="modal-title">Delete Guest</h6>
                    </div>

                    <div class="modal-body">
                        <p>Are you sure you want to delete this guest?</p>
                    </div>

                    <input type="hidden" id="deleteGuestId" name="id" />

                    <div class="modal-footer">
                        <button type="button" class="btn btn-link legitRipple" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-danger legitRipple">Confirm</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <!--Update Guest modal -->
    <div id="updateGuestModal" class="modal">
        <div class="modal-dialog">
            <form  method="post" action="{{ url('guest/edit') }}"> 
                {!! csrf_field() !!}
                {{ method_field('POST') }}
                <div class="modal-content">
                    <div class="modal-header bg-primary">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h6 class="modal-title">Update Guest</h6>
                    </div>

                    <div class="modal-body">
                        <div class="form-group">
                            <label><span class="red">*</span>First Name:</label>
                            <input type="text" id="guest_first_name" name="guest_first_name" class="form-control" required>
                        </div>
                         <div class="form-group">
                            <label><span class="red">*</span>Last Name:</label>
                            <input type="text" id="guest_last_name" name="guest_last_name" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Birthdate:</label>
                            <input onfocus="GuestBirthDateType()" type="datetime" name="guest_birth_date"
                             id="guest_birth_date" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Valid Passport:</label>
                            <select id="guest_passport" name="guest_passport" class="form-control">
                               <!--  <option value="">Choose One....</option>
                                <option value="Yes">Yes</option>
                                <option value="No">No</option> -->
                            </select>
                        </div> 
                        <div class="form-group">
                            <label>Expiration Date:</label>
                            <input onfocus="GuestExpireDateType()" type="datetime" name="guest_expire_date"
                             id="guest_expire_date" class="form-control">
                        </div>
                    </div>

                    <input type="hidden" id="updateGuestId" name="id" />

                    <div class="modal-footer">
                        <button type="button" class="btn btn-link legitRipple" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary legitRipple">Update</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">

                        <form action="{{ url('customer/edit').'/'.$customer->id }}" method="post" >
                            {!! csrf_field() !!}

                            <input type="hidden" value="{{ $customer->disney }}" id="disney">

                            @if ($authuser->role == 1)
                                <div class="form-group">
                                    <label><span class="red">*</span>Agent Name:</label>
                                    <select name="userId" class="form-control" required>
                                    @foreach($allusers as $user)
                                        @if($user->id == $customer->user_id)
                                            <option value="{{ $user->id }}" selected>{{ $user->name }}</option>
                                        @endif    
                                        <option value="{{ $user->id }}">{{ $user->name }}</option>
                                    @endforeach
                                    </select>
                                </div>
                            @else
                                <div class="form-group">
                                    <label>Agent Name:</label>
                                    <input type="text"  class="form-control" value="{{ $user->name }}" readonly>
                                </div>
                            @endif         

                            <div class="form-group">
                                <label><span class="red">*</span>First Name:</label>
                                <input type="text" name="first_name" value=" {{ $customer->first_name }}" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label><span class="red">*</span>Last Name:</label>
                                <input type="text" name="last_name" value=" {{ $customer->last_name }}" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label><span class="red">*</span>Email:</label>
                                <input type="email" name="email" value=" {{ $customer->email }}" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label>Country Code:</label>
                                <input type="text" value="+1"  class="form-control" readonly="">
                            </div>
                            <div class="form-group">
                                <label><span class="red">*</span>Phone Number:</label>
                                <input type="text" name="phone_no" value=" {{ $customer->phone_no }}" class="form-control" required>
                            </div>
                             <div class="form-group">
                                <label><span class="red">*</span>Address 1:</label>
                                <input type="text" name="address1" value=" {{ $customer->address1 }}" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label>Address 2:</label>
                                <input type="text" name="address2" value=" {{ $customer->address2 }}" class="form-control">
                            </div>
                            <div class="form-group">
                                <label><span class="red">*</span>City:</label>
                                <input type="text" name="city" value=" {{ $customer->city }}" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label><span class="red">*</span>State:</label>
                                <input type="text" maxlength="2" id="sessionNo" onkeypress="return isNumberKey(event)" name="state" class="form-control" value=" {{ $customer->state }}" required>

                            </div>
                            <div class="form-group">
                                <label><span class="red">*</span>Zip:</label>
                                <input type="text" name="zip" value=" {{ $customer->zip }}" class="form-control" required>
                            </div> 
                             <div class="form-group">
                                <label>Birthdate:</label>
                                <input id="birth_date" onfocus="BirthDateType()" type="datetime" name="birth_date" value=" {{ $customer->birth_date }}" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Valid Passport:</label>
                                <select name="passport" class="form-control">   
                                    <option @if($customer->passport == null) selected @endif value=""> 
                                    Choose One....</option>
                                    <option @if($customer->passport == "Yes") selected @endif value="Yes">
                                    Yes</option>
                                    <option @if($customer->passport == "No") selected @endif value="No" ">
                                    No</option>
                                </select>
                            </div> 
                            <div class="form-group">
                                <label>Expiration Date:</label>
                                <input id="expire_date" onfocus="ExpireDateType()" type="datetime" name="expire_date" value=" {{ $customer->expire_date }}" class="form-control">
                            </div>     
                            <div class="form-group">
                                <label>My Disney Experience User Name:</label>
                                <input type="text" name="disney_experience_username" value=" {{ $customer->disney_experience_username }}" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>My Disney Experience Password:</label>
                                <input type="text" name="disney_experience_password" value=" {{ $customer->disney_experience_password }}" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Referral:</label>
                                <input type="text" name="referral" value=" {{ $customer->referral }}" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Notes:</label>
                                <input type="text" name="notes" value=" {{ $customer->notes }}" class="form-control">
                            </div>

                             <!-- Customer Guest Table -->
                             <input type="hidden" name="count" id="count" value=0 />
                             @if(count($allUserGuests) > 0)

                              <hr />
                              @foreach($allUserGuests as $guest)
                                <div class="row">
                                    <div class="col-lg-10 text-left">
                                        <h5><strong>Guest No {{ ++$count }}</strong></h5>

                                    </div>
                                    @if($count == 2)
                                    <div class="col-lg-2 ">
                                        <h5><strong>Modify Guest</strong></h5>
                                    </div>
                                    @endif
                                </div>
                                <div class="row">
                                    <div class="col-lg-2">
                                        <h5><strong>First Name</strong></h5>
                                        <h6>{{ $guest->guest_first_name }}</h6>
                                    </div>
                                    <div class="col-lg-2">
                                         <h5><strong>Last Name</strong></h5>
                                        <h6>{{ $guest->guest_last_name }}</h6>
                                    </div>
                                     <div class="col-lg-2">
                                         <h5><strong>Birth Date</strong></h5>
                                        <h6>{{ $guest->guest_birth_date }}</h6>
                                    </div>
                                    <div class="col-lg-2">
                                         <h5><strong>Valid Passport</strong></h5>
                                        <h6>{{ $guest->guest_passport }}</h6>
                                    </div>
                                    <div class="col-lg-2">
                                         <h5><strong>Expiration Date</strong></h5>
                                        <h6>{{ $guest->guest_expire_date }}</h6>
                                    </div>
                                    <div class="col-lg-2">
                                       <br/>
                                       <button type="button" onclick="updateGuest({{$guest->id}})" class="btn btn-sm btn-primary">Edit</button>
                                       <button type="button" onclick="deleteGuest({{$guest->id}})" class="btn btn-danger btn-sm">Remove</button>
                                    </div>
                                </div>
                               @endforeach

                            @endif

                             <input type="hidden" name="countHeading" id="countHeading" value="{{ $count }}" />

                              <div id="guestSection">
                                
                              </div>

                            @if(count($allUserGuests) == 0)
                            <div class="form-group">
                                <button onclick="addGuest()" type="button" class="btn btn-success">Add Guest</button>
                            </div>  
                            @else
                            <div style="margin-right: 6em;" class="form-group text-right">
                                <button onclick="addGuest()" type="button" class="btn btn-success">Add Guest</button>
                            </div> 

                            @endif
                            <br />
                            <button type="submit" class="btn btn-primary pull-right">Update Customer</button>
                        </form>
                    </div>
                </div>
            </div>
        </div><!--/.row-->
    </div>
    
</div>

<script type="text/javascript">

    function BirthDateType(){
        $("#birth_date").attr('type', 'date');
    }

    function ExpireDateType(){
        $("#expire_date").attr('type', 'date');
    }

    function GuestBirthDateType(){
        $("#guest_birth_date").attr('type', 'date');
    }

    function GuestExpireDateType(){
        $("#guest_expire_date").attr('type', 'date');
    }

    function deleteGuest(id) {

        $('#deleteGuestId').val(id);
        $('#deleteGuestModal').modal('show');

    }

    function updateGuest(id) {

        $('#updateGuestId').val(id);

        $.get('/ajax-guestUpdate?guest_id='+id, function(data){

            console.log(data);

            $('#guest_first_name').val(data.guest_first_name);
            $('#guest_last_name').val(data.guest_last_name);
            $('#guest_birth_date').val(data.guest_birth_date);
            $('#guest_expire_date').val(data.guest_expire_date);

            if(data.guest_passport == null){
                $("#guest_passport").append('<option selected value="">Choose One....</option>');
                $("#guest_passport").append('<option value="Yes">Yes</option>');
                $("#guest_passport").append('<option value="No">No</option>');
            }
            else if(data.guest_passport == "Yes"){
                $("#guest_passport").append('<option value="">Choose One....</option>');
                $("#guest_passport").append('<option selected value="Yes">Yes</option>');
                $("#guest_passport").append('<option value="No">No</option>');
            }
            else if(data.guest_passport == "No"){
                $("#guest_passport").append('<option value="">Choose One....</option>');
                $("#guest_passport").append('<option value="Yes">Yes</option>');
                $("#guest_passport").append('<option selected value="No">No</option>');
            }
           
        });

        $('#updateGuestModal').modal('show');

    }

   var guestCount = 1;
   var headingCount =  document.getElementById('countHeading').value;
   headingCount = parseInt(headingCount) + 1;

   function addGuest(){

        var objTo = document.getElementById('guestSection');

        var guestDiv = document.createElement("div");
        guestDiv.setAttribute("class", "form-group removeclass" + guestCount);

        var removeGuestDiv = 'removeclass' + guestCount;
       
        var guestHeading = '<h3><strong>Guest No '+ headingCount +':</strong></h3>';

        var firstname = '<div class="form-group"><label><span class="red">*</span>First Name:</label><input type="text" name="guest_first_name'+ guestCount +'" id="guest_first_name'+ guestCount +'"  class="form-control" required></div>';

        var lastname = '<div class="form-group"><label><span class="red">*</span>Last Name:</label><input type="text" name="guest_last_name'+ guestCount +'" id="guest_last_name'+ guestCount +'"  class="form-control" required></div>';

        var birthdate = '<div class="form-group"><label>Birthdate:</label><input type="date" name="guest_birth_date'+ guestCount +'" id="guest_birth_date'+ guestCount +'"  class="form-control"></div>';

        var passport = '<div class="form-group"> <label>Valid Passport:</label><select name="guest_passport'+ guestCount +'" id="guest_passport'+ guestCount +'" class="form-control"><option value="">Choose One....</option><option value="Yes">Yes</option><option value="No">No</option></select></div>';


        var expirationdate = '<div class="form-group"><label>Expiration Date:</label><input type="date" name="guest_expire_date'+ guestCount +'" id="guest_expire_date'+ guestCount +'" class="form-control"></div>';

        var hiddenGuestId = '<input type="hidden" name="guest_no' + headingCount + '" id="guest_no' + headingCount + '" value='+ headingCount +' />';

        var deleteGuest = '<div class="form-group"><button onclick="removeGuest(' + guestCount + ');" type="button" class="btn btn-danger">Remove Guest</button></div>';

         guestDiv.innerHTML = guestHeading + firstname + lastname + birthdate + passport + expirationdate 
         + hiddenGuestId + '<br />';

        objTo.appendChild(guestDiv);

        document.getElementById('count').value = guestCount;

        guestCount++;
        headingCount++;

   }
</script>

@endsection