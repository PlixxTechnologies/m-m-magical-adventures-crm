@extends('admin.layout.dashboardx')
@section('content')
<!-- Datatables JS FILES -->

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.print.min.js"></script>

<!-- /Datatables JS FILES -->

 <!-- Page header -->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="{{ url('/home') }}">
                    <em class="fa fa-home"></em>
                </a></li>
                <li class="active">Trip History</li>
            </ol>
        </div><!--/.row-->
    <div class="container-fluid"> 
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Trip History</h1>
            </div>
        </div>  <!-- /page header -->
   </div>

<!-- Basic initialization -->
                    <div style="padding-top:20px; padding-bottom:20px; padding-right:20px; padding-left:20px; overflow-x: auto;"  class="panel panel-flat">
                         <a style="margin-left: 10px; margin-top: 10px" class="btn btn-primary btn-sm pull-left"  href="{{ url('customer/list')}}">
                                            Close
                                        </a>
                    
                       <center><h1>Customer Trip History</h1></center>

                       
                        <table id="example" class="display nowrap" style="width:100%">
                            <thead>
                                <tr>
                               
                                
                                <th data-sortable="true">Customer Name</th>
                               
                                <th data-sortable="true">Reservation Number</th>                               
                                <th data-sortable="true">Destination</th>
                                <th data-sortable="true">Travel Date</th>
                                <th data-sortable="true">Total Sale</th>
                                                             
                            </tr>
                            </thead>
                            <tbody>
                                                                          
                                      @foreach($trips as $trip)                                 

                                    <tr>

                                    <td>{{ $trip->customer->first_name }} {{ $trip->customer->last_name }}  </td>
                                    <td>
                                            <a target="_blank" href="{{ url('trip/view').'/'.$trip->id }}">
                                            {{ $trip->reservation_number }}
                                        </a>
                                        </td>
                                    <td>{{ $trip->destination }}</td>
                                    <td>
                                    {{ date("m", strtotime($trip->checkin_date)) }}-{{ date("d", strtotime($trip->checkin_date)) }}-{{ date("Y", strtotime($trip->checkin_date)) }}  
                                    </td>
                                    <td>{{ $trip->total_sale }}</td>                
                                  </tr>
                                     @endforeach         
                            </tbody>


                        </table>
                        
                       

                        
                    </div>
                    <!-- /basic initialization -->   
@endsection