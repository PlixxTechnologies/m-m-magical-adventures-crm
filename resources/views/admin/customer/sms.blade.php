@extends('admin.layout.dashboardx')
@section('content')
<!-- Datatables JS FILES -->

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.print.min.js"></script>

<!-- /Datatables JS FILES -->
<style type="text/css">
    .red{
        color: red;
    }
</style>
<!-- Page header -->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="{{ url('/home') }}">
                    <em class="fa fa-home"></em>
                </a></li>
                <li class="active">Send Text Message</li>
            </ol>
        </div><!--/.row-->
    <div class="container-fluid"> 
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Send Text Message</h1>
            </div>
        </div>  <!-- /page header -->
   </div>

    <div class="container-fluid">
        <br>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">

                        <form action="{{ url('sms') }}" method="post" >
                            {!! csrf_field() !!}
<!-- 
                            <div class="form-group">
                                <label><span class="red">*</span>Customers:</label>
                                <select name="customer_id" class="form-control">
                                    <option value="">Choose One....</option>
                                    @foreach($customers as $customer)
                                    <option value="{{ $customer->id }}">{{ $customer->first_name }} {{ $customer->last_name }}</option>
                                    @endforeach
                                </select>
                            </div>  --> 

                            <div class="form-group">
                                <label><span class="red">*</span>Customers:</label>
                                   <div class="multi-select-full">
                                       <!--  <select class="multiselect-select-all form-control" name="customer[]" multiple="multiple"> -->
                                            <select class="form-control selectpicker" name="customer[]" data-live-search="true" required=""> 
                                        @foreach($customers as $customer)
                                            <option value="{{ $customer->id }}">{{ $customer->first_name }} {{ $customer->last_name }}</option>
                                        @endforeach
                                        </select>
                                    </div>
                            </div>  
                            <div class="form-group">
                                <label>Message:</label>
                                <textarea class="form-control" name="msg" rows="4"></textarea>
                            </div>

                            <button type="submit" class="btn btn-primary pull-right">Send</button>
                        </form>
                    </div>
                </div>
            </div>
        </div><!--/.row-->
    </div>  <!--/.main-->
</div>
@endsection