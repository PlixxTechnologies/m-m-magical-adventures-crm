@extends('admin.layout.dashboardx')
@section('content')

<!-- Datatables JS FILES -->

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.print.min.js"></script>

<!-- /Datatables JS FILES -->

 <!-- Page header -->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="{{ url('/home') }}">
                    <em class="fa fa-home"></em>
                </a></li>
                <li class="active">All Customers</li>
            </ol>
        </div><!--/.row-->
    <div class="container-fluid"> 
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">All Customers</h1>
            </div>
        </div>  <!-- /page header -->
   </div>


<!-- Basic initialization -->
                    <div style="padding-top:20px; padding-bottom:20px; padding-right:20px; padding-left:20px; overflow-x: auto;" class="panel panel-flat">

                       <table id="example" class="display nowrap" style="width:100%">
                            <thead>
                                <tr>
                                    <th data-sortable="true">Action</th>
                                    <th data-sortable="true">Agent Name</th>
                                    <th data-sortable="true">Customer ID</th>
                                    <th data-sortable="true">First Name</th>
                                    <th data-sortable="true">Last Name</th>
                                    <th data-sortable="true">Email</th>
                                    <th data-sortable="true">Phone Number</th>
                                   <!--  <th data-sortable="true">Birth Date</th>
                                    <th data-sortable="true">Passport</th>
                                    <th data-sortable="true">Expire Date</th> -->                                                                
                                    <!-- <th data-sortable="true">Destination</th>
                                    <th data-sortable="true">Check In Date</th>
                                    <th data-sortable="true">Check Out Date</th>   -->                            
                                
                            </tr>
                            </thead>
                            <tbody>
                                  @foreach($customers as $customer)
                                                      
                                <tr>
                                    <td class="center">
                                         <a class="btn btn-info btn-sm" href="{{ url('customer/view').'/'.$customer->id }}" target="_blank">
                                            VIEW
                                        </a>
                                        <a class="btn btn-success btn-sm" href="{{ url('trip/create/').'/'.$customer->id }}">
                                            Add Trip
                                        </a>
                                        
                                        <a class="btn btn-info btn-sm" href="{{ url('customer/edit').'/'.$customer->id }}">
                                            EDIT
                                        </a>


                                           

                                         @if(Auth::user()->role == 1)
                                        <form style="display:inline" method="post" action="{{ url('customer/delete') }}/{{$customer->id}}" onsubmit="return confirm('Are you sure you want to delete this customer?');">
                                            {!! csrf_field() !!}
                                            {{ method_field('DELETE') }}
                                            <button type="submit" class="btn btn-danger btn-sm">DELETE</button>
                                        </form>

                                           
                                        @endif
                                        <a class="btn btn-primary btn-sm" target="_blank" href="  {{ url('customer/TripHistory').'/'.$customer->id }}">
                                            TRIP HiSTORY
                                        </a>

                                      
                                    </td>

                                    @if($customer->user == null)
                                    <td>
                                         No Agent
                                    </td>
                                    @else
                                    <td>
                                         {{ $customer->user->name }}
                                    </td>
                                    @endif
                                    <td>
                                        {{ $customer->id }}
                                    </td>
                                    <td>
                                        {{ $customer->first_name }}
                                    </td>
                                    <td>
                                        {{ $customer->last_name }}
                                    </td>
                                    <td>
                                        {{ $customer->email }}
                                    </td>

                                    <td>

                                        {{$customer->phone_no }}
                                    </td>
                                    <!-- <td>
                                        {{ $customer->birth_date }}
                                    </td>
                                    <td>
                                        {{ $customer->passport }}
                                    </td>                               
                                     <td>
                                        {{ $customer->expire_date }}
                                    </td> -->
                                  </tr>
                            @endforeach                               
                            </tbody>
                        </table>
                    </div>
                </div>
                    <!-- /basic initialization -->   

@endsection                