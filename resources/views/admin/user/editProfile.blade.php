@extends('admin.layout.dashboardx')

@section('content')
<!-- Datatables JS FILES -->

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.print.min.js"></script>

<!-- /Datatables JS FILES -->
<style>
.red{
    color: red;
}
</style>
@if (\Session::has('error'))
    <div class="alert alert-danger">
        <ul>
            <li>{!! \Session::get('error') !!}</li>
        </ul>
    </div>
@endif
<!-- Page header -->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="{{ url('/home') }}">
                    <em class="fa fa-home"></em>
                </a></li>
                <li class="active">Edit Agent Profile</li>
            </ol>
        </div><!--/.row-->
    <div class="container-fluid"> 
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Edit Agent Profile</h1>
            </div>
        </div>  <!-- /page header -->
   </div>
    <div class="container-fluid">
        <br>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">

                        <form action="{{ url('/user/p-edit-profile') }}" method="post">
                            {!! csrf_field() !!}
                            
                            <div class="form-group">
                                <label><span class="red">*</span>Name:</label>
                                <input type="text" name="name" value="{{ $user->name }}" class="form-control" required>
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label><span class="red">*</span>Email:</label>
                                <input type="email" name="email" value="{{ $user->email }}" class="form-control" required>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label><span class="red">*</span>Phone Number:</label>
                                <input type="text" name="phone" value="{{ $user->phone }}" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label><span class="red">*</span>Address:</label>
                                <input type="text" name="address" value="{{ $user->address }}" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label>Contract Signed Date:</label>
                                <input type="date" name="contract_date" value="{{ $user->contract_date }}" class="form-control"  disabled="">
                            </div>
                                               

                            <div class="form-group">
                                <label>Status:</label>
                                <select name="status" class="form-control" disabled="">
                                    <option>--Select Status--</option>
                                    <option value="active" @if($user->status=="active")selected @endif >Active</option>
                                    <option value="inactive" @if($user->status=="inactive")selected @endif>Inactive</option>
                                    <option value="terminated" @if($user->status=="terminated")selected @endif>Terminated</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label>My Mentor:</label>
                                <select name="user_id" class="form-control" disabled="">
                                    <option> --Select Mentor--</option>}
                                    
                                    @if($agentmentor != null)
                                    {
                                        <option value="{{$agentmentor->id}}" selected>{{$agentmentor->name}}</option>
                                    }

                                    @endif
                                </select>
                            </div>


                            <div class="form-group">
                                <label>My Commission:</label>
                                <select name="commission" class="form-control" disabled="">
                                    <option value="">Choose One</option>
                                    <option value="60" @if($user->commission == "60") selected @endif>60%</option>
                                    <option value="65"  @if($user->commission == "65") selected @endif>65%</option>
                                    <option value="70"  @if($user->commission == "70") selected @endif>70%</option>
                                    <option value="75"  @if($user->commission == "75") selected @endif>75%</option>
                                    <option value="80"  @if($user->commission == "80") selected @endif>80%</option>
                                    <option value="100"  @if($user->commission == "100") selected @endif>100%</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Sales Goal:</label>
                                <input type="text" name="sales_goal" value="{{ $user->sales_goal }}" class="form-control" disabled="">
                            </div>


                            <div class="form-group">
                                <label>My Anniversary Date :</label>
                                <input type="date" name="fdacs_date" class="form-control" value="{{$user->fdacs_date}}" disabled="">
                            </div>



                            <div id="changePasswordFields">
                                
                            </div>


                            <button type="submit" class="btn btn-primary pull-right">Update Agent</button>
                            <button type="button" class="btn btn-danger pull-left mr-5" onclick="getPasswordFields()">Change Password</button>
                        </form>

                    </div>
                </div>
            </div>
        </div><!--/.row-->
    </div>  <!--/.main-->
</div>

<script type="text/javascript">
    function getPasswordFields()
    {
        $('#changePasswordFields').html("");
        var passwordFields=`<div class="form-group">
                                <label><span class="red">*</span>Password:</label>
                                <input type="password" name="password" value="" minlength='6' min='6' class="form-control" placeholder="Enter New Password" required>
                               
                            </div>
                            <div class="form-group">
                                <label><span class="red">*</span>Confirm Password:</label>
                                <input id="password-confirm" type="password" minlength='6' min='6'  class="form-control" placeholder="Confirm Password" name="password_confirmation" required>

                            </div>`;
        $('#changePasswordFields').append(passwordFields);
    }
</script>

@endsection