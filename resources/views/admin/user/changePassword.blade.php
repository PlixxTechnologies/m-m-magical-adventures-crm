@extends('admin.layout.dashboardx')

@section('content')
<style>
.red{
    color: red;
}
</style>

@if (\Session::has('success'))
    <div class="alert alert-success">
        <ul>
            <li>{!! \Session::get('success') !!}</li>
        </ul>
    </div>
@endif


@if (\Session::has('error'))
    <div class="alert alert-danger">
        <ul>
            <li>{!! \Session::get('error') !!}</li>
        </ul>
    </div>
@endif



<!-- Page header -->
                <div class="page-header page-header-default">
                    <div class="page-header-content">
                        <div class="page-title">
                            <h4><span class="text-semibold">Change Password</span></h4>
                        </div>
                    </div>
                </div>
   <!-- /page header -->
    <div class="container">
        <br>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">

                        <form action="{{url('/p-changePassword/')}}" method="post">
                            {!! csrf_field() !!}
                            
                            <div class="form-group">
                                <label><span class="red">*</span>Old Password:</label>
                                <input type="password" name="oldPassword" placeholder="Old Passwod" class="form-control" required>
                               
                            </div>

                            <div class="form-group">
                                <label><span class="red">*</span>New Password:</label>
                                <input type="password" name="newPassword" min="6" minlength="6"  placeholder="New Password" class="form-control" required>
                               
                            </div>

                            <div class="form-group">
                                <label><span class="red">*</span>Confirm Password:</label>
                                <input type="password" name="confirmPassword" min="6" minlength="6"  placeholder="Confirm Passwod" class="form-control" required>
                               
                            </div>


                            <button type="submit" class="btn btn-primary pull-right">Change Password</button>
                        </form>

                    </div>
                </div>
            </div>
        </div><!--/.row-->
    </div>	<!--/.main-->

@endsection