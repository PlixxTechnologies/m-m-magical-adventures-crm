@extends('admin.layout.dashboardx')

@section('content')
<!-- Datatables JS FILES -->

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.print.min.js"></script>

<!-- /Datatables JS FILES -->

<!-- Page header -->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="{{ url('/home') }}">
                    <em class="fa fa-home"></em>
                </a></li>
                <li class="active">Edit Hotel</li>
            </ol>
        </div><!--/.row-->
    <div class="container-fluid"> 
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Edit Hotel</h1>
            </div>
        </div>  <!-- /page header -->
   </div>
   <div class="container-fluid">
        <br>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">

                        <form action="{{ url('hotel/edit')}}" method="post">
                            {!! csrf_field() !!}

                             <div class="form-group">
                        <label><span class="red">*</span>Hotel Name:</label>
                        <input type="text" value="{{$hotel->hotel_name}}" id="hotel_name" name="hotel_name" class="form-control" required>
                    </div>
                    @if($destination->name == "Walt Disney World" || $destination->name == "Carnival Cruise Line" || $destination->name == "Royal Caribbean Cruise Line")
                    <div class="form-group">
                         <label><span class="red">*</span>Category:</label>
                            <select name="category_id" class="form-control" required>
                                <option value="">Choose One....</option>
                                @foreach($categories as $category)
                                @if($category->id == $hotel->category_id)
                                <option selected value="{{ $category->id }}">{{ $category->category_name }}</option>
                                @else 
                                <option value="{{ $category->id }}">{{ $category->category_name }}</option>
                                @endif
                                @endforeach        
                            </select>
                    </div>      
                    @endif
                     <input type="hidden" value="{{$hotel->id}}" name="id" />

                    <button type="submit" class="btn btn-primary pull-right">Update</button>
                      </form>
                </div>
               

                    </div>
                </div>
            </div>
        </div><!--/.row-->
    </div>  <!--/.main-->
</div>

@endsection