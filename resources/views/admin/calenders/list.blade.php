@extends('admin.layout.dashboardx')
@section('content')

<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

<?php
use App\TripsDTO;
use Illuminate\Support\Collection;
?>

<style type="text/css">
    .fc-event, .fc-event-dot{
        background-color: #448ea7 !important;
        color: #000000 !important;
    }
</style>

<!-- Page header -->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="{{ url('/home') }}">
                    <em class="fa fa-home"></em>
                </a></li>
                <li class="active">Calendar</li>
            </ol>
        </div><!--/.row-->
    <div class="container-fluid"> 
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Calendar</h1>
            </div>
        </div>  <!-- /page header -->
   </div>

<div id="modal_theme_valuUP" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <form method="post">
            <div class="modal-content" style="width: max-content;">
                <div class="modal-header bg-primary text-center">
                    <h3 class="modal-title text-center">Trips Information</h3>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <div class="modal-body">
                    <input type="hidden" id="xyz" name="xyz" />
                    <div class="row">
                        <table class="table">
                          <thead>
                            <tr>
                              <th scope="col">Reservation Number</th>
                              <th scope="col">Customer Name</th>
                              <th scope="col">Customer ID</th>
                              <th scope="col">Fast Past Date</th>
                              <th scope="col">Advanced Dining Reservations</th>
                              <th scope="col">Final Payment Date</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <th scope="row">
                                <a  id="reser_number" href="" class="tr_Id"></a>
                                <p id="reser_numbers"></p>
                              </th>
                              <td>
                                <p id="cust_name"></p>
                              </td>
                              <td>
                                <p id="cust_id"></p>
                              </td>
                              <td>
                                <p id="fpd_id"></p>
                              </td>
                              <td>
                                <p id="adp_id"></p>
                              </td>
                              <td>
                                <p id="final_id"></p>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<div class="panel panel-flat" id="Information" style="display:none">
    <div class="panel-heading text-center">
        <h3>Search Information</h3>
    </div>
    <hr>
    <div class="panel panel-body">
        <div class="row">
            <div class="col-md-12" id="noTFound" style="display:none;">
                <h4 class="text-center">Data Not Found</h4>
            </div>
            <div class="col-md-12" id="noFound" style="display:block">
                <div class="row" >
                    <div class="col-md-2">
                        <h6>Reservation Number</h6>
                        <a id="reser_number_search" href="" class="trip_Id"></a>
                    </div>
                    <div class="col-md-2">
                        <h6>Customer Id</h6>
                        <p id="customer_Id_search"></p>
                    </div>

                    <div class="col-md-2">
                        <h6>Customer Name</h6>
                        <p id="customer_name_search"></p>
                    </div>

                    <div class="col-md-2">
                        <h6>Fast Pass Date</h6>
                        <p id="FPD_search"></p>
                    </div>
                    <div class="col-md-2">
                        <h6>Advance Dining Reservations</h6>
                        <p id="AdvanceDiningReservations_search"></p>
                    </div>
                    <div class="col-md-2">
                        <h6>Final Due Date</h6>
                        <p id="FinalDueDate_search"></p>
                    </div>
                    <!-- <div class="col-md-2">
                        <h6>Agent Name</h6>
                        <p id="agentname_search"></p>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
</div> 


@if($user->role == 1)

    <div class="row" style="margin-bottom: 30px;">
        <div class="col-md-6">
            <form>
                <label>Agent Name:</label>
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <select name="userId" class="form-control" required="" id="userId">
                                <option value="">Choose One</option>
                                @foreach ($use as $us) 

                                @if($us->id==$agent_id)
                                <option value="{{ $us->id }}" selected="" name="usID">{{ $us->name }}</option>
                                @else
                                <option value="{{ $us->id }}" name="usID">{{ $us->name }}</option>
                                @endif

                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <button type="submit"  class="btn btn-danger">Search</button>
                    </div>
                </div>
            </form>
        </div>

        <div class="col-lg-6">
             <form id="Searchform">

                    <label>Customer Id/Reservation #:</label>
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <input type="hidden" name="userId" id="getuserId">
                            <input type="search" name="search" id="getSearch" class="form-control" placeholder="Search" required style="height:auto;" />
                        </div>
                    </div>
                    <div class="col-md-4">
                         <button type="submit" onclick="SearchFunction()" class="btn btn-primary">Search</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endif


<style type="text/css">
    label {
        vertical-align: middle;
    }
    input:checked + .slider{
        background-color:#6495ED;
    }
    input:checked + .slider1{
        background-color:#5A62C1 !important;
    }
    input:checked + .slider2{
        background-color:#DE3163 !important;
    }
</style>
    <div class="row">
       <div class="col-md-4">
            <form method="post">
                <div class="form-group">
                    <label style="vertical-align: middle;">Fast Past Date </label>&nbsp;&nbsp;
                    <label class="switch">
                      <input type="checkbox" value="" id="CheckedFastPassDate" >
                      <span class="slider round"></span>
                    </label>
                </div>
            </form>
        </div>


        <div class="col-md-4">
            <div class="form-group">
                <label style="vertical-align: middle;">Advanced Dining Reservations</label>&nbsp;&nbsp;
                <label class="switch">
                  <input type="checkbox" id="CheckedAdR" >
                  <span class="slider round slider1"></span>
                </label>
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label style="vertical-align: middle;">Final Payment Date</label>&nbsp;&nbsp;
                <label class="switch">
                  <input type="checkbox" id="CheckedFinalPD" >
                  <span class="slider round slider2"></span>
                </label>
            </div>
        </div>
    </div>

<!-- if FPD = null && ADR = null && FinalPdate = null -->
<div class="panel panel-flat" style="padding-top: 30px; padding-bottom: 30px;" id="firstcalender">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header header-elements-inline">
                <!-- <h5 class="card-title">Event colors</h5> -->
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                        <a class="list-icons-item" data-action="reload"></a>
                        <a class="list-icons-item" data-action="remove"></a>
                    </div>
                </div>
            </div>
            
            <div style="overflow-x:auto;" class="card-body-fluid">
                abc
                <div class="fullcalendar-basic"></div>
            </div>
        </div>
    </div>
</div>

<!-- if FPD != null && ADR != null && FinalPdate != null -->

<div class="panel panel-flat" style="padding-top: 30px; padding-bottom: 30px; display:none;" id="secondcalender">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header header-elements-inline">
                <!-- <h5 class="card-title">Event colors</h5> -->
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                        <a class="list-icons-item" data-action="reload"></a>
                        <a class="list-icons-item" data-action="remove"></a>
                    </div>
                </div>
            </div>
            
            <div style="overflow-x:auto;" class="card-body-fluid">
                def
                <div class="fullcalendar-basics"></div>
            </div>
        </div>
    </div>
</div>

<!-- if FPD != null && ADR != null && FinalPdate = null-->
<div class="panel panel-flat" style="padding-top: 30px; padding-bottom: 30px; display:none;" id="thirdcalender">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header header-elements-inline">
                <!-- <h5 class="card-title">Event colors</h5> -->
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                        <a class="list-icons-item" data-action="reload"></a>
                        <a class="list-icons-item" data-action="remove"></a>
                    </div>
                </div>
            </div>
            
            <div style="overflow-x:auto;" class="card-body-fluid">
                ghi
                <div class="fullcalendar-basicss"></div>
            </div>
        </div>
    </div>
</div>

<!-- if FPD = null && ADR != null && FinalPdate == null -->
<div class="panel panel-flat" style="padding-top: 30px; padding-bottom: 30px; display:none;" id="fourthcalender">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header header-elements-inline">
                <!-- <h5 class="card-title">Event colors</h5> -->
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                        <a class="list-icons-item" data-action="reload"></a>
                        <a class="list-icons-item" data-action="remove"></a>
                    </div>
                </div>
            </div>
            
            <div style="overflow-x:auto;" class="card-body-fluid">
                jkl
                <div class="fullcalendar-basicsss"></div>
            </div>
        </div>
    </div>
</div>

<!-- if FPD != null && ADR = null && FinalPdate != null -->
<div class="panel panel-flat" style="padding-top: 30px; padding-bottom: 30px; display:none;" id="fifthcalender">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header header-elements-inline">
                <!-- <h5 class="card-title">Event colors</h5> -->
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                        <a class="list-icons-item" data-action="reload"></a>
                        <a class="list-icons-item" data-action="remove"></a>
                    </div>
                </div>
            </div>
            
            <div style="overflow-x:auto;" class="card-body-fluid">
                mno
                <div class="fullcalendar-basicssss"></div>
            </div>
        </div>
    </div>
</div>

<!-- if FPD = null && ADR = null && FinalPdate != null -->
<div class="panel panel-flat" style="padding-top: 30px; padding-bottom: 30px; display:none;" id="sixthcalender">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header header-elements-inline">
                <!-- <h5 class="card-title">Event colors</h5> -->
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                        <a class="list-icons-item" data-action="reload"></a>
                        <a class="list-icons-item" data-action="remove"></a>
                    </div>
                </div>
            </div>
            
            <div style="overflow-x:auto;" class="card-body-fluid">
                pqr
                <div class="fullcalendar-basicsssss"></div>
            </div>
        </div>
    </div>
</div>

<!-- if FPD != null && ADR = null && FinalPdate = null -->
<div class="panel panel-flat" style="padding-top: 30px; padding-bottom: 30px; display:none;" id="seventhcalender">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header header-elements-inline">
                <!-- <h5 class="card-title">Event colors</h5> -->
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                        <a class="list-icons-item" data-action="reload"></a>
                        <a class="list-icons-item" data-action="remove"></a>
                    </div>
                </div>
            </div>
            
            <div style="overflow-x:auto;" class="card-body-fluid">
                pqr
                <div class="fullcalendar-basicssssss"></div>
            </div>
        </div>
    </div>
</div>

<!-- if FPD = null && ADR != null && FinalPdate != null -->
<div class="panel panel-flat" style="padding-top: 30px; padding-bottom: 30px; display:none;" id="eigthcalender">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header header-elements-inline">
                <!-- <h5 class="card-title">Event colors</h5> -->
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                        <a class="list-icons-item" data-action="reload"></a>
                        <a class="list-icons-item" data-action="remove"></a>
                    </div>
                </div>
            </div>
            
            <div style="overflow-x:auto;" class="card-body-fluid">
                stu
                <div class="fullcalendar-basicsssssss"></div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    var obj = $('#userId').val();
    var FPD = "0";
    var ADR = "false";
    var FPDate = "false";
    
    var firstcal = document.getElementById('firstcalender');
    var secondcal = document.getElementById('secondcalender');
    var thirdcal = document.getElementById('thirdcalender');
    var fourthcal = document.getElementById('fourthcalender');
    var fifthcal = document.getElementById('fifthcalender');
    var sixthcal = document.getElementById('sixthcalender');
    var seventhcal = document.getElementById('seventhcalender');
    var eightcal = document.getElementById('eigthcalender');
    
        $.get('ajax-GetTrips?u_I_d='+obj+'&fpd='+FPD+'&adr='+ADR+'&FpDate='+FPDate, function(data){
                    $('.fullcalendar-basic').fullCalendar({
                        header: {
                            left: 'prev,next today',
                            center: 'title',
                            right: 'month,basicWeek,basicDay'
                        },
                        defaultDate: new Date(),
                        editable: true,
                        events: data, 

                        eventClick: function (event, jsEvent, view) {
                           if(event['customer_Id'] != null && event['customer_Id'] != "" && event['customer_name'] != null && event['customer_name'] != "" && event['reservation_number'] != null && event['reservation_number'] != "" || event['fastpassdate'] != null || event['fastpassdate'] != ""|| event['advancediningreservations'] != null || event['advancediningreservations'] != ""  || event['finalduedate'] != null || event['finalduedate'] != "")
                           {
                               $(".tr_Id").attr("href", href="{{ url('trip/view')}}/"+event['id']);
                               $("#reser_number").text(event['reservation_number']);
                               $("#cust_name").text(event['customer_name']);
                               $("#cust_id").text(event['customer_Id']);
                               $("#fpd_id").text(event['fastpassdate']);
                               $("#adp_id").text(event['advancediningreservations']);
                               $("#final_id").text(event['finalduedate']);                    
                               $("#modal_theme_valuUP").modal("show");
                           }
                        },

                        dayClick: function (date, jsEvent, view) {
                           

                        },

                        eventDrop: function (event) {
                            //alert(event.title + " and on starting date " + event.start.toISOString() + " and ending date " + event.end.toISOString());
                          
                        },
                        eventResize: function (event) {
                            //alert(info.title + " end is now " + info.end.toISOString());
                                            
                        },

                        eventAfterRender: function (event, element, view) {
                            var dataHoje = new Date();
                            if (event.start < dataHoje && event.end > dataHoje) {
                                //event.color = "#FFB347"; //Em andamento
                                element.css('background-color', '#FFB347');
                            } else if (event.start < dataHoje && event.end < dataHoje) {
                                //event.color = "#77DD77"; //Concluído OK
                                element.css('background-color', '#77DD77');
                            } else if (event.start > dataHoje && event.end > dataHoje) {
                                //event.color = "#AEC6CF"; //Não iniciado
                                element.css('background-color', '#AEC6CF');
                            }
                        }

                    });
                });


        $('#CheckedFastPassDate').change(function(){
            if(this.checked){
                FPD = 1;
                if(FPD == 1 && ADR == "true" && FPDate == "true"){
                    if(secondcal.style.display === 'none')
                    {
                        firstcal.style.display = 'none';
                        secondcal.style.display = 'block';
                        thirdcal.style.display = 'none';
                        fourthcal.style.display = 'none';
                        fifthcal.style.display = 'none';
                        sixthcal.style.display = 'none';
                        seventhcal.style.display = 'none';
                        eightcal.style.display = 'none';
                    }
                    $.get('ajax-GetTrips?u_I_d='+obj+'&fpd='+FPD+'&adr='+ADR+'&FpDate='+FPDate, function(data){
                        $('.fullcalendar-basics').fullCalendar({
                            header: {
                                left: 'prev,next today',
                                center: 'title',
                                right: 'month,basicWeek,basicDay'
                            },
                            defaultDate: new Date(),
                            editable: true,
                            events: data, 

                            eventClick: function (event, jsEvent, view) {
                               if(event['customer_Id'] != null && event['customer_Id'] != "" && event['customer_name'] != null && event['customer_name'] != "" && event['reservation_number'] != null && event['reservation_number'] != "" || event['fastpassdate'] != null || event['fastpassdate'] != ""|| event['advancediningreservations'] != null || event['advancediningreservations'] != ""  || event['finalduedate'] != null || event['finalduedate'] != "")
                               {
                                   $(".tr_Id").attr("href", href="{{ url('trip/view')}}/"+event['id']);
                                   $("#reser_number").text(event['reservation_number']);
                                   $("#cust_name").text(event['customer_name']);
                                   $("#cust_id").text(event['customer_Id']);
                                   $("#fpd_id").text(event['fastpassdate']);
                                   $("#adp_id").text(event['advancediningreservations']);
                                   $("#final_id").text(event['finalduedate']);                    
                                   $("#modal_theme_valuUP").modal("show");
                               }
                            },

                            dayClick: function (date, jsEvent, view) {
                               

                            },

                            eventDrop: function (event) {
                                //alert(event.title + " and on starting date " + event.start.toISOString() + " and ending date " + event.end.toISOString());
                              
                            },
                            eventResize: function (event) {
                                //alert(info.title + " end is now " + info.end.toISOString());
                                                
                            },

                            eventAfterRender: function (event, element, view) {
                                var dataHoje = new Date();
                                if (event.start < dataHoje && event.end > dataHoje) {
                                    //event.color = "#FFB347"; //Em andamento
                                    element.css('background-color', '#FFB347');
                                } else if (event.start < dataHoje && event.end < dataHoje) {
                                    //event.color = "#77DD77"; //Concluído OK
                                    element.css('background-color', '#77DD77');
                                } else if (event.start > dataHoje && event.end > dataHoje) {
                                    //event.color = "#AEC6CF"; //Não iniciado
                                    element.css('background-color', '#AEC6CF');
                                }
                            }

                        });
                    });
                }

                if(FPD == 1 && ADR == "true" && FPDate == "false"){
                    if(thirdcal.style.display === 'none')
                    {
                        firstcal.style.display = 'none';
                        secondcal.style.display = 'none';
                        thirdcal.style.display = 'block';

                        fourthcal.style.display = 'none';
                        fifthcal.style.display = 'none';
                        sixthcal.style.display = 'none';
                        seventhcal.style.display = 'none';
                        eightcal.style.display = 'none';
                    }
                    $.get('ajax-GetTrips?u_I_d='+obj+'&fpd='+FPD+'&adr='+ADR+'&FpDate='+FPDate, function(data){
                        $('.fullcalendar-basicss').fullCalendar({
                            header: {
                                left: 'prev,next today',
                                center: 'title',
                                right: 'month,basicWeek,basicDay'
                            },
                            defaultDate: new Date(),
                            editable: true,
                            events: data, 

                            eventClick: function (event, jsEvent, view) {
                               if(event['customer_Id'] != null && event['customer_Id'] != "" && event['customer_name'] != null && event['customer_name'] != "" && event['reservation_number'] != null && event['reservation_number'] != "" || event['fastpassdate'] != null || event['fastpassdate'] != ""|| event['advancediningreservations'] != null || event['advancediningreservations'] != ""  || event['finalduedate'] != null || event['finalduedate'] != "")
                               {
                                   $(".tr_Id").attr("href", href="{{ url('trip/view')}}/"+event['id']);
                                   $("#reser_number").text(event['reservation_number']);
                                   $("#cust_name").text(event['customer_name']);
                                   $("#cust_id").text(event['customer_Id']);
                                   $("#fpd_id").text(event['fastpassdate']);
                                   $("#adp_id").text(event['advancediningreservations']);
                                   $("#final_id").text(event['finalduedate']);                    
                                   $("#modal_theme_valuUP").modal("show");
                               }
                            },

                            dayClick: function (date, jsEvent, view) {
                               

                            },

                            eventDrop: function (event) {
                                //alert(event.title + " and on starting date " + event.start.toISOString() + " and ending date " + event.end.toISOString());
                              
                            },
                            eventResize: function (event) {
                                //alert(info.title + " end is now " + info.end.toISOString());
                                                
                            },

                            eventAfterRender: function (event, element, view) {
                                var dataHoje = new Date();
                                if (event.start < dataHoje && event.end > dataHoje) {
                                    //event.color = "#FFB347"; //Em andamento
                                    element.css('background-color', '#FFB347');
                                } else if (event.start < dataHoje && event.end < dataHoje) {
                                    //event.color = "#77DD77"; //Concluído OK
                                    element.css('background-color', '#77DD77');
                                } else if (event.start > dataHoje && event.end > dataHoje) {
                                    //event.color = "#AEC6CF"; //Não iniciado
                                    element.css('background-color', '#AEC6CF');
                                }
                            }

                        });
                    });
                }

                if(FPD == 1 && ADR == "false" && FPDate == "true"){
                    if(fifthcal.style.display === 'none')
                    {
                        firstcal.style.display = 'none';
                        secondcal.style.display = 'none';
                        thirdcal.style.display = 'none';
                        fourthcal.style.display = 'none';
                        fifthcal.style.display = 'block';
                        sixthcal.style.display = 'none';
                        seventhcal.style.display = 'none';
                        eightcal.style.display = 'none';
                    }
                    $.get('ajax-GetTrips?u_I_d='+obj+'&fpd='+FPD+'&adr='+ADR+'&FpDate='+FPDate, function(data){
                        $('.fullcalendar-basicssss').fullCalendar({
                            header: {
                                left: 'prev,next today',
                                center: 'title',
                                right: 'month,basicWeek,basicDay'
                            },
                            defaultDate: new Date(),
                            editable: true,
                            events: data, 

                            eventClick: function (event, jsEvent, view) {
                               if(event['customer_Id'] != null && event['customer_Id'] != "" && event['customer_name'] != null && event['customer_name'] != "" && event['reservation_number'] != null && event['reservation_number'] != "" || event['fastpassdate'] != null || event['fastpassdate'] != ""|| event['advancediningreservations'] != null || event['advancediningreservations'] != ""  || event['finalduedate'] != null || event['finalduedate'] != "")
                               {
                                   $(".tr_Id").attr("href", href="{{ url('trip/view')}}/"+event['id']);
                                   $("#reser_number").text(event['reservation_number']);
                                   $("#cust_name").text(event['customer_name']);
                                   $("#cust_id").text(event['customer_Id']);
                                   $("#fpd_id").text(event['fastpassdate']);
                                   $("#adp_id").text(event['advancediningreservations']);
                                   $("#final_id").text(event['finalduedate']);                    
                                   $("#modal_theme_valuUP").modal("show");
                               }
                            },

                            dayClick: function (date, jsEvent, view) {
                               

                            },

                            eventDrop: function (event) {
                                //alert(event.title + " and on starting date " + event.start.toISOString() + " and ending date " + event.end.toISOString());
                              
                            },
                            eventResize: function (event) {
                                //alert(info.title + " end is now " + info.end.toISOString());
                                                
                            },

                            eventAfterRender: function (event, element, view) {
                                var dataHoje = new Date();
                                if (event.start < dataHoje && event.end > dataHoje) {
                                    //event.color = "#FFB347"; //Em andamento
                                    element.css('background-color', '#FFB347');
                                } else if (event.start < dataHoje && event.end < dataHoje) {
                                    //event.color = "#77DD77"; //Concluído OK
                                    element.css('background-color', '#77DD77');
                                } else if (event.start > dataHoje && event.end > dataHoje) {
                                    //event.color = "#AEC6CF"; //Não iniciado
                                    element.css('background-color', '#AEC6CF');
                                }
                            }

                        });
                    });
                }

                if(FPD == 1 && ADR == "false" && FPDate == "false"){
                    if(seventhcal.style.display === 'none')
                    {
                        firstcal.style.display = 'none';
                        secondcal.style.display = 'none';
                        thirdcal.style.display = 'none';
                        fourthcal.style.display = 'none';
                        fifthcal.style.display = 'none';
                        sixthcal.style.display = 'none';
                        seventhcal.style.display = 'block';
                        eightcal.style.display = 'none';
                    }
                    $.get('ajax-GetTrips?u_I_d='+obj+'&fpd='+FPD+'&adr='+ADR+'&FpDate='+FPDate, function(data){
                        $('.fullcalendar-basicssssss').fullCalendar({
                            header: {
                                left: 'prev,next today',
                                center: 'title',
                                right: 'month,basicWeek,basicDay'
                            },
                            defaultDate: new Date(),
                            editable: true,
                            events: data, 

                            eventClick: function (event, jsEvent, view) {
                               if(event['customer_Id'] != null && event['customer_Id'] != "" && event['customer_name'] != null && event['customer_name'] != "" && event['reservation_number'] != null && event['reservation_number'] != "" || event['fastpassdate'] != null || event['fastpassdate'] != ""|| event['advancediningreservations'] != null || event['advancediningreservations'] != ""  || event['finalduedate'] != null || event['finalduedate'] != "")
                               {
                                   $(".tr_Id").attr("href", href="{{ url('trip/view')}}/"+event['id']);
                                   $("#reser_number").text(event['reservation_number']);
                                   $("#cust_name").text(event['customer_name']);
                                   $("#cust_id").text(event['customer_Id']);
                                   $("#fpd_id").text(event['fastpassdate']);
                                   $("#adp_id").text(event['advancediningreservations']);
                                   $("#final_id").text(event['finalduedate']);                    
                                   $("#modal_theme_valuUP").modal("show");
                               }
                            },

                            dayClick: function (date, jsEvent, view) {
                               

                            },

                            eventDrop: function (event) {
                                //alert(event.title + " and on starting date " + event.start.toISOString() + " and ending date " + event.end.toISOString());
                              
                            },
                            eventResize: function (event) {
                                //alert(info.title + " end is now " + info.end.toISOString());
                                                
                            },

                            eventAfterRender: function (event, element, view) {
                                var dataHoje = new Date();
                                if (event.start < dataHoje && event.end > dataHoje) {
                                    //event.color = "#FFB347"; //Em andamento
                                    element.css('background-color', '#FFB347');
                                } else if (event.start < dataHoje && event.end < dataHoje) {
                                    //event.color = "#77DD77"; //Concluído OK
                                    element.css('background-color', '#77DD77');
                                } else if (event.start > dataHoje && event.end > dataHoje) {
                                    //event.color = "#AEC6CF"; //Não iniciado
                                    element.css('background-color', '#AEC6CF');
                                }
                            }

                        });
                    });
                }
            }
            else{
                FPD = 0;
                if(FPD == 0 && ADR == "false" && FPDate == "false")
                {
                    if(firstcal.style.display === 'none')
                    {
                        firstcal.style.display = 'block';
                        secondcal.style.display = 'none';

                        thirdcal.style.display = 'none';
                        fourthcal.style.display = 'none';
                        fifthcal.style.display = 'none';
                        sixthcal.style.display = 'none';
                        seventhcal.style.display = 'none';
                        eightcal.style.display = 'none';
                    }
                    $.get('ajax-GetTrips?u_I_d='+obj+'&fpd='+FPD+'&adr='+ADR+'&FpDate='+FPDate, function(data){
                        $('.fullcalendar-basic').fullCalendar({
                            header: {
                                left: 'prev,next today',
                                center: 'title',
                                right: 'month,basicWeek,basicDay'
                            },
                            defaultDate: new Date(),
                            editable: true,
                            events: data, 

                            eventClick: function (event, jsEvent, view) {
                               if(event['customer_Id'] != null && event['customer_Id'] != "" && event['customer_name'] != null && event['customer_name'] != "" && event['reservation_number'] != null && event['reservation_number'] != "" || event['fastpassdate'] != null || event['fastpassdate'] != ""|| event['advancediningreservations'] != null || event['advancediningreservations'] != ""  || event['finalduedate'] != null || event['finalduedate'] != "")
                               {
                                   $(".tr_Id").attr("href", href="{{ url('trip/view')}}/"+event['id']);
                                   $("#reser_number").text(event['reservation_number']);
                                   $("#cust_name").text(event['customer_name']);
                                   $("#cust_id").text(event['customer_Id']);
                                   $("#fpd_id").text(event['fastpassdate']);
                                   $("#adp_id").text(event['advancediningreservations']);
                                   $("#final_id").text(event['finalduedate']);                    
                                   $("#modal_theme_valuUP").modal("show");
                               }
                            },

                            dayClick: function (date, jsEvent, view) {
                               

                            },

                            eventDrop: function (event) {
                                //alert(event.title + " and on starting date " + event.start.toISOString() + " and ending date " + event.end.toISOString());
                              
                            },
                            eventResize: function (event) {
                                //alert(info.title + " end is now " + info.end.toISOString());
                                                
                            },

                            eventAfterRender: function (event, element, view) {
                                var dataHoje = new Date();
                                if (event.start < dataHoje && event.end > dataHoje) {
                                    //event.color = "#FFB347"; //Em andamento
                                    element.css('background-color', '#FFB347');
                                } else if (event.start < dataHoje && event.end < dataHoje) {
                                    //event.color = "#77DD77"; //Concluído OK
                                    element.css('background-color', '#77DD77');
                                } else if (event.start > dataHoje && event.end > dataHoje) {
                                    //event.color = "#AEC6CF"; //Não iniciado
                                    element.css('background-color', '#AEC6CF');
                                }
                            }
                        });
                    });
                }
                
                if(FPD == 0 && ADR == "true" && FPDate == "false")
                {
                    if(fourthcal.style.display === 'none')
                    {
                        firstcal.style.display = 'none';
                        secondcal.style.display = 'none';
                        thirdcal.style.display = 'none';
                        fourthcal.style.display = 'block';

                        fifthcal.style.display = 'none';
                        sixthcal.style.display = 'none';
                        seventhcal.style.display = 'none';
                        eightcal.style.display = 'none';
                    }
                    $.get('ajax-GetTrips?u_I_d='+obj+'&fpd='+FPD+'&adr='+ADR+'&FpDate='+FPDate, function(data){
                        $('.fullcalendar-basicsss').fullCalendar({
                            header: {
                                left: 'prev,next today',
                                center: 'title',
                                right: 'month,basicWeek,basicDay'
                            },
                            defaultDate: new Date(),
                            editable: true,
                            events: data, 

                            eventClick: function (event, jsEvent, view) {
                               if(event['customer_Id'] != null && event['customer_Id'] != "" && event['customer_name'] != null && event['customer_name'] != "" && event['reservation_number'] != null && event['reservation_number'] != "" || event['fastpassdate'] != null || event['fastpassdate'] != ""|| event['advancediningreservations'] != null || event['advancediningreservations'] != ""  || event['finalduedate'] != null || event['finalduedate'] != "")
                               {
                                   $(".tr_Id").attr("href", href="{{ url('trip/view')}}/"+event['id']);
                                   $("#reser_number").text(event['reservation_number']);
                                   $("#cust_name").text(event['customer_name']);
                                   $("#cust_id").text(event['customer_Id']);
                                   $("#fpd_id").text(event['fastpassdate']);
                                   $("#adp_id").text(event['advancediningreservations']);
                                   $("#final_id").text(event['finalduedate']);                    
                                   $("#modal_theme_valuUP").modal("show");
                               }
                            },

                            dayClick: function (date, jsEvent, view) {
                               

                            },

                            eventDrop: function (event) {
                                //alert(event.title + " and on starting date " + event.start.toISOString() + " and ending date " + event.end.toISOString());
                              
                            },
                            eventResize: function (event) {
                                //alert(info.title + " end is now " + info.end.toISOString());
                                                
                            },

                            eventAfterRender: function (event, element, view) {
                                var dataHoje = new Date();
                                if (event.start < dataHoje && event.end > dataHoje) {
                                    //event.color = "#FFB347"; //Em andamento
                                    element.css('background-color', '#FFB347');
                                } else if (event.start < dataHoje && event.end < dataHoje) {
                                    //event.color = "#77DD77"; //Concluído OK
                                    element.css('background-color', '#77DD77');
                                } else if (event.start > dataHoje && event.end > dataHoje) {
                                    //event.color = "#AEC6CF"; //Não iniciado
                                    element.css('background-color', '#AEC6CF');
                                }
                            }
                        });
                    });
                }

                if(FPD == 0 && ADR == "false" && FPDate == "true")
                {
                    if(sixthcal.style.display === 'none')
                    {
                        firstcal.style.display = 'none';
                        secondcal.style.display = 'none';
                        thirdcal.style.display = 'none';
                        fourthcal.style.display = 'none';
                        fifthcal.style.display = 'none';
                        sixthcal.style.display = 'block';

                        seventhcal.style.display = 'none';
                        eightcal.style.display = 'none';
                    }
                    $.get('ajax-GetTrips?u_I_d='+obj+'&fpd='+FPD+'&adr='+ADR+'&FpDate='+FPDate, function(data){
                        $('.fullcalendar-basicsssss').fullCalendar({
                            header: {
                                left: 'prev,next today',
                                center: 'title',
                                right: 'month,basicWeek,basicDay'
                            },
                            defaultDate: new Date(),
                            editable: true,
                            events: data, 

                            eventClick: function (event, jsEvent, view) {
                               if(event['customer_Id'] != null && event['customer_Id'] != "" && event['customer_name'] != null && event['customer_name'] != "" && event['reservation_number'] != null && event['reservation_number'] != "" || event['fastpassdate'] != null || event['fastpassdate'] != ""|| event['advancediningreservations'] != null || event['advancediningreservations'] != ""  || event['finalduedate'] != null || event['finalduedate'] != "")
                               {
                                   $(".tr_Id").attr("href", href="{{ url('trip/view')}}/"+event['id']);
                                   $("#reser_number").text(event['reservation_number']);
                                   $("#cust_name").text(event['customer_name']);
                                   $("#cust_id").text(event['customer_Id']);
                                   $("#fpd_id").text(event['fastpassdate']);
                                   $("#adp_id").text(event['advancediningreservations']);
                                   $("#final_id").text(event['finalduedate']);                    
                                   $("#modal_theme_valuUP").modal("show");
                               }
                            },

                            dayClick: function (date, jsEvent, view) {
                               

                            },

                            eventDrop: function (event) {
                                //alert(event.title + " and on starting date " + event.start.toISOString() + " and ending date " + event.end.toISOString());
                              
                            },
                            eventResize: function (event) {
                                //alert(info.title + " end is now " + info.end.toISOString());
                                                
                            },

                            eventAfterRender: function (event, element, view) {
                                var dataHoje = new Date();
                                if (event.start < dataHoje && event.end > dataHoje) {
                                    //event.color = "#FFB347"; //Em andamento
                                    element.css('background-color', '#FFB347');
                                } else if (event.start < dataHoje && event.end < dataHoje) {
                                    //event.color = "#77DD77"; //Concluído OK
                                    element.css('background-color', '#77DD77');
                                } else if (event.start > dataHoje && event.end > dataHoje) {
                                    //event.color = "#AEC6CF"; //Não iniciado
                                    element.css('background-color', '#AEC6CF');
                                }
                            }
                        });
                    });
                }

                if(FPD == 0 && ADR == "true" && FPDate == "true")
                {
                    if(eightcal.style.display === 'none')
                    {
                        firstcal.style.display = 'none';
                        secondcal.style.display = 'none';
                        thirdcal.style.display = 'none';
                        fourthcal.style.display = 'none';
                        fifthcal.style.display = 'none';
                        sixthcal.style.display = 'none';
                        seventhcal.style.display = 'none';
                        eightcal.style.display = 'block';
                    }
                    $.get('ajax-GetTrips?u_I_d='+obj+'&fpd='+FPD+'&adr='+ADR+'&FpDate='+FPDate, function(data){
                        $('.fullcalendar-basicsssssss').fullCalendar({
                            header: {
                                left: 'prev,next today',
                                center: 'title',
                                right: 'month,basicWeek,basicDay'
                            },
                            defaultDate: new Date(),
                            editable: true,
                            events: data, 

                            eventClick: function (event, jsEvent, view) {
                               if(event['customer_Id'] != null && event['customer_Id'] != "" && event['customer_name'] != null && event['customer_name'] != "" && event['reservation_number'] != null && event['reservation_number'] != "" || event['fastpassdate'] != null || event['fastpassdate'] != ""|| event['advancediningreservations'] != null || event['advancediningreservations'] != ""  || event['finalduedate'] != null || event['finalduedate'] != "")
                               {
                                   $(".tr_Id").attr("href", href="{{ url('trip/view')}}/"+event['id']);
                                   $("#reser_number").text(event['reservation_number']);
                                   $("#cust_name").text(event['customer_name']);
                                   $("#cust_id").text(event['customer_Id']);
                                   $("#fpd_id").text(event['fastpassdate']);
                                   $("#adp_id").text(event['advancediningreservations']);
                                   $("#final_id").text(event['finalduedate']);                    
                                   $("#modal_theme_valuUP").modal("show");
                               }
                            },

                            dayClick: function (date, jsEvent, view) {
                               

                            },

                            eventDrop: function (event) {
                                //alert(event.title + " and on starting date " + event.start.toISOString() + " and ending date " + event.end.toISOString());
                              
                            },
                            eventResize: function (event) {
                                //alert(info.title + " end is now " + info.end.toISOString());
                                                
                            },

                            eventAfterRender: function (event, element, view) {
                                var dataHoje = new Date();
                                if (event.start < dataHoje && event.end > dataHoje) {
                                    //event.color = "#FFB347"; //Em andamento
                                    element.css('background-color', '#FFB347');
                                } else if (event.start < dataHoje && event.end < dataHoje) {
                                    //event.color = "#77DD77"; //Concluído OK
                                    element.css('background-color', '#77DD77');
                                } else if (event.start > dataHoje && event.end > dataHoje) {
                                    //event.color = "#AEC6CF"; //Não iniciado
                                    element.css('background-color', '#AEC6CF');
                                }
                            }
                        });
                    });
                }
            }

        })

        $('#CheckedAdR').change(function(){
            if(this.checked){
                ADR = "true";
                if(FPD == 1 && ADR == "true" && FPDate == "true"){
                    if(secondcal.style.display === 'none')
                    {
                        firstcal.style.display = 'none';
                        secondcal.style.display = 'block';

                        thirdcal.style.display = 'none';
                        fourthcal.style.display = 'none';
                        fifthcal.style.display = 'none';
                        sixthcal.style.display = 'none';
                        seventhcal.style.display = 'none';
                        eightcal.style.display = 'none';
                    }
                    $.get('ajax-GetTrips?u_I_d='+obj+'&fpd='+FPD+'&adr='+ADR+'&FpDate='+FPDate, function(data){
                        $('.fullcalendar-basics').fullCalendar({
                            header: {
                                left: 'prev,next today',
                                center: 'title',
                                right: 'month,basicWeek,basicDay'
                            },
                            defaultDate: new Date(),
                            editable: true,
                            events: data, 

                            eventClick: function (event, jsEvent, view) {
                               if(event['customer_Id'] != null && event['customer_Id'] != "" && event['customer_name'] != null && event['customer_name'] != "" && event['reservation_number'] != null && event['reservation_number'] != "" || event['fastpassdate'] != null || event['fastpassdate'] != ""|| event['advancediningreservations'] != null || event['advancediningreservations'] != ""  || event['finalduedate'] != null || event['finalduedate'] != "")
                               {
                                   $(".tr_Id").attr("href", href="{{ url('trip/view')}}/"+event['id']);
                                   $("#reser_number").text(event['reservation_number']);
                                   $("#cust_name").text(event['customer_name']);
                                   $("#cust_id").text(event['customer_Id']);
                                   $("#fpd_id").text(event['fastpassdate']);
                                   $("#adp_id").text(event['advancediningreservations']);
                                   $("#final_id").text(event['finalduedate']);                    
                                   $("#modal_theme_valuUP").modal("show");
                               }
                            },

                            dayClick: function (date, jsEvent, view) {
                               

                            },

                            eventDrop: function (event) {
                                //alert(event.title + " and on starting date " + event.start.toISOString() + " and ending date " + event.end.toISOString());
                              
                            },
                            eventResize: function (event) {
                                //alert(info.title + " end is now " + info.end.toISOString());
                                                
                            },

                            eventAfterRender: function (event, element, view) {
                                var dataHoje = new Date();
                                if (event.start < dataHoje && event.end > dataHoje) {
                                    //event.color = "#FFB347"; //Em andamento
                                    element.css('background-color', '#FFB347');
                                } else if (event.start < dataHoje && event.end < dataHoje) {
                                    //event.color = "#77DD77"; //Concluído OK
                                    element.css('background-color', '#77DD77');
                                } else if (event.start > dataHoje && event.end > dataHoje) {
                                    //event.color = "#AEC6CF"; //Não iniciado
                                    element.css('background-color', '#AEC6CF');
                                }
                            }

                        });
                    });
                }

                if(FPD == 1 && ADR == "true" && FPDate == "false"){
                    if(thirdcal.style.display === 'none')
                    {
                        firstcal.style.display = 'none';
                        secondcal.style.display = 'none';
                        thirdcal.style.display = 'block';

                        fourthcal.style.display = 'none';
                        fifthcal.style.display = 'none';
                        sixthcal.style.display = 'none';
                        seventhcal.style.display = 'none';
                        eightcal.style.display = 'none';
                    }
                    $.get('ajax-GetTrips?u_I_d='+obj+'&fpd='+FPD+'&adr='+ADR+'&FpDate='+FPDate, function(data){
                        $('.fullcalendar-basicss').fullCalendar({
                            header: {
                                left: 'prev,next today',
                                center: 'title',
                                right: 'month,basicWeek,basicDay'
                            },
                            defaultDate: new Date(),
                            editable: true,
                            events: data, 

                            eventClick: function (event, jsEvent, view) {
                               if(event['customer_Id'] != null && event['customer_Id'] != "" && event['customer_name'] != null && event['customer_name'] != "" && event['reservation_number'] != null && event['reservation_number'] != "" || event['fastpassdate'] != null || event['fastpassdate'] != ""|| event['advancediningreservations'] != null || event['advancediningreservations'] != ""  || event['finalduedate'] != null || event['finalduedate'] != "")
                               {
                                   $(".tr_Id").attr("href", href="{{ url('trip/view')}}/"+event['id']);
                                   $("#reser_number").text(event['reservation_number']);
                                   $("#cust_name").text(event['customer_name']);
                                   $("#cust_id").text(event['customer_Id']);
                                   $("#fpd_id").text(event['fastpassdate']);
                                   $("#adp_id").text(event['advancediningreservations']);
                                   $("#final_id").text(event['finalduedate']);                    
                                   $("#modal_theme_valuUP").modal("show");
                               }
                            },

                            dayClick: function (date, jsEvent, view) {
                               

                            },

                            eventDrop: function (event) {
                                //alert(event.title + " and on starting date " + event.start.toISOString() + " and ending date " + event.end.toISOString());
                              
                            },
                            eventResize: function (event) {
                                //alert(info.title + " end is now " + info.end.toISOString());
                                                
                            },

                            eventAfterRender: function (event, element, view) {
                                var dataHoje = new Date();
                                if (event.start < dataHoje && event.end > dataHoje) {
                                    //event.color = "#FFB347"; //Em andamento
                                    element.css('background-color', '#FFB347');
                                } else if (event.start < dataHoje && event.end < dataHoje) {
                                    //event.color = "#77DD77"; //Concluído OK
                                    element.css('background-color', '#77DD77');
                                } else if (event.start > dataHoje && event.end > dataHoje) {
                                    //event.color = "#AEC6CF"; //Não iniciado
                                    element.css('background-color', '#AEC6CF');
                                }
                            }

                        });
                    });
                }

                if(FPD == 0 && ADR == "true" && FPDate == "false")
                {
                    if(fourthcal.style.display === 'none')
                    {
                        firstcal.style.display = 'none';
                        secondcal.style.display = 'none';
                        thirdcal.style.display = 'none';
                        fourthcal.style.display = 'block';

                        fifthcal.style.display = 'none';
                        sixthcal.style.display = 'none';
                        seventhcal.style.display = 'none';
                        eightcal.style.display = 'none';
                    }
                    $.get('ajax-GetTrips?u_I_d='+obj+'&fpd='+FPD+'&adr='+ADR+'&FpDate='+FPDate, function(data){
                        $('.fullcalendar-basicsss').fullCalendar({
                            header: {
                                left: 'prev,next today',
                                center: 'title',
                                right: 'month,basicWeek,basicDay'
                            },
                            defaultDate: new Date(),
                            editable: true,
                            events: data, 

                            eventClick: function (event, jsEvent, view) {
                               if(event['customer_Id'] != null && event['customer_Id'] != "" && event['customer_name'] != null && event['customer_name'] != "" && event['reservation_number'] != null && event['reservation_number'] != "" || event['fastpassdate'] != null || event['fastpassdate'] != ""|| event['advancediningreservations'] != null || event['advancediningreservations'] != ""  || event['finalduedate'] != null || event['finalduedate'] != "")
                               {
                                   $(".tr_Id").attr("href", href="{{ url('trip/view')}}/"+event['id']);
                                   $("#reser_number").text(event['reservation_number']);
                                   $("#cust_name").text(event['customer_name']);
                                   $("#cust_id").text(event['customer_Id']);
                                   $("#fpd_id").text(event['fastpassdate']);
                                   $("#adp_id").text(event['advancediningreservations']);
                                   $("#final_id").text(event['finalduedate']);                    
                                   $("#modal_theme_valuUP").modal("show");
                               }
                            },

                            dayClick: function (date, jsEvent, view) {
                               

                            },

                            eventDrop: function (event) {
                                //alert(event.title + " and on starting date " + event.start.toISOString() + " and ending date " + event.end.toISOString());
                              
                            },
                            eventResize: function (event) {
                                //alert(info.title + " end is now " + info.end.toISOString());
                                                
                            },

                            eventAfterRender: function (event, element, view) {
                                var dataHoje = new Date();
                                if (event.start < dataHoje && event.end > dataHoje) {
                                    //event.color = "#FFB347"; //Em andamento
                                    element.css('background-color', '#FFB347');
                                } else if (event.start < dataHoje && event.end < dataHoje) {
                                    //event.color = "#77DD77"; //Concluído OK
                                    element.css('background-color', '#77DD77');
                                } else if (event.start > dataHoje && event.end > dataHoje) {
                                    //event.color = "#AEC6CF"; //Não iniciado
                                    element.css('background-color', '#AEC6CF');
                                }
                            }
                        });
                    });
                }

                if(FPD == 0 && ADR == "true" && FPDate == "true")
                {
                    if(eightcal.style.display === 'none')
                    {
                        firstcal.style.display = 'none';
                        secondcal.style.display = 'none';
                        thirdcal.style.display = 'none';
                        fourthcal.style.display = 'none';
                        fifthcal.style.display = 'none';
                        sixthcal.style.display = 'none';
                        seventhcal.style.display = 'none';
                        eightcal.style.display = 'block';
                    }
                    $.get('ajax-GetTrips?u_I_d='+obj+'&fpd='+FPD+'&adr='+ADR+'&FpDate='+FPDate, function(data){
                        $('.fullcalendar-basicsssssss').fullCalendar({
                            header: {
                                left: 'prev,next today',
                                center: 'title',
                                right: 'month,basicWeek,basicDay'
                            },
                            defaultDate: new Date(),
                            editable: true,
                            events: data, 

                            eventClick: function (event, jsEvent, view) {
                               if(event['customer_Id'] != null && event['customer_Id'] != "" && event['customer_name'] != null && event['customer_name'] != "" && event['reservation_number'] != null && event['reservation_number'] != "" || event['fastpassdate'] != null || event['fastpassdate'] != ""|| event['advancediningreservations'] != null || event['advancediningreservations'] != ""  || event['finalduedate'] != null || event['finalduedate'] != "")
                               {
                                   $(".tr_Id").attr("href", href="{{ url('trip/view')}}/"+event['id']);
                                   $("#reser_number").text(event['reservation_number']);
                                   $("#cust_name").text(event['customer_name']);
                                   $("#cust_id").text(event['customer_Id']);
                                   $("#fpd_id").text(event['fastpassdate']);
                                   $("#adp_id").text(event['advancediningreservations']);
                                   $("#final_id").text(event['finalduedate']);                    
                                   $("#modal_theme_valuUP").modal("show");
                               }
                            },

                            dayClick: function (date, jsEvent, view) {
                               

                            },

                            eventDrop: function (event) {
                                //alert(event.title + " and on starting date " + event.start.toISOString() + " and ending date " + event.end.toISOString());
                              
                            },
                            eventResize: function (event) {
                                //alert(info.title + " end is now " + info.end.toISOString());
                                                
                            },

                            eventAfterRender: function (event, element, view) {
                                var dataHoje = new Date();
                                if (event.start < dataHoje && event.end > dataHoje) {
                                    //event.color = "#FFB347"; //Em andamento
                                    element.css('background-color', '#FFB347');
                                } else if (event.start < dataHoje && event.end < dataHoje) {
                                    //event.color = "#77DD77"; //Concluído OK
                                    element.css('background-color', '#77DD77');
                                } else if (event.start > dataHoje && event.end > dataHoje) {
                                    //event.color = "#AEC6CF"; //Não iniciado
                                    element.css('background-color', '#AEC6CF');
                                }
                            }
                        });
                    });
                }
            }
            else{
                ADR = "false";

                if(FPD == 0 && ADR == "false" && FPDate == "false")
                {
                    if(firstcal.style.display === 'none')
                    {
                        firstcal.style.display = 'block';
                        secondcal.style.display = 'none';

                        thirdcal.style.display = 'none';
                        fourthcal.style.display = 'none';
                        fifthcal.style.display = 'none';
                        sixthcal.style.display = 'none';
                        seventhcal.style.display = 'none';
                        eightcal.style.display = 'none';
                    }
                    $.get('ajax-GetTrips?u_I_d='+obj+'&fpd='+FPD+'&adr='+ADR+'&FpDate='+FPDate, function(data){
                        $('.fullcalendar-basic').fullCalendar({
                            header: {
                                left: 'prev,next today',
                                center: 'title',
                                right: 'month,basicWeek,basicDay'
                            },
                            defaultDate: new Date(),
                            editable: true,
                            events: data, 

                            eventClick: function (event, jsEvent, view) {
                               if(event['customer_Id'] != null && event['customer_Id'] != "" && event['customer_name'] != null && event['customer_name'] != "" && event['reservation_number'] != null && event['reservation_number'] != "" || event['fastpassdate'] != null || event['fastpassdate'] != ""|| event['advancediningreservations'] != null || event['advancediningreservations'] != ""  || event['finalduedate'] != null || event['finalduedate'] != "")
                               {
                                   $(".tr_Id").attr("href", href="{{ url('trip/view')}}/"+event['id']);
                                   $("#reser_number").text(event['reservation_number']);
                                   $("#cust_name").text(event['customer_name']);
                                   $("#cust_id").text(event['customer_Id']);
                                   $("#fpd_id").text(event['fastpassdate']);
                                   $("#adp_id").text(event['advancediningreservations']);
                                   $("#final_id").text(event['finalduedate']);                    
                                   $("#modal_theme_valuUP").modal("show");
                               }
                            },

                            dayClick: function (date, jsEvent, view) {
                               

                            },

                            eventDrop: function (event) {
                                //alert(event.title + " and on starting date " + event.start.toISOString() + " and ending date " + event.end.toISOString());
                              
                            },
                            eventResize: function (event) {
                                //alert(info.title + " end is now " + info.end.toISOString());
                                                
                            },

                            eventAfterRender: function (event, element, view) {
                                var dataHoje = new Date();
                                if (event.start < dataHoje && event.end > dataHoje) {
                                    //event.color = "#FFB347"; //Em andamento
                                    element.css('background-color', '#FFB347');
                                } else if (event.start < dataHoje && event.end < dataHoje) {
                                    //event.color = "#77DD77"; //Concluído OK
                                    element.css('background-color', '#77DD77');
                                } else if (event.start > dataHoje && event.end > dataHoje) {
                                    //event.color = "#AEC6CF"; //Não iniciado
                                    element.css('background-color', '#AEC6CF');
                                }
                            }
                        });
                    });
                }

                if(FPD == 1 && ADR == "false" && FPDate == "true"){
                    if(fifthcal.style.display === 'none')
                    {
                        firstcal.style.display = 'none';
                        secondcal.style.display = 'none';
                        thirdcal.style.display = 'none';
                        fourthcal.style.display = 'none';
                        fifthcal.style.display = 'block';

                        sixthcal.style.display = 'none';
                        seventhcal.style.display = 'none';
                        eightcal.style.display = 'none';
                    }
                    $.get('ajax-GetTrips?u_I_d='+obj+'&fpd='+FPD+'&adr='+ADR+'&FpDate='+FPDate, function(data){
                        $('.fullcalendar-basicssss').fullCalendar({
                            header: {
                                left: 'prev,next today',
                                center: 'title',
                                right: 'month,basicWeek,basicDay'
                            },
                            defaultDate: new Date(),
                            editable: true,
                            events: data, 

                            eventClick: function (event, jsEvent, view) {
                               if(event['customer_Id'] != null && event['customer_Id'] != "" && event['customer_name'] != null && event['customer_name'] != "" && event['reservation_number'] != null && event['reservation_number'] != "" || event['fastpassdate'] != null || event['fastpassdate'] != ""|| event['advancediningreservations'] != null || event['advancediningreservations'] != ""  || event['finalduedate'] != null || event['finalduedate'] != "")
                               {
                                   $(".tr_Id").attr("href", href="{{ url('trip/view')}}/"+event['id']);
                                   $("#reser_number").text(event['reservation_number']);
                                   $("#cust_name").text(event['customer_name']);
                                   $("#cust_id").text(event['customer_Id']);
                                   $("#fpd_id").text(event['fastpassdate']);
                                   $("#adp_id").text(event['advancediningreservations']);
                                   $("#final_id").text(event['finalduedate']);                    
                                   $("#modal_theme_valuUP").modal("show");
                               }
                            },

                            dayClick: function (date, jsEvent, view) {
                               

                            },

                            eventDrop: function (event) {
                                //alert(event.title + " and on starting date " + event.start.toISOString() + " and ending date " + event.end.toISOString());
                              
                            },
                            eventResize: function (event) {
                                //alert(info.title + " end is now " + info.end.toISOString());
                                                
                            },

                            eventAfterRender: function (event, element, view) {
                                var dataHoje = new Date();
                                if (event.start < dataHoje && event.end > dataHoje) {
                                    //event.color = "#FFB347"; //Em andamento
                                    element.css('background-color', '#FFB347');
                                } else if (event.start < dataHoje && event.end < dataHoje) {
                                    //event.color = "#77DD77"; //Concluído OK
                                    element.css('background-color', '#77DD77');
                                } else if (event.start > dataHoje && event.end > dataHoje) {
                                    //event.color = "#AEC6CF"; //Não iniciado
                                    element.css('background-color', '#AEC6CF');
                                }
                            }

                        });
                    });
                }

                if(FPD == 1 && ADR == "false" && FPDate == "false"){
                    if(seventhcal.style.display === 'none')
                    {
                        firstcal.style.display = 'none';
                        secondcal.style.display = 'none';
                        thirdcal.style.display = 'none';
                        fourthcal.style.display = 'none';
                        fifthcal.style.display = 'none';
                        sixthcal.style.display = 'none';
                        seventhcal.style.display = 'block';
                        eightcal.style.display = 'none';
                    }
                    $.get('ajax-GetTrips?u_I_d='+obj+'&fpd='+FPD+'&adr='+ADR+'&FpDate='+FPDate, function(data){
                        $('.fullcalendar-basicssssss').fullCalendar({
                            header: {
                                left: 'prev,next today',
                                center: 'title',
                                right: 'month,basicWeek,basicDay'
                            },
                            defaultDate: new Date(),
                            editable: true,
                            events: data, 

                            eventClick: function (event, jsEvent, view) {
                               if(event['customer_Id'] != null && event['customer_Id'] != "" && event['customer_name'] != null && event['customer_name'] != "" && event['reservation_number'] != null && event['reservation_number'] != "" || event['fastpassdate'] != null || event['fastpassdate'] != ""|| event['advancediningreservations'] != null || event['advancediningreservations'] != ""  || event['finalduedate'] != null || event['finalduedate'] != "")
                               {
                                   $(".tr_Id").attr("href", href="{{ url('trip/view')}}/"+event['id']);
                                   $("#reser_number").text(event['reservation_number']);
                                   $("#cust_name").text(event['customer_name']);
                                   $("#cust_id").text(event['customer_Id']);
                                   $("#fpd_id").text(event['fastpassdate']);
                                   $("#adp_id").text(event['advancediningreservations']);
                                   $("#final_id").text(event['finalduedate']);                    
                                   $("#modal_theme_valuUP").modal("show");
                               }
                            },

                            dayClick: function (date, jsEvent, view) {
                               

                            },

                            eventDrop: function (event) {
                                //alert(event.title + " and on starting date " + event.start.toISOString() + " and ending date " + event.end.toISOString());
                              
                            },
                            eventResize: function (event) {
                                //alert(info.title + " end is now " + info.end.toISOString());
                                                
                            },

                            eventAfterRender: function (event, element, view) {
                                var dataHoje = new Date();
                                if (event.start < dataHoje && event.end > dataHoje) {
                                    //event.color = "#FFB347"; //Em andamento
                                    element.css('background-color', '#FFB347');
                                } else if (event.start < dataHoje && event.end < dataHoje) {
                                    //event.color = "#77DD77"; //Concluído OK
                                    element.css('background-color', '#77DD77');
                                } else if (event.start > dataHoje && event.end > dataHoje) {
                                    //event.color = "#AEC6CF"; //Não iniciado
                                    element.css('background-color', '#AEC6CF');
                                }
                            }

                        });
                    });
                }

                if(FPD == 0 && ADR == "false" && FPDate == "true")
                {
                    if(sixthcal.style.display === 'none')
                    {
                        firstcal.style.display = 'none';
                        secondcal.style.display = 'none';
                        thirdcal.style.display = 'none';
                        fourthcal.style.display = 'none';
                        fifthcal.style.display = 'none';
                        sixthcal.style.display = 'block';

                        seventhcal.style.display = 'none';
                        eightcal.style.display = 'none';
                    }
                    $.get('ajax-GetTrips?u_I_d='+obj+'&fpd='+FPD+'&adr='+ADR+'&FpDate='+FPDate, function(data){
                        $('.fullcalendar-basicsssss').fullCalendar({
                            header: {
                                left: 'prev,next today',
                                center: 'title',
                                right: 'month,basicWeek,basicDay'
                            },
                            defaultDate: new Date(),
                            editable: true,
                            events: data, 

                            eventClick: function (event, jsEvent, view) {
                               if(event['customer_Id'] != null && event['customer_Id'] != "" && event['customer_name'] != null && event['customer_name'] != "" && event['reservation_number'] != null && event['reservation_number'] != "" || event['fastpassdate'] != null || event['fastpassdate'] != ""|| event['advancediningreservations'] != null || event['advancediningreservations'] != ""  || event['finalduedate'] != null || event['finalduedate'] != "")
                               {
                                   $(".tr_Id").attr("href", href="{{ url('trip/view')}}/"+event['id']);
                                   $("#reser_number").text(event['reservation_number']);
                                   $("#cust_name").text(event['customer_name']);
                                   $("#cust_id").text(event['customer_Id']);
                                   $("#fpd_id").text(event['fastpassdate']);
                                   $("#adp_id").text(event['advancediningreservations']);
                                   $("#final_id").text(event['finalduedate']);                    
                                   $("#modal_theme_valuUP").modal("show");
                               }
                            },

                            dayClick: function (date, jsEvent, view) {
                               

                            },

                            eventDrop: function (event) {
                                //alert(event.title + " and on starting date " + event.start.toISOString() + " and ending date " + event.end.toISOString());
                              
                            },
                            eventResize: function (event) {
                                //alert(info.title + " end is now " + info.end.toISOString());
                                                
                            },

                            eventAfterRender: function (event, element, view) {
                                var dataHoje = new Date();
                                if (event.start < dataHoje && event.end > dataHoje) {
                                    //event.color = "#FFB347"; //Em andamento
                                    element.css('background-color', '#FFB347');
                                } else if (event.start < dataHoje && event.end < dataHoje) {
                                    //event.color = "#77DD77"; //Concluído OK
                                    element.css('background-color', '#77DD77');
                                } else if (event.start > dataHoje && event.end > dataHoje) {
                                    //event.color = "#AEC6CF"; //Não iniciado
                                    element.css('background-color', '#AEC6CF');
                                }
                            }
                        });
                    });
                }
            }
        })

        $('#CheckedFinalPD').change(function(){
            if(this.checked){
                FPDate = "true";
                if(FPD == 1 && ADR == "true" && FPDate == "true"){
                    if(secondcal.style.display === 'none')
                    {
                        firstcal.style.display = 'none';
                        secondcal.style.display = 'block';

                        thirdcal.style.display = 'none';
                        fourthcal.style.display = 'none';
                        fifthcal.style.display = 'none';
                        sixthcal.style.display = 'none';
                        seventhcal.style.display = 'none';
                        eightcal.style.display = 'none';
                    }
                    $.get('ajax-GetTrips?u_I_d='+obj+'&fpd='+FPD+'&adr='+ADR+'&FpDate='+FPDate, function(data){
                        $('.fullcalendar-basics').fullCalendar({
                            header: {
                                left: 'prev,next today',
                                center: 'title',
                                right: 'month,basicWeek,basicDay'
                            },
                            defaultDate: new Date(),
                            editable: true,
                            events: data, 

                            eventClick: function (event, jsEvent, view) {
                               if(event['customer_Id'] != null && event['customer_Id'] != "" && event['customer_name'] != null && event['customer_name'] != "" && event['reservation_number'] != null && event['reservation_number'] != "" || event['fastpassdate'] != null || event['fastpassdate'] != ""|| event['advancediningreservations'] != null || event['advancediningreservations'] != ""  || event['finalduedate'] != null || event['finalduedate'] != "")
                               {
                                   $(".tr_Id").attr("href", href="{{ url('trip/view')}}/"+event['id']);
                                   $("#reser_number").text(event['reservation_number']);
                                   $("#cust_name").text(event['customer_name']);
                                   $("#cust_id").text(event['customer_Id']);
                                   $("#fpd_id").text(event['fastpassdate']);
                                   $("#adp_id").text(event['advancediningreservations']);
                                   $("#final_id").text(event['finalduedate']);                    
                                   $("#modal_theme_valuUP").modal("show");
                               }
                            },

                            dayClick: function (date, jsEvent, view) {
                               

                            },

                            eventDrop: function (event) {
                                //alert(event.title + " and on starting date " + event.start.toISOString() + " and ending date " + event.end.toISOString());
                              
                            },
                            eventResize: function (event) {
                                //alert(info.title + " end is now " + info.end.toISOString());
                                                
                            },

                            eventAfterRender: function (event, element, view) {
                                var dataHoje = new Date();
                                if (event.start < dataHoje && event.end > dataHoje) {
                                    //event.color = "#FFB347"; //Em andamento
                                    element.css('background-color', '#FFB347');
                                } else if (event.start < dataHoje && event.end < dataHoje) {
                                    //event.color = "#77DD77"; //Concluído OK
                                    element.css('background-color', '#77DD77');
                                } else if (event.start > dataHoje && event.end > dataHoje) {
                                    //event.color = "#AEC6CF"; //Não iniciado
                                    element.css('background-color', '#AEC6CF');
                                }
                            }

                        });
                    });
                }

                if(FPD == 1 && ADR == "false" && FPDate == "true"){
                    if(fifthcal.style.display === 'none')
                    {
                        firstcal.style.display = 'none';
                        secondcal.style.display = 'none';
                        thirdcal.style.display = 'none';
                        fourthcal.style.display = 'none';
                        fifthcal.style.display = 'block';

                        sixthcal.style.display = 'none';
                        seventhcal.style.display = 'none';
                        eightcal.style.display = 'none';
                    }
                    $.get('ajax-GetTrips?u_I_d='+obj+'&fpd='+FPD+'&adr='+ADR+'&FpDate='+FPDate, function(data){
                        $('.fullcalendar-basicssss').fullCalendar({
                            header: {
                                left: 'prev,next today',
                                center: 'title',
                                right: 'month,basicWeek,basicDay'
                            },
                            defaultDate: new Date(),
                            editable: true,
                            events: data, 

                            eventClick: function (event, jsEvent, view) {
                               if(event['customer_Id'] != null && event['customer_Id'] != "" && event['customer_name'] != null && event['customer_name'] != "" && event['reservation_number'] != null && event['reservation_number'] != "" || event['fastpassdate'] != null || event['fastpassdate'] != ""|| event['advancediningreservations'] != null || event['advancediningreservations'] != ""  || event['finalduedate'] != null || event['finalduedate'] != "")
                               {
                                   $(".tr_Id").attr("href", href="{{ url('trip/view')}}/"+event['id']);
                                   $("#reser_number").text(event['reservation_number']);
                                   $("#cust_name").text(event['customer_name']);
                                   $("#cust_id").text(event['customer_Id']);
                                   $("#fpd_id").text(event['fastpassdate']);
                                   $("#adp_id").text(event['advancediningreservations']);
                                   $("#final_id").text(event['finalduedate']);                    
                                   $("#modal_theme_valuUP").modal("show");
                               }
                            },

                            dayClick: function (date, jsEvent, view) {
                               

                            },

                            eventDrop: function (event) {
                                //alert(event.title + " and on starting date " + event.start.toISOString() + " and ending date " + event.end.toISOString());
                              
                            },
                            eventResize: function (event) {
                                //alert(info.title + " end is now " + info.end.toISOString());
                                                
                            },

                            eventAfterRender: function (event, element, view) {
                                var dataHoje = new Date();
                                if (event.start < dataHoje && event.end > dataHoje) {
                                    //event.color = "#FFB347"; //Em andamento
                                    element.css('background-color', '#FFB347');
                                } else if (event.start < dataHoje && event.end < dataHoje) {
                                    //event.color = "#77DD77"; //Concluído OK
                                    element.css('background-color', '#77DD77');
                                } else if (event.start > dataHoje && event.end > dataHoje) {
                                    //event.color = "#AEC6CF"; //Não iniciado
                                    element.css('background-color', '#AEC6CF');
                                }
                            }

                        });
                    });
                }

                if(FPD == 0 && ADR == "true" && FPDate == "true")
                {
                    if(eightcal.style.display === 'none')
                    {
                        firstcal.style.display = 'none';
                        secondcal.style.display = 'none';
                        thirdcal.style.display = 'none';
                        fourthcal.style.display = 'none';
                        fifthcal.style.display = 'none';
                        sixthcal.style.display = 'none';
                        seventhcal.style.display = 'none';
                        eightcal.style.display = 'block';
                    }
                    $.get('ajax-GetTrips?u_I_d='+obj+'&fpd='+FPD+'&adr='+ADR+'&FpDate='+FPDate, function(data){
                        $('.fullcalendar-basicsssssss').fullCalendar({
                            header: {
                                left: 'prev,next today',
                                center: 'title',
                                right: 'month,basicWeek,basicDay'
                            },
                            defaultDate: new Date(),
                            editable: true,
                            events: data, 

                            eventClick: function (event, jsEvent, view) {
                               if(event['customer_Id'] != null && event['customer_Id'] != "" && event['customer_name'] != null && event['customer_name'] != "" && event['reservation_number'] != null && event['reservation_number'] != "" || event['fastpassdate'] != null || event['fastpassdate'] != ""|| event['advancediningreservations'] != null || event['advancediningreservations'] != ""  || event['finalduedate'] != null || event['finalduedate'] != "")
                               {
                                   $(".tr_Id").attr("href", href="{{ url('trip/view')}}/"+event['id']);
                                   $("#reser_number").text(event['reservation_number']);
                                   $("#cust_name").text(event['customer_name']);
                                   $("#cust_id").text(event['customer_Id']);
                                   $("#fpd_id").text(event['fastpassdate']);
                                   $("#adp_id").text(event['advancediningreservations']);
                                   $("#final_id").text(event['finalduedate']);                    
                                   $("#modal_theme_valuUP").modal("show");
                               }
                            },

                            dayClick: function (date, jsEvent, view) {
                               

                            },

                            eventDrop: function (event) {
                                //alert(event.title + " and on starting date " + event.start.toISOString() + " and ending date " + event.end.toISOString());
                              
                            },
                            eventResize: function (event) {
                                //alert(info.title + " end is now " + info.end.toISOString());
                                                
                            },

                            eventAfterRender: function (event, element, view) {
                                var dataHoje = new Date();
                                if (event.start < dataHoje && event.end > dataHoje) {
                                    //event.color = "#FFB347"; //Em andamento
                                    element.css('background-color', '#FFB347');
                                } else if (event.start < dataHoje && event.end < dataHoje) {
                                    //event.color = "#77DD77"; //Concluído OK
                                    element.css('background-color', '#77DD77');
                                } else if (event.start > dataHoje && event.end > dataHoje) {
                                    //event.color = "#AEC6CF"; //Não iniciado
                                    element.css('background-color', '#AEC6CF');
                                }
                            }
                        });
                    });
                }

                if(FPD == 0 && ADR == "false" && FPDate == "true")
                {
                    if(sixthcal.style.display === 'none')
                    {
                        firstcal.style.display = 'none';
                        secondcal.style.display = 'none';
                        thirdcal.style.display = 'none';
                        fourthcal.style.display = 'none';
                        fifthcal.style.display = 'none';
                        sixthcal.style.display = 'block';

                        seventhcal.style.display = 'none';
                        eightcal.style.display = 'none';
                    }
                    $.get('ajax-GetTrips?u_I_d='+obj+'&fpd='+FPD+'&adr='+ADR+'&FpDate='+FPDate, function(data){
                        $('.fullcalendar-basicsssss').fullCalendar({
                            header: {
                                left: 'prev,next today',
                                center: 'title',
                                right: 'month,basicWeek,basicDay'
                            },
                            defaultDate: new Date(),
                            editable: true,
                            events: data, 

                            eventClick: function (event, jsEvent, view) {
                               if(event['customer_Id'] != null && event['customer_Id'] != "" && event['customer_name'] != null && event['customer_name'] != "" && event['reservation_number'] != null && event['reservation_number'] != "" || event['fastpassdate'] != null || event['fastpassdate'] != ""|| event['advancediningreservations'] != null || event['advancediningreservations'] != ""  || event['finalduedate'] != null || event['finalduedate'] != "")
                               {
                                   $(".tr_Id").attr("href", href="{{ url('trip/view')}}/"+event['id']);
                                   $("#reser_number").text(event['reservation_number']);
                                   $("#cust_name").text(event['customer_name']);
                                   $("#cust_id").text(event['customer_Id']);
                                   $("#fpd_id").text(event['fastpassdate']);
                                   $("#adp_id").text(event['advancediningreservations']);
                                   $("#final_id").text(event['finalduedate']);                    
                                   $("#modal_theme_valuUP").modal("show");
                               }
                            },

                            dayClick: function (date, jsEvent, view) {
                               

                            },

                            eventDrop: function (event) {
                                //alert(event.title + " and on starting date " + event.start.toISOString() + " and ending date " + event.end.toISOString());
                              
                            },
                            eventResize: function (event) {
                                //alert(info.title + " end is now " + info.end.toISOString());
                                                
                            },

                            eventAfterRender: function (event, element, view) {
                                var dataHoje = new Date();
                                if (event.start < dataHoje && event.end > dataHoje) {
                                    //event.color = "#FFB347"; //Em andamento
                                    element.css('background-color', '#FFB347');
                                } else if (event.start < dataHoje && event.end < dataHoje) {
                                    //event.color = "#77DD77"; //Concluído OK
                                    element.css('background-color', '#77DD77');
                                } else if (event.start > dataHoje && event.end > dataHoje) {
                                    //event.color = "#AEC6CF"; //Não iniciado
                                    element.css('background-color', '#AEC6CF');
                                }
                            }
                        });
                    });
                }
            }
            else{
                FPDate = "false";
                if(FPD == 0 && ADR == "false" && FPDate == "false")
                {
                    if(firstcal.style.display === 'none')
                    {
                        firstcal.style.display = 'block';
                        secondcal.style.display = 'none';

                        thirdcal.style.display = 'none';
                        fourthcal.style.display = 'none';
                        fifthcal.style.display = 'none';
                        sixthcal.style.display = 'none';
                        seventhcal.style.display = 'none';
                        eightcal.style.display = 'none';
                    }
                    $.get('ajax-GetTrips?u_I_d='+obj+'&fpd='+FPD+'&adr='+ADR+'&FpDate='+FPDate, function(data){
                        $('.fullcalendar-basic').fullCalendar({
                            header: {
                                left: 'prev,next today',
                                center: 'title',
                                right: 'month,basicWeek,basicDay'
                            },
                            defaultDate: new Date(),
                            editable: true,
                            events: data, 

                            eventClick: function (event, jsEvent, view) {
                               if(event['customer_Id'] != null && event['customer_Id'] != "" && event['customer_name'] != null && event['customer_name'] != "" && event['reservation_number'] != null && event['reservation_number'] != "" || event['fastpassdate'] != null || event['fastpassdate'] != ""|| event['advancediningreservations'] != null || event['advancediningreservations'] != ""  || event['finalduedate'] != null || event['finalduedate'] != "")
                               {
                                   $(".tr_Id").attr("href", href="{{ url('trip/view')}}/"+event['id']);
                                   $("#reser_number").text(event['reservation_number']);
                                   $("#cust_name").text(event['customer_name']);
                                   $("#cust_id").text(event['customer_Id']);
                                   $("#fpd_id").text(event['fastpassdate']);
                                   $("#adp_id").text(event['advancediningreservations']);
                                   $("#final_id").text(event['finalduedate']);                    
                                   $("#modal_theme_valuUP").modal("show");
                               }
                            },

                            dayClick: function (date, jsEvent, view) {
                               

                            },

                            eventDrop: function (event) {
                                //alert(event.title + " and on starting date " + event.start.toISOString() + " and ending date " + event.end.toISOString());
                              
                            },
                            eventResize: function (event) {
                                //alert(info.title + " end is now " + info.end.toISOString());
                                                
                            },

                            eventAfterRender: function (event, element, view) {
                                var dataHoje = new Date();
                                if (event.start < dataHoje && event.end > dataHoje) {
                                    //event.color = "#FFB347"; //Em andamento
                                    element.css('background-color', '#FFB347');
                                } else if (event.start < dataHoje && event.end < dataHoje) {
                                    //event.color = "#77DD77"; //Concluído OK
                                    element.css('background-color', '#77DD77');
                                } else if (event.start > dataHoje && event.end > dataHoje) {
                                    //event.color = "#AEC6CF"; //Não iniciado
                                    element.css('background-color', '#AEC6CF');
                                }
                            }
                        });
                    });
                }

                if(FPD == 0 && ADR == "true" && FPDate == "false")
                {
                    if(fourthcal.style.display === 'none')
                    {
                        firstcal.style.display = 'none';
                        secondcal.style.display = 'none';
                        thirdcal.style.display = 'none';
                        fourthcal.style.display = 'block';

                        fifthcal.style.display = 'none';
                        sixthcal.style.display = 'none';
                        seventhcal.style.display = 'none';
                        eightcal.style.display = 'none';
                    }
                    $.get('ajax-GetTrips?u_I_d='+obj+'&fpd='+FPD+'&adr='+ADR+'&FpDate='+FPDate, function(data){
                        $('.fullcalendar-basicsss').fullCalendar({
                            header: {
                                left: 'prev,next today',
                                center: 'title',
                                right: 'month,basicWeek,basicDay'
                            },
                            defaultDate: new Date(),
                            editable: true,
                            events: data, 

                            eventClick: function (event, jsEvent, view) {
                               if(event['customer_Id'] != null && event['customer_Id'] != "" && event['customer_name'] != null && event['customer_name'] != "" && event['reservation_number'] != null && event['reservation_number'] != "" || event['fastpassdate'] != null || event['fastpassdate'] != ""|| event['advancediningreservations'] != null || event['advancediningreservations'] != ""  || event['finalduedate'] != null || event['finalduedate'] != "")
                               {
                                   $(".tr_Id").attr("href", href="{{ url('trip/view')}}/"+event['id']);
                                   $("#reser_number").text(event['reservation_number']);
                                   $("#cust_name").text(event['customer_name']);
                                   $("#cust_id").text(event['customer_Id']);
                                   $("#fpd_id").text(event['fastpassdate']);
                                   $("#adp_id").text(event['advancediningreservations']);
                                   $("#final_id").text(event['finalduedate']);                    
                                   $("#modal_theme_valuUP").modal("show");
                               }
                            },

                            dayClick: function (date, jsEvent, view) {
                               

                            },

                            eventDrop: function (event) {
                                //alert(event.title + " and on starting date " + event.start.toISOString() + " and ending date " + event.end.toISOString());
                              
                            },
                            eventResize: function (event) {
                                //alert(info.title + " end is now " + info.end.toISOString());
                                                
                            },

                            eventAfterRender: function (event, element, view) {
                                var dataHoje = new Date();
                                if (event.start < dataHoje && event.end > dataHoje) {
                                    //event.color = "#FFB347"; //Em andamento
                                    element.css('background-color', '#FFB347');
                                } else if (event.start < dataHoje && event.end < dataHoje) {
                                    //event.color = "#77DD77"; //Concluído OK
                                    element.css('background-color', '#77DD77');
                                } else if (event.start > dataHoje && event.end > dataHoje) {
                                    //event.color = "#AEC6CF"; //Não iniciado
                                    element.css('background-color', '#AEC6CF');
                                }
                            }
                        });
                    });
                }

                if(FPD == 1 && ADR == "true" && FPDate == "false"){
                    if(thirdcal.style.display === 'none')
                    {
                        firstcal.style.display = 'none';
                        secondcal.style.display = 'none';
                        thirdcal.style.display = 'block';

                        fourthcal.style.display = 'none';
                        fifthcal.style.display = 'none';
                        sixthcal.style.display = 'none';
                        seventhcal.style.display = 'none';
                        eightcal.style.display = 'none';
                    }
                    $.get('ajax-GetTrips?u_I_d='+obj+'&fpd='+FPD+'&adr='+ADR+'&FpDate='+FPDate, function(data){
                        $('.fullcalendar-basicss').fullCalendar({
                            header: {
                                left: 'prev,next today',
                                center: 'title',
                                right: 'month,basicWeek,basicDay'
                            },
                            defaultDate: new Date(),
                            editable: true,
                            events: data, 

                            eventClick: function (event, jsEvent, view) {
                               if(event['customer_Id'] != null && event['customer_Id'] != "" && event['customer_name'] != null && event['customer_name'] != "" && event['reservation_number'] != null && event['reservation_number'] != "" || event['fastpassdate'] != null || event['fastpassdate'] != ""|| event['advancediningreservations'] != null || event['advancediningreservations'] != ""  || event['finalduedate'] != null || event['finalduedate'] != "")
                               {
                                   $(".tr_Id").attr("href", href="{{ url('trip/view')}}/"+event['id']);
                                   $("#reser_number").text(event['reservation_number']);
                                   $("#cust_name").text(event['customer_name']);
                                   $("#cust_id").text(event['customer_Id']);
                                   $("#fpd_id").text(event['fastpassdate']);
                                   $("#adp_id").text(event['advancediningreservations']);
                                   $("#final_id").text(event['finalduedate']);                    
                                   $("#modal_theme_valuUP").modal("show");
                               }
                            },

                            dayClick: function (date, jsEvent, view) {
                               

                            },

                            eventDrop: function (event) {
                                //alert(event.title + " and on starting date " + event.start.toISOString() + " and ending date " + event.end.toISOString());
                              
                            },
                            eventResize: function (event) {
                                //alert(info.title + " end is now " + info.end.toISOString());
                                                
                            },

                            eventAfterRender: function (event, element, view) {
                                var dataHoje = new Date();
                                if (event.start < dataHoje && event.end > dataHoje) {
                                    //event.color = "#FFB347"; //Em andamento
                                    element.css('background-color', '#FFB347');
                                } else if (event.start < dataHoje && event.end < dataHoje) {
                                    //event.color = "#77DD77"; //Concluído OK
                                    element.css('background-color', '#77DD77');
                                } else if (event.start > dataHoje && event.end > dataHoje) {
                                    //event.color = "#AEC6CF"; //Não iniciado
                                    element.css('background-color', '#AEC6CF');
                                }
                            }

                        });
                    });
                }

                if(FPD == 1 && ADR == "false" && FPDate == "false"){
                    if(seventhcal.style.display === 'none')
                    {
                        firstcal.style.display = 'none';
                        secondcal.style.display = 'none';
                        thirdcal.style.display = 'none';
                        fourthcal.style.display = 'none';
                        fifthcal.style.display = 'none';
                        sixthcal.style.display = 'none';
                        seventhcal.style.display = 'block';
                        eightcal.style.display = 'none';
                    }
                    $.get('ajax-GetTrips?u_I_d='+obj+'&fpd='+FPD+'&adr='+ADR+'&FpDate='+FPDate, function(data){
                        $('.fullcalendar-basicssssss').fullCalendar({
                            header: {
                                left: 'prev,next today',
                                center: 'title',
                                right: 'month,basicWeek,basicDay'
                            },
                            defaultDate: new Date(),
                            editable: true,
                            events: data, 

                            eventClick: function (event, jsEvent, view) {
                               if(event['customer_Id'] != null && event['customer_Id'] != "" && event['customer_name'] != null && event['customer_name'] != "" && event['reservation_number'] != null && event['reservation_number'] != "" || event['fastpassdate'] != null || event['fastpassdate'] != ""|| event['advancediningreservations'] != null || event['advancediningreservations'] != ""  || event['finalduedate'] != null || event['finalduedate'] != "")
                               {
                                   $(".tr_Id").attr("href", href="{{ url('trip/view')}}/"+event['id']);
                                   $("#reser_number").text(event['reservation_number']);
                                   $("#cust_name").text(event['customer_name']);
                                   $("#cust_id").text(event['customer_Id']);
                                   $("#fpd_id").text(event['fastpassdate']);
                                   $("#adp_id").text(event['advancediningreservations']);
                                   $("#final_id").text(event['finalduedate']);                    
                                   $("#modal_theme_valuUP").modal("show");
                               }
                            },

                            dayClick: function (date, jsEvent, view) {
                               

                            },

                            eventDrop: function (event) {
                                //alert(event.title + " and on starting date " + event.start.toISOString() + " and ending date " + event.end.toISOString());
                              
                            },
                            eventResize: function (event) {
                                //alert(info.title + " end is now " + info.end.toISOString());
                                                
                            },

                            eventAfterRender: function (event, element, view) {
                                var dataHoje = new Date();
                                if (event.start < dataHoje && event.end > dataHoje) {
                                    //event.color = "#FFB347"; //Em andamento
                                    element.css('background-color', '#FFB347');
                                } else if (event.start < dataHoje && event.end < dataHoje) {
                                    //event.color = "#77DD77"; //Concluído OK
                                    element.css('background-color', '#77DD77');
                                } else if (event.start > dataHoje && event.end > dataHoje) {
                                    //event.color = "#AEC6CF"; //Não iniciado
                                    element.css('background-color', '#AEC6CF');
                                }
                            }

                        });
                    });
                }
            }
        })

</script>

<script type="text/javascript">
    function SearchFunction(){
        var getuserId = $('#userId').val();
        var SearchField = $('#getSearch').val();
        var div0 = document.getElementById('Information');
        var div1 = document.getElementById('noTFound');
        var div2 = document.getElementById('noFound');
        $('#getuserId').val(getuserId);

        $.ajax({
               type:'GET',
               url:'ajax-GetTrips?u_I_d='+getuserId+'&search='+SearchField,
               success:function(data) {
                if(data.length > 0){
                    for (var i = 0; i < data.length; i++) {
                        $(".trip_Id").attr("href", href="{{ url('trip/view')}}/"+data[i].id);
                        $('#reser_number_search').text(data[i].reservation_number);
                        $('#customer_Id_search').text(data[i].customer_Id);
                        $('#customer_name_search').text(data[i].customer_name);
                        $('#FPD_search').text(data[i].fastpassdate);
                        $('#AdvanceDiningReservations_search').text(data[i].advancediningreservations);
                        $('#FinalDueDate_search').text(data[i].finalduedate);
                    }
                    if(div0.style.display === 'none'){
                        div0.style.display = 'block';
                        if(div2.style.display === 'none'){
                            div2.style.display = 'block';
                            div1.style.display = 'none';
                        }
                    }
                    else{
                        if(div2.style.display === 'none'){
                            div2.style.display = 'block';
                            div1.style.display = 'none';
                        }
                    }
                }
                else{
                    if(div0.style.display === 'none'){
                        div0.style.display = 'block';
                        if(div1.style.display === 'none'){
                            div1.style.display = 'block';
                            div2.style.display = 'none';
                        }
                    }
                    else{
                        if(div1.style.display === 'none'){
                            div1.style.display = 'block';
                            div2.style.display = 'none';
                        }
                    }
                }
                  
               }
            });
    }
    $("#Searchform").submit(function(e) {
                      e.preventDefault();
                    });
    
</script>
@endsection
