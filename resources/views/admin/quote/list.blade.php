@extends('admin.layout.dashboardx')
@section('content')
<!-- Datatables JS FILES -->

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.print.min.js"></script>

<!-- /Datatables JS FILES -->

<!-- Page header -->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="{{ url('/home') }}">
                    <em class="fa fa-home"></em>
                </a></li>
                <li class="active">All Quotes</li>
            </ol>
        </div><!--/.row-->
    <div class="container-fluid"> 
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">All Quotes</h1>
            </div>
        </div>  <!-- /page header -->
   </div>


    <div style="padding-top:20px; padding-bottom:20px; padding-right:20px; padding-left:20px; overflow-x:auto;" class="panel panel-flat">
        <table id="example" class="display nowrap text-center" style="width:100%">
            <thead>
                <tr>
                    <th class="text-center">Action</th>
                    <th class="text-center">Agent Name</th>
                    <th class="text-center">Quote ID</th>
                    <th class="text-center">Quote Date</th>
                    <th class="text-center">Customer Name</th>
                    <th class="text-center">Destination</th>
                </tr>
            </thead>
            <tbody>
                  @foreach($quotes as $quote)                                          
                    <td class="center">
                        <a class="btn btn-success btn-sm" target="_blank"  href="{{ url('quote/view').'/'.$quote->id }}">
                            VIEW
                        </a>
                        <a class="btn btn-info btn-sm" href="{{ url('quote/edit').'/'.$quote->id }}" >
                            EDIT
                        </a>
                        <a class="btn btn-warning btn-sm" href="{{ url('quote/convert').'/'.$quote->id }}" >
                            Convert to a Trip
                        </a>
                        @if($user->role == 1)
                        <form style="display:inline" method="post" action="{{ url('quote/delete') }}/{{$quote->id}}" onsubmit="return confirm('Are you sure you want to delete this trip?');">
                            {!! csrf_field() !!}
                            {{ method_field('DELETE') }}
                            <button type="submit" class="btn btn-danger btn-sm">DELETE</button>
                        </form>
                         @endif

                    </td>                               

                    @if($quote->user == null)
                    <td>
                         Agent has been deleted
                    </td>
                    @else
                    <td>
                         {{ $quote->user->name }}
                    </td>
                    @endif

                    <td>{{ $quote->id }}</td>
                    
                    <td>
                        {{ date("m", strtotime($quote->booking_date)) }}-{{ date("d", strtotime($quote->booking_date)) }}-{{ date("Y", strtotime($quote->booking_date)) }}    
                    </td>

                    @if($quote->lead == null)
                    <td>
                         Lead has been deleted
                    </td>
                    @else
                    <td>
                        <a target="_blank"  href="{{ url('quote/view').'/'.$quote->id }}">
                            {{ $quote->lead->first_name }} {{ $quote->lead->last_name }}
                        </a>
                    </td>
                    @endif

                   <td>
                        {{ $quote->destination }}
                    </td>

                  </tr>
            @endforeach                               
            </tbody>
        </table>
    </div>

                       
@endsection