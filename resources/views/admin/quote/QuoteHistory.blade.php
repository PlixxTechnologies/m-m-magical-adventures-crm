@extends('admin.layout.dashboardx')
@section('content')

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.print.min.js"></script>

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="{{ url('/home') }}">
                    <em class="fa fa-home"></em>
                </a></li>
                <li class="active">Quote History</li>
            </ol>
        </div>

        <div class="container-fluid"> 
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Quote History</h1>
                </div>
            </div>
        </div>

       <center><h1>Customer Quote History</h1></center>

        <table id="example" class="display nowrap text-center" style="width:100%">
            <thead>
                <tr>
                    <th class="text-center" data-sortable="true">Customer Name</th>
                    <th class="text-center" data-sortable="true">Reservation Number</th>
                    <th class="text-center" data-sortable="true">Destination</th>
                    <th class="text-center" data-sortable="true">Travel Date</th>
                    <th class="text-center" data-sortable="true">Total Sale</th>                        
                </tr>
            </thead>
            <tbody>                                         
                @foreach($quotelist as $quote)                                 
                    <tr>
                        <td>
                            {{ $quote->lead->first_name }} {{ $quote->lead->last_name }}
                        </td>
                        
                        <td>
                            <a target="_blank" href="{{ url('quote/view').'/'.$quote->id }}">
                            	{{ $quote->reservation_number }}
                        	</a>
                        </td>
                        
                        <td>
                            {{ $quote->destination }}
                        </td>
                        
                        <td>
                            {{ date("m", strtotime($quote->checkin_date)) }}-{{ date("d", strtotime($quote->checkin_date)) }}-{{ date("Y", strtotime($quote->checkin_date)) }}  
                        </td>
                        
                        <td>
                            {{ $quote->total_sale }}
                        </td>                
                    </tr>
                @endforeach         
            </tbody>
        </table>
    </div> 
@endsection