@extends('admin.layout.dashboardx')

@section('content')
<!-- Datatables JS FILES -->

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.print.min.js"></script>

<!-- /Datatables JS FILES -->

   <!-- Page header -->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="{{ url('/home') }}">
                    <em class="fa fa-home"></em>
                </a></li>
                <li class="active">Add Category & Destination</li>
            </ol>
        </div><!--/.row-->
            <div class="container-fluid"> 
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Add Category </h1>
            </div>
        </div>  <!-- /page header -->
   </div>

    <div class="container-fluid">
        <br>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">

                        <form action="{{ url('destination/saveCategory') }}" method="post">
                            {!! csrf_field() !!}

                            <div class="form-group">
                                <label>*Name:</label>
                                <input type="text" name="destination_category" class="form-control" required>
                            </div>
                            <button type="submit" class="btn btn-primary pull-right">Save Category</button>
                        </form>

                    </div>
                </div>
            </div>
        </div><!--/.row-->
    </div>  <!--/.main-->



    <div class="container-fluid"> 
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Add Destination</h1>
            </div>
        </div>  <!-- /page header -->
   </div>

    <div class="container-fluid">
        <br>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">

                        <form action="{{ url('destination/save') }}" method="post">
                            {!! csrf_field() !!}

                            <div class="form-group">
                                <label>*Name:</label>
                                <input type="text" name="name" class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label><span class="red">*</span>Select Supplier Category:</label>
                               <!--  <select name="destination_category" class="form-control" id="disney_select" onchange="switchDivs()" required>
                              <option value="Choose One...a">Choose One....</option>
                                <option value="xyz">XYZ</option>
                                <option value="Universal Studios Florida">Universal Studios Florida</option>   
                                <option value="Disney Cruise Line">Disney Cruise Line</option> 
                                <option value="Royal Caribbean Cruise Line">Royal Caribbean Cruise Line</option>  
                                <option value="Walt Disney World">Walt Disney World</option> 
                                </select> -->




                  <select name="destination_category" class="form-control" required>
                    <option value="All"  selected>Choose One....</option>
                    @foreach($categorynames as $categoryname)
                        <option value="{{ $categoryname->destination_category }}">{{ $categoryname->destination_category }}</option>
                    @endforeach
                </select>  
                            </div> 
                            <button type="submit" class="btn btn-primary pull-right">Save Destination</button>
                        </form>

                    </div>
                </div>
            </div>
        </div><!--/.row-->
    </div>  <!--/.main-->
</div>

@endsection