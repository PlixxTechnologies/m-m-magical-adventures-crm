@extends('admin.layout.dashboardx')
@section('content')
<!-- Datatables JS FILES -->

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.print.min.js"></script>

<!-- /Datatables JS FILES -->
   <!-- Page header -->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="{{ url('/home') }}">
                    <em class="fa fa-home"></em>
                </a></li>
                <li class="active">All TO-DO's</li>
            </ol>
        </div><!--/.row-->
    <div class="container-fluid"> 
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">All TO-DO's</h1>
            </div>
        </div>  <!-- /page header -->
   </div>
<!-- Basic initialization -->
                    <div style="padding-top:20px; padding-bottom:20px; padding-right:20px; padding-left:20px;" class="panel panel-flat">
                        <div style="overflow-x:auto;">
                        <table id="example" class="display nowrap" style="width:100%"> 
                            <thead>
                                <tr>
                                <th>Action</th>
                                <th data-sortable="true" >ID</th>
                                <th data-sortable="true">Agent Name</th>
                                <th data-sortable="true">Customer Name</th>
                                <th data-sortable="true">Trip Reservation Number</th>
                                <th data-sortable="true">Promo Applied</th>
                                <th data-sortable="true">Magical Express Calendar</th>
                                <th data-sortable="true">Calendar Reminders Set</th>
                                <th data-sortable="true">Advanced Dining Reservations/BBB/Tours</th>
                                <th data-sortable="true">Magical Express Completed</th> 
                                <th data-sortable="true">Fast Pass Selection</th>  
                                <th data-sortable="true">Final Payment Due</th>                                
                                <th data-sortable="true">Itinerary and Tip Sheets Sent</th>   
                                <th data-sortable="true">Call in Room Request</th>
                                <th data-sortable="true">Enable Notifications</th>                                    
                            </tr>
                            </thead>
                            <tbody>
                                  @foreach($todos as $todo)
                                  <td class="center">
                                    <a href="{{ url('/toggleTodo/'.$todo->id) }}" class="btn btn-primary">Mark Not Complete</a>
                                        <a class="btn btn-info btn-sm" href="{{ url('todo/edit').'/'.$todo->id }}">
                                            EDIT
                                        </a>
                                        @if($user->role == 1)
                                        <form style="display:inline" method="post" action="{{ url('todo/delete') }}/{{$todo->id}}" onsubmit="return confirm('Are you sure you want to delete this todo?');">
                                            {!! csrf_field() !!}
                                            {{ method_field('DELETE') }}
                                            <button type="submit" class="btn btn-danger btn-sm">DELETE</button>
                                        </form>
                                        @endif
                                    </td>                                                    
                                    <td>
                                         {{ $todo->id }}
                                    </td>
                                    @if($todo->user == null)
                                    <td>
                                         Agent has been deleted
                                    </td>
                                    @else
                                    <td>
                                         {{ $todo->user->name }}
                                    </td>
                                    @endif

                                    @if($todo->customer == null)
                                    <td>
                                         Customer has been deleted
                                    </td>
                                    @else
                                    <td>
                                          {{ $todo->customer->first_name }} {{ $todo->customer->last_name }}
                                    </td>
                                    @endif


                                    @if($todo->trip == null)
                                    <td>
                                         Trip has been deleted
                                    </td>
                                    @else
                                    <td>
                                        {{ $todo->trip->reservation_number }}
                                    </td>
                                    @endif

                                    @if($todo->trip == null)
                                    <td>
                                         Trip has been deleted
                                    </td>
                                    @else
                                    <td>
                                        {{ $todo->trip->promo_code }}
                                    </td>
                                    @endif

                                    @if($todo->trip == null)
                                    <td>
                                         Trip has been deleted
                                    </td>
                                    @else
                                    <td>
                                        {{ $todo->trip->magicalexpresscalendar }}
                                    </td>
                                    @endif
                                    
                                    @if($todo->trip == null)
                                    <td>
                                         Trip has been deleted
                                    </td>
                                    @else
                                    <td>
                                        {{ $todo->trip->reminder }}
                                    </td>
                                    @endif

                                    @if($todo->trip == null)
                                    <td>
                                         Trip has been deleted
                                    </td>
                                    @else
                                    <td>
                                        {{ date("m", strtotime($todo->trip->advanced_dining_reservations)) }}-{{ date("d", strtotime($todo->trip->advanced_dining_reservations)) }}-{{ date("Y", strtotime($todo->trip->advanced_dining_reservations)) }}
                                    </td>
                                    @endif

                                    @if($todo->trip == null)
                                    <td>
                                         Trip has been deleted
                                    </td>
                                    @else
                                    <td>
                                        {{ $todo->trip->magical_express_completed }}
                                    </td>
                                    @endif

                                    @if($todo->trip == null)
                                    <td>
                                         Trip has been deleted
                                    </td>
                                    @else
                                    <td>

                                        {{ date("m", strtotime($todo->trip->fast_pass_date)) }}-{{ date("d", strtotime($todo->trip->fast_pass_date)) }}-{{ date("Y", strtotime($todo->trip->fast_pass_date)) }}
                                    </td>
                                    @endif

                                    @if($todo->trip == null)
                                    <td>
                                         Trip has been deleted
                                    </td>
                                    @else
                                    <td>
                                        {{ date("m", strtotime($todo->trip->final_payment_due)) }}-{{ date("d", strtotime($todo->trip->final_payment_due)) }}-{{ date("Y", strtotime($todo->trip->final_payment_due)) }}
                                    </td>
                                    @endif

                                    @if($todo->trip == null)
                                    <td>
                                         Trip has been deleted
                                    </td>
                                    @else
                                    <td>
                                        {{ $todo->trip->itinerary_tip_sheets }}
                                    </td>
                                    @endif

                                    @if($todo->trip == null)
                                    <td>
                                         Trip has been deleted
                                    </td>
                                    @else
                                    <td>
                                        {{ $todo->trip->call_room_requests }}
                                    </td>
                                    @endif
                                    <td>
                                        {{ $todo->todonotification }}
                                    </td>                            
                                    
 
                                    
                   
                                  </tr>
                            @endforeach                               
                            </tbody>
                        </table>
                    </div>
                    </div>
                </div>
                    <!-- /basic initialization -->   
@endsection