@extends('admin.layout.dashboardx')

@section('content')
<!-- Datatables JS FILES -->
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.print.min.js"></script>

<!-- /Datatables JS FILES -->
<!-- Quick stats boxes -->

 <!-- Page header -->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="{{ url('/home') }}">
                    <em class="fa fa-home"></em>
                </a></li>
                <li class="active">TODO LIST</li>
            </ol>
        </div><!--/.row-->
    <div class="container-fluid"> 
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">TODO LIST</h1>
            </div>
        </div><!--/.row-->    <!-- /page header -->
   </div>


<div class="container-fluid">
    <!-- TASK LIST STARTS -->
    <div class="panel panel-default">
        <div class="panel-heading">
           My To-do List
            <!-- <ul class="pull-right panel-settings panel-button-tab-right">
                <li class="dropdown"><a class="pull-right dropdown-toggle" data-toggle="dropdown" href="#">
                    <em class="fa fa-cogs"></em>
                </a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li>
                            <ul class="dropdown-settings">
                                <li><a href="#">
                                    <em class="fa fa-cog"></em> Settings 1
                                </a></li>
                                <li class="divider"></li>
                                <li><a href="#">
                                    <em class="fa fa-cog"></em> Settings 2
                                </a></li>
                                <li class="divider"></li>
                                <li><a href="#">
                                    <em class="fa fa-cog"></em> Settings 3
                                </a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul> -->

            <span class="pull-right clickable panel-toggle panel-button-tab-left">
                <em class="fa fa-toggle-up"></em>
            </span>
        </div>

        <div class="panel-body">
            @foreach($todolistings as $todolisting)
            <ul class="todo-list">
                <li class="todo-list-item">
                    <div class="checkbox">
                        <label for="checkbox-1">{{ $todolisting->id }}</label>
                        <label for="checkbox-2">{{ $todolisting->tasks }}</label>
                    </div>
                    <div class="pull-right action-buttons">
                        <em class="fa fa-tasks"></em>
                    </div>
                </li>
            </ul>
            @endforeach
        </div>
        
        <form action="{{ url('todolistings/save') }}" method="post">
            {!! csrf_field() !!}
            <div class="panel-footer">
                <div class="input-group">
                    <input id="btn-input" type="text" name="tasks" class="form-control" placeholder="Add new task" />
                    <span class="input-group-btn">
                        <button type="submit" class="btn btn-primary btn-md">Add</button>
                    </span>
                </div>
            </div>
        </form>
    </div>
    <!-- TASK LIST ENDS -->



<div class="row">
            <div style="padding-top:20px; padding-bottom:20px; padding-right:20px; padding-left:20px;"  class="panel panel-body">
                 <h1><b><center>Upcoming TO DO LIST</center></b></h1>
                <hr>
                <div style="overflow-x:auto;">
              <table id="example" class="display nowrap" style="width:100%">  
                    <thead>
                        <tr>
                            <th data-sortable="true">Customer Name</th>
                            <th data-sortable="true">Fast Pass Date</th>
                            <th data-sortable="true">Advanced Dining Reservations</th>
                            <th data-sortable="true">Itinerary Tip Sheets</th>
                            <th data-sortable="true">Final Payment Due</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                             @foreach($todolist as $todo)  
                                <tr>
                                    @if($todo->customer == null)
                                        <td>
                                            No Customer
                                        </td>
                                    @else
                                        <td>
                                            {{ $todo->customer->first_name }} {{ $todo->customer->last_name }}
                                        </td>
                                    @endif
                                    
                                    @if($todo->trip == null)
                                    <td>
                                        
                                    </td>
                                    <td>
                                        
                                    </td>
                                    <td>
                                        
                                    </td>
                                    <td>
                                        
                                    </td>
                                    @else
                                    <td>
                                        {{ $todo->trip->fast_pass_date }}
                                    </td>
                                    <td>
                                        {{ $todo->trip->advanced_dining_reservations }}
                                    </td>
                                    <td>
                                        {{ $todo->trip->itinerary_tip_sheets }}
                                    </td>
                                    <td>
                                        {{ $todo->trip->final_payment_due }}
                                    </td>
                                    <td>
                                        <a href="{{ url('/toggleTodo/'.$todo->id) }}" class="btn btn-success">Mark as Complete</a>
                                    </td>
                                    @endif
                                </tr>
                             @endforeach 
                    </tbody>
                </table>
            </div>
            </div>
        </div>
</div> <!--Datatable Ends-->
<!-- <div class="container-fluid">
    <div class="row">
            <div class="panel panel-body"> 
                <h1><b><center>Agent Stats</center></b></h1>
                <hr>
            <div class="row">
                <div class="col-lg-4">
                    <h3><strong>This Month's Stats</strong></h3>
                </div>

                 <div class="col-lg-4">
                    <div class="panel bg-teal-400" style="background-color:#FFB53E">
                        <div class="panel-body">
                            <center><em class="fa fa-users fa-lg"></em></center>
                            <h4 class="no-margin"><center>{{ $customers_month }}</center></h4>
                            <center>Customers</center>
                        </div>
                    </div>
                </div>                    

                <div class="col-lg-4">
                    <div class="panel bg-blue-400" style="background-color:#7FB7E6">
                        <div class="panel-body">
                            <center><em class="fa fa-suitcase fa-lg"></em></center>
                            <h4 class="no-margin"><center>{{ $trips_month }}</center></h4>
                            <center>Trips</center>
                        </div>
                    </div>
                </div>                    
            </div>

            <div class="row">
                <div class="col-lg-4">
                     <h3><strong>Overall Stats</strong></h3>
                </div>

                <div class="col-lg-4">
                    <div class="panel bg-teal-400" style="background-color:#FFB53E">
                        <div class="panel-body">
                            <center><em class="fa fa-users fa-lg"></em></center>
                            <h4 class="no-margin"><center>{{ $total_customer }}</center></h4>
                            <center>Customers</center>
                        </div>
                    </div>
                </div>             

                <div class="col-lg-4">
                    <div class="panel bg-blue-400" style="background-color:#7FB7E6">
                        <div class="panel-body">
                            <center><em class="fa fa-suitcase fa-lg"></em></center>
                            <h4 class="no-margin"><center>{{ $total_trips }}</center></h4>
                            <center>Trips</center>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> -->

</div> <!--Main Div End-->



<!-- <div class="container-fluid">
</div> --> 
@endsection