@extends('admin.layout.dashboardx')
@section('content')
<!-- Datatables JS FILES -->

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.print.min.js"></script>

<!-- /Datatables JS FILES -->
<style>
.red{
    color: red;
}
</style>
   <!-- Page header -->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="{{ url('/home') }}">
                    <em class="fa fa-home"></em>
                </a></li>
                <li class="active">Add To-Do</li>
            </ol>
        </div><!--/.row-->
    <div class="container-fluid"> 
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Add To-Do</h1>
            </div>
        </div>  <!-- /page header -->
   </div>


    <div class="container-fluid">
        <br>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">

                        <form action="{{ url('todo/save').'/'.$trip->id }}" method="post" >
                            {!! csrf_field() !!}

                            <input type="hidden" class="form-control" name="user_id" value="{{ $user->id }}">
							<input type="hidden" class="form-control" name="customer_id" value="{{ $customer->id }}">
							<input type="hidden" class="form-control" name="trip_id" value="{{ $trip->id }}">
     
							<div class="form-group">
                                <label>Agent Name:</label>
                                <input type="text"  class="form-control" value="{{ $user->name }}" readonly>
                            </div>
                            <div class="form-group">
                                <label>Customer Name:</label>
                                <input type="text"  class="form-control" value="{{ $name }}" readonly>
                            </div>
                            <div class="form-group">
                                <label>Trip Reservation Number:</label>
                                <input type="text"  class="form-control" value="{{ $trip->reservation_number }}" readonly>
                            </div>
                            <div class="form-group">
                                    <label>Fast Pass Date:</label>
                                    <input type="date" name="fast_pass_date" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Promo Applied:</label>
                                <input type="text" name="promo_code" class="form-control">
                            </div>
                            
                            <div class="form-group">
                                <label>Calendar Reminders Set:</label>
                                <select name="reminder" class="form-control">
                                    <option value="">Choose One....</option>
                                    <option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Advanced Dining Reservations/BBB/Tours:</label>
                                <input type="date" name="advanced_dining_reservations" class="form-control">
                            </div>
                            @if($trip->magical_express == "Yes")
                            <div class="form-group">
                                <label>Magical Express Completed:</label>
                                <select name="magical_express_completed" class="form-control">
                                    <option value="">Choose One....</option>
                                    <option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Magical Express Calendar:</label>
                                <input type="date" name="magicalexpresscalendar" class="form-control">
                            </div>
                            @endif
                            <div class="form-group">
                                <label><span class="red">*</span>Final Payment Due:</label>
                                <input type="date" name="final_payment_due" class="form-control" required="">
                            </div>                            
                            <div class="form-group">
                                <label>Itinerary and Tip Sheets Sent:</label>
                                <input type="date" name="itinerary_tip_sheetsmagical_express_completed" class="form-control">
                            </div>
                            
                            <div class="form-group">
                                <label>Call in Room Request:</label>
                                <select name="call_room_requests" class="form-control">
                                    <option value="">Choose One....</option>
                                    <option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label><span class="red">*</span>Enable Notifications:</label>
                                <select name="todonotification" class="form-control" required="">
                                    <option value="SMS">SMS</option>
                                    <option value="Email">Email</option>
                                    <option value="Both">Both</option>
                                </select>
                            </div>                            
                            <button type="submit" class="btn btn-primary pull-right">Save</button>
                        </form>
                    </div>
                </div>
            </div>
        </div><!--/.row-->
    </div>  <!--/.main-->
</div>


@endsection