@extends('admin.layout.dashboardx')

@section('content')
<!-- Datatables JS FILES -->

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.print.min.js"></script>

<!-- /Datatables JS FILES -->
<!-- Page header -->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="{{ url('/home') }}">
                    <em class="fa fa-home"></em>
                </a></li>
                <li class="active">Edit Dining Plan</li>
            </ol>
        </div><!--/.row-->
    <div class="container-fluid"> 
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Edit Dining Plan</h1>
            </div>
        </div>  <!-- /page header -->
   </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">

                        <form action="{{ url('diningplan/update').'/'.$diningplan->id }}" method="post" >
                            {!! csrf_field() !!}

                            <input type="hidden" name="id" value="{{ $diningplan->id }}">
                        
                            <div class="form-group">
                                <label>Name:</label>
                                <input type="text" name="name" value=" {{ $diningplan->name }}" class="form-control" readonly>
                            </div>
                            <div class="form-group">
                                <label>*Adult Price:</label>
                                <input type="text" name="adultprice" value=" {{ $diningplan->adultprice }}" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label>*Child Price:</label>
                                <input type="text" name="childprice" value=" {{ $diningplan->childprice }}" class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label>*Year:</label>
                                <select class="form-control" name="year" required>
                                    <option value="">Choose a year....</option>
                                    @foreach($years as $year)        
                                        <option value="{{ $year->year }}">{{ $year->year }}</option>
                                    @endforeach                                    
                                </select>
                            </div>
                            <button type="submit" class="btn btn-primary pull-right">Update Dining Plan</button>
                        </form>
                    </div>
                </div>
            </div>
        </div><!--/.row-->
    </div>	<!--/.main-->
</div>
@endsection