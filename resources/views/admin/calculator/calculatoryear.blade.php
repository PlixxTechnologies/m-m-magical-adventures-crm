@extends('admin.layout.dashboardx')
@section('content')
<!-- Datatables JS FILES -->

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.print.min.js"></script>

<!-- /Datatables JS FILES -->

   <!-- Page header -->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="{{ url('/home') }}">
                    <em class="fa fa-home"></em>
                </a></li>
                <li class="active">Add Calculator Year</li>
            </ol>
        </div><!--/.row-->
    <div class="container-fluid"> 
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Add Calculator Year</h1>
            </div>
        </div>  <!-- /page header -->
   </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">

                        <form action="{{ url('calculator-year') }}" method="post" >
                            {!! csrf_field() !!}

                            <div class="form-group">
                                <label>Year:</label>
                                <input type="text" class="form-control" name="year">
                            </div>

                            <button type="submit" class="btn btn-primary pull-right">Save Year</button>
                        </form>
                    </div>
                </div>
            </div>
        </div><!--/.row-->
    </div>  <!--/.main-->


<!-- Basic initialization -->
                    <div style="padding-top:20px; padding-bottom:20px; padding-right:20px; padding-left:20px; overflow-x:auto;"  class="panel panel-flat">
                       <table id="example" class="display nowrap" style="width:100%"> 
                            <thead>
                                <tr>
                                <th>Year</th>
                                @if(Auth::user()->role == 1)
                                    <th>Action</th>
                                    @endif
                            </tr>
                            </thead>
                            <tbody>
                                  @foreach($calculatoryears as $calculatoryear)
                                    <td>
                                        {{ $calculatoryear->year }}
                                    </td>
                            @if(Auth::user()->role == 1)
                                    <td class="center">
                                        <form style="display:inline" method="post" action="{{ url('calculator-year/delete') }}/{{$calculatoryear->id}}" onsubmit="return confirm('Are you sure you want to delete this year?');">
                                            {!! csrf_field() !!}
                                            {{ method_field('DELETE') }}
                                            <button type="submit" class="btn btn-danger">DELETE</button>
                                        </form>
                                    </td>
                                @endif
                                  </tr>
                            @endforeach                               
                            </tbody>
                        </table>
                    </div>
                </div>
                    <!-- /basic initialization -->   
@endsection