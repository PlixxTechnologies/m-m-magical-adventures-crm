@extends('admin.layout.dashboard')
@section('content')
<!-- Datatables JS FILES -->

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.print.min.js"></script>

<!-- /Datatables JS FILES -->

 <!-- Page header -->
                <div class="page-header page-header-default">
                    <div class="page-header-content">
                        @if(Auth::user() == null)                      
                            <center><h1 id="h1"><strong>1-844-9MICKEY &nbsp; &nbsp; &nbsp; &nbsp; bookings@MandMMagicalAdventures.com</strong></h1></center><br>
                        @endif
                        <div class="page-title">                            
                            <center><h1 id="h1"><strong>M&M Magical Adventures Disney Dining Plan Calculator</strong></h1></center>
                        </div>
                        <form action="{{ url('dining-plan-calculator') }}" method="post" >
                            {!! csrf_field() !!}
                        <div class="row">
                            <div class="col-md-2 form-group">
                            </div>

                            <div class="col-md-2 form-group">
                                <span>Year</span>
                                <select name="year" class="form-control">
                                    @foreach($years as $calculatoryear)
                                    <option value="{{ $calculatoryear->year }}">{{ $calculatoryear->year }}</option>     @endforeach                               
                                </select>
                            </div>
                            <div class="col-md-2 form-group">
                                <span># of Adults</span>
                                <select name="noofadults" class="form-control">
                                    @if($noofadults == null)                                    
                                     @for ($x = 0; $x <= 12; $x++) {
                                        <option value="{{ $x }}">{{ $x }}</option> 
                                    @endfor
                                    @else
                                    <option value="{{ $noofadults }}" selected>{{ $noofadults }}</option> 
                                     @for ($x = 0; $x <= 12; $x++) {
                                        <option value="{{ $x }}">{{ $x }}</option> 
                                    @endfor
                                    @endif                                   
                                </select>
                            </div>
                            <div class="col-md-2 form-group">
                                <span># of Children (Ages 3-9)</span>
                                <select name="noofchildren" class="form-control">
                                    @if($noofchildren == null)
                                    @for ($x = 0; $x <= 12; $x++) {
                                        <option value="{{ $x }}">{{ $x }}</option> 
                                    @endfor 
                                    @else
                                    <option value="{{ $noofchildren }}" selected>{{ $noofchildren }}</option>
                                    @for ($x = 0; $x <= 12; $x++) {
                                        <option value="{{ $x }}">{{ $x }}</option> 
                                    @endfor 
                                    @endif
                                                                    
                                </select>
                            </div>
                            <div class="col-md-2 form-group">
                                <span># of Nights Staying</span>
                                <select name="noofnights" class="form-control">
                                    @if($noofnights == null)
                                    @for ($x = 1; $x <= 30; $x++) {
                                        <option value="{{ $x }}">{{ $x }}</option> 
                                    @endfor 
                                    @else
                                    <option value="{{ $noofnights }}" selected>{{ $noofnights }}</option>
                                    @for ($x = 1; $x <= 30; $x++) {
                                        <option value="{{ $x }}">{{ $x }}</option> 
                                    @endfor 
                                    @endif                                                                      
                                </select>
                            </div>
                             <div class="col-md-1 form-group">
                               <br><button class="btn btn-primary btn-sm" type="submit">Get Result</button>
                            </div>
                         </div>
                     </form>
                    </div>
                 </div>  
  <!-- Bordered striped table -->
  @if(Auth::user() != null)
  
    <input type="hidden" value="{{ Auth::user()->name }}" id="name">
    <input type="hidden" value="{{ Auth::user()->email }}" id="email">
    <input type="hidden" value="{{ Auth::user()->phone }}" id="phone">
  
@endif

                    <div class="col-md-offset-1">                       

                        <div class="table-responsive">
                           @if(Auth::user() == null)
                            <table class="table datatable-button-html5-image1">                                
                                <thead>                                    
                                    <tr>
                                        <th><strong>Disney Quick Service Dining Plan</strong></th>
                                        <th>Per Day:</th>
                                        <th>Total:</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        @if($noofadults == null)     
                                        <td>Adult Dining Plan: 0 adult(s)</td>
                                        @else
                                        <td>Adult Dining Plan: {{ $noofadults }} adult(s)</td>
                                        @endif

                                        @if($quickserviceadultperday == null)
                                        <td>$0.00</td>
                                        @else
                                        <td>${{ $quickserviceadultperday }}</td>
                                        @endif

                                       @if($quickserviceadulttotal == null)
                                        <td>$0.00</td>
                                        @else
                                        <td>${{ $quickserviceadulttotal }}</td>
                                        @endif                                    
                                    </tr>
                                    <tr>
                                        @if($noofchildren == null)
                                        <td>Child Dining Plan: 0 children</td>
                                        @else
                                        <td>Child Dining Plan: {{ $noofchildren }} children</td>
                                        @endif

                                        @if($quickservicechildperday == null)
                                        <td>$0.00</td>
                                        @else
                                        <td>${{$quickservicechildperday}}</td>
                                        @endif

                                        @if($quickservicechildtotal == null)
                                        <td>$0.00</td>
                                        @else
                                        <td>${{$quickservicechildtotal }}</td>
                                        @endif                                        
                                        
                                    </tr>
                                    <tr>
                                        @if($noofadults == null && $noofchildren == null)
                                        <td>Total Rate: 0 adult(s) + 0 children</td>

                                        @elseif($noofadults != null && $noofchildren == null)
                                        <td>Total Rate: {{ $noofadults }} adult(s) + 0 children</td>

                                        @elseif($noofadults == null && $noofchildren != null)
                                        <td>Total Rate: 0 adult(s) + {{ $noofchildren }} children</td>

                                        @else
                                        <td>Total Rate: {{ $noofadults }} adult(s) + {{ $noofchildren }} children</td>
                                        @endif

                                        @if($quickservicetotalperday == null)
                                        <td><strong>$0.00</strong></td>
                                        @else
                                        <td><strong>${{ $quickservicetotalperday }}</strong></td>
                                        @endif

                                        @if($quickservicetotal == null)
                                        <td><strong>$0.00</strong></td>
                                        @else
                                        <td><strong>${{ $quickservicetotal }}</strong></td>
                                        @endif 
                                    </tr>                               
                                </tbody>
                            </table>
                            @else                            
                            <table class="table datatable-button-html5-image">
                                <thead>
                                    <tr>
                                        <th><strong>Disney Quick Service Dining Plan</strong></th>
                                        <th>Per Day:</th>
                                        <th>Total:</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        @if($noofadults == null)     
                                        <td>Adult Dining Plan: 0 adult(s)</td>
                                        @else
                                        <td>Adult Dining Plan: {{ $noofadults }} adult(s)</td>
                                        @endif

                                        @if($quickserviceadultperday == null)
                                        <td>$0.00</td>
                                        @else
                                        <td>${{ $quickserviceadultperday }}</td>
                                        @endif

                                       @if($quickserviceadulttotal == null)
                                        <td>$0.00</td>
                                        @else
                                        <td>${{ $quickserviceadulttotal }}</td>
                                        @endif                                    
                                    </tr>
                                    <tr>
                                        @if($noofchildren == null)
                                        <td>Child Dining Plan: 0 children</td>
                                        @else
                                        <td>Child Dining Plan: {{ $noofchildren }} children</td>
                                        @endif

                                        @if($quickservicechildperday == null)
                                        <td>$0.00</td>
                                        @else
                                        <td>${{$quickservicechildperday}}</td>
                                        @endif

                                        @if($quickservicechildtotal == null)
                                        <td>$0.00</td>
                                        @else
                                        <td>${{$quickservicechildtotal }}</td>
                                        @endif                                        
                                        
                                    </tr>
                                    <tr>
                                        @if($noofadults == null && $noofchildren == null)
                                        <td>Total Rate: 0 adult(s) + 0 children</td>

                                        @elseif($noofadults != null && $noofchildren == null)
                                        <td>Total Rate: {{ $noofadults }} adult(s) + 0 children</td>

                                        @elseif($noofadults == null && $noofchildren != null)
                                        <td>Total Rate: 0 adult(s) + {{ $noofchildren }} children</td>

                                        @else
                                        <td>Total Rate: {{ $noofadults }} adult(s) + {{ $noofchildren }} children</td>
                                        @endif

                                        @if($quickservicetotalperday == null)
                                        <td><strong>$0.00</strong></td>
                                        @else
                                        <td><strong>${{ $quickservicetotalperday }}</strong></td>
                                        @endif

                                        @if($quickservicetotal == null)
                                        <td><strong>$0.00</strong></td>
                                        @else
                                        <td><strong>${{ $quickservicetotal }}</strong></td>
                                        @endif 
                                    </tr>                               
                                </tbody>
                            </table>
                            @endif
                        </div>
                    </div>
                    <!-- /bordered striped table -->
                    <br><br>
                    <!-- Bordered striped table -->
                    <div class="col-md-offset-1">                       

                        <div class="table-responsive">
                            @if(Auth::user() == null)
                            <table class="table datatable-button-html5-image1">
                                <thead>
                                    <tr>
                                        <th><strong>Disney Dining Plan</strong></th>
                                        <th>Per Day:</th>
                                        <th>Total:</th>
                                    </tr>
                                </thead>
                                <tbody>
                                     <tr>
                                        @if($noofadults == null)     
                                        <td>Adult Dining Plan: 0 adult(s)</td>
                                        @else
                                        <td>Adult Dining Plan: {{ $noofadults }} adult(s)</td>
                                        @endif

                                        @if($disneydiningadultperday == null)
                                        <td>$0.00</td>
                                        @else
                                        <td>${{ $disneydiningadultperday }}</td>
                                        @endif

                                       @if($disneydiningadulttotal == null)
                                        <td>$0.00</td>
                                        @else
                                        <td>${{ $disneydiningadulttotal }}</td>
                                        @endif                                    
                                    </tr>
                                    <tr>
                                        @if($noofchildren == null)
                                        <td>Child Dining Plan: 0 children</td>
                                        @else
                                        <td>Child Dining Plan: {{ $noofchildren }} children</td>
                                        @endif

                                        @if($disneydiningchildperday == null)
                                        <td>$0.00</td>
                                        @else
                                        <td>${{$disneydiningchildperday}}</td>
                                        @endif

                                        @if($disneydiningchildtotal == null)
                                        <td>$0.00</td>
                                        @else
                                        <td>${{$disneydiningchildtotal }}</td>
                                        @endif                                        
                                        
                                    </tr>
                                    <tr>
                                        @if($noofadults == null && $noofchildren == null)
                                        <td>Total Rate: 0 adult(s) + 0 children</td>

                                        @elseif($noofadults != null && $noofchildren == null)
                                        <td>Total Rate: {{ $noofadults }} adult(s) + 0 children</td>

                                        @elseif($noofadults == null && $noofchildren != null)
                                        <td>Total Rate: 0 adult(s) + {{ $noofchildren }} children</td>

                                        @else
                                        <td>Total Rate: {{ $noofadults }} adult(s) + {{ $noofchildren }} children</td>
                                        @endif

                                        @if($disneydiningtotalperday == null)
                                        <td><strong>$0.00</strong></td>
                                        @else
                                        <td><strong>${{ $disneydiningtotalperday }}</strong></td>
                                        @endif

                                        @if($disneydiningtotal == null)
                                        <td><strong>$0.00</strong></td>
                                        @else
                                        <td><strong>${{ $disneydiningtotal }}</strong></td>
                                        @endif 
                                    </tr>                                   
                                </tbody>
                            </table>
                            @else
                            <table class="table datatable-button-html5-image">
                                <thead>
                                    <tr>
                                        <th><strong>Disney Dining Plan</strong></th>
                                        <th>Per Day:</th>
                                        <th>Total:</th>
                                    </tr>
                                </thead>
                                <tbody>
                                     <tr>
                                        @if($noofadults == null)     
                                        <td>Adult Dining Plan: 0 adult(s)</td>
                                        @else
                                        <td>Adult Dining Plan: {{ $noofadults }} adult(s)</td>
                                        @endif

                                        @if($disneydiningadultperday == null)
                                        <td>$0.00</td>
                                        @else
                                        <td>${{ $disneydiningadultperday }}</td>
                                        @endif

                                       @if($disneydiningadulttotal == null)
                                        <td>$0.00</td>
                                        @else
                                        <td>${{ $disneydiningadulttotal }}</td>
                                        @endif                                    
                                    </tr>
                                    <tr>
                                        @if($noofchildren == null)
                                        <td>Child Dining Plan: 0 children</td>
                                        @else
                                        <td>Child Dining Plan: {{ $noofchildren }} children</td>
                                        @endif

                                        @if($disneydiningchildperday == null)
                                        <td>$0.00</td>
                                        @else
                                        <td>${{$disneydiningchildperday}}</td>
                                        @endif

                                        @if($disneydiningchildtotal == null)
                                        <td>$0.00</td>
                                        @else
                                        <td>${{$disneydiningchildtotal }}</td>
                                        @endif                                        
                                        
                                    </tr>
                                    <tr>
                                        @if($noofadults == null && $noofchildren == null)
                                        <td>Total Rate: 0 adult(s) + 0 children</td>

                                        @elseif($noofadults != null && $noofchildren == null)
                                        <td>Total Rate: {{ $noofadults }} adult(s) + 0 children</td>

                                        @elseif($noofadults == null && $noofchildren != null)
                                        <td>Total Rate: 0 adult(s) + {{ $noofchildren }} children</td>

                                        @else
                                        <td>Total Rate: {{ $noofadults }} adult(s) + {{ $noofchildren }} children</td>
                                        @endif

                                        @if($disneydiningtotalperday == null)
                                        <td><strong>$0.00</strong></td>
                                        @else
                                        <td><strong>${{ $disneydiningtotalperday }}</strong></td>
                                        @endif

                                        @if($disneydiningtotal == null)
                                        <td><strong>$0.00</strong></td>
                                        @else
                                        <td><strong>${{ $disneydiningtotal }}</strong></td>
                                        @endif 
                                    </tr>                                   
                                </tbody>
                            </table>
                            @endif
                        </div>
                    </div>
                    <!-- /bordered striped table -->
                    <br><br>
                    <!-- Bordered striped table -->
                    <div class="col-md-offset-1">                      

                        <div class="table-responsive">
                            @if(Auth::user() == null)
                            <table class="table datatable-button-html5-image1">
                                <thead>
                                    <tr>
                                        <th><strong>Disney Deluxe Dining Plan</strong></th>
                                        <th>Per Day:</th>
                                        <th>Total:</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        @if($noofadults == null)     
                                        <td>Adult Dining Plan: 0 adult(s)</td>
                                        @else
                                        <td>Adult Dining Plan: {{ $noofadults }} adult(s)</td>
                                        @endif

                                        @if($deluxediningadultperday == null)
                                        <td>$0.00</td>
                                        @else
                                        <td>${{ $deluxediningadultperday }}</td>
                                        @endif

                                       @if($deluxediningadulttotal == null)
                                        <td>$0.00</td>
                                        @else
                                        <td>${{ $deluxediningadulttotal }}</td>
                                        @endif                                    
                                    </tr>
                                    <tr>
                                        @if($noofchildren == null)
                                        <td>Child Dining Plan: 0 children</td>
                                        @else
                                        <td>Child Dining Plan: {{ $noofchildren }} children</td>
                                        @endif

                                        @if($deluxediningchildperday == null)
                                        <td>$0.00</td>
                                        @else
                                        <td>${{$deluxediningchildperday}}</td>
                                        @endif

                                        @if($deluxediningchildtotal == null)
                                        <td>$0.00</td>
                                        @else
                                        <td>${{$deluxediningchildtotal }}</td>
                                        @endif                                        
                                        
                                    </tr>
                                    <tr>
                                        @if($noofadults == null && $noofchildren == null)
                                        <td>Total Rate: 0 adult(s) + 0 children</td>

                                        @elseif($noofadults != null && $noofchildren == null)
                                        <td>Total Rate: {{ $noofadults }} adult(s) + 0 children</td>

                                        @elseif($noofadults == null && $noofchildren != null)
                                        <td>Total Rate: 0 adult(s) + {{ $noofchildren }} children</td>

                                        @else
                                        <td>Total Rate: {{ $noofadults }} adult(s) + {{ $noofchildren }} children</td>
                                        @endif

                                        @if($deluxediningtotalperday == null)
                                        <td><strong>$0.00</strong></td>
                                        @else
                                        <td><strong>${{ $deluxediningtotalperday }}</strong></td>
                                        @endif

                                        @if($deluxediningtotal == null)
                                        <td><strong>$0.00</strong></td>
                                        @else
                                        <td><strong>${{ $deluxediningtotal }}</strong></td>
                                        @endif 
                                    </tr>                                  
                                </tbody>
                            </table>
                            @else
                            <table class="table datatable-button-html5-image">
                                <thead>
                                    <tr>
                                        <th><strong>Disney Deluxe Dining Plan</strong></th>
                                        <th>Per Day:</th>
                                        <th>Total:</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        @if($noofadults == null)     
                                        <td>Adult Dining Plan: 0 adult(s)</td>
                                        @else
                                        <td>Adult Dining Plan: {{ $noofadults }} adult(s)</td>
                                        @endif

                                        @if($deluxediningadultperday == null)
                                        <td>$0.00</td>
                                        @else
                                        <td>${{ $deluxediningadultperday }}</td>
                                        @endif

                                       @if($deluxediningadulttotal == null)
                                        <td>$0.00</td>
                                        @else
                                        <td>${{ $deluxediningadulttotal }}</td>
                                        @endif                                    
                                    </tr>
                                    <tr>
                                        @if($noofchildren == null)
                                        <td>Child Dining Plan: 0 children</td>
                                        @else
                                        <td>Child Dining Plan: {{ $noofchildren }} children</td>
                                        @endif

                                        @if($deluxediningchildperday == null)
                                        <td>$0.00</td>
                                        @else
                                        <td>${{$deluxediningchildperday}}</td>
                                        @endif

                                        @if($deluxediningchildtotal == null)
                                        <td>$0.00</td>
                                        @else
                                        <td>${{$deluxediningchildtotal }}</td>
                                        @endif                                        
                                        
                                    </tr>
                                    <tr>
                                        @if($noofadults == null && $noofchildren == null)
                                        <td>Total Rate: 0 adult(s) + 0 children</td>

                                        @elseif($noofadults != null && $noofchildren == null)
                                        <td>Total Rate: {{ $noofadults }} adult(s) + 0 children</td>

                                        @elseif($noofadults == null && $noofchildren != null)
                                        <td>Total Rate: 0 adult(s) + {{ $noofchildren }} children</td>

                                        @else
                                        <td>Total Rate: {{ $noofadults }} adult(s) + {{ $noofchildren }} children</td>
                                        @endif

                                        @if($deluxediningtotalperday == null)
                                        <td><strong>$0.00</strong></td>
                                        @else
                                        <td><strong>${{ $deluxediningtotalperday }}</strong></td>
                                        @endif

                                        @if($deluxediningtotal == null)
                                        <td><strong>$0.00</strong></td>
                                        @else
                                        <td><strong>${{ $deluxediningtotal }}</strong></td>
                                        @endif 
                                    </tr>                                  
                                </tbody>
                            </table>
                            @endif
                        </div>
                    </div>
                    <!-- /bordered striped table -->
                    <center><a class="btn btn-lg btn-primary" target="_blank" href="http://mandmmagicaladventures.com/free-quote.html" >Click for a Free Disney Vacation Quote</a></center>
 <!-- Footer --><br>
                    <div class="footer text-muted">
                        &nbsp &copy; 2019 Powered by <a href="https://www.nodlays.com/" target="_blank">Nodlays</a>
                    </div>
                    <!-- /footer -->

                </div>
                <!-- /content area -->

            </div>
            <!-- /main content -->

        </div>
        <!-- /page content -->

    </div>
    <!-- /page container -->
<script type="text/javascript">
    
    $(document).ready(function(){

        $('#DataTables_Table_0_filter').hide();
        $('#DataTables_Table_0_info').hide();
        $('#DataTables_Table_0_paginate').hide();
        $('#DataTables_Table_0_length').hide();
        

        $('#DataTables_Table_1_filter').hide();
        $('#DataTables_Table_1_info').hide();
        $('#DataTables_Table_1_paginate').hide();
        $('#DataTables_Table_1_length').hide();
        

        $('#DataTables_Table_2_filter').hide();
        $('#DataTables_Table_2_info').hide();
        $('#DataTables_Table_2_paginate').hide();
        $('#DataTables_Table_2_length').hide();

    });

    
</script>


@endsection









             