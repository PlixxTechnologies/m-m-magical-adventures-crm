<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
           

            <div class="content">
                <div class="title m-b-md">
                    M&M Magical Adventures
                </div>

                <div class="row">
                    <h2> Customer Information </h4> 
                    <span>Agent Name: </span>{{ $c->customer->user->name }} &nbsp;&nbsp; <span>Customer Name: </span> {{ $c->customer->first_name }} {{ $c->customer->last_name}} <br>
                    <span>Email: </span>{{ $c->customer->email }} &nbsp;&nbsp; <span>Country Code: </span>{{ $c->customer->country_code }} <br>
                    <span>Phone Number: </span>{{ $c->customer->phone_no }} &nbsp;&nbsp; <span>Address: </span>{{ $c->customer->address1 }} <br>
                    <span>City: </span>{{ $c->customer->city }} &nbsp;&nbsp; <span>State: </span>{{ $c->customer->state }} <br>
                    <span>Zip: </span>{{ $c->customer->zip }}
                </div>
                <br>
                <div class="row">
                    <h2> Trip Information </h4> 
                    <span>Reservation Number: </span>{{ $c->reservation_number }} &nbsp;&nbsp; <span>Booking Date: </span>{{ $c->booking_date }} <br>
                    <span>Check In Date: </span>{{ $c->checkin_date }} &nbsp;&nbsp; <span>Check Out Date: </span>{{ $c->checkout_date }} <br>
                    <span>Trip Status: </span>{{ $c->trip_status }} &nbsp;&nbsp; <span>Total Sale: </span>{{ $c->total_sale }} <br>
                    <span>Total Commission: </span>{{ $c->commission }} &nbsp;&nbsp; <span>Expected Commission: </span>{{ $c->expected_commission }} <br>
                    <span>Destinations: </span>{{ $c->destination }}
                </div>
            </div>
        </div>
    </body>
</html>
