@extends('admin.layout.dashboardx')

@section('content')
<!-- Datatables JS FILES -->

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.print.min.js"></script>

<!-- /Datatables JS FILES -->
   <!-- Page header -->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="{{ url('/home') }}">
                    <em class="fa fa-home"></em>
                </a></li>
                <li class="active">Destination Commission Reports</li>
            </ol>
        </div><!--/.row-->
    <div class="container-fluid"> 
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Destination Commission Reports</h1>
            </div>
        </div>  <!-- /page header -->
   </div>

<div class="container-fluid">
   
    <div class="row">
        <form action="{{ url('getcommission-destination-reports') }}" method="post">
          {!! csrf_field() !!}
          <input type="hidden" name="id" value="{{ $id }}">
        <div class="col-md-4 col-lg-4" style="padding: 20px;">
            <div class="form-group">
                <label>*Start Date:</label>
                <input type="date" name="startdate" class="form-control" @if($startdate != null) value="{{ $startdate }}" @endif required>
            </div>
        </div>

        <div class="col-md-offset-2 col-md-4 col-lg-offset-2 col-lg-4" style="padding: 20px;">
            <div class="form-group">
                <label>*End Date:</label>
                <input type="date" name="enddate" class="form-control"  @if($enddate != null) value="{{ $enddate }}" @endif required>
            </div>
        </div> 
  </div>
    <button type="submit" class="btn btn-primary pull-left">Get Report</button>
  </form>
</div>

<br>

@if($trips != null)
 <br><h3 class="box-title">Total Commission : <strong>${{ number_format($commissionTotal, 2) }}</strong></h3>
 <h3 class="box-title">Total Expected Commission : <strong>${{ number_format($expectedCommissionTotal, 2) }}</strong></h3>
  <h3 class="box-title">Total Sales : <strong>${{ number_format($TotalSales, 2) }}</strong></h3>
<br>
 <!-- Basic initialization -->
 <div style="padding-top:20px; padding-bottom:20px; padding-right:20px; padding-left:20px; overflow-x:auto;" class="panel panel-flat">
                    <div class="panel panel-flat">
                        <div class="panel-heading">
                            <h5 class="panel-title">Customers List</h5>
                        </div>
                <table id="example" class="display nowrap" style="width:100%">
                            <thead>
                                <tr>
                                <th data-sortable="true">Agent Name</th>
                                <th data-sortable="true">Trip ID</th>
                                <th data-sortable="true">Customer Name</th>
                                <th data-sortable="true">Reservation Number</th> 
                                <th data-sortable="true">Booking Date</th> 
                                <th data-sortable="true">Check In Date</th>
                                <th data-sortable="true">Check Out Date</th>
                                <th data-sortable="true">Destination</th>
                                <th data-sortable="true">Total Commission</th>
                                <th data-sortable="true">Expected Commission</th>
                                <th data-sortable="true">Total Sale</th>
                            </tr>
                            </thead>
                            <tbody>
                                  @foreach($trips as $trip)
                                                
                                    <td>
                                         {{ $trip->user->name }}
                                    </td>
                                    <td>
                                         {{ $trip->id }}
                                    </td>
                                    <td>
                                        {{ $trip->customer->first_name }} {{ $trip->customer->last_name }}
                                    </td>
                                    <td>
                                        {{ $trip->reservation_number }}
                                    </td>
                                    <td>
                                        {{ $trip->booking_date }}
                                    </td>
                                    <td>
                                        {{ $trip->checkin_date }}
                                    </td>
                                   <td>
                                        {{ $trip->checkout_date }}
                                   </td>
                                   <td>
                                        {{ $trip->destination }}
                                    </td> 
                                    <td>
                                        {{ number_format($trip->commission, 2) }}
                                    </td>
                                    <td>
                                        {{ number_format($trip->expected_commission, 2) }}
                                    </td>
                                    <td>
                                        {{ number_format($trip->total_sale, 2) }}
                                    </td>
                                  </tr>
                            @endforeach                               
                            </tbody>                            
                        </table>
                    </div>
                    </div>
                    <!-- /basic initialization -->
                    @endif

</div>
</div>
@endsection

