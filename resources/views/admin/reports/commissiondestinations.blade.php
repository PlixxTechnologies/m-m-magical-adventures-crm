@extends('admin.layout.dashboardx')

@section('content')
<!-- Datatables JS FILES -->

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.print.min.js"></script>

<!-- /Datatables JS FILES -->
   <!-- Page header -->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="{{ url('/home') }}">
                    <em class="fa fa-home"></em>
                </a></li>
                <li class="active">Destinations</li>
            </ol>
        </div><!--/.row-->
    <div class="container-fluid"> 
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Destinations</h1>
            </div>
        </div>  <!-- /page header -->
   </div>
        
<br>
<div class="container-fluid">

    <div class="row"> 
    <div class="form-group col-md-2 ">
    </div>

   <form action="{{ url('commission-destination-reports') }}" method="post" >
                            {!! csrf_field() !!}
        <div class="form-group col-md-12 ">
          <label><b>Destinations</b></label>
          <select class="form-control selectpicker" name="id" data-live-search="true" required="">
            <option value="">Choose One</option>
            @foreach($destinations as $destination)
              <option value="{{ $destination->id }}">{{ $destination->name }}</option>
            @endforeach  
          </select> 
       </div>
<br>
       <div class="form-group col-md-4">
        <button class="btn btn-primary" type="Submit">Submit</button>
      </div>

      <div class="form-group col-md-2 ">
    </div>
    </form>

</div>
</div>
</div>

@endsection