 @extends('admin.layout.dashboardx')

@section('content')
<!-- Datatables JS FILES -->

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.print.min.js"></script>

<!-- /Datatables JS FILES -->
<style>
.red{
    color: red;
}
</style>
<!-- Page header -->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="{{ url('/home') }}">
                    <em class="fa fa-home"></em>
                </a></li>
                <li class="active">Add Lead</li>
            </ol>
        </div><!--/.row-->
    <div class="container-fluid"> 
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Add Lead</h1>
            </div>
        </div>  <!-- /page header -->
   </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">

                        <form action="{{ url('lead/save') }}" method="post" >
                            {!! csrf_field() !!}

                            @if ($user->role == 1)
                                <div class="form-group">
                                    <label><span class="red">*</span>Agent Name:</label>
                                    <select name="userId" class="form-control" required>
                                        <option value="">Choose One</option>
                                    @foreach($allusers as $user)    
                                        <option value="{{ $user->id }}">{{ $user->name }}</option>
                                    @endforeach
                                    </select>
                                </div>
                            @else
                                <div class="form-group">
                                    <label>Agent Name:</label>
                                    <input type="text"  class="form-control" value="{{ $user->name }}" readonly>
                                </div>
                            @endif                            


                            <div class="form-group">
                                <label><span class="red">*</span>First Name:</label>
                                <input type="text" name="first_name"  class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label><span class="red">*</span>Last Name:</label>
                                <input type="text" name="last_name"  class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label><span class="red">*</span>Email:</label>
                                <input type="email" name="email" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label>Country Code:</label>
                                <input type="text" value="+1" class="form-control" readonly="">
                            </div>

                            <div class="form-group">
                                <label><span class="red">*</span>Phone Number:</label>
                                <input id="bar" type="text" name="phone_no" class="form-control" onkeydown="javascript:backspacerDOWN(this,event);" onkeyup="javascript:backspacerUP(this,event);" required>
                            </div>
                             <div class="form-group">
                                <label><span class="red">*</span>Address 1:</label>
                                <input type="text" name="address1" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label>Address 2:</label>
                                <input type="text" name="address2" class="form-control">
                            </div>
                            <div class="form-group">
                                <label><span class="red">*</span>City:</label>
                                <input type="text" name="city" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label><span class="red">*</span>State:</label>
                                <input type="text" maxlength="2" id="sessionNo" onkeypress="return isNumberKey(event)" name="state" class="form-control" required>

                            </div>
                            <div class="form-group">
                                <label><span class="red">*</span>Zip:</label>
                                <input type="text" name="zip" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label>Birthdate:</label>
                                <input type="date" name="birth_date" class="form-control">
                            </div>

                            <br />
                            <!-- Add Guest -->
                            <input type="hidden" name="count" id="count" value=0 />
                            <div id="guestSection">
                                
                            </div>
                            <div class="form-group">
                                <button onclick="addGuest()" type="button" class="btn btn-success">Add Guest</button>
                            </div>   
                            <button type="submit" class="btn btn-primary pull-right">Save Lead</button>
                        </form>
                    </div>
                </div>
            </div>
        </div><!--/.row-->
    </div>  <!--/.main-->
</div>

<script type="text/javascript">

   var guestCount = 1;
   var headingCount = 2;

   function addGuest(){

        var objTo = document.getElementById('guestSection');

        var guestDiv = document.createElement("div");
        guestDiv.setAttribute("class", "form-group removeclass" + guestCount);

        var removeGuestDiv = 'removeclass' + guestCount;
       
        var guestHeading = '<h3><strong>Guest No '+ headingCount +':</strong></h3>';

        var firstname = '<div class="form-group"><label><span class="red">*</span>First Name:</label><input type="text" name="guest_first_name'+ guestCount +'" id="guest_first_name'+ guestCount +'"  class="form-control" required></div>';

        var lastname = '<div class="form-group"><label><span class="red">*</span>Last Name:</label><input type="text" name="guest_last_name'+ guestCount +'" id="guest_last_name'+ guestCount +'"  class="form-control" required></div>';

        var birthdate = '<div class="form-group"><label>Birthdate:</label><input type="date" name="guest_birth_date'+ guestCount +'" id="guest_birth_date'+ guestCount +'"  class="form-control"></div>';

        var passport = '<div class="form-group"> <label>Valid Passport:</label><select name="guest_passport'+ guestCount +'" id="guest_passport'+ guestCount +'" class="form-control"><option value="">Choose One....</option><option value="Yes">Yes</option><option value="No">No</option></select></div>';


        var expirationdate = '<div class="form-group"><label>Expiration Date:</label><input type="date" name="guest_expire_date'+ guestCount +'" id="guest_expire_date'+ guestCount +'" class="form-control"></div>';

        var hiddenGuestId = '<input type="hidden" name="guest_no' + headingCount + '" id="guest_no' + headingCount + '" value='+ headingCount +' />';

        var deleteGuest = '<div class="form-group"><button onclick="removeGuest(' + guestCount + ');" type="button" class="btn btn-danger pull-right">Remove Guest</button></div>';

         guestDiv.innerHTML = guestHeading + firstname + lastname + birthdate + passport + expirationdate 
         + hiddenGuestId + deleteGuest;

        objTo.appendChild(guestDiv);

        document.getElementById('count').value = guestCount;

        guestCount++;
        headingCount++;

   }

   function removeGuest(gid){

         $('.removeclass' + gid).remove();
         guestCount--;
         headingCount--;
         document.getElementById('count').value = guestCount;
   }

</script>

<script type="text/javascript">
    var zChar = new Array(' ', '(', ')', '-', '.');
    var maxphonelength = 13;
    var phonevalue1;
    var phonevalue2;
    var cursorposition;

    function ParseForNumber1(object) {
        phonevalue1 = ParseChar(object.value, zChar);
    }

    function ParseForNumber2(object) {
        phonevalue2 = ParseChar(object.value, zChar);
    }

    function backspacerUP(object, e) {
        if (e) {
            e = e
        } else {
            e = window.event
        }
        if (e.which) {
            var keycode = e.which
        } else {
            var keycode = e.keyCode
        }

        ParseForNumber1(object)

        if (keycode >= 48) {
            ValidatePhone(object)
        }
    }

    function backspacerDOWN(object, e) {
        if (e) {
            e = e
        } else {
            e = window.event
        }
        if (e.which) {
            var keycode = e.which
        } else {
            var keycode = e.keyCode
        }
        ParseForNumber2(object)
    }

    function GetCursorPosition() {

        var t1 = phonevalue1;
        var t2 = phonevalue2;
        var bool = false
        for (i = 0; i < t1.length; i++) {
            if (t1.substring(i, 1) != t2.substring(i, 1)) {
                if (!bool) {
                    cursorposition = i
                    bool = true
                }
            }
        }
    }

    function ValidatePhone(object) {

        var p = phonevalue1

        p = p.replace(/[^\d]*/gi, "")

        if (p.length < 3) {
            object.value = p
        } else if (p.length == 3) {
            pp = p;
            d4 = p.indexOf('(')
            d5 = p.indexOf(')')
            if (d4 == -1) {
                pp = "(" + pp;
            }
            if (d5 == -1) {
                pp = pp + ")";
            }
            object.value = pp;
        } else if (p.length > 3 && p.length < 7) {
            p = "(" + p;
            l30 = p.length;
            p30 = p.substring(0, 4);
            p30 = p30 + ")"

            p31 = p.substring(4, l30);
            pp = p30 + p31;

            object.value = pp;

        } else if (p.length >= 7) {
            p = "(" + p;
            l30 = p.length;
            p30 = p.substring(0, 4);
            p30 = p30 + ")"

            p31 = p.substring(4, l30);
            pp = p30 + p31;

            l40 = pp.length;
            p40 = pp.substring(0, 8);
            p40 = p40 + "-"

            p41 = pp.substring(8, l40);
            ppp = p40 + p41;

            object.value = ppp.substring(0, maxphonelength);
        }

        GetCursorPosition()

        if (cursorposition >= 0) {
            if (cursorposition == 0) {
                cursorposition = 2
            } else if (cursorposition <= 2) {
                cursorposition = cursorposition + 1
            } else if (cursorposition <= 5) {
                cursorposition = cursorposition + 2
            } else if (cursorposition == 6) {
                cursorposition = cursorposition + 2
            } else if (cursorposition == 7) {
                cursorposition = cursorposition + 4
                e1 = object.value.indexOf(')')
                e2 = object.value.indexOf('-')
                if (e1 > -1 && e2 > -1) {
                    if (e2 - e1 == 4) {
                        cursorposition = cursorposition - 1
                    }
                }
            } else if (cursorposition < 11) {
                cursorposition = cursorposition + 3
            } else if (cursorposition == 11) {
                cursorposition = cursorposition + 1
            } else if (cursorposition >= 12) {
                cursorposition = cursorposition
            }

            var txtRange = object.createTextRange();
            txtRange.moveStart("character", cursorposition);
            txtRange.moveEnd("character", cursorposition - object.value.length);
            txtRange.select();
        }

    }

    function ParseChar(sStr, sChar) {
        if (sChar.length == null) {
            zChar = new Array(sChar);
        } else zChar = sChar;

        for (i = 0; i < zChar.length; i++) {
            sNewStr = "";

            var iStart = 0;
            var iEnd = sStr.indexOf(sChar[i]);

            while (iEnd != -1) {
                sNewStr += sStr.substring(iStart, iEnd);
                iStart = iEnd + 1;
                iEnd = sStr.indexOf(sChar[i], iStart);
            }
            sNewStr += sStr.substring(sStr.lastIndexOf(sChar[i]) + 1, sStr.length);

            sStr = sNewStr;
        }

        return sNewStr;
    }
    var clipboard = new Clipboard('.btn');

    clipboard.on('success', function(e) {
        console.log(e);
    });

    clipboard.on('error', function(e) {
        console.log(e);
    });
</script>

@endsection