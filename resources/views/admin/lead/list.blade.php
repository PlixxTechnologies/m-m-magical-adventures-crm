@extends('admin.layout.dashboardx')
@section('content')

<!-- Datatables JS FILES -->

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.print.min.js"></script>

<!-- /Datatables JS FILES -->

 <!-- Page header -->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="{{ url('/home') }}">
                <em class="fa fa-home"></em>
            </a></li>
            <li class="active">All Leads</li>
        </ol>
    </div>

    <div class="container-fluid"> 
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">All Leads</h1>
            </div>
        </div>
    </div>


    <div style="padding-top:20px; padding-bottom:20px; padding-right:20px; padding-left:20px; overflow-x: auto;" class="panel panel-flat">

       <table id="example" class="text-center display nowrap" style="width:100%">
            <thead>
                <tr>
                    <th class="text-center" data-sortable="true">Action</th>
                    <th data-sortable="true" class="text-center">Agent Name</th>
                    <th data-sortable="true" class="text-center">Customer ID</th>
                    <th data-sortable="true" class="text-center">First Name</th>
                    <th data-sortable="true" class="text-center">Last Name</th>
                    <th data-sortable="true" class="text-center">Email</th>
                    <th data-sortable="true" class="text-center">Phone Number</th>
                </tr>
            </thead>
            <tbody>
                @foreach($leads as $lead)
                    <tr>
                        <td class="center">
                            <a class="btn btn-success btn-sm" href="{{ url('quote/create/').'/'.$lead->id }}">
                                CREATE QUOTE
                            </a>
                            
                            @if(Auth::user()->role == 1)
                                <a class="btn btn-info btn-sm" href="{{ url('lead/edit').'/'.$lead->id }}">
                                    EDIT
                                </a>
                            @endif

                               

                            @if(Auth::user()->role == 1)
                                <form style="display:inline" method="post" action="{{ url('lead/delete').'/'.$lead->id }}" onsubmit="return confirm('Are you sure you want to delete this customer?');">
                                    {!! csrf_field() !!}
                                    {{ method_field('DELETE') }}
                                    <button type="submit" class="btn btn-danger btn-sm">DELETE</button>
                                </form>       
                            @endif

                            <a class="btn btn-primary btn-sm"  href="  {{ url('quote/QuoteHistory').'/'.$lead->id }}">
                                QUOTE HISTORY
                            </a>
                        </td>

                        @if($lead->user == null)
                        <td>
                             No Agent
                        </td>
                        @else
                        <td>
                             {{ $lead->user->name }}
                        </td>
                        @endif
                        <td>
                            {{ $lead->id }}
                        </td>
                        <td>
                            {{ $lead->first_name }}
                        </td>
                        <td>
                            {{ $lead->last_name }}
                        </td>
                        <td>
                            {{ $lead->email }}
                        </td>
                        <td>
                            {{ $lead->phone_no }}
                        </td>
                    </tr>
                @endforeach                               
            </tbody>
        </table>
    </div>
</div>

@endsection                