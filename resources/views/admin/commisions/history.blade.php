@extends('admin.layout.dashboardx')

@section('content')
<!-- Datatables JS FILES -->

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.print.min.js"></script>

<!-- /Datatables JS FILES -->  

<style>
    .red{
color:red;
    }
</style>
<!-- Page header -->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="{{ url('/home') }}">
                    <em class="fa fa-home"></em>
                </a></li>
                <li class="active">Commission Report History</li>
            </ol>
        </div><!--/.row-->
    <div class="container-fluid"> 
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Commission Report History</h1>
            </div>
        </div>  <!-- /page header -->
   </div>

<div class="container-fluid">


<!-- Basic initialization -->
<div style="padding-top:20px; padding-bottom:20px; padding-right:20px; padding-left:20px; overflow-x: auto;"   class="panel panel-flat">
    <div class="panel-heading">
        <div class="col-md-12">
            <!-- <h5 class="panel-title">Trips List</h5> -->
        </div>
    </div>

    <table id="example" class="display nowrap" style="width:100%">
        <thead>
            <tr>
                <th data-sortable="true">Report ID</th>
                <th data-sortable="true">Agent Name</th>
                <th data-sortable="true">Report Creation Date</th>
                <th data-sortable="true">Export Date</th>
                <th data-sortable="true">Payment Status</th>
                <th data-sortable="true">Commissions Paid</th>
            </tr>
        </thead>
        <tbody>
              @foreach($report as $reports)
              <tr>
                <td>
                    {{ $reports->id }}
                </td>              
                <td>
                     {{ $reports->agentName }}
                </td>
                <td>
                    {{ date('m-d-Y', strtotime($reports->creation_date)) }}
                </td>
                <td>
                    {{ 
                        date('m-d-Y', strtotime($reports->export_date)) 
                    }}
                </td>
                @if($reports->paymentstatus == 1)
                <td>
                    <span class="bg-success p-5">Processed</span>
                </td>
                @endif
                @if($reports->paymentstatus == 2)
                <td>
                    <span class="bg-primary p-5">Processing</span>
                </td>
                @endif
                @if($reports->paymentstatus == 3)
                <td>
                    <span class="bg-danger p-5">Incomplete</span>
                </td>
                @endif
                @if($reports->paymentstatus == 4)
                <td>
                    <span class="p-5" style="background:#f1f1f1;">Canceled</span>
                </td>
                @endif
                @if($reports->paymentstatus == 5)
                <td>
                    <span class="bg-success p-5">Processed</span>
                </td>
                @endif
                @if($reports->paymentstatus == 6)
                <td>
                    <span class="bg-warning p-5">Awaiting Supplier</span>
                </td>
                @endif
                @if($reports->paymentstatus == 5 || $reports->paymentstatus == 1)
                <td>
                    <strong>$</strong>{{ $reports->expectedcommssion}}
                </td>
                @else
                <td>
                    
                </td>
                @endif
              </tr>
            @endforeach                               
        </tbody>                            
    </table>
</div>
<!-- /basic initialization -->


</div>
</div>

@endsection

