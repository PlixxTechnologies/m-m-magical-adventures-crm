@extends('admin.layout.dashboardx')

@section('content')
<!-- Datatables JS FILES -->

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.print.min.js"></script>

<!-- /Datatables JS FILES -->  
<style>
    .red{
color:red;
    }
</style>
<!-- Page header -->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="{{ url('/home') }}">
                    <em class="fa fa-home"></em>
                </a></li>
                <li class="active">View Reports Data</li>
            </ol>
        </div><!--/.row-->
    <div class="container-fluid"> 
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">View Reports Data</h1>
            </div>
        </div>  <!-- /page header -->
   </div>

<style type="text/css">
    .modal-content{
        width: 150%;
    }
</style>

<div id="modal_theme_reports" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <form method="post">
            <div class="modal-content">
                <div class="modal-header bg-primary text-center">
                    <h3 class="modal-title text-center">Trips Information</h3>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <div class="modal-body">
                    <input type="hidden" id="xyz" name="xyz" />
                    <div class="row">
                        <table class="table custom-table">
                          <thead>
                            <tr>
                              <th scope="col">ReportID</th>
                              <th scope="col">Agent Name</th>
                              <th scope="col">Travel Month</th>
                              <th scope="col">Report Creation Date</th>
                              <th scope="col">Export Date</th>
                              <th scope="col">Payment Status</th>
                              <th scope="col">Expected Commission</th>
                              <th scope="col">YTD Commission</th>
                              <th scope="col">Action</th>
                            </tr>
                          </thead>
                          <tbody>
                          </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<div class="container">

@if($Savereports != null || $Savereports != "")
    @if(Auth::user()->role == 1)
        <!-- Basic initialization -->
        <div style="padding-top:20px; padding-bottom:20px; padding-right:20px; padding-left:20px;" class="panel panel-flat">
            <div class="panel-heading">
                <div class="col-md-12">
                    <!-- <h5 class="panel-title">View Reports Data</h5> -->
                </div>
            </div>

            <table id="example" class="display nowrap" style="width:100%">
                <thead>
                    <tr>
                        <th data-sortable="true">Agent Name</th>
                        <th data-sortable="true">Customer Name</th>
                        <th data-sortable="true">Reservation Number</th>
                        <th data-sortable="true">Destination</th>
                        <th data-sortable="true">Check In Date</th>
                        <th data-sortable="true">Check Out Date</th>
                        <th data-sortable="true">Commission</th>
                        <th data-sortable="true">Expected Commission</th>
                        <th data-sortable="true">Set Status</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($Savereports as $reports)
                        <tr>
                            
                            <td>
                                {{ $reports->agentName }}
                            </td>
                            <td>
                                {{ $reports->CustomerName }}
                            </td>

                            <td>
                                {{ $reports->reservation }}
                            </td>
                            <td>
                                {{ $reports->destination }}
                            </td>

                            <td>
                                {{ date('m-d-Y', strtotime($reports->CheckinDate)) }}
                            </td>
                            <td>
                                {{ date('m-d-Y', strtotime($reports->CheckoutDate)) }}
                            </td>
                            <td>
                                {{ $reports->ytdcommission }}
                            </td>
                            <td>
                                {{ $reports->expectedcommsion }}
                            </td>
                            @if($getpay == 0)
                                @if($reports->paymentstatus == 1)
                                <td>
                                    <span class="bg-success p-5">Processed</span>
                                </td>
                                @endif
                                @if($reports->paymentstatus == 2)
                                <td>
                                    <span class="bg-primary p-5">Processing</span>
                                </td>
                                @endif
                                @if($reports->paymentstatus == 3)
                                <td>
                                    <span class="bg-danger p-5">Incomplete</span>
                                </td>
                                @endif
                                @if($reports->paymentstatus == 4)
                                <td>
                                    <span class="p-5" style="background:#f1f1f1;">Canceled</span>
                                </td>
                                @endif
                                @if($reports->paymentstatus == 5)
                                <td>
                                    <span class="bg-success p-5">Paid</span>
                                </td>
                                @endif
                                @if($reports->paymentstatus == 6)
                                <td>
                                    <span class="bg-warning p-5">Awaiting_Supplier</span>
                                </td>
                                @endif
                            @endif    
                            @if($getpay != 0)
                                @if($reports->paymentstatus == 1)
                                <td>
                                    <span class="bg-success p-5">Processed</span>
                                </td>
                                @endif
                                @if($reports->paymentstatus == 2)
                                <td>
                                    <span class="bg-primary p-5">Processing</span>
                                </td>
                                @endif
                                @if($reports->paymentstatus == 3)
                                <td>
                                    <span class="bg-danger p-5">Incomplete</span>
                                </td>
                                @endif
                                @if($reports->paymentstatus == 4)
                                <td>
                                    <span class="p-5" style="background:#f1f1f1;">Canceled</span>
                                </td>
                                @endif
                                @if($reports->paymentstatus == 5)
                                <td>
                                    <span class="bg-success p-5">Paid</span>
                                </td>
                                @endif
                                @if($reports->paymentstatus == 6)
                                <td>
                                    <span class="bg-warning p-6">Awaiting_Supplier</span>
                                </td>
                                @endif
                            @endif                            
                        </tr>
                    @endforeach                               
                </tbody>                            
            </table>
        </div>
        <!-- /basic initialization -->
    @endif
    @if(Auth::user()->role != 1)
        <!-- Basic initialization -->
        <div class="panel panel-flat">
            <div class="panel-heading">
                <div class="col-md-12">
                    <h5 class="panel-title">Trips List</h5>
                </div>
            </div>

            <table class="table datatable-button-init-custom">
                <thead>
                    <tr>
                    <th data-sortable="true">Agent Name</th>
                    <th data-sortable="true">Customer Name</th>
                    <th data-sortable="true">Reservation Number</th>
                    <th data-sortable="true">Destination</th>
                    <th data-sortable="true">Check In Date</th>
                    <th data-sortable="true">Check Out Date</th>
                    <th data-sortable="true">Commission</th>
                    <th data-sortable="true">Expected Commission</th>
                    <th data-sortable="true">Set Status</th>
                </tr>
                </thead>
                <tbody>
                      @foreach($Savereports as $trip)
                        <td>
                            Null
                        </td>
                        <td>
                            Null
                        </td>
                        <td>
                            {{ $trip->reservation_number }}
                        </td>   
                        <td>
                            {{ $trip->destination }}
                        </td>                                   
                        <td>
                            {{ $trip->checkin_date }}
                        </td>
                       <td>
                            {{ $trip->checkout_date }}
                       </td>
                        <td>
                            {{ $trip->commission }}
                        </td>
                         <td>
                            {{ $trip->total_sale }}
                        </td>
                        <td>
                            {{ $trip->expected_commission }}
                        </td>
                       <!-- <td>
                           {{ Carbon\Carbon::parse($trip->created_at)->format('d M Y') }}
                        </td> -->
                      </tr>
                @endforeach                               
                </tbody>                            
            </table>
        </div>
        <!-- /basic initialization -->
    @endif
@endif

</div>

@endsection

