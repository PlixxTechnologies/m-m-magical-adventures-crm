@extends('admin.layout.dashboardx')

@section('content')
<!-- Datatables JS FILES -->

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.print.min.js"></script>

<!-- /Datatables JS FILES -->
<style>
    .red{
color:red;
    }
</style>

<!-- Page header -->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="{{ url('/home') }}">
                    <em class="fa fa-home"></em>
                </a></li>
                <li class="active">Commission Payout</li>
            </ol>
        </div><!--/.row-->
    <div class="container-fluid"> 
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Commission Payout</h1>
            </div>
        </div><!--/.row-->    <!-- /page header -->
   </div>

<style type="text/css">
    .modal-content{
        width: 150%;
    }
</style>

<div class="container-fluid">

@if($Savereports != null || $Savereports != "")
    @if(Auth::user()->role == 1)
        <!-- Basic initialization -->
        <div style="padding-top:20px; padding-bottom:20px; padding-right:20px; padding-left:20px; overflow-x:auto;"  class="panel panel-flat">
            <div class="panel-heading">
                <div class="col-md-12">
                    <h5 class="panel-title">Edit Commission Payout</h5>
                </div>
            </div>

           <table id="example" class="display nowrap" style="width:100%">
                <thead>
                    <tr>
                        <th data-sortable="true">Agent Name</th>
                        <th data-sortable="true">Customer Name</th>
                        <th data-sortable="true">Reservation Number</th>
                        <th data-sortable="true">Destination</th>
                        <th data-sortable="true">Check In Date</th>
                        <th data-sortable="true">Check Out Date</th>
                        <th data-sortable="true">Commission</th>
                        <th data-sortable="true">Expected Commission</th>
                        <th data-sortable="true">Set Status</th>
                        <th data-sortable="true">Set Status All Paid
                            <form id="CheckForm" action="{{ url('ChecktoPaid') }}" method="post" >
                                {!! csrf_field() !!}
                                <input type="checkbox" id="checkAll">
                                <input type="hidden" name="savereportid" id="savereportid" value="{{$wajeeh}}">
                            </form>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($Savereports as $reports)
                        <tr>
                            
                            <td>
                                {{ $reports->agentName }}
                            </td>
                            <td>
                                {{ $reports->CustomerName }}
                            </td>

                            <td>
                                {{ $reports->reservation }}
                            </td>
                            <td>
                                {{ $reports->destination }}
                            </td>

                            <td>
                                {{ date('m-d-Y', strtotime($reports->CheckinDate)) }}
                            </td>
                            <td>
                                {{ date('m-d-Y', strtotime($reports->CheckoutDate)) }}
                            </td>
                            <td>
                                {{ $reports->ytdcommission }}
                            </td>
                            <td>
                                {{ $reports->expectedcommsion }}
                            </td>
                            <td>
                                <form id="reportForm" action="{{ url('statusChange') }}" method="post" >
                                    {!! csrf_field() !!}
                                    <?php
                                        $abc = "status".$reports->id;
                                     ?>
                                    <select onchange="Report('{{$reports->id}}')" id="{{$abc}}">
                                        @if($reports->paymentstatus == null)
                                            <option value="" selected="selected">Status</option>
                                            <option value="1">Processed</option>
                                            <option value="2">Processing</option>
                                            <option value="3">Incomplete</option>
                                            <option value="4">Canceled</option>
                                            <option value="5">Paid</option>
                                            <option value="6">Awaiting Supplier</option>

                                        @endif
                                        
                                        @if($reports->paymentstatus == 1)
                                            <option value="1" selected="selected">Processed</option>
                                            <option value="2">Processing</option>
                                            <option value="3">Incomplete</option>
                                            <option value="4">Canceled</option>
                                            <option value="5">Paid</option>
                                            <option value="6">Awaiting Supplier</option>
                                        @endif
                                        
                                        @if($reports->paymentstatus == 2)
                                            <option value="1">Processed</option>
                                            <option value="2" selected="selected">Processing</option>
                                            <option value="3">Incomplete</option>
                                            <option value="4">Canceled</option>
                                            <option value="5">Paid</option>
                                            <option value="6">Awaiting Supplier</option>
                                        @endif

                                        @if($reports->paymentstatus == 3)
                                            <option value="1">Processed</option>
                                            <option value="2">Processing</option>
                                            <option value="3" selected="selected">Incomplete</option>
                                            <option value="4">Canceled</option>
                                            <option value="5">Paid</option>
                                            <option value="6">Awaiting Supplier</option>
                                        @endif

                                        @if($reports->paymentstatus == 4)
                                            <option value="1">Processed</option>
                                            <option value="2">Processing</option>
                                            <option value="3">Incomplete</option>
                                            <option value="4" selected="selected">Canceled</option>
                                            <option value="5">Paid</option>
                                            <option value="6">Awaiting Supplier</option>
                                        @endif

                                        @if($reports->paymentstatus == 5)
                                            <option value="1">Processed</option>
                                            <option value="2">Processing</option>
                                            <option value="3">Incomplete</option>
                                            <option value="4">Canceled</option>
                                            <option value="5" selected="selected">Paid</option>
                                            <option value="6">Awaiting Supplier</option>
                                        @endif

                                        @if($reports->paymentstatus == 6)
                                            <option value="1">Processed</option>
                                            <option value="2">Processing</option>
                                            <option value="3">Incomplete</option>
                                            <option value="4">Canceled</option>
                                            <option value="5">Paid</option>
                                            <option value="6" selected="selected">Awaiting Supplier</option>
                                        @endif
                                    </select>
                                    <input type="hidden" name="status" id="status1">
                                    <input type="hidden" name="hid" id="hid">
                                    <input type="hidden" name="actualid" value="{{$wajeeh}}">
                                </form>
                            </td>
                            <td>
                                <form id="StatusForm" action="{{ url('ChecktoPaid') }}" method="post" >
                                    {!! csrf_field() !!}
                                    <?php
                                        $check = "check".$reports->id;
                                     ?>
                                    <input type="checkbox" name="allpaid" onclick="SingleRecord('{{ $reports->id }}')" id="{{$check}}">
                                    <input type="hidden" name="check" id="check1">
                                    <input type="hidden" name="dataid" id="dataid">
                                    <input type="hidden" name="actualids" value="{{$wajeeh}}">
                                </form>
                            </td>
                        </tr>
                    @endforeach                               
                </tbody>                            
            </table>
        </div>
    </div>
        <!-- /basic initialization -->
    @endif
@endif

<script type="text/javascript">
    $("#checkAll").click(function(){
        $('input:checkbox').not(this).prop('checked', this.checked);
        $("#CheckForm").submit();
    });
    
    function SingleRecord(getreportid){
        var check = $("#check"+getreportid).val();
        if(check != "" && check != undefined && check != null && getreportid != "" && getreportid != undefined && getreportid != null){
            $("#check1").val(check);
            $("#dataid").val(getreportid);
            //alert($("#check1").val());
            //alert($("#dataid").val());
            $("#StatusForm").submit();
        }
    }

    function Report(reportid) {
        var status = $('#status'+reportid).val();
        if(status != "" && status != undefined && status != null && reportid != "" && reportid != undefined && reportid != null){
            $("#hid").val(reportid);
            $("#status1").val(status);
            $("#reportForm").submit();
        }
        // $.ajaxSetup({
        //   headers: {
        //     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        //   }
        // });
        // var status = $('#status').val();
        // var reportid = $('#reportid').val();
        // console.log(status);

        // if(status != "" && reportid != ""){
        //         $.ajax({
        //             url: '/statusChange',
        //             type: "post",
        //             dataType: "json",
        //             cache:false,
        //             data: {
        //                 '_token': "{{ csrf_token() }}",
        //                 'Id': status,
        //                 'reportid': reportid,
        //             },
        //             success: function(response){// What to do if we succeed
        //                     alert(response);
        //                 },
        //             error: function(response){
        //                 alert('Error', response);
        //             }
        //         });
            
        // }
        
    }

    // $(document).ready(function() {
    //     $("").on('change', function() {
    //         var level = $(this).val();
    //         if(level){
    //             $.ajax({
    //                 url: 'statusChange',
    //                 type: "post",
    //                 data: {
    //                     val: level
    //                 },
    //                 success: function(response){// What to do if we succeed
    //                         alert(response);
    //                     }
    //             });
    //         }
    //     });
    // });
</script>
</div>

@endsection

