@extends('admin.layout.dashboardx')

@section('content')
<!-- Datatables JS FILES -->

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.print.min.js"></script>

<!-- /Datatables JS FILES -->  
<style>
    .red{
color:red;
    }
</style>

<!-- Page header -->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="{{ url('/home') }}">
                    <em class="fa fa-home"></em>
                </a></li>
                <li class="active">My Commission Reports</li>
            </ol>
        </div><!--/.row-->
    <div class="container-fluid"> 
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">My Commission Reports</h1>
            </div>
        </div>  <!-- /page header -->
   </div>

<div class="container-fluid">

<div class="row">
    <form action="{{ url('my-commission') }}" method="get">
        {!! csrf_field() !!}
        <input type="hidden" name="id" id="agentId" value="{{ Auth::user()->id }}">
        <div class="col-md-4 col-lg-4" style="padding: 20px;">
            <div class="form-group">
                <label>Travel Month</label>
                <select name="month" class="form-control">
                    <option value="">Select Travel Month</option>
                    <option value="January">January</option>
                    <option value="February">February</option>
                    <option value="March">March</option>
                    <option value="April">April</option>
                    <option value="May">May</option>
                    <option value="June">June</option>
                    <option value="July">July</option>
                    <option value="August">August</option>
                    <option value="September">September</option>
                    <option value="October">October</option>
                    <option value="November">November</option>
                    <option value="December">December</option>
                 </select> 
            </div>
        </div>

        <div class="col-md-4 col-lg-4" style="padding-top: 47px;"> 
            <button type="submit" class="btn btn-primary">Get Report</button>
        </div>
    </form>
</div>
<br>

<!-- Basic initialization -->
<div style="padding-top:20px; padding-bottom:20px; padding-right:20px; padding-left:20px; overflow-x: auto;" class="panel panel-flat">


    <table id="example" class="display nowrap" style="width:100%">
        <thead>
            <tr>
                <th data-sortable="true">Report ID</th>
                <th data-sortable="true">Agent Name</th>
                <th data-sortable="true">Travel Month</th>
                <th data-sortable="true">Report Creation Date</th>
                <th data-sortable="true">Export Date</th>
                <th data-sortable="true">Payment Status</th>
                <th data-sortable="true">Expected Commission</th>
                <th data-sortable="true">Commissions Paid</th>
                <th data-sortable="true">Cancel Report</th>
                <th data-sortable="true">Action</th>
            </tr>
        </thead>
        <tbody>
              @foreach($report as $reports)
              @if($reports->isActive != 0)
              <tr>
                <td>
                    {{ $reports->id }}
                </td>              
                <td>
                     {{ Auth::user()->name }}
                </td>
                <td>
                    {{ $reports->travelmonth }}
                </td>
                <td>
                    {{ date('m-d-Y', strtotime($reports->creation_date)) }}
                </td>
                <td>
                    {{ date('m-d-Y', strtotime($reports->export_date)) }}
                </td>
                @if($reports->paymentstatus == null || $reports->paymentstatus == "")
                <td>
                    
                </td>
                @endif
                @if($reports->paymentstatus == 1)
                <td>
                    <span class="bg-success p-5">Processed</span>
                </td>
                @endif
                @if($reports->paymentstatus == 2)
                <td>
                    <span class="bg-primary p-5">Processing</span>
                </td>
                @endif
                @if($reports->paymentstatus == 3)
                <td>
                    <span class="bg-danger p-5">Incomplete</span>
                </td>
                @endif
                @if($reports->paymentstatus == 4)
                <td>
                    <span class="p-5" style="background:#f1f1f1;">Canceled</span>
                </td>
                @endif
                @if($reports->paymentstatus == 5)
                <td>
                    <span class="bg-success p-5">Paid</span>
                </td>
                @endif
                @if($reports->paymentstatus == 6)
                <td>
                    <span class="bg-warning p-5">Awaiting_Supplier</span>
                </td>
                @endif
                <td>
                   <strong>$</strong> {{ number_format($reports->expectedcommssion, 2) }}
                </td>
                @if($reports->paymentstatus == 5 || $reports->paymentstatus == 1)
                <td>
                    <strong>$</strong>{{ number_format($reports->expectedcommssion, 2) }}
                </td>
                @else
                <td>
                    
                </td>
                @endif
                @if($reports->paymentstatus != 4)
                <td>
                    <form id="cancelrequestForm" method="post" action="{{ url('cancel-request').'/'.$reports->id }}">
                        {!! csrf_field() !!}
                        <input type="hidden" name="role" id="agentIds">
                        <input type="hidden" name="startdate" id="startdates">
                        <input type="hidden" name="enddate" id="enddates">
                        <button class="btn btn-secondary btn-sm" onclick="CancelRequest('{{$reports->id}}')">Cancel Report</button>
                    </form>
                </td>
                @else
                <td>
                    
                </td>
                @endif

                <td>
                    <a class="btn" style="background: #f1f1f1; color: #000000" href="{{ url('commisions/history').'/'.$reports->id }}" target="_blank">History</a>
                </td>

              </tr>
              @endif
            @endforeach                               
        </tbody>                            
    </table>
</div>
<!-- /basic initialization -->


</div>
</div>


<script type="text/javascript">

    function CancelRequest(id){
        var objAgent = $('#agentId').val();
        var objstartdate = $('#startdate').val();
        var objenddate = $('#enddate').val();

        if(objAgent != "" && objAgent != undefined && objAgent != null &&
         objstartdate != "" && objstartdate != undefined && objstartdate != null &&
         objenddate != "" && objenddate != undefined && objenddate != null){
            $('#agentIds').val(objAgent);
            $('#startdates').val(objstartdate);
            $('#enddates').val(objenddate);
            $("#cancelrequestForm").submit();
        }
    }
</script>

@endsection

