@extends('admin.layout.dashboardx')
@section('content')
<!-- Datatables JS FILES -->

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.print.min.js"></script>

<!-- /Datatables JS FILES -->
<!-- Page header -->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="{{ url('/home') }} ">
                    <em class="fa fa-home"></em>
                </a></li>
                <li class="active">Audit Logs</li>
            </ol>
        </div><!--/.row-->
    <div class="container-fluid"> 
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Audit Logs</h1>
            </div>
        </div>  <!-- /page header -->
   </div>

<!-- Basic initialization -->
                    <div style="padding-top:20px; padding-bottom:20px; padding-right:20px; padding-left:20px; overflow-x:auto;" class="panel panel-flat">

                       <table id="example" class="display nowrap" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Action</th>
									<th data-sortable="true">Trip Id</th>
									<th data-sortable="true">Reservation Number</th>
									<th data-sortable="true">Date</th>
									<th data-sortable="true">Event</th>
									<th data-sortable="true">Agent Name</th>
								</tr>                            
                            </thead>
                            <tbody>
                                  @foreach($auditlog as $row)             
                                <tr>
                                    <td>
                                        <form style="display:inline" method="post" action="{{url('auditlog/delete')}}/{{$row['id']}}" onsubmit="return confirm('Are you sure you want to delete this Log?');">
                                            {!! csrf_field() !!}
                                            {{ method_field('DELETE') }}
                                            <button type="submit" class="btn btn-danger btn-sm">DELETE</button>
                                        </form>
                                    </td>

                                    @if($row['tripId'] == null)
                                    <td value="{{ $row['id'] }}">
                                         No Trip
                                    </td>
                                    @else
                                    <td value="{{ $row['id'] }}">
                                         {{ $row['tripId'] }}
                                    </td>
                                    @endif

                                    @if($row['reservation_number'] == null)
                                    <td>
                                         No Reservation
                                    </td>
                                    @else
                                    <td>
                                         {{ $row['reservation_number'] }}
                                    </td>
                                    @endif
                                    <td>
                                        {{ $row['change_date'] }}
                                    </td>
                                    <td>
                                        {{ $row['event'] }}
                                    </td>
                                    <td>
                                        {{ $row['agent_name'] }}
                                    </td>
                                  </tr>
                            @endforeach                               
                            </tbody>
                        </table>
                    </div>
                    <!-- /basic initialization -->   
                    

                </div>
                <!-- /content area -->

            </div>
            <!-- /main content -->

@endsection
