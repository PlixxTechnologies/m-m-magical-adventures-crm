@extends('admin.layout.dashboardx')
@section('content')

<p>
	<div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Total Sales of Year
                </div>
                <div class="panel-body">
                    <div class="canvas-wrapper">
                        <canvas class="main-chart" id="line-chart" height="200" width="600"></canvas>
                    </div>
                    <form  id="agentGraphId" action="{{ url('/home').'/'.Auth::user()->id }}" method="POST">
                        <input type="hidden" name="agentId" id="agentId" value="{{ Auth::user()->id }} ">
                    </form>
                </div>
            </div>
        </div>
    </div>
</p>

<script type="text/javascript">

var lineChartData = {
    labels : data.month,
    datasets : [
        {
            label: "My First dataset",
            fillColor : "rgba(220,220,220,0.2)",
            strokeColor : "rgba(220,220,220,1)",
            pointColor : "rgba(220,220,220,1)",
            pointStrokeColor : "#fff",
            pointHighlightFill : "#fff",
            pointHighlightStroke : "rgba(220,220,220,1)",
            data : [1,2,3]
        },
        {
            label: "My Second dataset",
            fillColor : "rgba(48, 164, 255, 0.2)",
            strokeColor : "rgba(48, 164, 255, 1)",
            pointColor : "rgba(48, 164, 255, 1)",
            pointStrokeColor : "#fff",
            pointHighlightFill : "#fff",
            pointHighlightStroke : "rgba(48, 164, 255, 1)",
            data : [4,5,6]
        }
    ]
}

    
</script>

@endsection
