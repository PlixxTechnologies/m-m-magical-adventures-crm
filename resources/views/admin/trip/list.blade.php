@extends('admin.layout.dashboardx')
@section('content')
<!-- Datatables JS FILES -->

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.print.min.js"></script>

<!-- /Datatables JS FILES -->

<!-- Page header -->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="{{ url('/home') }}">
                    <em class="fa fa-home"></em>
                </a></li>
                <li class="active">All Trips</li>
            </ol>
        </div><!--/.row-->
    <div class="container-fluid"> 
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">All Trips</h1>
            </div>
        </div>  <!-- /page header -->
   </div>


<!-- Basic initialization -->
                    <div style="padding-top:20px; padding-bottom:20px; padding-right:20px; padding-left:20px; overflow-x:auto;" class="panel panel-flat">
                        <table id="example" class="display nowrap" style="width:100%">
                            <thead>
                                <tr>
                                <th>Action</th>
                                <th>Agent Name</th>
                                <th>Trip ID</th>
                                <th>Booking Date</th>
                                <th>Reservation Number</th>                               
                                <th>Customer Name</th>
                                <th>Check In Date</th>
                                <th>Check Out Date</th>
                                <th>Destination</th>
                                <th>Trip Status</th>
                                <th>Total Sale</th>                          
                            </tr>
                            </thead>
                            <tbody>
                                  @foreach($trips as $trip)                                          
                                    <td class="center">
                                        <a class="btn btn-success btn-sm" target="_blank" @if($trip->customer != null) href="{{ url('trip/view').'/'.$trip->id }}" @endif>
                                            VIEW
                                        </a>
                                        @if($trip->todo == null)
                                        <a class="btn btn-default btn-sm" @if($trip->customer != null) href="{{ url('todo/create').'/'.$trip->id }}" @endif>
                                           ADD TO DO
                                        </a>
                                        @endif
                                        <a class="btn btn-info btn-sm" @if($trip->customer != null) href="{{ url('trip/edit').'/'.$trip->id }}" @endif>
                                            EDIT
                                        </a>

                                        <a class="btn btn-primary btn-sm" target="_blank" @if($trip->customer != null) href="{{ url('trip/AddPayment').'/'.$trip->id }}" @endif>
                                            ADD Payment
                                        </a> 
                                        @if($user->role == 1)
                                        <form style="display:inline" method="post" action="{{ url('trip/delete') }}/{{$trip->id}}" onsubmit="return confirm('Are you sure you want to delete this trip?');">
                                            {!! csrf_field() !!}
                                            {{ method_field('DELETE') }}
                                            <button type="submit" class="btn btn-danger btn-sm">DELETE</button>
                                        </form>
                                         @endif

                                        @if($trip->status == 0)
                                         <form method="post" action="{{ url('trip/CancelTrip') }}/{{$trip->id}}" onsubmit="confirm('Are you sure you want to Cancel this trip?');">
                                            {!! csrf_field() !!}
                                            
                                            <button type="submit" class="btn btn-secondary btn-sm">Cancel Trip</button>
                                             
                                         </form>
                                        @else
                                            <a class="btn btn-warning btn-sm">
                                                Trip Cancelled
                                            </a> 
                                        @endif
                                    </td>                               

                                    @if($trip->user == null)
                                    <td>
                                         Agent has been deleted
                                    </td>
                                    @else
                                    <td>
                                         {{ $trip->user->name }}
                                    </td>
                                    @endif

                                    <td>{{ $trip->id }}</td>
                                    
                                    <td>
                                        {{ date("m", strtotime($trip->booking_date)) }}-{{ date("d", strtotime($trip->booking_date)) }}-{{ date("Y", strtotime($trip->booking_date)) }}    
                                    </td>

                                    <td>
                                    <a target="_blank" href="{{ url('trip/view').'/'.$trip->id }}">    {{ $trip->reservation_number }}</a>
                                    </td>
                                    @if($trip->customer == null)
                                    <td>
                                         Customer has been deleted
                                    </td>
                                    @else
                                    <td>
                                     <a target="_blank" href="{{ url('trip/view').'/'.$trip->id }}">    {{ $trip->customer->first_name }} {{ $trip->customer->last_name }}</a>
                                    </td>
                                    @endif
                                    @if($user->role == 1 || $trip->user_id == $user->id )                                    
                                        <td>
                                            {{ date("m", strtotime($trip->checkin_date)) }}-{{ date("d", strtotime($trip->checkin_date)) }}-{{ date("Y", strtotime($trip->checkin_date)) }}                                            
                                        </td>
                                        <td>
                                            {{ date("m", strtotime($trip->checkout_date)) }}-{{ date("d", strtotime($trip->checkout_date)) }}-{{ date("Y", strtotime($trip->checkout_date)) }}    
                                        </td>
                                    @else
                                        <td>
                                            Not Displayed
                                        </td>
                                        <td>
                                            Not Displayed
                                        </td>
                                    @endif

                                   <td>
                                        {{ $trip->destination }}
                                    </td>
                                    @if($trip->status != 1)
                                        <td>
                                            {{ $trip->trip_status }}
                                        </td>
                                    @else
                                        <td>
                                            Canceled
                                        </td>
                                    @endif
                                    <td>
                                        {{ $trip->total_sale }}
                                    </td>       
                                  </tr>
                            @endforeach                               
                            </tbody>
                        </table>
                    </div>
                    <!-- /basic initialization -->



                    <!-- Page header 
                <div class="page-header page-header-default">
                    <div class="page-header-content">
                        <div class="page-title">
                            <h4><span class="text-semibold">TO-Do's Trips</span></h4>
                        </div>
                    </div>
                </div>
  
                    <div class="panel panel-flat">

                        <table class="table datatable-button-init-basic">
                            <thead>
                                <tr>
                                <th>Action</th>
                                <th data-sortable="true">Agent Name</th>
                                <th data-sortable="true">Trip ID</th>
                                <th data-sortable="true">Booking Date</th>
                                <th data-sortable="true">Reservation Number</th>                               
                                <th data-sortable="true">Customer Name</th>
                                <th data-sortable="true">Check In Date</th>
                                <th data-sortable="true">Check Out Date</th>
                                <th data-sortable="true">Destination</th>
                                <th data-sortable="true">Trip Status</th>
                                <th data-sortable="true">Total Sale</th>                          
                            </tr>
                            </thead>
                            <tbody>
                                  @foreach($todoTrips as $trip)                                                  
                                    <td class="center">
                                        <a class="btn btn-success btn-sm" target="_blank" href="{{ url('trip/view').'/'.$trip->id }}">
                                            VIEW
                                        </a>
                                        <a class="btn btn-default btn-sm" href="{{ url('todo/create').'/'.$trip->id }}">
                                           ADD TO DO
                                        </a>
                                        <a class="btn btn-info btn-sm" href="{{ url('trip/edit').'/'.$trip->id }}">
                                            EDIT
                                        </a>

                                        <a class="btn btn-primary btn-sm" target="_blank" href="{{ url('trip/AddPayment').'/'.$trip->id }}">
                                            ADD Payment
                                        </a> 
                                        @if($user->role == 1)
                                        <form style="display:inline" method="post" action="{{ url('trip/delete') }}/{{$trip->id}}" onsubmit="return confirm('Are you sure you want to delete this trip?');">
                                            {!! csrf_field() !!}
                                            {{ method_field('DELETE') }}
                                            <button type="submit" class="btn btn-danger btn-sm">DELETE</button>
                                        </form>
                                         @endif

                                         
                                    </td>                               

                                    @if($trip->user == null)
                                    <td>
                                         Agent has been deleted
                                    </td>
                                    @else
                                    <td>
                                         {{ $trip->user->name }}
                                    </td>
                                    @endif

                                    <td>{{ $trip->id }}</td>
                                    
                                    <td>
                                        {{ date("m", strtotime($trip->booking_date)) }}-{{ date("d", strtotime($trip->booking_date)) }}-{{ date("Y", strtotime($trip->booking_date)) }}    
                                    </td>

                                    <td>
                                        {{ $trip->reservation_number }}
                                    </td>
                                    @if($trip->customer == null)
                                    <td>
                                         Customer has been deleted
                                    </td>
                                    @else
                                    <td>
                                         {{ $trip->customer->first_name }} {{ $trip->customer->last_name }}
                                    </td>
                                    @endif
                                    @if($user->role == 1 || $trip->user_id == $user->id )                                    
                                        <td>
                                            {{ date("m", strtotime($trip->checkin_date)) }}-{{ date("d", strtotime($trip->checkin_date)) }}-{{ date("Y", strtotime($trip->checkin_date)) }}                                            
                                        </td>
                                        <td>
                                            {{ date("m", strtotime($trip->checkout_date)) }}-{{ date("d", strtotime($trip->checkout_date)) }}-{{ date("Y", strtotime($trip->checkout_date)) }}    
                                        </td>
                                    @else
                                        <td>
                                            Not Displayed
                                        </td>
                                        <td>
                                            Not Displayed
                                        </td>
                                    @endif

                                   <td>
                                        {{ $trip->destination }}
                                    </td>
                                    <td>
                                        {{ $trip->trip_status }}
                                    </td>
                                    <td>
                                        {{ $trip->total_sale }}
                                    </td>       
                                  </tr>
                            @endforeach                               
                            </tbody>
                        </table>
                    </div>-->
                       
@endsection