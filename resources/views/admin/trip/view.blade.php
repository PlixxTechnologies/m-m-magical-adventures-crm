@extends('admin.layout.dashboardx')
@section('content')

<!-- Datatables JS FILES -->

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.print.min.js"></script>

<!-- /Datatables JS FILES -->
<!-- Page header -->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="{{ url('/home') }}">
                    <em class="fa fa-home"></em>
                </a></li>
                <li class="active">View Trips</li>
            </ol>
        </div><!--/.row-->
    <div class="container-fluid"> 
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">View Trips</h1>
            </div>
        </div>  <!-- /page header -->
   </div>

<div class="container-fluid">
<!-- Simple lists -->
<div class="row pull-right">
<input type="button" onclick="printDiv('printableArea')" value="Print" class="btn btn-primary btn-lg" />

    </div>

<div id="printableArea">
    <div class="row" >
        <center>
            <h5 class="text-muted">Agent Name</h5>
                <h3 class="media-heading text-semibold">{{ $trip->user->name }}</h3>
        </center>  
    </div>
<br>



                    <div class="row">
                        <div class="col-md-6">
                            <!-- Simple list -->
                            <div class="panel panel-flat">
                                <div class="panel-heading" style="padding-top: 30px">
                                    <center><h3 class="panel-title"><b>Customer Information</b></h3></center>
                                </div>

                                <div class="panel-body">
                                    <!-- <ul class="media-list">
                                        <li class="media"> -->
                                            <div class="col-md-6">
                                                <span class="text-muted">First Name</span>
                                                <div class="media-heading text-semibold">{{ $trip->customer->first_name }}</div>
                                            </div>  
                                            <div class="col-md-6">
                                                <span class="text-muted">Last Name</span>
                                                <div class="media-heading text-semibold">{{ $trip->customer->last_name }}</div>
                                            </div>                                           
                                        <!-- </li>
                                        <br>
                                        <li class="media"> -->
                                            <div class="col-md-6">
                                                <span class="text-muted">Email</span>
                                                <div class="media-heading text-semibold">{{ $trip->customer->email }}</div>
                                            </div>  
                                            <div class="col-md-6">
                                                <span class="text-muted">Country Code</span>
                                                <div class="media-heading text-semibold">{{ $trip->customer->country_code }}</div>
                                            </div>                                           
                                        <!-- </li>
                                        <br>
                                        <li class="media"> -->
                                            <div class="col-md-6">
                                                <span class="text-muted">Phone Number</span>
                                                <div class="media-heading text-semibold">{{ $trip->customer->phone_no }}</div>
                                            </div>  
                                            <div class="col-md-6">
                                                <span class="text-muted">Address</span>
                                                <div class="media-heading text-semibold">{{ $trip->customer->address1 }}</div>
                                            </div>                                           
                                        <!-- </li>
                                        <br>
                                        <li class="media"> -->
                                            <div class="col-md-6">
                                                <span class="text-muted">City</span>
                                                <div class="media-heading text-semibold">{{ $trip->customer->city }}</div>
                                            </div>  
                                            <div class="col-md-6">
                                                <span class="text-muted">State</span>
                                                <div class="media-heading text-semibold">{{ $trip->customer->state }}</div>
                                            </div>                                           
                                       <!--  </li>
                                        <br>
                                        <li class="media"> -->
                                            <div class="col-md-6">
                                                <span class="text-muted">Zip</span>
                                                <div class="media-heading text-semibold">{{ $trip->customer->zip }}</div>
                                            </div>

                                            @if($trip->customer->referral != null)  
                                            <div class="col-md-6">
                                                <span class="text-muted">Referral</span>
                                                <div class="media-heading text-semibold">{{ $trip->customer->referral }}</div>
                                            </div>
                                            @else
                                            <div class="col-md-6">
                                                <span class="text-muted">Referral</span>
                                                <div class="media-heading text-semibold"></div><br>
                                            </div>
                                            @endif                                           
                                        <!-- </li>
                                        <br>
                                        <li class="media"> -->
                                            @if($trip->customer->disney_experience_username != null) 
                                            <div class="col-md-6">
                                                <span class="text-muted">Disney Experience Username</span>
                                                <div class="media-heading text-semibold">{{ $trip->customer->disney_experience_username }}</div>
                                            </div>
                                            @else
                                            <div class="col-md-6">
                                                <span class="text-muted">Disney Experience Username</span>
                                                <div class="media-heading text-semibold"></div><br>
                                            </div>
                                            @endif

                                            @if($trip->customer->disney_experience_password != null)  
                                            <div class="col-md-6">
                                                <span class="text-muted">Disney Experience Password</span>
                                                <div class="media-heading text-semibold">{{ $trip->customer->disney_experience_password }}</div>
                                            </div>
                                            @else
                                            <div class="col-md-6">
                                                <span class="text-muted">Disney Experience Password</span>
                                                <div class="media-heading text-semibold"></div><br>
                                            </div>
                                            @endif
                                       <!--  </li>
                                        <br>
                                        <li class="media"> -->
                                            @if($trip->customer->notes != null)  
                                            <div class="col-md-6">
                                                <span class="text-muted">Notes</span>
                                                <div class="media-heading text-semibold">{{ $trip->customer->notes }}</div>
                                            </div> 
                                            @else
                                            <div class="col-md-6">
                                                <span class="text-muted">Notes</span>
                                                <div class="media-heading text-semibold"></div><br>
                                            </div> 
                                            @endif

                                            @if($trip->user->name != null)  
                                            <div class="col-md-6">
                                                <span class="text-muted">Agent Name</span>
                                                <div class="media-heading text-semibold">{{ $trip->user->name }}</div>
                                            </div> 
                                            @else
                                            <div class="col-md-6">
                                                <span class="text-muted">Agent Name</span>
                                                <div class="media-heading text-semibold">Agent has been deleted</div>
                                            </div> 
                                            @endif 
                                                                                      
                                        <!-- </li> -->

                                    </ul>
                                </div>
                            </div>
                            <!-- /simple list -->

                        </div>

                        <div class="col-md-6">
                            <!-- Simple list -->
                            <div class="panel panel-flat">
                                <div class="panel-heading" style="padding-top: 30px">
                                    <center><h3 class="panel-title"><b>Trip Information</b></h3></center>
                                </div>
                                <div class="panel-body">
                                    <ul class="media-list">
                                       <!--  <li class="media"> -->
                                            <div class="col-md-6">
                                                <span class="text-muted">Reservation Number</span>
                                                <div class="media-heading text-semibold"><a target="_blank" href="{{ url('trip/view').'/'.$trip->id }}">{{ $trip->reservation_number }}</a></div>
                                            </div>  
                                            <div class="col-md-6">
                                                <span class="text-muted">Booking Date</span>
                                                <div class="media-heading text-semibold"> {{ date("m", strtotime($trip->booking_date)) }}-{{ date("d", strtotime($trip->booking_date)) }}-{{ date("Y", strtotime($trip->booking_date)) }}</div>
                                            </div>                                           
                                        <!-- </li>
                                        <br>
                                        <li class="media"> -->
                                            <div class="col-md-6">
                                                <span class="text-muted">Check In Date</span>
                                                <div class="media-heading text-semibold">
                                                    {{ date("m", strtotime($trip->checkin_date)) }}-{{ date("d", strtotime($trip->checkin_date)) }}-{{ date("Y", strtotime($trip->checkin_date)) }}
                                                </div>
                                            </div>  
                                            <div class="col-md-6">
                                                <span class="text-muted">Check Out Date</span>
                                                <div class="media-heading text-semibold"> {{ date("m", strtotime($trip->checkout_date)) }}-{{ date("d", strtotime($trip->checkout_date)) }}-{{ date("Y", strtotime($trip->checkout_date)) }}</div>
                                            </div>                                           
                                       <!--  </li>
                                        <br>
                                        <li class="media"> -->
                                            <div class="col-md-6">
                                                <span class="text-muted">Trip Status</span>
                                                @if($trip->status ==0 )
                                                    <div class="media-heading text-semibold">{{ $trip->trip_status }}</div>
                                                @else
                                                    <div class="media-heading text-semibold">Canceled</div>
                                                @endif
                                            </div>  
                                            <div class="col-md-6">
                                                <span class="text-muted">Total Sale</span>
                                                <div class="media-heading text-semibold">${{ $trip->total_sale }}</div>
                                            </div>                                           
                                       <!--  </li>
                                        <br>
                                        <li class="media"> -->
                                            <div class="col-md-6">
                                                <span class="text-muted">Total Commission</span>
                                                <div class="media-heading text-semibold">${{ $trip->commission }}</div>
                                            </div>  
                                            <div class="col-md-6">
                                                <span class="text-muted">Expected Commission</span>
                                                <div class="media-heading text-semibold">${{ $trip->expected_commission }}</div>
                                            </div>                                           
                                        <!-- </li>
                                        <br>
                                        <li class="media"> -->
                                            @if($trip->travel_with != null || $trip->travel_with != "")
                                            @if($flag == "true")
                                            <div class="col-md-6">
                                                <span class="text-muted">Travel With</span>
                                                <div class="media-heading text-semibold">{{substr($trip->travel_with, 0,-1)}}</div>
                                            </div>
                                            @else 
                                            <div class="col-md-6">
                                                <span class="text-muted">Travel With</span>
                                                <div class="media-heading text-semibold">{{$trip->travel_with}}</div>
                                            </div>
                                            @endif
                                            @endif

                                            @if($trip->special_request != null || $trip->special_request != "")
                                             <div class="col-md-6">
                                                <span class="text-muted">Special Occasions or Requests</span>
                                                <div class="media-heading text-semibold">{{ $trip->special_request }}</div>
                                            </div>
                                            @endif                                         
                                        <!-- </li>
                                        <br>
                                        <li class="media"> -->
                                            @if($trip->destination != null || $trip->destination != "")
                                            <div class="col-md-6">
                                                <span class="text-muted">Destinations</span>
                                                <div class="media-heading text-semibold">{{ $trip->destination }}</div>
                                            </div>
                                            @endif

                                            @if($trip->disneyworld_hotel_name != null || $trip->disneyworld_hotel_name != "")  
                                            <div class="col-md-6">
                                                <span class="text-muted">Walt Disney World Hotel Name </span>
                                                <div class="media-heading text-semibold">{{ $trip->disneyworld_hotel_name }}</div>
                                            </div> 
                                            @endif                                       
                                        <!-- </li>
                                        <br>
                                        <li class="media"> -->
                                            @if($trip->magical_express != null || $trip->magical_express != "")
                                            <div class="col-md-6">
                                                <span class="text-muted">Magical Express</span>
                                                <div class="media-heading text-semibold">{{ $trip->magical_express }}</div>
                                            </div>  
                                            @endif

                                            @if($trip->memory_maker != null || $trip->memory_maker != "")
                                            <div class="col-md-6">
                                                <span class="text-muted">Memory Maker </span>
                                                <div class="media-heading text-semibold">{{ $trip->memory_maker }}</div>
                                            </div>  
                                            @endif                                      
                                        <!-- </li>
                                        <br>
                                        <li class="media"> -->
                                            @if($trip->disney_dining_plan != null || $trip->disney_dining_plan != "")
                                            <div class="col-md-6">
                                                <span class="text-muted">Dining Plan</span>
                                                <div class="media-heading text-semibold">{{ $trip->disney_dining_plan }}</div>
                                            </div>  
                                            @endif

                                            @if($trip->magic_band_color != null || $trip->magic_band_color != "")
                                            <div class="col-md-6">
                                                <span class="text-muted">Magic Band Color Selection </span>
                                                <div class="media-heading text-semibold">{{ $trip->magic_band_color }}</div>
                                            </div> 
                                            @endif                                       
                                        <!-- </li>
                                        <br>
                                        <li class="media"> -->
                                            @if($trip->disney_experience_username != null || $trip->disney_experience_username != "")
                                            <div class="col-md-6">
                                                <span class="text-muted">My Disney Experience User Name</span>
                                                <div class="media-heading text-semibold">{{ $trip->disney_experience_username }}</div>
                                            </div>
                                            @endif

                                            @if($trip->disney_experience_password != null || $trip->disney_experience_password != "") 
                                            <div class="col-md-6">
                                                <span class="text-muted">My Disney Experience Password </span>
                                                <div class="media-heading text-semibold">{{ $trip->disney_experience_password }}</div>
                                            </div>
                                            @endif                                        
                                        <!-- </li>
                                        <br>
                                        <li class="media"> -->
                                            @if($trip->disneyresort_hotel_name != null || $trip->disneyresort_hotel_name != "")
                                            <div class="col-md-6">
                                                <span class="text-muted">Disneyland Hotel Name</span>
                                                <div class="media-heading text-semibold">{{ $trip->disneyresort_hotel_name }}</div>
                                            </div>
                                            @endif 

                                            @if($trip->good_neighbor_hotel != null || $trip->good_neighbor_hotel != "") 
                                            <div class="col-md-6">
                                                <span class="text-muted">Good Neighbor Hotel </span>
                                                <div class="media-heading text-semibold">{{ $trip->good_neighbor_hotel }}</diV>
                                            </div>
                                            @endif                                        
                                        <!-- </li>
                                        <br>
                                        <li class="media"> -->
                                            @if($trip->ship_name != null || $trip->ship_name != "")
                                            <div class="col-md-6">
                                                <span class="text-muted">Ship Name</span>
                                                <div class="media-heading text-semibold">{{ $trip->ship_name }}</div>
                                            </div> 
                                            @endif 

                                            @if($trip->castaway_member != null || $trip->castaway_member != "")
                                            <div class="col-md-6">
                                                <span class="text-muted">Castaway Member </span>
                                                <div class="media-heading text-semibold">{{ $trip->castaway_member }}</div>
                                            </div>  
                                            @endif                                      
                                        <!-- </li>
                                        <br>
                                        <li class="media"> -->
                                            @if($trip->bus_transportation != null || $trip->bus_transportation != "")
                                            <div class="col-md-6">
                                                <span class="text-muted">Bus Transportation</span>
                                                <div class="media-heading text-semibold">{{ $trip->bus_transportation }}</div>
                                            </div> 
                                            @endif

                                            @if($trip->universal_orlando_resort_hotel != null || $trip->universal_orlando_resort_hotel != "") 
                                            <div class="col-md-6">
                                                <span class="text-muted">Universal Orlando Resort Hotels </span>
                                                <div class="media-heading text-semibold">{{ $trip->universal_orlando_resort_hotel }}</div>
                                            </div>  
                                            @endif                                      
                                        <!-- </li>
                                        <br>
                                        <li class="media"> -->
                                            @if($trip->partners_hotel != null || $trip->partners_hotel != "")
                                            <div class="col-md-6">
                                                <span class="text-muted">Partners Hotel</span>
                                                <div class="media-heading text-semibold">{{ $trip->partners_hotel }}</div>
                                            </div>  
                                            @endif

                                            @if($trip->ticket_type != null || $trip->ticket_type != "")
                                            <div class="col-md-6">
                                                <span class="text-muted">Ticket Only Sale </span>
                                                <div class="media-heading text-semibold">{{ $trip->ticket_type }}</div>
                                            </div>  
                                            @endif                                      
                                        <!-- </li>
                                        <br>
                                        <li class="media"> -->
                                            @if($trip->insurance != null || $trip->insurance != "")
                                            <div class="col-md-6">
                                                <span class="text-muted">Insurance</span>
                                                <div class="media-heading text-semibold">{{ $trip->insurance }}</div>
                                            </div> 
                                            @endif

                                            @if($trip->notes != null || $trip->notes != "") 
                                            <div class="col-md-6">
                                                <span class="text-muted">Notes </span>
                                                <div class="media-heading text-semibold">{{ $trip->notes }}</div>
                                            </div>   
                                            @endif                                     
                                        <!-- </li> -->

                                    </ul>
                                </div>
                            </div>
                            <!-- /simple list -->

                        </div>
                    </div>
                    <!-- /simple lists -->
</div>
</div>

                    <script type="text/javascript">
                        function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}
                    </script>
@endsection