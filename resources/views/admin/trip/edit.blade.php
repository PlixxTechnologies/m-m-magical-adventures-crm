@extends('admin.layout.dashboardx')

@section('content')
<!-- Datatables JS FILES -->

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.print.min.js"></script>

<!-- /Datatables JS FILES -->
<style>
.red{
    color: red;
}

.select2-selection--multiple .select2-selection__choice {
    background-color: #eee;
    color: black;
}
</style>
<!-- Page header -->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="{{ url('/home') }}">
                    <em class="fa fa-home"></em>
                </a></li>
                <li class="active">Edit Trip</li>
            </ol>
        </div><!--/.row-->
    <div class="container-fluid"> 
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Edit Trip</h1>
            </div>
        </div>  <!-- /page header -->
   </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">

                        <form action="{{ url('trip/edit').'/'.$trip->id }}" method="post" >
                            {!! csrf_field() !!}

							<input type="hidden" class="form-control" name="customer_id" value="{{ $trip->customer_id }}">


                            @if($authuser->role == 1)
                            <div class="form-group">
                                 <label><span class="red">*</span>Agent Name:</label>
                                    <select name="user_id" class="form-control" required>
                                        @foreach($users as $user)
                                            <option value="{{ $user->id }}" @if($user->id == $trip->user_id) selected @endif>{{ $user->name }}</option>
                                        @endforeach                                   
                                    </select>
                                </div>
                            @else
                                <div class="form-group">
                                <label>Agent Name:</label>
                                <input type="text"  class="form-control" value="{{ $authuser->name }}" readonly>
                                <input type="hidden" class="form-control" name="user_id" value="{{ $authuser->id }}">
                                </div>
                            @endif
                            
                            <div class="form-group">
                                <label>Customer Name:</label>
                                <input type="text" value="{{ $name }}" class="form-control" readonly>
                            </div>
                           
                            <div class="form-group">
                                <label><span class="red">*</span>Reservation Number:</label>
                                <input type="text" name="reservation_number" value=" {{ $trip->reservation_number }}" class="form-control" required>
                            </div>

                           <div class="form-group">
                                <label><span class="red">*</span>Booking Date:</label>
                                <input id="bookingDate" onfocus="DateType()" type="datetime" name="booking_date" value=" {{ $trip->booking_date }}" class="form-control" class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label><span class="red">*</span>Check In Date:</label>
                                <input type="date" name="checkin_date" class="form-control" value="{{ $trip->checkin_date }}" required>
                            </div>
                            <div class="form-group">
                                <label>Check Out Date:</label>
                                <input type="date" name="checkout_date" class="form-control" value="{{ $trip->checkout_date }}">
                                <span class="red"><b>Note: </b></span>If a check-out date is not selected then system will automatically save the <b>check-in date</b> as check-out date.
                            </div>
                            <div class="form-group">
                                <label><span class="red">*</span>Trip Status:</label>
                                <select name="trip_status" class="form-control" required>
                                    <option value="">Choose One....</option>
                                    <option value="Inquiry" @if($trip->trip_status == "Inquiry") selected @endif>Inquiry</option>
                                    <option value="Quote Sent" @if($trip->trip_status == "Quote Sent") selected @endif>Quote Sent</option>
                                    <option value="Booked Deposit" @if($trip->trip_status == "Booked Deposit") selected @endif>Booked Deposit</option>
                                    <option value="On Hold" @if($trip->trip_status == "On Hold") selected @endif>On Hold</option>
                                    <option value="Paid in Full" @if($trip->trip_status == "Paid in Full") selected @endif>Paid in Full</option>
                                    <option value="Complete – Commission Paid" @if($trip->trip_status == "Complete – Commission Paid") selected @endif>Complete – Commission Paid</option>
                                    <option value="Canceled" @if($trip->trip_status == "Canceled") selected @endif>Canceled</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label><span class="red">*</span>Total Sale:</label>
                                <input type="number" step="any" name="total_sale" value="{{ $trip->total_sale }}" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label>Total Commission:</label>
                                 <input type="number" step="any" name="commission" id="commission" onfocusout="mycommission()" value="{{ $trip->commission }}" class="form-control">
                                <input type="hidden" id="user_commission" value="{{ $authuser->commission }}">
                            </div>
                            <div class="form-group">
                                <label>Expected Commission:</label>
                                <input type="number" step="any" name="expected_commission" id="expected_commission" value="{{ $trip->expected_commission }}" class="form-control">
                            </div>
                            @if($flag == "false" && $trip->travel_with != null && count($guests) == 0)
                            <div class="form-group">
                                <label>Travel With:</label>
                                <input type="text" name="travel_with" value="{{ $trip->travel_with }}" class="form-control">
                            </div>
                            @elseif($flag == "false" && $trip->travel_with != null && count($guests) > 0)
                            <div class="form-group form-group-material">
                                        <label class="control-label has-margin animate is-visible">Travel With:</label>
                                        <!-- <select name="travel_with[]" multiple="" class="select select2-hidden-accessible" tabindex="-1" aria-hidden="true"> -->

                                     <select class="form-control selectpicker" name="travel_with[]" multiple="" data-live-search="true" tabindex="-1">
                                            @foreach($guests as $guest)
                                                <option 
                                                value="{{ $guest->guest_first_name.' '.$guest->guest_last_name }}" selected="selected">
                                                    {{  $guest->guest_first_name.' '.$guest->guest_last_name }}
                                                </option>
                                            @endforeach 
                                        </select>
                                    </div>
                            @else
                            <?php $x=0; $count = 0?>
                             <div class="form-group form-group-material">
                                        <label class="control-label has-margin animate is-visible">Travel With:</label>
                                      <!--   <select name="travel_with[]" multiple="" class="select select2-hidden-accessible" tabindex="-1" aria-hidden="true"> -->
                                      <select class="form-control selectpicker" multiple="" name="travel_with[]" data-live-search="true" tabindex="-1">
                                            @foreach($guests as $guest)
                                            <?php $x=0; ?>
                                               @foreach($travelWith as $travel)
                                               @if($travel == $guest->guest_first_name.' '.$guest->guest_last_name)
                                              
                                               <?php $x=1; ?>
                                                @endif
                                                @endforeach 

                                                @if($x==1) 
                                                <option 
                                                value="{{ $guest->guest_first_name.' '.$guest->guest_last_name }}" selected="selected">
                                                   {{  $guest->guest_first_name.' '.$guest->guest_last_name }}
                                                </option>
                                               @else
                                               <option 
                                                value="{{ $guest->guest_first_name.' '.$guest->guest_last_name }}">
                                                   {{  $guest->guest_first_name.' '.$guest->guest_last_name }}
                                                </option>
                                                @endif
                                               
                                            @endforeach 

                                        </select>
                             </div>
                             @endif
                            <div class="form-group">
                                <label>Special Occasions or Requests:</label>
                                <input type="text" name="special_request" value="{{ $trip->special_request }}" class="form-control">
                            </div> 
                            <div class="form-group">
                                <label><span class="red">*</span>Destinations:</label>
                                <select name="destination" class="form-control" id="disney_select" onchange="switchDivs()" >
                                    <option value="">Choose One....</option>
                                   <!--  <option value="Walt Disney World" @if($trip->destination == "Walt Disney World") selected @endif>Walt Disney World</option>
                                    <option value="Disneyland Resort" @if($trip->destination == "Disneyland Resort") selected @endif>Disneyland Resort </option>
                                    <option value="Disney Cruise Line" @if($trip->destination == "Disney Cruise Line") selected @endif>Disney Cruise Line</option>
                                    <option value="Universal Studios Florida" @if($trip->destination == "Universal Studios Florida") selected @endif>Universal Studios Florida</option> -->
                                    @foreach($destinations as $destination) 
     <option value="{{ $destination->destination_name}}" @if($trip->destination ==  $destination->destination_name ) selected @endif >{{$destination->destination_name}}</option>
                                    @endforeach
                                </select>
                            </div>    



                                <script type="text/javascript">
                       function yesnoCheck(that) {
                                if (that.value == "other") {
                                    document.getElementById("ifYes").style.display = "block";
                                } else {
                                    document.getElementById("ifYes").style.display = "none";
                                }
                            }
                 </script>

                 <div class="form-group ">
                 <label><span class="red">*</span>Referral:</label>
                <select onchange="yesnoCheck(this);" name="Referal" value="Referal" class="form-control" required>
                                    @if($trip->Referal != null){
                                        <option value="{{ $trip->Referal }}" selected="">{{ $trip->Referal }}</option>
                                        <option disabled value="">--Select--</option>
                                        <option value="Facebook">Facebook</option>
                                        <option value="Instagram">Instagram</option>
                                        <option value="Google">Google</option>
                                        <option value="Friend">Friend</option>
                                        <option value="Family">Family</option>
                                        <option value="Social Media Ad">Social Media Ad</option>
                                        <option value="Disney/Universal FAM">Disney/Universal FAM</option>
                                        <option value="other">Other</option>
                                    }
                                    @else{
                                        <option disabled selected value="">--Select--</option>
                                        <option value="Facebook">Facebook</option>
                                        <option value="Instagram">Instagram</option>
                                        <option value="Google">Google</option>
                                        <option value="Friend">Friend</option>
                                        <option value="Family">Family</option>
                                        <option value="Social Media Ad">Social Media Ad</option>
                                        <option value="Disney/Universal FAM">Disney/Universal FAM</option>
                                        <option value="other">Other</option>
                                    }
                                    @endif
                                </select><br>
                                <div id="ifYes" style="display: none;">
                                   <input placeholder="Type Referral" type="text" id="other" name="other" class="form-control"/>
                                </div>
                            </div>
    



                            <div id="first">
                            <div class="form-group" id = "worldHotels">
                                <label>Walt Disney World Hotel Name:</label>
                                <select name="disneyworld_hotel_name" class="form-control">
                                    <option value="">Choose One....</option>
                                    @foreach($valueResorts as $value)
                                          <option value="{{ $value->hotel_name }}" @if($trip->disneyworld_hotel_name ==  $value->hotel_name ) selected @endif>{{ $value->hotel_name }}</option>
                                          @endforeach
                                          <option disabled><b>Moderate Resorts</b></option>
                                          @foreach($moderateResorts as $value)
                                          <option value="{{ $value->hotel_name }}"@if($trip->disneyworld_hotel_name ==  $value->hotel_name ) selected @endif>{{ $value->hotel_name }}</option>
                                          @endforeach
                                          <option disabled><b>Deluxe Resorts </b></option>
                                          @foreach($deluxeResorts as $value)
                                          <option value="{{ $value->hotel_name }}"@if($trip->disneyworld_hotel_name ==  $value->hotel_name ) selected @endif>{{ $value->hotel_name }}</option>
                                          @endforeach
                                          <option disabled><b>Deluxe Villas </b></option>
                                          @foreach($deluxeVillas as $value)
                                          <option value="{{ $value->hotel_name }}"@if($trip->disneyworld_hotel_name ==  $value->hotel_name ) selected @endif>{{ $value->hotel_name }}</option>
                                          @endforeach
                                </select>
                            </div>                                
                            <div class="form-group">
                                    <label>Dining Plan:</label>
                                    <select name="disney_dining_plan" class="form-control">
                                        <option @if($trip->disney_dining_plan == "None") selected @endif value="None">None</option>
                                        <option @if($trip->disney_dining_plan == "Quick Service Dining") selected @endif value="Quick Service Dining">Quick Service Dining</option>
                                        <option @if($trip->disney_dining_plan == "Disney Dining Plan") selected @endif value="Disney Dining Plan">Disney Dining Plan</option>
                                        <option @if($trip->disney_dining_plan == "Deluxe Dining Plan") selected @endif value="Deluxe Dining Plan">Deluxe Dining Plan</option>
                                    </select>
                            </div>
                            <div class="form-group">
                                <label>Memory Maker:</label>
                                <select name="memory_maker" class="form-control">
                                    <option value="">Choose One....</option>
                                    <option value="Yes" @if($trip->memory_maker == "Yes" || $trip->memory_maker == "YES" || $trip->memory_maker == "yes") selected @endif>Yes</option>
                                    <option value="No" @if($trip->memory_maker == "No" || $trip->memory_maker == "no" || $trip->memory_maker == "NO") selected @endif>No</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Magical Express:</label>
                                <select name="magical_express" class="form-control">
                                    <option value="">Choose One....</option>
                                    <option value="Yes" @if($trip->magical_express == "Yes" || $trip->magical_express == "yes" || $trip->magical_express == "YES") selected @endif>Yes</option>
                                    <option value="No" @if($trip->magical_express == "No" || $trip->magical_express == "no" || $trip->magical_express == "NO") selected @endif>No</option>
                                </select>
                            </div>
                            
                            <div class="form-group">
                                <label>Magic Band Color Selection:</label>
                                <input type="text" name="magic_band_color" value="{{ $trip->magic_band_color }}" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>My Disney Experience User Name:</label>
                                <input type="text" name="disney_experience_username" value="{{ $trip->customer->disney_experience_username }}" class="form-control" readonly>
                            </div>
                            <div class="form-group">
                                <label>My Disney Experience Password:</label>
                                <input type="text" name="disney_experience_password" value="{{ $trip->customer->disney_experience_password }}" class="form-control" readonly>
                            </div>    
                        </div>

                            <div id="second">                            
                            <div class="form-group" id ="resortHotels">
                                    <label>Disneyland Hotel Name:</label>
                                    <select name="disneyresort_hotel_name" class="form-control">
                                        <option value="">Choose One....</option>
                                         @foreach($disneylandResortHotels as $value)
                                        <option value="{{ $value->hotel_name }}"@if($trip->disneyresort_hotel_name == $value->hotel_name) selected @endif>{{ $value->hotel_name }}</option>
                                        @endforeach

                                        
                                    </select>
                                </div>
                                <div id="goodneighbour">
                                    <div class="form-group">
                                        <label>Good Neighbor Hotel</label>
                                        <input type="text" name="good_neighbor_hotel" value="{{ $trip->good_neighbor_hotel }}" class="form-control">
                                    </div>
                                </div>                                
                            </div>

                         <div id="third">
                             <div class="form-group">
                                 <label>Ship Name:</label>
                                 <select name="ship_name2" class="form-control">
                                     <option value="">Choose One....</option>

                                     @foreach($disneyCruiseLineShips as $value)
                                     <option value="{{ $value->hotel_name }}" @if($trip->ship_name == $value->hotel_name) selected @endif>{{ $value->hotel_name }}</option>
                                     @endforeach

                                   
                                 </select>
                             </div>
                            <div class="form-group">
                                <label>Castaway Member:</label>
                                <input type="text" name="castaway_member" value=" {{ $trip->castaway_member }}" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Bus Transportation:</label>
                               <div class="checkbox">
                                  <label><input type="checkbox" name="bus_transportation" @if($trip->bus_transportation == "Yes" || $trip->bus_transportation == "YES" || $trip->bus_transportation == "yes") checked @endif value="Yes">Yes</label>
                                </div>
                                <div class="checkbox">
                                  <label><input type="checkbox" name="bus_transportation"  @if($trip->bus_transportation == "No" || $trip->bus_transportation == "NO" || $trip->bus_transportation == "no") checked @endif value="No">No</label>
                                </div>
                            </div>
                            </div>

                                <div id="fourth">
                                <!-- <div id="universalorlandoresorthotels"> -->
                                    <div class="form-group">
                                        <label>Universal Orlando Resort Hotels:</label>
                                        <select name="universal_orlando_resort_hotel" class="form-control" id="universal_orlando_resort_hotel" onchange="switchOrlandoDiv()">
                                            <option value="">Choose One....</option>
                                            
                                           @foreach($universalStudiosHotels as $value)
                                            <option value="{{ $value->hotel_name }}"  @if($trip->universal_orlando_resort_hotel == $value->hotel_name) selected @endif>{{ $value->hotel_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                <!-- </div> -->

                                <div id="PartnersHotel">
                                    <div class="form-group">
                                        <label>Partners Hotel</label>
                                        <input type="text" name="partners_hotel" value="{{ $trip->partners_hotel }}" class="form-control">
                                    </div>
                                </div>
                            </div>

                            <div id="ticketdiv">
                            <div class="form-group">
                                <label>Ticket Only Sale:</label>
                                <select name="ticket_type" class="form-control">
                                    <option value="">Choose One....</option>
                                    <option value="Partner Hotels" @if($trip->ticket_type == "Park Ticket Only") selected @endif>Park Ticket Only</option>
                                    <option value="Special Event Only" @if($trip->ticket_type == "Special Event Only") selected @endif>Special Event Only</option>
                                </select>
                            </div>
                        </div>
<!--start from here 2 remains -->
                         <div id="fifth">
                             <div class="form-group">
                                 <label>Ship Name:</label>
                                 <select name="ship_name1" class="form-control">
                                     <option value="">Choose One....</option>
                                     <optgroup label="XL Class">
                                    @foreach($xl as $value)
                                            <option value="{{ $value->hotel_name }}" @if($trip->ship_name == $value->hotel_name) selected @endif >{{ $value->hotel_name }}</option>
                                            @endforeach
                                    </optgroup>
                                    <optgroup label="Vista Class">
                                    @foreach($vista as $value)
                                            <option value="{{ $value->hotel_name }}" @if($trip->ship_name == $value->hotel_name) selected @endif>{{ $value->hotel_name }}</option>
                                            @endforeach
                                    </optgroup>
                                    <optgroup label="Sunshine Class">
                                    @foreach($sunshine as $value)
                                            <option value="{{ $value->hotel_name }}" @if($trip->ship_name == $value->hotel_name) selected @endif>{{ $value->hotel_name }}</option>
                                            @endforeach
                                    </optgroup>
                                    <optgroup label="Dream Class">
                                      @foreach($dream as $value)
                                            <option value="{{ $value->hotel_name }}" @if($trip->ship_name == $value->hotel_name) selected @endif>{{ $value->hotel_name }}</option>
                                            @endforeach
                                    </optgroup>
                                    <optgroup label="Splendor Class">
                                      @foreach($splendor as $value)
                                            <option value="{{ $value->hotel_name }}" @if($trip->ship_name == $value->hotel_name) selected @endif>{{ $value->hotel_name }}</option>
                                            @endforeach
                                    </optgroup>
                                     <optgroup label="Conquest Class">
                                     @foreach($conquest as $value)
                                            <option value="{{ $value->hotel_name }}" @if($trip->ship_name == $value->hotel_name) selected @endif>{{ $value->hotel_name }}</option>
                                            @endforeach
                                    </optgroup>
                                     <optgroup label="Spirit Class">
                                    @foreach($spirit as $value)
                                            <option value="{{ $value->hotel_name }}" @if($trip->ship_name == $value->hotel_name) selected @endif>{{ $value->hotel_name }}</option>
                                            @endforeach
                                    </optgroup>
                                    <optgroup label="Fantasy Class">
                                    @foreach($fantasy as $value)
                                            <option value="{{ $value->hotel_name }}" @if($trip->ship_name == $value->hotel_name) selected @endif>{{ $value->hotel_name }}</option>
                                            @endforeach
                                    </optgroup>
                                 </select>
                             </div>
                        
                        </div>

                         <div id="sixth">
                             <div class="form-group">
                                 <label>Ship Name:</label>
                                 <select name="ship_name" class="form-control">
                                     <option value="">Choose One....</option>
                                     <optgroup label="Oasis Class">
                                     @foreach($oasis as $value)
                                            <option value="{{ $value->hotel_name }}" @if($trip->ship_name == $value->hotel_name) selected @endif>{{ $value->hotel_name }}</option>
                                            @endforeach
                                    </optgroup>
                                    <optgroup label="Quantum Class">
                                     @foreach($quantum as $value)
                                            <option value="{{ $value->hotel_name }}" @if($trip->ship_name == $value->hotel_name) selected @endif>{{ $value->hotel_name }}</option>
                                            @endforeach
                                    </optgroup>
                                   <optgroup label="Freedom Class">
                                   @foreach($freedom as $value)
                                            <option value="{{ $value->hotel_name }}" @if($trip->ship_name == $value->hotel_name) selected @endif>{{ $value->hotel_name }}</option>
                                            @endforeach
                                    </optgroup>
                                   <optgroup label="Voyager Class">
                                    @foreach($voyager as $value)
                                            <option value="{{ $value->hotel_name }}" @if($trip->ship_name == $value->hotel_name) selected @endif>{{ $value->hotel_name }}</option>
                                            @endforeach
                                    </optgroup>
                                     <optgroup label="Radiance Class">
                                      @foreach($radiance as $value)
                                            <option value="{{ $value->hotel_name }}" @if($trip->ship_name == $value->hotel_name) selected @endif>{{ $value->hotel_name }}</option>
                                            @endforeach
                                    </optgroup>
                                    <optgroup label="Vision Class">
                                     @foreach($vision as $value)
                                            <option value="{{ $value->hotel_name }}" @if($trip->ship_name == $value->hotel_name) selected @endif>{{ $value->hotel_name }}</option>
                                            @endforeach
                                    </optgroup>
                                    <optgroup label="Other">
                                    @foreach($other as $value)
                                            <option value="{{ $value->hotel_name }}" @if($trip->ship_name == $value->hotel_name) selected @endif>{{ $value->hotel_name }}</option>
                                            @endforeach
                                    </optgroup>
                                 </select>
                             </div>
                        
                        </div>

                            <div class="form-group">
                                    <label>Insurance:</label>
                                    <select name="insurance" class="form-control">
                                        <option value="">Choose One....</option>
                                        <option @if($trip->insurance == "YES" || $trip->insurance == "Yes" || $trip->insurance == "yes") selected @endif value="Yes">Yes</option>
                                        <option @if($trip->insurance == "No" || $trip->insurance == "no" || $trip->insurance == "NO") selected @endif value="No">No</option>
                                    </select>
                            </div>
                            <div class="form-group">
                                <label>Notes:</label>
                                <textarea name="notes" class="form-control" rows="4">{{ $trip->notes }}</textarea>
                            </div>
                            <button type="submit" class="btn btn-primary pull-right">Update Trip</button>
                        </form>
                    </div>
                </div>
            </div>
        </div><!--/.row-->
    </div>	<!--/.main-->
</div>

<script type="text/javascript">

if($("#bookingDate").val() == " "){
    $("#bookingDate").val('1970-01-01');
}

function DateType(){
     $("#bookingDate").attr('type', 'date');
}

    function mycommission() {    
    var x = $('#commission').val();        
    var usercommission = $('#user_commission').val();

        if(x != "")
            $('#expected_commission').val(Math.round(parseFloat(x) * ((parseFloat(usercommission)/100) * 100)) / 100);
}

/*
$( document ).ready(function() {
    var selectedItem=document.getElementById('disney_select');

    if(selectedItem.value=="Walt Disney World")
    {
        document.getElementById('first').style.display='block';
        document.getElementById('worldHotels').style.display='block';
        document.getElementById('resortHotels').style.display='none';
        document.getElementById('second').style.display='none';
        document.getElementById('third').style.display='none';
        document.getElementById('fourth').style.display='none';
        document.getElementById('ticketdiv').style.display='block';
    }
    else if(selectedItem.value=="Disneyland Resort")
    {
        document.getElementById('second').style.display='block';

        document.getElementById('worldHotels').style.display='none';
        document.getElementById('resortHotels').style.display='block';
        document.getElementById('first').style.display='none';
        document.getElementById('third').style.display='none';
        document.getElementById('fourth').style.display='none';
        document.getElementById('ticketdiv').style.display='block';

    }
    else if(selectedItem.value=='Disney Cruise Line')
    {
        document.getElementById('first').style.display='none';
        document.getElementById('worldHotels').style.display='none';
        document.getElementById('resortHotels').style.display='none';
        document.getElementById('third').style.display='block';
        document.getElementById('second').style.display='none';
        document.getElementById('fourth').style.display='none';
         document.getElementById('ticketdiv').style.display='none';

    }
    else if(selectedItem.value=='Universal Studios Florida')
    {
        document.getElementById('first').style.display='none';
        document.getElementById('worldHotels').style.display='none';
        document.getElementById('resortHotels').style.display='none';
        document.getElementById('third').style.display='none';
        document.getElementById('second').style.display='none';
        document.getElementById('fourth').style.display='block';
        document.getElementById('ticketdiv').style.display='block';

    }
    else
    {
        document.getElementById('first').style.display='none';
        document.getElementById('worldHotels').style.display='none';
        document.getElementById('resortHotels').style.display='none';
        document.getElementById('second').style.display='none';
        document.getElementById('third').style.display='none';
        document.getElementById('fourth').style.display='none';
        document.getElementById('ticketdiv').style.display='none';
    }
});


function switchDivs()
{
    var selectedItem=document.getElementById('disney_select');

    if(selectedItem.value=="Walt Disney World")
    {
        document.getElementById('first').style.display='block';
        document.getElementById('worldHotels').style.display='block';
        document.getElementById('resortHotels').style.display='none';
        document.getElementById('second').style.display='none';
        document.getElementById('third').style.display='none';
        document.getElementById('fourth').style.display='none';
        document.getElementById('ticketdiv').style.display='block';
    }
    else if(selectedItem.value=="Disneyland Resort")
    {
        document.getElementById('second').style.display='block';

        document.getElementById('worldHotels').style.display='none';
        document.getElementById('resortHotels').style.display='block';
        document.getElementById('first').style.display='none';
        document.getElementById('third').style.display='none';
        document.getElementById('fourth').style.display='none';
        document.getElementById('ticketdiv').style.display='block';

    }
    else if(selectedItem.value=='Disney Cruise Line')
    {
        document.getElementById('first').style.display='none';
        document.getElementById('worldHotels').style.display='none';
        document.getElementById('resortHotels').style.display='none';
        document.getElementById('third').style.display='block';
        document.getElementById('second').style.display='none';
        document.getElementById('fourth').style.display='none';
         document.getElementById('ticketdiv').style.display='none';

    }
    else if(selectedItem.value=='Universal Studios Florida')
    {
        document.getElementById('first').style.display='none';
        document.getElementById('worldHotels').style.display='none';
        document.getElementById('resortHotels').style.display='none';
        document.getElementById('third').style.display='none';
        document.getElementById('second').style.display='none';
        document.getElementById('fourth').style.display='block';
        document.getElementById('ticketdiv').style.display='block';

    }
    else
    {
        document.getElementById('first').style.display='none';
        document.getElementById('worldHotels').style.display='none';
        document.getElementById('resortHotels').style.display='none';
        document.getElementById('second').style.display='none';
        document.getElementById('third').style.display='none';
        document.getElementById('fourth').style.display='none';
        document.getElementById('ticketdiv').style.display='none';
    }
}*/


$( document ).ready(function() {
   var selectedItem=document.getElementById('disney_select');

    if(selectedItem.value=="Walt Disney World")
    {
        document.getElementById('first').style.display='block';
        document.getElementById('worldHotels').style.display='block';
        document.getElementById('resortHotels').style.display='none';
        document.getElementById('second').style.display='none';
        document.getElementById('third').style.display='none';
        document.getElementById('fourth').style.display='none';
        document.getElementById('ticketdiv').style.display='block';
        document.getElementById('sixth').style.display='none';
        document.getElementById('fifth').style.display='none';
    }
    else if(selectedItem.value=="Disneyland Resort")
    {
        document.getElementById('second').style.display='block';
        document.getElementById('worldHotels').style.display='none';
        document.getElementById('resortHotels').style.display='block';
        document.getElementById('first').style.display='none';
        document.getElementById('third').style.display='none';
        document.getElementById('fourth').style.display='none';
        document.getElementById('ticketdiv').style.display='block';
        document.getElementById('fifth').style.display='none';
        document.getElementById('sixth').style.display='none';

    }
    else if(selectedItem.value=='Disney Cruise Line')
    {
        document.getElementById('first').style.display='none';
        document.getElementById('worldHotels').style.display='none';
        document.getElementById('resortHotels').style.display='none';
        document.getElementById('third').style.display='block';
        document.getElementById('second').style.display='none';
        document.getElementById('fourth').style.display='none';
        document.getElementById('ticketdiv').style.display='none';
        document.getElementById('fifth').style.display='none';
        document.getElementById('sixth').style.display='none';

    }
    else if(selectedItem.value=='Universal Studios Florida')
    {
        document.getElementById('first').style.display='none';
        document.getElementById('worldHotels').style.display='none';
        document.getElementById('resortHotels').style.display='none';
        document.getElementById('third').style.display='none';
        document.getElementById('second').style.display='none';
        document.getElementById('fourth').style.display='block';
        document.getElementById('ticketdiv').style.display='block';
        document.getElementById('fifth').style.display='none';
        document.getElementById('sixth').style.display='none';

    }
    else if(selectedItem.value=='Carnival Cruise Line')
    {
        document.getElementById('fifth').style.display='block';
        document.getElementById('first').style.display='none';
        document.getElementById('worldHotels').style.display='none';
        document.getElementById('resortHotels').style.display='none';
        document.getElementById('second').style.display='none';
        document.getElementById('third').style.display='none';
        document.getElementById('fourth').style.display='none';
        document.getElementById('ticketdiv').style.display='none';
        document.getElementById('sixth').style.display='none';
    }
    else if(selectedItem.value=='Royal Caribbean Cruise Line')
    {
        document.getElementById('sixth').style.display='block';
        document.getElementById('fifth').style.display='none';
        document.getElementById('first').style.display='none';
        document.getElementById('worldHotels').style.display='none';
        document.getElementById('resortHotels').style.display='none';
        document.getElementById('second').style.display='none';
        document.getElementById('third').style.display='none';
        document.getElementById('fourth').style.display='none';
        document.getElementById('ticketdiv').style.display='none';
    }
    else
    {
        document.getElementById('first').style.display='none';
        document.getElementById('worldHotels').style.display='none';
        document.getElementById('resortHotels').style.display='none';
        document.getElementById('second').style.display='none';
        document.getElementById('third').style.display='none';
        document.getElementById('fourth').style.display='none';
        document.getElementById('ticketdiv').style.display='none';
        document.getElementById('fifth').style.display='none';
        document.getElementById('sixth').style.display='none';
    }
});


function switchDivs()
{
    var selectedItem=document.getElementById('disney_select');

    if(selectedItem.value=="Walt Disney World")
    {
        document.getElementById('first').style.display='block';
        document.getElementById('worldHotels').style.display='block';
        document.getElementById('resortHotels').style.display='none';
        document.getElementById('second').style.display='none';
        document.getElementById('third').style.display='none';
        document.getElementById('fourth').style.display='none';
        document.getElementById('ticketdiv').style.display='block';
        document.getElementById('sixth').style.display='none';
        document.getElementById('fifth').style.display='none';
    }
    else if(selectedItem.value=="Disneyland Resort")
    {
        document.getElementById('second').style.display='block';
        document.getElementById('worldHotels').style.display='none';
        document.getElementById('resortHotels').style.display='block';
        document.getElementById('first').style.display='none';
        document.getElementById('third').style.display='none';
        document.getElementById('fourth').style.display='none';
        document.getElementById('ticketdiv').style.display='block';
        document.getElementById('fifth').style.display='none';
        document.getElementById('sixth').style.display='none';

    }
    else if(selectedItem.value=='Disney Cruise Line')
    {
        document.getElementById('first').style.display='none';
        document.getElementById('worldHotels').style.display='none';
        document.getElementById('resortHotels').style.display='none';
        document.getElementById('third').style.display='block';
        document.getElementById('second').style.display='none';
        document.getElementById('fourth').style.display='none';
        document.getElementById('ticketdiv').style.display='none';
        document.getElementById('fifth').style.display='none';
        document.getElementById('sixth').style.display='none';

    }
    else if(selectedItem.value=='Universal Studios Florida')
    {
        document.getElementById('first').style.display='none';
        document.getElementById('worldHotels').style.display='none';
        document.getElementById('resortHotels').style.display='none';
        document.getElementById('third').style.display='none';
        document.getElementById('second').style.display='none';
        document.getElementById('fourth').style.display='block';
        document.getElementById('ticketdiv').style.display='block';
        document.getElementById('fifth').style.display='none';
        document.getElementById('sixth').style.display='none';

    }
    else if(selectedItem.value=='Carnival Cruise Line')
    {
       
        document.getElementById('fifth').style.display='block';
        document.getElementById('first').style.display='none';
        document.getElementById('worldHotels').style.display='none';
        document.getElementById('resortHotels').style.display='none';
        document.getElementById('second').style.display='none';
        document.getElementById('third').style.display='none';
        document.getElementById('fourth').style.display='none';
        document.getElementById('ticketdiv').style.display='none';
        document.getElementById('sixth').style.display='none';
    }
    else if(selectedItem.value=='Royal Caribbean Cruise Line')
    {
        document.getElementById('sixth').style.display='block';
        document.getElementById('fifth').style.display='none';
        document.getElementById('first').style.display='none';
        document.getElementById('worldHotels').style.display='none';
        document.getElementById('resortHotels').style.display='none';
        document.getElementById('second').style.display='none';
        document.getElementById('third').style.display='none';
        document.getElementById('fourth').style.display='none';
        document.getElementById('ticketdiv').style.display='none';
    }
    else
    {
        document.getElementById('first').style.display='none';
        document.getElementById('worldHotels').style.display='none';
        document.getElementById('resortHotels').style.display='none';
        document.getElementById('second').style.display='none';
        document.getElementById('third').style.display='none';
        document.getElementById('fourth').style.display='none';
        document.getElementById('ticketdiv').style.display='none';
        document.getElementById('fifth').style.display='none';
        document.getElementById('sixth').style.display='none';
    }
}

</script>
@endsection