(function($) {
	"use strict";
	$(document).ready(function() {

        /*  [ Single Product Slider ]
         - - - - - - - - - - - - - - - - - - - - */
        var preview_carousel = $( '.product-detail .images .p-preview.owl-carousel' );
        var thumb_carousel = $( '.product-detail .images .p-thumb-dt' );
        preview_carousel.owlCarousel({
            items: 1,
            dots: false,
            nav: false
        });
        preview_carousel.on('changed.owl.carousel', function(event) {
            var index_owl = event.item.index;
            thumb_carousel.find( 'li' ).removeClass( 'active' );
            thumb_carousel.find( 'li' ).eq( index_owl ).addClass( 'active' );
        });
        thumb_carousel.find( 'li a' ).each(function(index) {
            $( this ).on( 'click', function(e) {
                e.preventDefault();
                preview_carousel.trigger('to.owl.carousel', [index,300,true]);
            });
        });

        /*  [ Add minus and plus number quantity ]
         - - - - - - - - - - - - - - - - - - - - */
        if( $( '.quantity' ).length > 0 ) {
            var form_cart = $( 'form .quantity' );
            form_cart.prepend( '<span class="minus"><i class="fa fa-angle-left"></i></span>' );
            form_cart.append( '<span class="plus"><i class="fa fa-angle-right"></i></span>' );

            var minus = form_cart.find( $( '.minus' ) );
            var plus  = form_cart.find( $( '.plus' ) );

            minus.on( 'click', function(){
                var qty = $( this ).parent().find( '.qty' );
                if ( qty.val() <= 1 ) {
                    qty.val( 1 );
                } else {
                    qty.val( ( parseInt( qty.val(), 10 ) - 1 ) );
                }
            });
            plus.on( 'click', function(){
                var qty = $( this ).parent().find( '.qty' );
                qty.val( ( parseInt( qty.val(), 10 ) + 1 ) );
            });
        }

        /*  [ Tabs ]
         - - - - - - - - - - - - - - - - - - - - */

        $( '.tabs-infor ul li:first-child').addClass('tab-open');
        $( '.tabs-infor ul li .tab-title' ).on( 'click' , function(e){
            e.preventDefault();
            $( '.tabs-infor ul li .tab-title').not(this).parent().removeClass('tab-open');
            $( this ).parent().toggleClass( 'tab-open' );
        });

        /*  [ prettyPhoto ]
         - - - - - - - - - - - - - - - - - - - - */
        $("a[data-gal^='prettyPhoto']").prettyPhoto({
            hook: 'data-gal',
            animation_speed:'normal',
            theme:'light_square',
            slideshow:3000,
            social_tools: false
        });

        /*  [ Blog slider ]
         - - - - - - - - - - - - - - - - - - - - */
        $(".blog-slider .owl-carousel").owlCarousel({
            loop:true,
            margin:0,
            nav:true,
            navText: ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
            dots: false,
            responsive:{
                0:{
                    items:1
                },
                480:{
                    items:1
                },
                600:{
                    items:2
                },
                1000:{
                    items:2
                }
            }
        });

        /*  [ Category slider ]
         - - - - - - - - - - - - - - - - - - - - */
        $(".slider-category .owl-carousel").owlCarousel({
            loop:true,
            margin: 10,
            nav:true,
            navText: ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
            dots: true,
            responsive:{
                0:{
                    items:1
                },
                480:{
                    items:2
                },
                600:{
                    items:3
                },
                1000:{
                    items:5
                }
            }
        });

        /*  [ Partner slider ]
         - - - - - - - - - - - - - - - - - - - - */
        $(".partner-slider .owl-carousel").owlCarousel({
            loop:true,
            margin:100,
            nav:false,
            dots: false,
            responsive:{
                0:{
                    items:2
                },
                480:{
                    items:3
                },
                600:{
                    items:4
                },
                1000:{
                    items:6
                }
            }
        });

        /*  [ Featured Products slider ]
         - - - - - - - - - - - - - - - - - - - - */
        $(".featured-products .owl-carousel").owlCarousel({
            loop:true,
            margin:0,
            nav:true,
            navText: ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
            dots: false,
            responsive:{
                0:{
                    items:1
                },
                480:{
                    items:1
                },
                600:{
                    items:1
                },
                1000:{
                    items:1
                }
            }
        });

        /*  [ Main Menu ]
         - - - - - - - - - - - - - - - - - - - - */
        $( '.sub-menu' ).each(function() {
            $( this ).parent().addClass( 'has-child' ).find( '> a' ).append( '<span class="arrow"><i class="fa fa-caret-down"></i></span>' );
        });
        $( '.main-menu .arrow' ).on( 'click', function(e) {
            e.preventDefault();
            $( this ).parents('li').toggleClass('open-sub');
            $( this ).parents('li').find( '> .sub-menu' ).slideToggle(400);
        });
        $( '.mobile-menu' ).on( 'click', function() {
            $( this ).parents( '.main-menu' ).toggleClass('open');
        });

        /*  [ Dropdown ]
         - - - - - - - - - - - - - - - - - - - - */
        $( '.dropdown' ).each(function() {
            var _this = $( this );
            $( this ).find('a').on( 'click', function(e) {
                e.preventDefault();
                $( _this ).toggleClass( 'open' );
                var value = $( this ).text();
                $( _this ).find( '> ul > li > a' ).text( value );
            });
        });

        /*  [ jQuery Countdown ]
         - - - - - - - - - - - - - - - - - - - - */
        var endDate = 'February 15, 2017';
        $( '.countdown ul' ).countdown({
            date: endDate,
            render: function(data) {
                $(this.el).html(
                    '<li><span>' + this.leadingZeros(data.days, 2) + '</span>/ Days</li>'
                        + '<li><span>' + this.leadingZeros(data.hours, 2) + '</span>/Hours</li>'
                        + '<li><span>' + this.leadingZeros(data.min, 2) + '</span>/Mins</li>'
                        + '<li><span>' + this.leadingZeros(data.sec, 2) + '</span>/Secs</li>'
                );
            }
        });

        /*  [ Home slider ]
         - - - - - - - - - - - - - - - - - - - - */
        $(".home-slider .owl-carousel").owlCarousel({
            loop:true,
            margin:0,
            nav:false,
            dots: true,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:1
                },
                1000:{
                    items:1
                }
            }
        });

        /*  [ Poster slider ]
         - - - - - - - - - - - - - - - - - - - - */
        $(".poster-slider .owl-carousel").owlCarousel({
            loop:true,
            margin:0,
            nav:false,
            dots: true,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:1
                },
                1000:{
                    items:1
                }
            }
        });

        /*  [ Supper market slider ]
         - - - - - - - - - - - - - - - - - - - - */
        $(".supper-market .slider-product .owl-carousel").owlCarousel({
            loop:true,
            nav:true,
            navText: ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
            dots: false,
            responsive:{
                0:{
                    items:1
                },
                480:{
                    items:2
                },
                768:{
                    items:3
                },
                1000:{
                    items:3
                }
            }
        });

        /*  [ Product slider in Home page ]
         - - - - - - - - - - - - - - - - - - - - */
        $(".slider-product .owl-carousel").owlCarousel({
            loop:true,
            nav:true,
            navText: ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
            dots: false,
            responsive:{
                0:{
                    items:1
                },
                480:{
                    items:2
                },
                600:{
                    items:3
                },
                1000:{
                    items:4
                }
            }
        });

        /*  [ Blog slider Supper market ]
         - - - - - - - - - - - - - - - - - - - - */
        $(".supper-market .blog-box .owl-carousel").owlCarousel({
            loop:true,
            margin:30,
            nav:true,
            navText: ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
            dots: false,
            responsive:{
                0:{
                    items:1
                },
                480:{
                    items:1
                },
                600:{
                    items:2
                },
                1000:{
                    items:3
                }
            }
        });

        /*  [ Blog slider in Home page ]
         - - - - - - - - - - - - - - - - - - - - */
        $(".blog-box .owl-carousel").owlCarousel({
            loop:true,
            margin:30,
            nav:true,
            navText: ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
            dots: false,
            responsive:{
                0:{
                    items:1
                },
                480:{
                    items:1
                },
                600:{
                    items:2
                },
                1000:{
                    items:2
                }
            }
        });

        /*  [ Product slider in Home page ]
         - - - - - - - - - - - - - - - - - - - - */
        $(".slider-product-content .owl-carousel").owlCarousel({
            loop:true,
            margin:0,
            nav:true,
            navText: ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
            dots: false,
            responsive:{
                0:{
                    items:1
                },
                480:{
                    items:1
                },
                600:{
                    items:1
                },
                1000:{
                    items:1
                }
            }
        });

        /*  [ Tab content slider ]
         - - - - - - - - - - - - - - - - - - - - */
        $(".tab-content .owl-carousel").owlCarousel({
            loop:true,
            margin:0,
            nav:true,
            navText: ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
            dots: false,
            responsive:{
                0:{
                    items:1
                },
                480:{
                    items:1
                },
                600:{
                    items:1
                },
                1000:{
                    items:1
                }
            }
        });

        /*  [ Custom Tab ]
         - - - - - - - - - - - - - - - - - - - - */
        $('.tab-container .tab-content:not(:nth-child(2))').hide();
        $('.tab-heading li a').on('click', function(e){
            e.preventDefault();
            $('.tab-heading li a').removeClass('active');
            $(this).addClass('active');
            $('.tab-container .tab-content').hide();
            var _id = $(this).attr('href');
            $(_id).fadeIn();
        });

        /*  [ Testimonial slider ]
         - - - - - - - - - - - - - - - - - - - - */
        $(".testimonial .owl-carousel").owlCarousel({
            loop:true,
            margin:0,
            nav:false,
            dots: true,
            navText: ['', '<i class="fa fa-angle-right"></i>'],
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:1
                },
                1000:{
                    items:1
                }
            }
        });

        /*  [ Partner 2 slider ]
         - - - - - - - - - - - - - - - - - - - - */
        $(".partner .owl-carousel").owlCarousel({
            loop:true,
            margin:100,
            nav:false,
            dots: true,
            navText: ['', '<i class="fa fa-angle-right"></i>'],
            responsive:{
                0:{
                    items:2
                },
                600:{
                    items:4
                },
                1000:{
                    items:6
                }
            }
        });

        /*  [ Set star rating width ]
        - - - - - - - - - - - - - - - - - - - - */
        $( '.star-rating' ).each(function() {
            var w = $( this ).find( 'span' ).attr( 'data-width' );
            $( this ).find( 'span' ).width( w );
        });

        /*  [ Animat add class ]
        - - - - - - - - - - - - - - - - - - - - */
        $('.animated').addClass("hidden-animate").viewportChecker({
            classToAdd: 'visible zoomIn',
            offset: 200
        });

    });

    /*  [ Animate Function ]
         - - - - - - - - - - - - - - - - - - - - */
    $.fn.viewportChecker = function(useroptions){
        // Define options and extend with user
        var options = {
            classToAdd: 'visible',
            offset: 100,
            callbackFunction: function(elem){}
        };
        $.extend(options, useroptions);

        // Cache the given element and height of the browser
        var $elem = this,
            windowHeight = $(window).height();

        this.checkElements = function(){
            // Set some vars to check with
            var scrollElem = ((navigator.userAgent.toLowerCase().indexOf('webkit') != -1) ? 'body' : 'html'),
                viewportTop = $(scrollElem).scrollTop(),
                viewportBottom = (viewportTop + windowHeight);

            $elem.each(function(){
                var $obj = $(this);
                // If class already exists; quit
                if ($obj.hasClass(options.classToAdd)){
                    return;
                }

                // define the top position of the element and include the offset which makes is appear earlier or later
                var elemTop = Math.round( $obj.offset().top ) + options.offset,
                    elemBottom = elemTop + ($obj.height());

                // Add class if in viewport
                if ((elemTop < viewportBottom) && (elemBottom > viewportTop)){
                    $obj.addClass(options.classToAdd);

                    // Do the callback function. Callback wil send the jQuery object as parameter
                    options.callbackFunction($obj);
                }
            });
        };

        // Run checkelements on load and scroll
        $(window).scroll(this.checkElements);
        this.checkElements();

        // On resize change the height var
        $(window).resize(function(e){
            windowHeight = e.currentTarget.innerHeight;
        });
    };

    $(document).on('click','.favBtn',function (e) {
        e.preventDefault();
        var item_id = $(this).attr('data-id');
        var url = $(this).attr('data-url');
        $.ajax({
                url: url+'/user/favourites/'+item_id,
                type: 'GET',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                datatype:'text',
                success: function (data) {
                    //console.log(data);
                },
                error: function (data) {
                    //var errors = data.responseJSON;
                    //console.log(errors);
                }
            });
    });

    setTimeout(function() {
        $('.flash-message').fadeOut('slow');
    }, 10000);

    //Shopping Cart Code

    $.cookie.defaults.path = '/';
    $.cookie.defaults.domain = 'imagecentral.pk';
    //$.removeCookie('cart_item');
    //$.removeCookie('cart_total');

    //Shopping Cart
    if($('#order-confirmation').length > 0) {
        $.removeCookie('cart_item');
        $.removeCookie('cart_total');
    }

    update_header_cart();

    function update_header_cart(){
        var items_list = $.cookie("cart_item");
        var cart_total = 0;
        var html = '';

        if(items_list) {
            var ArrayData = JSON.parse(items_list);
            $.each(ArrayData, function(index, value) {
                cart_total += Number(value.price);
                html += '<li class="clearfix"><a class="p-thumb" href="'+value.slug+'"><img src="'+value.thumb+'" alt=""></a><div class="p-info"><a class="p-title" href="'+value.slug+'">'+value.name+'</a><span class="price"><ins><span class="amount">Rs. '+value.price+'</span></ins></span><a class="remove" href=""><i data-total="1500" data-id="'+value.id+'" class="pe-7s-close icon-trash"></i></a></div></li>';
            });
            $(".cart_list").html(html);
            $("#header-cart-total").text('Rs. '+cart_total);
            $("#cart-header-count").text(ArrayData.length);
            $(".checkout-bar #total").text(cart_total);
            if(!$("#checkout-page").length > 0 && cart_total > 0 && !$("#order-confirmation").length) {
                $("#checkout-bar").removeClass("hidden-xs hidden-lg hidden-md");
            }
            $.cookie("cart_total" , cart_total);
        } else {
            $("#header-cart-total").text('Rs. '+cart_total);
            $("#cart-header-count").text(0);
            $(".checkout-bar #total").text(0);
        }

    }

    $(document).on('click','.remove .icon-trash',function (event) {
        event.preventDefault();
        var id = $(this).attr('data-id');
        var total = $(this).attr('data-total');
        var items_list = $.cookie("cart_item");
        var ArrayData = JSON.parse(items_list);
        if(items_list) {
            $.each(ArrayData, function( index, value ) {
                if(value.id == id) {
                    ArrayData.splice(index, 1);
                    return false;
                }
            });
        }
        $.cookie("cart_item", JSON.stringify(ArrayData));
        var cart_total = $.cookie("cart_total");
        $.cookie("cart_total" , Number(cart_total)-Number(total));
        update_header_cart();
    });

    $(document).on('click','.trash-cart-page',function () {
        event.preventDefault();
        var id = $(this).attr('data-id');
        var total = $(this).attr('data-total');

        var items_list = $.cookie("cart_item");
        var ArrayData = JSON.parse(items_list);
        if(items_list) {
            $.each(ArrayData, function( index, value ) {
                if(value.id == id) {
                    ArrayData.splice(index, 1);
                    return false;
                }
            });
        }
        $.cookie("cart_item", JSON.stringify(ArrayData));
        var cart_total = $.cookie("cart_total");
        $.cookie("cart_total" , Number(cart_total)-Number(total));
        update_header_cart();

        location.reload();
    });


    $(document).on('click','.add-cart-button',function (e) {
        e.preventDefault();

        var price = $('#product_price').text();
        var id = $('#product_id').text();
        var name = $('#product_name').text();
        var slug = $('#product_slug').text();
        var thumb = $('#product_thumb').text();

        var ArrayData = [];
        var item = {};
        var items_list = $.cookie("cart_item");
        if(items_list) {
            ArrayData = JSON.parse(items_list);
            var existing = false;

            $.each(ArrayData, function( index, value ) {
                if(value.id == id) {
                    existing = true;
                    return false;
                }
            });

            if(!existing) {
                item.id = id;
                item.name = name;
                item.price = price;
                item.slug = slug;
                item.thumb = thumb;
                ArrayData.push(item);
            }
            $.cookie("cart_item", JSON.stringify(ArrayData));

        } else {
            item.id = id;
            item.name = name;
            item.price = price;
            item.slug = slug;
            item.thumb = thumb;

            ArrayData.push(item);
            $.cookie("cart_item", JSON.stringify(ArrayData));
        }
        update_header_cart();
        $(".product-view-cart").removeClass("hidden-lg");
        $(".product-view-cart").removeClass("hidden-md");
        $(".product-view-cart").removeClass("hidden-xs");
        $(".add-cart-button").parent().addClass("added-cart-button");
        $(".product-view-cart").removeClass("added-cart-button");
    });

    if($("#paymentForm").length > 0) {
        setTimeout(function(){
            $('#paymentForm').submit();
        },5000);
    }
    if($('#submitEasyPay').length > 0) {
        $('#submitEasyPay').submit();
    }

    if($('#order-confirmation').length > 0) {
        $.removeCookie('cart_item');
        $.removeCookie('cart_total');
    }
})(jQuery);